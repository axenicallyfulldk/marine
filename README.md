# Marine
Marine - project devoted to storing and viewing data about SSS, MBES, seismic surveys, mosaics and interpreted models. Project is written on PHP/Laravel and Go.

## File hierarchy
- webmarine - contains back-end written in Laravel and front-end written in Vue.
- segyviewer - service that maintance websocket connections to return seismic data from segy files. Written in Go.
- docker - files for application containerization
- docker-compose-dev.yaml - configuration file to run application on the development computer 
- docker-compose-prod.yaml - configuration file to run application on the server
