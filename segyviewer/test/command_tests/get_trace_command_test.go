package command_test

import (
	"encoding/json"
	"fmt"
	"segyviewer/internal/command"
	"segyviewer/pkg/segy"
	"testing"
	"unsafe"
)

func getTraceOfSimpleSegy(ti int64, sampleCount int32) []float32 {
	var trace []float32
	for i := 0; i < int(sampleCount); i++ {
		trace = append(trace, float32(int64(i)+ti))
	}
	return trace
}

func buildTraceRequest(ti int) []byte {
	// return map[string]interface{}{"command": "getTrace", "traceIndex": ti}
	return []byte(fmt.Sprintf(`{"command": "getTrace", "traceIndex": %d}`, ti))
}

func getBytesFromPtr(ptr uintptr, len int, cap int) []byte {
	var sl = struct {
		addr uintptr
		len  int
		cap  int
	}{ptr, len, cap}
	return *(*[]byte)(unsafe.Pointer(&sl))
}

func buildGetTraceResp(ti int64, trace []float32) map[string]interface{} {
	res := map[string]interface{}{}
	res["command"] = "getTrace"
	res["traceIndex"] = ti
	res["trace"] = getBytesFromPtr(uintptr(unsafe.Pointer(&trace[0])), len(trace)*4, len(trace)*4)

	data, _ := json.Marshal(res)
	json.Unmarshal(data, &res)
	return res
}

func TestGetTraceCommand(t *testing.T) {

	segyf, _ := segy.OpenFile("../simple.sgy")

	sampleCount := segyf.GetSampleCount()

	type test struct {
		ti     int
		trace  []float32
		hasErr bool
	}

	tests := []test{
		{0, getTraceOfSimpleSegy(0, sampleCount), false},                              // First trace
		{int(segyf.GetTraceCount() - 1), getTraceOfSimpleSegy(0, sampleCount), false}, // Last trace
		{-1, nil, true},                         // Out of range trace
		{int(segyf.GetTraceCount()), nil, true}, // Out of range trace
	}

	handler, _ := command.CreateGetTraceCommandHandler(segyf)

	for _, test := range tests {
		res, err := handler.Handle(buildTraceRequest(test.ti))
		if test.hasErr {
			if err == nil {
				t.Errorf("Expected to recieve error, but error was not recieved on input %v", test.ti)
			}
			continue
		}

		var resp map[string]interface{}
		json.Unmarshal(res, &resp)

		expectedRes := buildGetTraceResp(int64(test.ti), getTraceOfSimpleSegy(int64(test.ti), sampleCount))
		if fmt.Sprint(expectedRes) != fmt.Sprint(resp) {
			t.Errorf("Expected: %v\nReceived: %v", expectedRes, resp)
		}
	}
}
