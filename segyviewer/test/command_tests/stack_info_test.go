package command_test

import (
	"encoding/json"
	"segyviewer/internal/command"
	"segyviewer/internal/stack2drepo"
	"segyviewer/pkg/segy"
	"segyviewer/third_party/sqlserialize"
	"testing"
)

func compareInfoResponses(resp1, resp2 *command.GetStackInfoResponse) bool {
	eq1 := resp1.Name == resp2.Name
	eq2 := resp1.TraceCount == resp2.TraceCount
	eq3 := resp1.SampleCount == resp2.SampleCount
	eq4 := resp1.SampleInterval == resp2.SampleInterval
	eq5 := (resp1.MinmaxNorm != nil && resp2.MinmaxNorm != nil) && (*resp1.MinmaxNorm == *resp2.MinmaxNorm)

	return eq1 && eq2 && eq3 && eq4 && eq5
}

func TestSimpleSegy(t *testing.T) {
	segystack := stack2drepo.Segy2dStack{
		ID:         1,
		SurveyID:   1,
		Name:       "test stack",
		Alias:      "test stack",
		Path:       "simple.sgy",
		MinmaxNorm: sqlserialize.NullFloat64{1.0, true},
	}

	segyf, _ := segy.OpenFile("../simple.sgy")

	expectedResp := command.GetStackInfoResponse{
		Name:           segystack.Name,
		TraceCount:     segyf.GetTraceCount(),
		SampleCount:    segyf.GetSampleCount(),
		SampleInterval: segyf.GetSampleInterval(),
		MinmaxNorm:     &segystack.MinmaxNorm.Float64,
	}

	handler, _ := command.CreateStackInfoCommandHandler(&segystack, segyf)

	data, _ := handler.Handle([]byte(`"command": "getInfo"`))

	var resp command.GetStackInfoResponse
	json.Unmarshal(data, &resp)

	if compareInfoResponses(&expectedResp, &resp) {
		t.Errorf("Expected: %v\nReceived: %v", expectedResp, resp)
	}
}
