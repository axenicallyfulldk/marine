package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"segyviewer/internal/stack2drepo"
	"segyviewer/internal/stack2drepo/postgres"

	"github.com/joho/godotenv"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var pgsqlDB *sql.DB

func getPostgresDb(dataSourceName string) (*sql.DB, error) {
	if pgsqlDB != nil {
		return pgsqlDB, nil
	}

	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	pgsqlDB = db

	return pgsqlDB, nil
}

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

var stack2dRepo stack2drepo.ISegy2dStackRepo

func getStack2dRepo() (stack2drepo.ISegy2dStackRepo, error) {
	if stack2dRepo != nil {
		return stack2dRepo, nil
	}

	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		getEnv("DB_HOST", "127.0.0.1"), getEnvAsInt("DB_PORT", 5432),
		getEnv("DB_USERNAME", "postgres"), getEnv("DB_PASSWORD", "postgres"),
		getEnv("DB_DATABASE", "marine"))

	dbConn, err := getPostgresDb(psqlInfo)
	if err != nil {
		return nil, err
	}

	stack2dRepo = postgres.CreateSegy2dStackRepo(dbConn)

	return stack2dRepo, nil
}

func main() {

	// Create router
	r := mux.NewRouter()

	r.HandleFunc("/api/seismic2d-stack-segy/{id:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		stackRepo, err := getStack2dRepo()
		if err != nil {
			log.Printf("Error: %v", err)
			return
		}

		conn, err := CreateStackWebSocketConnection(stackRepo, w, r)
		if err != nil {
			ip, agent := getIPAndUserAgent(r)
			log.Printf("IP: %s, Agent: %s, Error: %v", ip, agent, err)
			return
		}

		conn.Serve()
	})

	addr := ":" + getEnv("PORT", "8080")
	http.ListenAndServe(addr, r)
}
