package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"path"
	"segyviewer/internal/command"
	"segyviewer/internal/stack2drepo"
	"segyviewer/pkg/segy"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  2048,
	WriteBufferSize: 4096,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func CreateCommandHandler(stack *stack2drepo.Segy2dStack, segy *segy.SegyFile) (command.ICommandHandler, error) {
	handler := command.CreateHandlerRegistry()

	sich, err1 := command.CreateStackInfoCommandHandler(stack, segy)
	grch, err2 := command.CreateGetTraceCommandHandler(segy)
	gtirch, err3 := command.CreateGetTracesInRangeCommandHandler(segy)

	if err1 != nil || err2 != nil || err3 != nil {
		return nil, fmt.Errorf(strings.Join([]string{err1.Error(), err2.Error(), err3.Error()}, "\n"))
	}

	handler.SetHandler(sich)
	handler.SetHandler(grch)
	handler.SetHandler(gtirch)

	return handler, nil
}

func closeWebSocketConnection(conn *websocket.Conn, closeCode int, msg string) {
	// conn.SetWriteDeadline(time.Now().Add(2 * time.Second))
	conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(closeCode, msg))
	// time.Sleep(3 * time.Second)
	conn.Close()
}

func getIPAndUserAgent(r *http.Request) (ip string, userAgent string) {
	ip = r.Header.Get("X-Forwarded-For")
	if ip == "" {
		ip = strings.Split(r.RemoteAddr, ":")[0]
	}

	userAgent = r.UserAgent()
	return ip, userAgent
}

type StackWebSocketConnection struct {
	conn      *websocket.Conn
	handler   command.ICommandHandler
	ip        string
	userAgent string
}

func (swsc *StackWebSocketConnection) GetIP() string {
	return swsc.ip
}

func (swsc *StackWebSocketConnection) GetAgent() string {
	return swsc.userAgent
}

func CreateStackWebSocketConnection(stackRepo stack2drepo.ISegy2dStackRepo, w http.ResponseWriter, r *http.Request) (*StackWebSocketConnection, error) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return nil, fmt.Errorf("Failed to upgrade connection: %v", err)
	}

	stackIDStr, hasID := mux.Vars(r)["id"]
	if !hasID {
		err = fmt.Errorf("Request doesn't contain stack's id")
		closeWebSocketConnection(conn, 1011, err.Error())
		return nil, err
	}

	stackID, err := strconv.Atoi(stackIDStr)
	if err != nil {
		err = fmt.Errorf("Failed to cast id to integer: %s", err.Error())
		closeWebSocketConnection(conn, 1011, err.Error())
		return nil, err
	}

	stack, err := stackRepo.GetByID(context.Background(), stackID)
	if err != nil {
		err := fmt.Errorf("Failed to get info about stack %d: %v", stackID, err)
		closeWebSocketConnection(conn, 1011, err.Error())
		return nil, err
	}

	segyPath := path.Join(getEnv("DATA_FOLDER", "./"), stack.Path)

	segy, err := segy.OpenFile(segyPath)
	if err != nil {
		err := fmt.Errorf("Failed to open segy file %s: %v", segyPath, err)
		closeWebSocketConnection(conn, 1011, err.Error())
		return nil, err
	}

	// Build command handler
	handler, err := CreateCommandHandler(stack, segy)
	if err != nil {
		closeWebSocketConnection(conn, 1011, err.Error())
		return nil, err
	}

	ip, agent := getIPAndUserAgent(r)

	return &StackWebSocketConnection{conn: conn, handler: handler, ip: ip, userAgent: agent}, nil
}

func (swsc *StackWebSocketConnection) Serve() {
	for {
		// Read message from browser
		msgType, msg, err := swsc.conn.ReadMessage()
		if err != nil {
			if websocket.IsCloseError(err) || websocket.IsUnexpectedCloseError(err) {
				log.Print(fmt.Sprintf("IP: %s, Agent: %s, Connection closed: %s\n", swsc.ip, swsc.userAgent, err.Error()))
				return
			}

			log.Printf("IP: %s, Agent: %s, Failed to read message: %s\n", swsc.ip, swsc.userAgent, err.Error())
			continue
		}

		// Handle request
		resp, err := swsc.handler.Handle(msg)
		if err != nil {
			// Write error to user
			resp = []byte(fmt.Sprintf(`{"command": "error", "msg": "%s"}`, err.Error()))
		}

		// Write message back to browser
		if err = swsc.conn.WriteMessage(msgType, resp); err != nil {
			log.Printf("Failed to send message to client %s: %s\n", "clientIP", err.Error())
			if websocket.IsCloseError(err) || websocket.IsUnexpectedCloseError(err) {
				return
			}
		}
	}
}
