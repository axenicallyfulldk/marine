package segy

import (
	"bytes"
	"encoding/binary"
	"fmt"

	//"log"
	"os"
	"reflect"
	"unsafe"

	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("segygo")
var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
)

const Version = "0.1"

var formatSize = map[SampleFormat]int8{
	IbmFloat:       4,
	ComplementInt4: 4,
	ComplementInt2: 2,
	FixedPoint4:    4,
	IeeeFloat4:     4,
	IeeeFloat8:     8,
	ComplementInt3: 3,
	ComplementInt1: 1,
	ComplementInt8: 8,
	Uint4:          4,
	Uint2:          2,
	Uint8:          8,
	Uint3:          3,
	Uint1:          1,
}

const firstByteMask uint32 = 256 - 1
const fourthByteMask uint32 = firstByteMask << 24
const signMask uint32 = 1 << 31

func ibmToFloat32(ibmNum uint32) float32 {
	// Good visualization for understanding difference between IBM and IEEE
	// floating points
	// https://agilescientific.com/blog/2017/3/29/more-precise-segy

	// The idea of an approach is to get IBM exponent and fraction as IEEE
	// floating point numbers and than multiply them.

	// Extract exponent from IBM number
	var x uint32 = (ibmNum & (^signMask) & fourthByteMask) >> 24
	// Convert ibm exponent to ieee exponent and add sign of original number
	var uintExp uint32 = ((4*x-129)<<23)&(^signMask) | (ibmNum & signMask)

	var exp float32 = *(*float32)(unsafe.Pointer(&uintExp))

	// Extract fraction, delete least significant bit and set exponent to 127
	// to get 1*fraction
	var uintb uint32 = (((1<<24)-1)&ibmNum)>>1 | (127 << 23)

	var fraction float32 = *(*float32)(unsafe.Pointer(&uintb))
	// Since one is added to the fraction in IEEE number implicitly we have
	// to substract it.
	// return exp*(fraction-1.0)
	return exp*fraction - exp
}

func IbmToFloat32(ibmNum uint32) float32 {
	return ibmToFloat32(ibmNum)
}

func isPowerOfTwo(x int8) bool {
	return (x != 0) && ((x & (x - 1)) == 0)
}

func isFormatSupported(sampleFormat SampleFormat) bool {
	switch sampleFormat {
	case IbmFloat:
		return true
	case IeeeFloat4:
		return true
	case IeeeFloat8:
		return true
	}
	return false
}

type SegyFile struct {
	fileName      string
	byteOrder     binary.ByteOrder
	header        BinHeader
	traceCount    int64
	sampleFormat  SampleFormat
	sampleSize    int8
	ebcidicHeader []byte
	file          *os.File
	LogLevel      logging.Level
}

func CreateFile(filename string) (SegyFile, error) {
	var s SegyFile
	var binHdr BinHeader
	f, err := os.Create(filename)
	defer f.Close()

	if err != nil {
		return s, err
	}

	s.LogLevel = logging.WARNING

	// Setup proper logging
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend1Formatter := logging.NewBackendFormatter(backend1, format)
	logging.SetBackend(backend1Formatter)
	logging.SetLevel(s.LogLevel, "")

	log.Debugf("Creating SEG-Y file: %s", s.fileName)

	s.fileName = filename
	s.header = binHdr
	s.traceCount = 0
	s.file = f

	accum := make([]byte, 3200)
	//r := bytes.NewWriter(accum)
	//binary.Write()
	buff := bytes.NewBuffer(accum)
	if err = binary.Write(buff, binary.BigEndian, &s.header); err != nil {
		log.Errorf("Error creating buffer to hold binary header for segy file: %s. Msg: %s", s.fileName, err)
		return s, err
	}

	n, err := f.Write(buff.Bytes())
	if err != nil {
		log.Errorf("Error writing binary header to segy file: %s. Msg: %s", s.fileName, err)
		return s, err
	}
	log.Debugf("Wrote %d bytes to file: %s", n, s.fileName)

	return s, err
}

func OpenFile(filename string) (*SegyFile, error) {
	return OpenFileWithEndian(filename, binary.BigEndian)
}

func OpenFileWithEndian(filename string, byteOrder binary.ByteOrder) (*SegyFile, error) {
	var s SegyFile
	var binHdr BinHeader

	s.fileName = filename
	s.byteOrder = byteOrder
	s.LogLevel = logging.WARNING

	file, err := os.OpenFile(s.fileName, os.O_RDONLY, 0666)
	if err != nil {
		return &s, err
	}
	defer file.Close()

	// Setup proper logging
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend1Formatter := logging.NewBackendFormatter(backend1, format)
	logging.SetBackend(backend1Formatter)
	logging.SetLevel(s.LogLevel, "")

	s.ebcidicHeader = make([]byte, ebcidicHeaderSize, ebcidicHeaderSize)
	file.Read(s.ebcidicHeader)

	segyHeaderRaw := make([]byte, segyHeaderSize)
	file.Read(segyHeaderRaw)

	r := bytes.NewReader(segyHeaderRaw)
	log.Debugf("Number of bytes: %d", r.Len())

	if err = binary.Read(r, s.byteOrder, &binHdr); err != nil {
		log.Errorf("Error reading segy file. %s", err)
		return &s, err
	}

	s.header = binHdr
	s.sampleFormat = (SampleFormat)(binHdr.Format)
	s.sampleSize = formatSize[s.sampleFormat]
	s.traceCount = s.getTraceCount()

	if !isFormatSupported(s.sampleFormat) {
		return &s, fmt.Errorf("given sample format is unsupported")
	}

	return &s, err
}

func (s *SegyFile) SetVerbose(verbose bool) {
	if verbose {
		s.LogLevel = logging.DEBUG
		logging.SetLevel(s.LogLevel, "")
	} else {
		s.LogLevel = logging.WARNING
		logging.SetLevel(s.LogLevel, "")
	}

}

func (s *SegyFile) GetFileName() string {
	return s.fileName
}

func (s *SegyFile) GetTraceCount() int64 {
	return s.traceCount
}

func (s *SegyFile) getTraceCount() int64 {
	file, err := os.Open(s.fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	fi, err := file.Stat()
	if err != nil {
		log.Warning("unable to get Stat()")
		log.Fatal(err)
	}
	size := fi.Size()
	nSamples := s.header.Hns
	txtAndBinarySize := int64(ebcidicHeaderSize + segyHeaderSize)
	nTraces := ((size - txtAndBinarySize) / (int64(traceHeaderSize) + int64(nSamples)*int64(unsafe.Sizeof(float32(1)))))

	return nTraces
}

func (s *SegyFile) GetSampleCount() int32 {
	return int32(s.header.Hns)
}

func (s *SegyFile) GetSampleInterval() int32 {
	return int32(s.header.Hdt)
}

func (s *SegyFile) GetFormat() SampleFormat {
	return s.sampleFormat
}

func (s *SegyFile) GetHeader() map[string]interface{} {
	m := make(map[string]interface{})
	v := reflect.ValueOf(s.header)
	for i := 0; i < v.NumField(); i++ {
		key := v.Type().Field(i).Name
		val := v.Field(i).Interface()
		log.Debugf("name = %s, value = %d", key, val)
		m[key] = val
	}

	return m
}

func (s *SegyFile) ReadTrace(traceIndex int64) (*Trace, error) {
	trace := Trace{}

	// Open file
	file, err := os.Open(s.fileName)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer file.Close()

	traceBuff := make([]float32, s.GetSampleCount())
	byteBuff := make([]byte, s.GetSampleCount()*int32(s.sampleSize))
	trace.Data = traceBuff

	trcHdrBuff := make([]byte, traceHeaderSize)

	file.Seek(ebcidicHeaderSize+segyHeaderSize+traceIndex*(int64(traceHeaderSize)+int64(s.GetSampleCount())*int64(s.sampleSize)), 0)

	_, err = file.Read(trcHdrBuff)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	trcHdrReader := bytes.NewReader(trcHdrBuff)
	err = binary.Read(trcHdrReader, s.byteOrder, &trace.TraceHeader)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	_, err = file.Read(byteBuff)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	switch s.sampleFormat {
	case IbmFloat:
		for i := range trace.Data {
			rawVal := s.byteOrder.Uint32(byteBuff[i*int(s.sampleSize) : (i+1)*int(s.sampleSize)])
			trace.Data[i] = ibmToFloat32(rawVal)
		}
	case IeeeFloat4:
		for i := range trace.Data {
			rawVal := s.byteOrder.Uint32(byteBuff[i*int(s.sampleSize) : (i+1)*int(s.sampleSize)])
			rawPtr := *(*float32)(unsafe.Pointer(&rawVal))
			trace.Data[i] = rawPtr
		}
	case IeeeFloat8:
		for i := range trace.Data {
			rawVal := s.byteOrder.Uint64(byteBuff[i*int(s.sampleSize) : (i+1)*int(s.sampleSize)])
			rawPtr := *(*float64)(unsafe.Pointer(&rawVal))
			trace.Data[i] = float32(rawPtr)
		}
	}

	return &trace, nil
}

func (s *SegyFile) ReadTracesInRange(traceIndexS, traceIndexE int64) ([]*Trace, error) {
	// Validate params
	if traceIndexS < 0 || traceIndexS >= s.GetTraceCount() || traceIndexE < 0 || traceIndexE > s.GetTraceCount() {
		return nil, fmt.Errorf("trace index is out of the range")
	}
	if traceIndexS >= traceIndexE {
		return nil, fmt.Errorf("start index is bigger than end index")
	}

	// Open file
	file, err := os.Open(s.fileName)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer file.Close()

	traceCount := int32(traceIndexE - traceIndexS)

	traceByteSize := traceHeaderSize + s.GetSampleCount()*int32(s.sampleSize)

	byteBuff := make([]byte, traceByteSize*traceCount)

	file.Seek(ebcidicHeaderSize+segyHeaderSize+traceIndexS*int64(traceByteSize), 0)

	_, err = file.Read(byteBuff)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	traces := make([]*Trace, 0, traceCount)

	for traceIndex := traceIndexS; traceIndex < traceIndexE; traceIndex++ {
		offset := int32(traceIndex-traceIndexS) * traceByteSize

		var trace Trace
		trace.Data = make([]float32, s.GetSampleCount())

		trcHdrReader := bytes.NewReader(byteBuff[offset : offset+traceHeaderSize])
		err = binary.Read(trcHdrReader, s.byteOrder, &trace.TraceHeader)
		if err != nil {
			log.Fatal(err)
			return nil, err
		}

		samplesBytes := byteBuff[offset+traceHeaderSize : offset+traceByteSize]
		switch s.sampleFormat {
		case IbmFloat:
			for i := range trace.Data {
				rawVal := s.byteOrder.Uint32(samplesBytes[i*int(s.sampleSize) : (i+1)*int(s.sampleSize)])
				trace.Data[i] = ibmToFloat32(rawVal)
			}
		case IeeeFloat4:
			for i := range trace.Data {
				rawVal := s.byteOrder.Uint32(samplesBytes[i*int(s.sampleSize) : (i+1)*int(s.sampleSize)])
				rawPtr := *(*float32)(unsafe.Pointer(&rawVal))
				trace.Data[i] = rawPtr
			}
		case IeeeFloat8:
			for i := range trace.Data {
				rawVal := s.byteOrder.Uint64(samplesBytes[i*int(s.sampleSize) : (i+1)*int(s.sampleSize)])
				rawPtr := *(*float64)(unsafe.Pointer(&rawVal))
				trace.Data[i] = float32(rawPtr)
			}
		}

		traces = append(traces, &trace)
	}

	return traces, nil
}
