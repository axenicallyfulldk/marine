package segy

type ISegyFile interface {
	GetFileName() string
	GetTraceCount() int64
	GetSampleCount() int32
	GetSampleInterval() int32
	GetFormat() SampleFormat
	ReadTrace(traceIndex int64) (*Trace, error)
	ReadTracesInRange(traceIndexS, traceIndexE int64) ([]*Trace, error)
}
