module segyviewer

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.3
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
)
