package stack2drepo

import (
	"segyviewer/third_party/sqlserialize"
)

type Segy2dStack struct {
	ID         int
	SurveyID   int
	Name       string
	Alias      string
	Path       string
	MinmaxNorm sqlserialize.NullFloat64
}
