package stack2drepo

import (
	"context"
)

type ISegy2dStackRepo interface {
	GetByID(ctx context.Context, id int) (*Segy2dStack, error)
	GetAll(ctx context.Context) ([]*Segy2dStack, error)
}
