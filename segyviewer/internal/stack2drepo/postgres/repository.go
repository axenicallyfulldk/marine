package postgres

import (
	"context"
	"database/sql"
	"segyviewer/internal/stack2drepo"
)

type Segy2dStackRepo struct {
	db *sql.DB
}

func CreateSegy2dStackRepo(db *sql.DB) *Segy2dStackRepo {
	return &Segy2dStackRepo{db: db}
}

const getByIDQuery = "select id, name, alias, file_path, minmax_norm from seismic2d_stack where id = $1"

func (r *Segy2dStackRepo) GetByID(ctx context.Context, id int) (*stack2drepo.Segy2dStack, error) {
	row := r.db.QueryRowContext(ctx, getByIDQuery, id)
	var stack stack2drepo.Segy2dStack
	err := row.Scan(&stack.ID, &stack.Name, &stack.Alias, &stack.Path, &stack.MinmaxNorm)
	return &stack, err
}

func (r *Segy2dStackRepo) GetAll(ctx context.Context) ([]*stack2drepo.Segy2dStack, error) {
	// TODO: Replace stub with actual implementation
	return []*stack2drepo.Segy2dStack{}, nil
}
