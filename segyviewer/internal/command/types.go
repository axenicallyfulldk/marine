package command

import (
	"encoding/json"
	"unsafe"
)

type Trace []float32

func getBytesFromPtr(ptr uintptr, len int, cap int) []byte {
	var sl = struct {
		addr uintptr
		len  int
		cap  int
	}{ptr, len, cap}
	return *(*[]byte)(unsafe.Pointer(&sl))
}

func (t *Trace) MarshalJSON() ([]byte, error) {
	return json.Marshal(getBytesFromPtr(uintptr(unsafe.Pointer(&(*t)[0])), len(*t)*4, len(*t)*4))
}

type Command struct {
	Command string `json:"command"`
}

type GetStackInfoCommand struct {
	Command
}

type GetStackInfoResponse struct {
	GetStackInfoCommand
	Name           string   `json:"name"`
	TraceCount     int64    `json:"traceCount"`
	SampleCount    int32    `json:"sampleCount"`
	SampleInterval int32    `json:"sampleInterval"`
	MinmaxNorm     *float64 `json:"minmaxNorm"`
}

type GetTraceCommand struct {
	Command
	TraceIndex int64 `json:"traceIndex"`
}

type GetTraceResponse struct {
	GetTraceCommand
	Trace *Trace `json:"trace"`
}

type GetTracesInRangeCommand struct {
	Command
	TraceRange [2]int64 `json:"traceRange"`
}

type GetTracesInRangeResponse struct {
	GetTracesInRangeCommand
	Traces []*Trace `json:"traces"`
}
