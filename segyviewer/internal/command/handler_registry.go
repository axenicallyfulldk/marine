package command

import (
	"encoding/json"
	"fmt"
	"strings"
)

type HandlerRegistry struct {
	handlers map[string]ICommandHandler
}

func CreateHandlerRegistry() *HandlerRegistry {
	return &HandlerRegistry{handlers: map[string]ICommandHandler{}}
}

func (hr *HandlerRegistry) GetCommandName() string {
	names := make([]string, len(hr.handlers))

	i := 0
	for n := range hr.handlers {
		names[i] = n
	}

	return strings.Join(names, "\n")
}

func (hr *HandlerRegistry) SetHandler(handler ICommandHandler) {
	hr.handlers[handler.GetCommandName()] = handler
}

func (hr *HandlerRegistry) DelHandlerByName(name string) {
	delete(hr.handlers, name)
}

func (hr *HandlerRegistry) DelHandler(handler ICommandHandler) {
	hr.DelHandlerByName(handler.GetCommandName())
}

func (hr *HandlerRegistry) Handle(cmdRaw []byte) ([]byte, error) {
	var cmd Command
	err := json.Unmarshal(cmdRaw, &cmd)
	if err != nil {
		return nil, err
	}

	handler, ok := hr.handlers[cmd.Command]
	if !ok {
		return nil, fmt.Errorf("There is no handler for command %s", cmd.Command)
	}

	return handler.Handle(cmdRaw)
}
