package command

import (
	"encoding/json"
	"fmt"
	"segyviewer/internal/stack2drepo"
	"segyviewer/pkg/segy"
)

// Command for recieving stack info
type GetStackInfoCommandHandler struct {
	stack *stack2drepo.Segy2dStack
	segy  segy.ISegyFile
}

func CreateStackInfoCommandHandler(stack *stack2drepo.Segy2dStack, segy segy.ISegyFile) (*GetStackInfoCommandHandler, error) {
	return &GetStackInfoCommandHandler{stack, segy}, nil
}

func (gsi *GetStackInfoCommandHandler) GetCommandName() string {
	return "getInfo"
}

func (gsi *GetStackInfoCommandHandler) Handle(cmdRaw []byte) ([]byte, error) {
	var command GetStackInfoCommand
	err := json.Unmarshal(cmdRaw, &command)
	if err != nil {
		return nil, err
	}

	var resp GetStackInfoResponse

	resp.GetStackInfoCommand = command

	resp.Name = gsi.stack.Name

	resp.TraceCount = gsi.segy.GetTraceCount()
	resp.SampleCount = gsi.segy.GetSampleCount()
	resp.SampleInterval = gsi.segy.GetSampleInterval()

	if gsi.stack.MinmaxNorm.Valid {
		resp.MinmaxNorm = &gsi.stack.MinmaxNorm.Float64
	}

	respRaw, err := json.Marshal(resp)
	return respRaw, err
}

// Command for recieving stack's trace
type GetTraceCommandHandler struct {
	segy *segy.SegyFile
}

func CreateGetTraceCommandHandler(segy *segy.SegyFile) (*GetTraceCommandHandler, error) {
	return &GetTraceCommandHandler{segy}, nil
}

func (gsi *GetTraceCommandHandler) GetCommandName() string {
	return "getTrace"
}

func (gsi *GetTraceCommandHandler) Handle(cmdRaw []byte) ([]byte, error) {
	var command GetTraceCommand
	err := json.Unmarshal(cmdRaw, &command)
	if err != nil {
		return nil, err
	}

	if command.TraceIndex < 0 || command.TraceIndex >= gsi.segy.GetTraceCount() {
		return nil, fmt.Errorf("trace index is invalid")
	}

	trace, err := gsi.segy.ReadTrace(command.TraceIndex)
	if err != nil {
		return nil, err
	}

	var resp GetTraceResponse
	resp.GetTraceCommand = command
	tr := Trace(trace.Data)
	resp.Trace = &tr

	respRaw, err := json.Marshal(resp)
	return respRaw, err
}

// Command for recieving range of traces
type GetTracesInRangeCommandHandler struct {
	segy *segy.SegyFile
}

func CreateGetTracesInRangeCommandHandler(segy *segy.SegyFile) (*GetTracesInRangeCommandHandler, error) {
	return &GetTracesInRangeCommandHandler{segy}, nil
}

func (gsi *GetTracesInRangeCommandHandler) GetCommandName() string {
	return "getTracesInRange"
}

// {command: "getTracesInRange", traceRange: [10,20]} - get range of traces. {command: "getTracesInRange", traceRange: [10,20], traces: [...]}
func (gsi *GetTracesInRangeCommandHandler) Handle(cmdRaw []byte) ([]byte, error) {
	var command GetTracesInRangeCommand
	err := json.Unmarshal(cmdRaw, &command)
	if err != nil {
		return nil, err
	}

	var resp GetTracesInRangeResponse
	resp.GetTracesInRangeCommand = command
	if command.TraceRange[0] < 0 || command.TraceRange[1] >= gsi.segy.GetTraceCount() {
		traces, err := gsi.segy.ReadTracesInRange(command.TraceRange[0], command.TraceRange[1])
		if err != nil {
			return nil, err
		}
		for i := 0; i < len(traces); i++ {
			var trace Trace = traces[i].Data
			resp.Traces = append(resp.Traces, &trace)
		}
	}

	respRaw, err := json.Marshal(resp)
	return respRaw, err
}
