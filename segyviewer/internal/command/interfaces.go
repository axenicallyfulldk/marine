package command

type ICommandHandler interface {
	Handle(cmdRaw []byte) ([]byte, error)
	GetCommandName() string
}
