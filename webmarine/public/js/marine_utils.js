// import LRUCache from "lru-cache";

const Colors = ['#AA0DFE', '#3283FE', '#85660D', '#782AB6', '#565656', '#1C8356', '#16FF32', '#F7E1A0', '#E2E2E2', '#1CBE4F', '#C4451C', '#DEA0FD', '#FE00FA', '#325A9B', '#FEAF16', '#F8A19F', '#90AD1C', '#F6222E', '#1CFFCE', '#2ED9FF', '#B10DA1', '#C075A6', '#FC1CBF', '#B00068', '#FBE426', '#FA0087'];

// Class that associate colors with given ids
class ColorMap {
    constructor(ids){
        this.map = new Map();
        ids.sort();
        for(let i=0; i<ids.length; ++i){
            this.map.set(ids[i], Colors[i%Colors.length]);
        }
    }

    get(id){
        return this.map.get(id);
    }
}

// Returns color map that associated colors with given ids
export function createColorMap(ids){
    return new ColorMap(ids);
}

// Returns tool name based on brand and model
function getToolName(tool){
    return [tool.brand,tool.model].filter(el => el != null).join(" ");
}

// Repository for tools that are used for sss and mbes surveys
class ToolRep {
    // Sincle there are very few tools we will download them all at once
    constructor(tools) {
        this.idMap = new Map();
        this.nameMap = new Map();

        let that = this;
        let initRep = function(toolsData){
            for(let tool of toolsData){
                that.idMap.set(tool.id, tool);
                that.nameMap.set(getToolName(tool), tool);
            }
        };

        // if tools is undefiend than we have to get them from server
        if(tools == null){
            this.toolsPromise = axios.get("/api/tools").then(response => initRep(response.data));
        }else{
            this.toolPromise = new Promise((resolve, reject) => resolve(initRep(tools)));
        }
    }

    async getIds(){
        await this.toolsPromise;
        let that = this;
        return Array.from(that.idMap.keys());
    }

    async getList(){
        await this.toolsPromise;
        return Array.from(this.idMap.values());
    }

    async getById(toolId){
        await this.toolsPromise;
        let that = this;
        return that.idMap.get(toolId);
    }

    async getByName(toolName){
        await this.toolsPromise;
        let that = this;
        return that.nameMap.get(toolName);
    }
}

// Returns repository for sssmbestools
let toolRep = null;
export function getSssMbesToolRep(){
    if(toolRep == null){
        toolRep = new ToolRep();
    }
    
    return toolRep;
}

// Repository for sss and mbes surveys
class SssMbesTypeRep {
    constructor(types) {
        this.idMap = new Map();
        this.nameMap = new Map();
        this.keyMap = new Map();

        let that = this;
        let initRep = function(typesData){
            for(let mtype of typesData){
                that.idMap.set(mtype.id, mtype);
                that.nameMap.set(mtype.name, mtype);
                that.keyMap.set(mtype.key, mtype);
            }
        };

        // if types is undefiend than we have to get them from server
        if(types == null){
            this.typePromise = axios.get("/api/survey-types").then(response => initRep(response.data));
        }else{
            this.typePromise = new Promise((resolve, reject) => initRep(resolve, types));
        }
    }

    async getIds(){
        await this.typePromise;
        return Array.from(this.idMap.keys());
    }

    async getList(){
        await this.typePromise;
        return Array.from(this.idMap.values());
    }

    async getById(toolId){
        await this.typePromise;
        return this.idMap.get(toolId);
    }

    async getByName(toolName){
        await this.typePromise;
        return this.nameMap.get(toolName);
    }

    async getByKey(toolKey){
        await this.typePromise;
        return this.keyMap.get(toolKey);
    }
}

let sssmbesTypeRep = null;
// Return repository for sss and mbes surveys
export function getSssMbesTypeRep(){
    if(sssmbesTypeRep == null){
        sssmbesTypeRep = new SssMbesTypeRep();
    }
    
    return sssmbesTypeRep;
}

// Repository for mosaics
class MosaicTypeRep {
    // Since there are very few mosaic types we will acquire them all at once
    // in constructor
    constructor(types) {
        this.idMap = new Map();
        this.nameMap = new Map();
        this.keyMap = new Map();

        let that = this;
        let initRep = function(typesData){
            for(let mtype of typesData){
                that.idMap.set(mtype.id, mtype);
                that.nameMap.set(mtype.name, mtype);
                that.keyMap.set(mtype.key, mtype);
            }
        };

        // if types is undefiend than we have to get them from server
        if(types == null){
            this.typePromise = axios.get("/api/mosaic-types").then(response => initRep(response.data));
        }else{
            this.typePromise = new Promise((resolve, reject) => initRep(types));
        }
    }

    async getIds(){
        await this.typePromise;
        return Array.from(this.idMap.keys());
    }

    async getList(){
        await this.typePromise;
        return Array.from(this.idMap.values());
    }

    async getById(toolId){
        await this.typePromise;
        return this.idMap.get(toolId);
    }

    async getByName(toolName){
        await this.typePromise;
        return this.nameMap.get(toolName);
    }

    async getByKey(toolKey){
        await this.typePromise;
        return this.keyMap.get(toolKey);
    }
}

let mosaicTypeRep = null;
export function getMosaicTypeRep(){
    if(mosaicTypeRep == null){
        mosaicTypeRep = new MosaicTypeRep();
    }

    return mosaicTypeRep;
}


function filterToParameter(filters, filterName){
    if(filterName in filters && filters[filterName] != null)
        return filterName + "=" + filters[filterName];
    return null;
}

function filterToParameters(filters){
    let params = [];
    for(const param in filters){
        if(filters[param] != null) params.push(filterToParameter(filters, param));
    }
    return params;
}

class ZoneReqBuilder{
    constructor(baseUrl){
        this.baseUrl = baseUrl;
    }

    getById(zoneId){
        return this.baseUrl+"/"+zoneId;
    }

    getList(filters){
        let req = this.baseUrl;
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }
}
let zoneReqBuilder = new ZoneReqBuilder("/api/zones");


class ZoneRep {
    constructor(){
    }

    async getById(zoneId){
        if(zoneId){
            let req = zoneReqBuilder.getById(zoneId);
            let response = await axios.get(req);
            return response.data;
        }
        
        return null;
    }

    async getList(filters){
        let req = zoneReqBuilder.getList(filters);
        let response = await axios.get(req);
        return response.data;
    }
}

class CachedZoneRep extends ZoneRep{
    constructor(){
        super();
        this.zoneCache = new LRUCache(50);
    }

    async getById(zoneId){
        let zone = this.zoneCache.get(zoneId);
        if(zone === undefined) {
            zone = super.getById(zoneId);
            this.zoneCache.set(zoneId, zone)
            return zone;
        }
        return zone;
    }
}

let zoneRep = null;
export function getZoneRep(){
    if(zoneRep == null) zoneRep = new CachedZoneRep();
    
    return zoneRep;
}


class SssMbesSurveyReqBuilder extends ZoneReqBuilder {
    constructor(baseUrl){
        super(baseUrl);
    }

    getUniques(filters){
        let req = "/api/sssmbes-survey-unique-properties";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

    getFileDownloadLink(id){
        return `${this.baseUrl}/${id}/file`;
    }

    getArchDownloadLink(id){
        return `${this.baseUrl}/${id}/arch`;
    }
}
let sssMbesSurveyReqBuilder = new SssMbesSurveyReqBuilder("/api/sssmbes-surveys");

export function getSssMbesSurveyReqBuilder(){
    return sssMbesSurveyReqBuilder;
}

class SssMbesSurveyRep {
    constructor(){

    }

    async getById(surveyId){
        let req = sssMbesSurveyReqBuilder.getById(surveyId);
        let response = await axios.get(req);
        return response.data;
    }

    async getList(filters){
        let req = sssMbesSurveyReqBuilder.getList(filters);
        let response = await axios.get(req);
        return response.data;
    }

    async getUniques(filters){
        let req = sssMbesSurveyReqBuilder.getUniques(filters);
        let response = await axios.get(req);
        return response.data;
    }
}

let sssMbesSurveyRep = null;
export function getSssMbesSurveyRep(){
    if(sssMbesSurveyRep == null) sssMbesSurveyRep = new SssMbesSurveyRep();
    
    return sssMbesSurveyRep;
}


class MosaicReqBuilder extends ZoneReqBuilder{
    constructor(baseUrl){
        super(baseUrl);
    }

    getUniques(filters){
        let req = "/api/mosaic-unique-properties";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

    getFileDownloadLink(id){
        return `${this.baseUrl}/${id}/file`;
    }

    getArchDownloadLink(id){
        return `${this.baseUrl}/${id}/arch`;
    }
}
let mosaicReqBuilder = new MosaicReqBuilder("/api/mosaics");
export function getMosaicReqBuilder(){
    return mosaicReqBuilder;
}

class MosaicRep {
    constructor(){

    }

    async getById(mosaicId){
        let req = mosaicReqBuilder.getById(mosaic);
        let response = await axios.get(req);
        return response.data;
    }

    async getList(filters){
        let req = mosaicReqBuilder.getList(filters);
        let response = await axios.get(req);
        return response.data;
    }

    async getUniques(filters){
        let req = mosaicReqBuilder.getUniques(filters);
        let response = await axios.get(req);
        return response.data;
    }
}

// Returns repository (singleton)
let mosaicRep = null;
export function getMosaicRep(){
    if(mosaicRep == null) mosaicRep = new MosaicRep();
    
    return mosaicRep;
}


class StatReqBuilder{
    constructor(baseUrl){
        this.baseUrl = baseUrl;
    }

    getSssMbesSurveysByYear(filters){
        let req = this.baseUrl + "/sssmbes-surveys/years";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

    getSssMbesSurveysByType(filters){
        let req = this.baseUrl + "/sssmbes-surveys/types";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

    getSssMbesSurveysByTools(filters){
        let req = this.baseUrl + "/sssmbes-surveys/tools";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

    getSssMbesSurveysByDirection(filters){
        let req = this.baseUrl + "/sssmbes-surveys/directions";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

    getMosaicsByYear(filters){
        let req = this.baseUrl + "/mosaics/years";
        if(filters != null){
            let params = filterToParameters(filters);
            if(params.length) req += "?" + params.join("&");
        }
        return req;
    }

}
let statReqBuilder = new StatReqBuilder("/api/statistics");


class StatRep {
    constructor(){}

    async getSssMbesSurveysByYear(filters){
        let req = statReqBuilder.getSssMbesSurveysByYear(filters);
        let response = await axios.get(req);
        return response.data;
    }

    async getSssMbesSurveysByType(filters){
        let req = statReqBuilder.getSssMbesSurveysByType(filters);
        let response = await axios.get(req);
        return response.data;
    }

    async getSssMbesSurveysByTools(filters){
        let req = statReqBuilder.getSssMbesSurveysByTools(filters);
        let response = await axios.get(req);
        return response.data;
    }

    async getSssMbesSurveysByDirection(filters){
        let req = statReqBuilder.getSssMbesSurveysByDirection(filters);
        let response = await axios.get(req);
        return response.data;
    }

    async getMosaicsByYear(filters){
        let req = statReqBuilder.getMosaicsByYear(filters);
        let response = await axios.get(req);
        return response.data;
    }
}

let statRep = null;
export function getStatRep(){
    if(statRep == null) statRep = new StatRep();
    
    return statRep;
}