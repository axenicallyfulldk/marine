var preLoader = document.getElementById("before-load");

function fadePreLoader(el) {
    el.style.opacity = 1;
    var interPreLoader = setInterval(function () {
        el.style.opacity = el.style.opacity - 0.05;
        if (el.style.opacity <= 0.05) {
            clearInterval(interPreLoader);
            preLoader.style.display = "none";
        }
    }, 16);
}

window.onload = function () {
 setTimeout(function () {
    fadePreLoader(preLoader);
 }, 10000);
};
