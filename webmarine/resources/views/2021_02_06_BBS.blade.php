@extends('layouts.app')

@section('content')

<div style="position: relative; z-index:0" id="map"></div>
<script>
var allSurveys;
var lat=66.529, lon=33.239, zoom=12, zoneId, defaultYear,YearSelector, MethodSelector, ToolSelector, DirectSelector, request_string;
var years=[],methods=[],tools=[],directions=[],data=[], response_data=[];
//
	var m= L.map('map',{zoomControl: false}).setView([lat, lon], zoom);
	var osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
	var basedLayerGroup = new L.LayerGroup();
	basedLayerGroup.addLayer(osm);
	basedLayerGroup.addTo(m);
	m.addLayer(osm);
	m.options.minZoom = 11;
	m.options.maxZoom = 14;
	L.control.zoom({ position: 'bottomleft' }).addTo(m);
// Requests ro Selectors
componentDidMount = async () => {
  // Make first two requests
  const [firstResponse, secondResponse] = await Promise.all([
    axios.get("/api/zones?name=Пролив Великая Салма"),
    axios.get("/api/statistics/surveys/years")
  ]);
    this.defaultYear = firstResponse.data[0].defaultYear;
    this.zoneId = await firstResponse.data[0].id;
    console.log('Default Year: '+ firstResponse.data[0].defaultYear);
//    console.log('Zone ID: '+ firstResponse.data[0].id);
    console.log('Zone ID: '+ zoneId);
//---Years
    this.years= secondResponse.data.map(element => element.year);
    console.log("Years_0: " + years[0] + "| Years_1: " + years[1] + "| Years_2: " + years[2]);
    this.YearSelector = years.reduce(function (totalString, year) {
                                        if (year === this.defaultYear) {
                                            totalString += "<option selected>"+year+"</otion>";
                                        } else {
                                            totalString += "<option>"+year+"</option>";
                                        }
                                         return totalString;
                                        }, "");
     console.log('YearSelectorString: ' + this.YearSelector);
  // Make third request using responses from the first two
    const thirdResponse = await axios.get("/api/surveys?parent="+firstResponse.data[0].id+"&year="+firstResponse.data[0].defaultYear);
    this.data = dataToGeoJson(thirdResponse.data);
//console.log(data[0].year);
//---Methods
    this.methods = [...new Set(thirdResponse.data.map(item => item.type))];
    console.log("Methods_length: " + methods.length);
    console.log("Methods_0: " + methods[0] + "| Methods_1: " + methods[1]);
    this.MethodSelector = methods.reduce(function (totalString, item) {
					 if(item === 'sss') totalString += "<option value='sss'>ГЛБО</option>";
					 if(item === 'mbes') totalString += "<option value='mbes'>МЛЭ</option>";
					 //totalString += "<option>"+item+"</option>";
                                         return totalString;
                                        }, "<option value='AM' selected>Методы</option>");
     console.log('MethodSelectorString: ' + this.MethodSelector);
//---Tools
    this.tools = [...new Set(thirdResponse.data.map(item =>item.tool.id + "#" +   item.tool.brand + "_" + item.tool.model))];
    console.log("Tools_length: " + tools.length);
    console.log("Tool_0: " + tools[0] + "| Tool_1: " + tools[1]);
    this.ToolSelector = tools.reduce(function (totalString, item) {
					 let items = item.split('#');
					 totalString += "<option value="+items[0]+">"+items[1]+"</option>";
                                         return totalString;
                                        }, "<option value='AT' selected>Зонды</option>");
     console.log('ToolSelectorString: ' + this.ToolSelector);
//---Directions
    this.directions = [...new Set(thirdResponse.data.map(item =>  item.direction))];
    console.log("directions_length: " + directions.length);
    console.log("Direction_0: " + directions[0] + "| Direction_1: " + directions[1]);
    this.DirectSelector = directions.reduce(function (totalString, item) {
					 if(item === 'X') totalString += "<option value='X'>Поперечное</option>";
					 if(item === 'L') totalString += "<option value='L'>Продольное</option>";
					 if(item === 'D') totalString += "<option value='D'>Диагональное</option>";
					 //totalString += "<option>"+item+"</option>";
                                         return totalString;
                                        }, "<option value='AD' selected>Направления</option>");
     console.log('DirectSelectorString: ' + this.DirectSelector);
//
//Control YearSelector
	var legend = L.control({position: 'topright'});
	legend.onAdd = function (map) {
		var container = L.DomUtil.create('div', 'YearSelector');
console.log('YearSelectorString2: ' + YearSelector);
		container.innerHTML = '<br/><select id="YearSelector" onchange="OnSelectionChange (this)">'+YearSelector+'</select>';
		container.firstChild.onmousedown = container.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container;
	};
	legend.addTo(m);
//Control MethodSelector
	var legend1 = L.control({position: 'topright'});
	legend1.onAdd = function (map) {
		var container1 = L.DomUtil.create('div', 'MethodSelector');
console.log('MethodSelectorString2: ' + MethodSelector);
		container1.innerHTML = '<select id="MethodSelector" onchange="OnSelectionChange (this)">'+MethodSelector+'</select>';
		container1.firstChild.onmousedown = container1.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container1;
	};
	legend1.addTo(m);
//Control ToolSelector
	var legend2 = L.control({position: 'topright'});
	legend2.onAdd = function (map) {
		var container2 = L.DomUtil.create('div', 'ToolSelector');
console.log('ToolSelectorString2: ' + ToolSelector);
		container2.innerHTML = '<select id="ToolSelector" onchange="OnSelectionChange (this)">'+ToolSelector+'</select>';
		container2.firstChild.onmousedown = container2.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container2;
	};
	legend2.addTo(m);
//Control DirectSelector
	var legend3 = L.control({position: 'topright'});
	legend3.onAdd = function (map) {
		var container3 = L.DomUtil.create('div', 'DirectSelector');
console.log('DirectSelectorString2: ' + DirectSelector);
		container3.innerHTML = '<select id="DirectSelector" onchange="OnSelectionChange (this)">'+DirectSelector+'</select>';
		container3.firstChild.onmousedown = container3.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container3;
	};
	legend3.addTo(m);
//---allSurveys
//console.log("data_features[0].properties.fileName: " + data[0].features[0].properties.fileName);

allSurveys = L.geoJson(data, {

			style: function (feature) {
				return feature.properties && feature.properties.style;
			},
			onEachFeature: onEachFeature
		});
allSurveys.addTo(m);
}//end async componentDidMount
componentDidMount();

//---Functions Block
        function OnSelectionChange (select) {
            var selectedOption = select.options[select.selectedIndex];
            console.log ("The selected option is " + selectedOption.value + " | The selector id is " + select.id);
            if(select.id === "YearSelector"){
		document.getElementById( "MethodSelector").selectedIndex = "0"; 
		document.getElementById( "ToolSelector").selectedIndex = "0";
		document.getElementById( "DirectSelector").selectedIndex = "0";
		request_string = "/api/surveys?parent="+zoneId+"&year="+selectedOption.value;
		LayerUpdate(request_string);

            }
            if(select.id === "MethodSelector"){
//		console.log("Method: " + selectedOption.value);
//		console.log("Method SelectedYear: " + document.getElementById( "YearSelector").value);
		request_string = "/api/surveys?parent="+zoneId+"&year="+document.getElementById( "YearSelector").value;
		if(selectedOption.value != "AM"){ request_string += "&type="+selectedOption.value; }
		if(document.getElementById( "ToolSelector").value != "AT"){ request_string += "&tool="+document.getElementById( "ToolSelector").value; }	
		if(document.getElementById( "DirectSelector").value != "AD"){ request_string += "&direction="+document.getElementById( "DirectSelector").value; }	
		console.log("Method zoneId: " + zoneId);
		console.log("Method request_string: " + request_string);
		LayerUpdate(request_string);
            }
            if(select.id === "ToolSelector"){
		console.log("ToolSelector value: " + selectedOption.value);
		var bm = selectedOption.text.split("_");
		console.log("Brand: " + bm[0] + " Model: " + bm[1]);
		request_string = "/api/surveys?parent="+zoneId+"&year="+document.getElementById( "YearSelector").value;
		if(selectedOption.value != "AT"){ request_string += "&tool="+selectedOption.value; }
		if(document.getElementById( "MethodSelector").value != "AM"){ request_string += "&type="+document.getElementById( "MethodSelector").value; }
		if(document.getElementById( "DirectSelector").value != "AD"){ request_string += "&direction="+document.getElementById( "DirectSelector").value; }
		console.log("Direction request_string: " + request_string);
		LayerUpdate(request_string);	
            }
            if(select.id === "DirectSelector"){
		console.log("Direction: " + selectedOption.value);
		request_string = "/api/surveys?parent="+zoneId+"&year="+document.getElementById( "YearSelector").value;
		if(selectedOption.value != "AD"){ request_string += "&direction="+selectedOption.value; }
		if(document.getElementById( "ToolSelector").value != "AT"){ request_string += "&tool="+document.getElementById( "ToolSelector").value; }
		if(document.getElementById( "MethodSelector").value != "AM"){ request_string += "&type="+document.getElementById( "MethodSelector").value; }
		console.log("Direction request_string: " + request_string);
		LayerUpdate(request_string);	
            }
        }
//---REST API Request functions
function LayerUpdate(request_string){
	m.removeLayer(allSurveys); data=[];
	fetch(request_string)
	.then(function(response){ return response.json() })
	.then(function(response_data){

//	console.log("Response_data: " + response_data);
		data = dataToGeoJson(response_data);
		allSurveys = L.geoJson(data, {
			style: function (feature) {
				return feature.properties && feature.properties.style;
			},
			onEachFeature: onEachFeature
		});
		allSurveys.addTo(m);
});// end fetch
}//end function
//---To GeoJSON Functions
// Converts date string presented in "dd.mm.yyyy" format
// to ISO format "yyyy.mm.dd"
function rusDateStrToIsoDateStr(rusDateStr){
	return rusDateStr.split(".").reverse().join("-");
}

// Parse date present in russian format "dd.mm.yyyy"
function parseRusDate(rusDateStr){
	return new Date(rusDateStrToIsoDateStr(rusDateStr));
}

// Determines entity type
function getEntityType(entity){
	if("creationDate" in entity) return "mosaic";
	if("startDate" in entity) return "survey";
	return "unknown";
}

// Returns field used for entity
function getEntityDateField(entityType){
	if(entityType == "survey") return "startDate";
	if(entityType == "mosaic") return "creationDate";
	return null;
 }

// Converts survey or mosaic to GeoJSON feature
function survMosaicToFeature(survMosaic){
    let feature = {"type":"Feature", "properties": {}, "geometry": {"type":"Polygon"}};
    let type = getEntityType(survMosaic);
    let dateField = getEntityDateField(type);
    feature.id = survMosaic.id;
    feature.geometry.coordinates = survMosaic.polygon;
    feature.properties.name = survMosaic.name;
    feature.properties.dateField = survMosaic[dateField];
//    feature.properties.type = survMosaic.type;
    feature.properties.direction = survMosaic.direction;
    feature.properties.startDate = survMosaic.startDate;
    feature.properties.brand_model = survMosaic.tool.brand + "_" + survMosaic.tool.model;

    feature.isDownloadable = survMosaic.isDownloadable;
    if(survMosaic.type === "sss"){
        feature.properties.type = "ГЛБО";
        if(feature.properties.brand_model === "Klein_3900"){
		feature.properties.style = surveyStyle("sss","Klein_3900",2,"#33f","#DAFF00",0.8);
	} else if(feature.properties.brand_model === "Benthos_C3D"){
		feature.properties.style = surveyStyle("sss","Benthos_C3D",2,"#33f","#FFDD00",0.8);
	}
    } else if(survMosaic.type === "mbes"){
        feature.properties.type = "МЛЭ";
        if(feature.properties.brand_model === "Klein_3900"){
		feature.properties.style = surveyStyle("mbes","Klein_3900",3,"#f33","#DAFF00",0.8);
	} else if(feature.properties.brand_model === "Benthos_C3D"){
		feature.properties.style = surveyStyle("mbes","Benthos_C3D",2,"#33f","#FFDD00",0.8);
	}
    } else {
	feature.properties.type = survMosaic.type;
    }
    if(type == "survey"){
        feature.properties.vesselName = survMosaic.trip.vessel.name;
//console.log("VesselName: "+survMosaic.trip.vessel.name);
console.log("VesselName: "+feature.properties.vesselName);
    }
    return feature;
}
//Return style for survey
function surveyStyle(type,tool,weight,borderColor,fillColor,fillOpacity){
	let survey_style={"weight": 2, "color": "#555","opacity": 1, "fillColor": "#333", "fillOpacity": 0.3};
        survey_style.weight = weight; 
        survey_style.color = borderColor; 
	survey_style.fillColor = fillColor;
	survey_style.fillOpacity = fillOpacity;
	return survey_style;
}
// Converts array of surveys(mosaics) to GeoJSON
function dataToGeoJson(data){
	if(data.length == 0) return [];
	let entityType = getEntityType(data[0]);
	let dateField = getEntityDateField(entityType);
	let types = [];
	let tools = [];
	// split elements by year, find used types and tools;
	let dataByYear = {};
	for (element of data){
		let year = parseRusDate(element[dateField]).getFullYear();
		if(!(year in dataByYear)) dataByYear[year] = [];
		dataByYear[year].push(survMosaicToFeature(element));
		if(!types.includes(element.type)) types.push(element.type);
		if(entityType == "survey"){
			let toolName = element.tool.brand + " " + element.tool.model;
			if(!tools.includes(toolName)) tools.push(toolName);
		}
	}

	// Build geoJson.
	let res = [];
	for(year in dataByYear){
		let featureCollection = {"type":"FeatureCollection"};
		featureCollection.year = year;
		featureCollection.stypes = types;
		featureCollection.features = dataByYear[year];
		if(entityType == "survey")
			featureCollection.tools = tools;
		res.push(featureCollection);
	}

	return res;
}
////-estart function onEachFeature
function onEachFeature(feature, layer) {

        		layer.bindContextMenu({
            			contextmenu: true,
                                contextmenuInheritItems: false,
                                hideOnSelect: true,
	        		contextmenuItems: [
            { text: feature.properties.name.toString(), callback: function () { alert('Имя профиля: '+feature.properties.name); } },
            { separator: true },
            { text: 'Тип съемки: '+feature.properties.type, callback: function () { alert('Тип съемки: '+feature.properties.type); } },
            { text: 'Дата рейса: '+feature.properties.startDate, callback: function () { alert('Дата рейса: '+feature.properties.startDate); } },
            { text: 'Зонд: '+feature.properties.brand_model, callback: function () { alert('Зонд: '+feature.properties.brand_model); } },
            { text: 'НИС: '+feature.properties.vesselName, callback: function () { alert('Судно: '+feature.properties.vesselName); } },
            { separator: true },
            { text: 'Скрыть', callback: function () { hide(); } }
                                ]
        		});

                        // layer.on( 'mouseout', function(){ alert("HaHaHa!!!"); } );
}//-end function onEachFeature
//---End of Functions Block
</script>

@endsection
