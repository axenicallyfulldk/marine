@extends('layouts.app')

@section('content')

<div style="position: relative; z-index:0" id="map"></div>

<!-- <script type="module" src="/js/marine_utils.js"></script> -->
<script type="module">

import {createColorMap, getSssMbesToolRep, getMosaicTypeRep, getSssMbesTypeRep, getZoneRep, getStatRep, getSssMbesSurveyRep, getMosaicRep} from '/js/marine_utils.js';

let toolRep = getSssMbesToolRep();
let toolColors = createColorMap(Array.from(await toolRep.getIds()));
let mosaicTypeRep = getMosaicTypeRep();
let sssmbesTypeRep = getSssMbesTypeRep();

// let statRep = getStatRep();
// console.log(await statRep.getSssMbesByYear());
// console.log(await statRep.getSssMbesByType());
// console.log(await statRep.getSssMbesByTools());
// console.log(await statRep.getSssMbesByDirection());

let survRep = getMosaicRep();
console.log(await survRep.getList());


var allSurveys;
var lat=66.529, lon=33.239, zoom=12;
var zoneId;
var defaultYear,YearSelector, MethodSelector, ToolSelector, DirectSelector, request_string;
var years=[],methods=[],tools=[],directions=[],data=[], response_data=[];

// Initialize leaflet
var m= L.map('map',{zoomControl: false}).setView([lat, lon], zoom);
var osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
var basedLayerGroup = new L.LayerGroup();
basedLayerGroup.addLayer(osm);
basedLayerGroup.addTo(m);
m.addLayer(osm);
m.options.minZoom = 11;
m.options.maxZoom = 14;
L.control.zoom({ position: 'bottomleft' }).addTo(m);

// Requests ro Selectors
let componentDidMount = async () => {
	// Request zone info
	let zoneReq = await axios.get("/api/zones?name=Пролив Великая Салма");
	
	defaultYear = zoneReq.data[0].defaultYear;
	zoneId = zoneReq.data[0].id;
	console.log('Default Year: '+ zoneReq.data[0].defaultYear);
	console.log('Zone ID: '+ zoneId);

	// Request statistics by zone
	let statReq = await axios.get(`/api/statistics/mosaics/years?parent=${zoneId}`);
	
	// Fetch years
	years= statReq.data.map(item => item.year);
	console.log("Years: [" + years.join(",")+"]");

	// Build options string
	YearSelector = "";
	for(let year of years){
		if(year == defaultYear)
			YearSelector = YearSelector + `<option selected>${year}</otion>`;
		else
			YearSelector = YearSelector + `<option>${year}</otion>`;
	}
	console.log('YearSelectorString: ' + YearSelector);
	
	// Make third request using responses from the first two
	// Request mosaics in zone and default year
	let mosaicReq = await axios.get("/api/mosaics?parent="+zoneId+"&year="+defaultYear);
	data = await dataToGeoJson(mosaicReq.data);
	
	// Extract mosaic types
	methods = [...new Set(mosaicReq.data.map(item => item.type))];
	console.log("Methods: " + methods.join(","));
	MethodSelector = methods.reduce(function (totalString, item) {
		if(item === 'sss') totalString += "<option value='sss'>ГЛБО</option>";
		if(item === 'mbes') totalString += "<option value='mbes'>МЛЭ</option>";
		if(item === 'sssmbes') totalString += "<option value='mix'>МЛЭ+ГЛБО</option>";
		return totalString;
	}, "<option value='AM' selected>Методы</option>");
	console.log('MethodSelectorString: ' + MethodSelector);


	let toolToString = function(tool){
		return tool.id+"#"+[tool.brand,tool.model].filter(el => el != null).join(" ");
	}
	
	//Extract tools
	tools = [...new Set(mosaicReq.data.map(item => toolToString(item.mainTool)))];
	console.log("Tools: "+tools.join(", "));
	
	ToolSelector = tools.reduce(function (totalString, item) {
		let items = item.split('#');
		totalString += "<option value="+items[0]+">"+items[1]+"</option>";
		return totalString;
	}, "<option value='AT' selected>Зонды</option>");
	
	console.log('ToolSelectorString: ' + ToolSelector);

	// Extract directions
	directions = [...new Set(mosaicReq.data.map(item =>  item.inputDirection))];
	console.log("Directions: "+directions.join(", "));
	// console.log("directions_length: " + directions.length);
	DirectSelector = directions.reduce(function (totalString, item) {
		if(item === 'X') totalString += "<option value='X'>Поперечное</option>";
		if(item === 'L') totalString += "<option value='L'>Продольное</option>";
		if(item === 'D') totalString += "<option value='D'>Диагональное</option>";
		if(item === 'I') totalString += "<option value='I'>Разное</option>";
		return totalString;
	}, "<option value='AD' selected>Направления</option>");
	console.log('DirectSelectorString: ' + DirectSelector);
	
	// Add control for year selection
	var legend = new L.Control({ position: 'topright' });
	legend.onAdd = function (map) {
		var container = L.DomUtil.create('div', 'YearSelector');
		console.log('YearSelectorString2: ' + YearSelector);
		container.innerHTML = '<br/><select id="YearSelector" onchange="OnSelectionChange (this)">'+YearSelector+'</select>';
		container.firstChild.onmousedown = container.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container;
	};
	legend.addTo(m);

	// Add control for mosaic type selection
	var legend1 = new L.Control({ position: 'topright' });
	legend1.onAdd = function (map) {
		var container1 = L.DomUtil.create('div', 'MethodSelector');
		console.log('MethodSelectorString2: ' + MethodSelector);
		container1.innerHTML = '<select id="MethodSelector" onchange="OnSelectionChange (this)">'+MethodSelector+'</select>';
		container1.firstChild.onmousedown = container1.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container1;
	};
	legend1.addTo(m);
	
	// Add control for tool selection
	var legend2 = new L.Control({ position: 'topright' });
	legend2.onAdd = function (map) {
		var container2 = L.DomUtil.create('div', 'ToolSelector');
		console.log('ToolSelectorString2: ' + ToolSelector);
		container2.innerHTML = '<select id="ToolSelector" onchange="OnSelectionChange (this)">'+ToolSelector+'</select>';
		container2.firstChild.onmousedown = container2.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container2;
	};
	legend2.addTo(m);

	// Add control for direction selection
	var legend3 = new L.Control({ position: 'topright' });
	legend3.onAdd = function (map) {
		var container3 = L.DomUtil.create('div', 'DirectSelector');
		console.log('DirectSelectorString2: ' + DirectSelector);
		container3.innerHTML = '<select id="DirectSelector" onchange="OnSelectionChange (this)">'+DirectSelector+'</select>';
		container3.firstChild.onmousedown = container3.firstChild.ondblclick = L.DomEvent.stopPropagation;
		return container3;
	};
	legend3.addTo(m);

	//---allSurveys
	//console.log("data_features[0].properties.fileName: " + data[0].features[0].properties.fileName);
	
	allSurveys = L.geoJson(data, {
		style: function (feature) {
			// return feature.properties && feature.properties.style;
			return feature.properties.style;
		},
		onEachFeature: onEachFeature
	});
	allSurveys.addTo(m);
}//end async componentDidMount
	
componentDidMount();

//---Functions Block
window.OnSelectionChange = function(select){
// function OnSelectionChange (select) {
	var selectedOption = select.options[select.selectedIndex];
	console.log ("The selected option is " + selectedOption.value + " | The selector id is " + select.id);
	if(select.id === "YearSelector"){
		document.getElementById( "MethodSelector").selectedIndex = "0"; 
		document.getElementById( "ToolSelector").selectedIndex = "0";
		document.getElementById( "DirectSelector").selectedIndex = "0";
		request_string = "/api/mosaics?parent="+zoneId+"&year="+selectedOption.value;
		LayerUpdate(request_string);
	}
	if(select.id === "MethodSelector"){
//		console.log("Method: " + selectedOption.value);
//		console.log("Method SelectedYear: " + document.getElementById( "YearSelector").value);
		request_string = "/api/mosaics?parent="+zoneId+"&year="+document.getElementById( "YearSelector").value;
		if(selectedOption.value != "AM"){ request_string += "&type="+selectedOption.value; }
		if(document.getElementById( "ToolSelector").value != "AT"){ request_string += "&mainTool="+document.getElementById( "ToolSelector").value; }	
		if(document.getElementById( "DirectSelector").value != "AD"){ request_string += "&inputDirection="+document.getElementById( "DirectSelector").value; }	
		console.log("Method zoneId: " + zoneId);
		console.log("Method request_string: " + request_string);
		LayerUpdate(request_string);
	}
	if(select.id === "ToolSelector"){
		console.log("ToolSelector value: " + selectedOption.value);
		var bm = selectedOption.text.split("_");
		console.log("Brand: " + bm[0] + " Model: " + bm[1]);
		request_string = "/api/mosaics?parent="+zoneId+"&year="+document.getElementById( "YearSelector").value;
		if(selectedOption.value != "AT"){ request_string += "&mainTool="+selectedOption.value; }
		if(document.getElementById( "MethodSelector").value != "AM"){ request_string += "&type="+document.getElementById( "MethodSelector").value; }
		if(document.getElementById( "DirectSelector").value != "AD"){ request_string += "&inputDirection="+document.getElementById( "DirectSelector").value; }
		console.log("Direction request_string: " + request_string);
		LayerUpdate(request_string);	
	}
	if(select.id === "DirectSelector"){
		console.log("Direction: " + selectedOption.value);
		request_string = "/api/mosaics?parent="+zoneId+"&year="+document.getElementById( "YearSelector").value;
		if(selectedOption.value != "AD"){ request_string += "&inputDirection="+selectedOption.value; }
		if(document.getElementById( "ToolSelector").value != "AT"){ request_string += "&mainTool="+document.getElementById( "ToolSelector").value; }
		if(document.getElementById( "MethodSelector").value != "AM"){ request_string += "&type="+document.getElementById( "MethodSelector").value; }
		console.log("Direction request_string: " + request_string);
		LayerUpdate(request_string);	
	}
}

//---REST API Request functions
async function LayerUpdate(request_string){
	m.removeLayer(allSurveys); data=[];
	fetch(request_string)
	.then(function(response){ return response.json() })
	.then(function(response_data){
		//	console.log("Response_data: " + response_data);
		// data = await dataToGeoJson(response_data);
		return dataToGeoJson(response_data);
	})
	.then(function(geoJsonData){
		allSurveys = L.geoJson(geoJsonData, {
			style: function (feature) {
				return feature.properties && feature.properties.style;
			},
			onEachFeature: onEachFeature
		});
		allSurveys.addTo(m);
	});// end fetch
}//end function


//---To GeoJSON Functions
// Converts date string presented in "dd.mm.yyyy" format
// to ISO format "yyyy-mm-dd"
function rusDateStrToIsoDateStr(rusDateStr){
	return rusDateStr.split(".").reverse().join("-");
}

// Parse date present in russian format "dd.mm.yyyy"
function parseRusDate(rusDateStr){
	return new Date(rusDateStrToIsoDateStr(rusDateStr));
}

// Determines entity type
function getEntityType(entity){
	if("creationDate" in entity) return "mosaic";
	if("startDate" in entity) return "survey";
	return "unknown";
}

// Returns field used for entity
function getEntityDateField(entityType){
	if(entityType == "survey") return "startDate";
	if(entityType == "mosaic") return "creationDate";
	return null;
 }

// Converts survey or mosaic to GeoJSON feature
async function survMosaicToFeature(survMosaic){
    let feature = {"type":"Feature", "properties": {}, "geometry": {"type":"Polygon"}};
    let entityType = getEntityType(survMosaic);
    let dateField = getEntityDateField(entityType);
    feature.id = survMosaic.id;
    feature.geometry.coordinates = survMosaic.area;
    feature.properties.name = survMosaic.name;
    feature.properties.dateField = survMosaic[dateField];
	if(entityType == "mosaic"){
		feature.properties.type = (await mosaicTypeRep.getByKey(survMosaic.type)).name;
	}else{
		feature.properties.type = (await sssmbesTypeRep.getByKey(survMosaic.type)).name;
	}
	feature.properties.direction = survMosaic.inputDirection;
	feature.properties.brand_model = [survMosaic.mainTool.brand, survMosaic.mainTool.model].filter((el)=>{return el != null;}).join(" ");

    feature.properties.isDownloadable = survMosaic.isDownloadable?true:false;
	let color = toolColors.get(survMosaic.mainTool.id);
	feature.properties.style = surveyStyle(1, color, color, 0.8);

    if(entityType == "survey"){
        feature.properties.vesselName = survMosaic.trip.vessel.name;
    }
    return feature;
}

//Returns style for survey
// function surveyStyle(type,tool,weight,borderColor,fillColor,fillOpacity){
// 	let survey_style={"weight": 2, "color": "#555","opacity": 1, "fillColor": "#333", "fillOpacity": 0.3};
//     survey_style.weight = weight; 
//     survey_style.color = borderColor; 
// 	survey_style.fillColor = fillColor;
// 	survey_style.fillOpacity = fillOpacity;
// 	return survey_style;
// }
function surveyStyle(weight,borderColor,fillColor,fillOpacity){
	let survey_style={"weight": 2, "color": "#555","opacity": 1, "fillColor": "#333", "fillOpacity": 0.3};
    survey_style.weight = weight; 
    survey_style.color = borderColor; 
	survey_style.fillColor = fillColor;
	survey_style.fillOpacity = fillOpacity;
	return survey_style;
}

// Converts array of surveys(mosaics) to GeoJSON
async function dataToGeoJson(data){
	if(data.length == 0) return [];
	let entityType = getEntityType(data[0]);
	let dateField = getEntityDateField(entityType);
	let types = [];
	let tools = [];
	// split elements by year, find used types and tools;
	let dataByYear = {};
	for (let element of data){
		let year = parseRusDate(element[dateField]).getFullYear();
		if(!(year in dataByYear)) dataByYear[year] = [];
		dataByYear[year].push(await survMosaicToFeature(element));
		if(!types.includes(element.type)) types.push(element.type);
		if(entityType == "survey"){
			let toolName =  [element.tool.brand, element.tool.model].filter(function(el){return el != null;}).join(" ");
			if(!tools.includes(toolName)) tools.push(toolName);
		}else if(entityType == "mosaic"){
			let toolName = [element.mainTool.brand, element.mainTool.model].filter(function(el){return el != null;}).join(" ");
			if(!tools.includes(toolName)) tools.push(toolName);
		}
	}

	// Build geoJson.
	let res = [];
	for(let year in dataByYear){
		let featureCollection = {"type":"FeatureCollection"};
		featureCollection.year = year;
		featureCollection.stypes = types;
		featureCollection.features = dataByYear[year];
		if(entityType == "survey")
			featureCollection.tools = tools;
		res.push(featureCollection);
	}

	return res;
}

//Function Download
function download(url, filename) {
	fetch(url).then(function(t) {
		return t.blob().then((b)=>{
			var a = document.createElement("a");
			a.href = URL.createObjectURL(b);
			a.setAttribute("download", filename);
			a.click();
			a.remove();
		});
	});
}

////-start function onEachFeature
function onEachFeature(feature, layer) {
	let cmenu = {
		contextmenu: true,
		contextmenuInheritItems: false,
		hideOnSelect: true,
		contextmenuItems: [
            { text: feature.properties.name.toString(), callback: function () { alert('Имя профиля: '+feature.properties.name); } },
            { separator: true },
            { text: 'Тип съемки: '+feature.properties.type, callback: function () { alert('Тип съемки: '+feature.properties.type); } },
            { text: 'Зонд: '+feature.properties.brand_model, callback: function () { alert('Зонд: '+feature.properties.brand_model); } },
            { text: '', callback: function () { download("/api/mosaics/"+feature.id+"/file",feature.properties.name+".xtf")} },
            { text: '', callback: function () { download("/api/mosaics/"+feature.id+"/arch",feature.properties.name+".zip")} },
            { separator: true },
            { text: 'Скрыть', callback: function () { hide(); } }
		]
	};
	if((feature.id === 71)&&(feature.properties.isDownloadable)){
		cmenu.contextmenuItems[4].text = 'Файл съемки';
		cmenu.contextmenuItems[5].text = 'Архив съемки';
	}
	layer.bindContextMenu(cmenu);
	// layer.on( 'mouseout', function(){ alert("HaHaHa!!!"); } );
}//-end function onEachFeature
//---End of Functions Block
</script>

@endsection
