@extends('layouts.app')

@section('content')

<div class="loader" id="before-load">
   <div class='cssload-loader'>
      <div class='cssload-inner cssload-one'></div>
      <div class='cssload-inner cssload-two'></div>
      <div class='cssload-inner cssload-three'></div>
   </div>
</div>

<div id="ThreeJS" style="position: absolute; left:0px; top:0px">
  <div style='position: absolute; padding-left: 10px; font-size:200%; color: white;'>
	<!-- <strong>Received Events :</strong> -->
	<div id='logs'></div>
  </div>
</div>
<script>
        var container, scene, camera, renderer, raycaster;
	var mouse = { x: 0, y: 0 }, INTERSECTED;
	var canvas1, context1, sprite1;
	var Earth, WhiteSea;

        ThreeJS.setAttribute('width', window.innerWidth);
        ThreeJS.setAttribute('height', window.innerHeight);

//Renderer
	// RENDERER
        renderer = new THREE.WebGLRenderer( {antialias:true} );
        renderer.setClearColor(0x000000);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
//        document.body.appendChild(renderer.domElement);
	container = document.getElementById( 'ThreeJS' );
	container.appendChild( renderer.domElement );
	renderer.autoClear = false;

	var scene	= new THREE.Scene()
	var camera	= new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 10000)
	camera.position.set(0,150,6000);
	camera.lookAt(scene.position);
//Contrtols
        var controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.enableDamping = true;   //damping 
	controls.dampingFactor = 0.25;   //damping inertia
	controls.minDistance = 4000.0;
	controls.maxDistance = 6000.0;
//Lights
        var light = new THREE.AmbientLight(0xffffff);  
        scene.add(light);
        var directionalLight = new THREE.DirectionalLight(0xffbb94, 0.2); 
        directionalLight.position.set( 250, 210, 0 ); 
        directionalLight.target.position.set( 0, 0, 0 ); 

        scene.add( directionalLight );

	//////////////////////////////////////////////////////////////////////////////////
	//		init domEvents							//
	//////////////////////////////////////////////////////////////////////////////////
	
	var domEvents	= new THREEx.DomEvents(camera, renderer.domElement);

	//////////////////////////////////////////////////////////////////////////////////
	//		comment								//
	//////////////////////////////////////////////////////////////////////////////////
//Loader
	var manager = new THREE.LoadingManager();
	manager.onProgress = function (item, loaded, total) {
		console.log(item, loaded, total);
	};

var onProgress = function ( xhr ) {
                        if ( xhr.lengthComputable ) {
                            var percentComplete = xhr.loaded / xhr.total * 100;
                            console.log( Math.round(percentComplete, 2) + '% downloaded' );
                        }
};

var onError = function ( xhr ) {
                        alert("Loading Error!");
};
			// Create a material
			var textureLoader = new THREE.TextureLoader();
			var map = textureLoader.load("{{ asset('storage/models/globe/color_etopo2_ice_low.jpg') }}");
			var material = new THREE.MeshPhongMaterial({map: map});
			//
//alert('Loading start');
		        var loader = new THREE.OBJLoader(manager);
			loader.load( "{{ asset('storage/models/globe/Globe90_90sd3x32.obj') }}", function ( object ) {
 			// For any meshes in the model, add our material.
			Earth = object.getObjectByName('Sphere');
			Earth.material = material;
			WhiteSea = object.getObjectByName('Sphere.001');
			WhiteSea.material = material;
			object.scale.set(2900, 2900, 2900);
			//object.position.set(0, 0, 0);
			object.rotation.x=0.9;
			object.rotation.y=-2.6;
			//object.position.y=-0.9;

			scene.add( object );
		//////////////////////////////////////////////////////////////////////////////////
		//		display the events log						//
		//////////////////////////////////////////////////////////////////////////////////
		THREEx.DomEvents.eventNames.forEach(function(eventName){
			if( eventName === 'mousemove' )	return
			domEvents.addEventListener(WhiteSea, eventName, function(event){
				var domElement	= document.querySelector('#logs');
				if( eventName === 'mouseover' ){ domElement.innerHTML	= 'Белое море'; document.body.style.cursor = 'pointer'; tooltip.show('<strong>Белое море</strong>');}
				if( eventName === 'mouseout' ){ domElement.innerHTML	= ''; document.body.style.cursor = 'default'; tooltip.hide(); }
				if( eventName === 'click' ){ tooltip.hide(); window.open('/home/WhiteSea','_self'); }
			}, false)

		})
//alert('Loading complete');
var preLoader = document.getElementById("before-load");
fadePreLoader(preLoader);
}, onProgress, onError);

//Animation
       function animate()
       {
       controls.update();
         requestAnimationFrame(animate);  
         renderer.render (scene, camera);
//	 update();
      }
      animate();
</script>

@endsection
