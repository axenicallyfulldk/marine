@extends('layouts.app')

@push('styles')
@endpush

@section('content')


<div id="seismic_stack_viewer" style="height:100%">
    <router-view></router-view>
</div>

@endsection
