<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('css/leaflet.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/leaflet.contextmenu.css') }}"/>

    @stack('styles')    
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-light shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('auth.login') }}</a>
                                </li>
                            @endif
                            
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="z-index:2000">
                                    @if (Route::has('home'))
                                        <a class="dropdown-item" href="{{ route('home') }}">{{ __('Главная') }}</a>
                                    @endif
                                    @if (Route::has('survey-stat'))
                                        <a class="dropdown-item" href="{{ route('survey-stat') }}">{{ __('stat.sssmbes_survey') }}</a>
                                    @endif
                                    @if (Route::has('mosaic-stat'))
                                        <a class="dropdown-item" href="{{ route('mosaic-stat') }}">{{ __('stat.mosaic') }}</a>
                                    @endif

                                    @if (Route::has('sssmbes-batch-import') && Auth::user()->editSssMbesSurveysByDefault())
                                        <a class="dropdown-item" href="{{ route('sssmbes-batch-import') }}">{{ __('Массовая загрузка ГЛБО/МЛЭ') }}</a>
                                    @endif

                                    @if (Route::has('seismic-batch-import') && Auth::user()->editSeismic2dSurveysByDefault())
                                        <a class="dropdown-item" href="{{ route('seismic-batch-import') }}">{{ __('Массовая загрузка сейсмики') }}</a>
                                    @endif

                                    @if (Route::has('model-batch-import') && Auth::user()->editModelsByDefault())
                                        <a class="dropdown-item" href="{{ route('model-batch-import') }}">{{ __('Массовая загрузка моделей') }}</a>
                                    @endif

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('auth.logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
