@extends('layouts.app')

@push('styles')
@endpush

@section('content')


<div id="entity_map" style="height:100%">
    <router-view></router-view>
</div>

@endsection
