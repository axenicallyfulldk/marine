<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'auth' => 'Авторизация',
    'failed' => 'Эти учётыне данные не соответствуют нашим записям.',
    'password' => 'Введённый пароль некорректен.',
    'throttle' => 'Слишком много попыток входа. Попробуйте снова чере :seconds секунд.',
    'login' => 'Войти',
    'logout' => 'Выйти',
    'email' => 'E-mail',
    'pass' => 'Пароль',
    'remember' => 'Запомни меня',

];
