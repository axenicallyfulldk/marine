function base64ToFloatArray (dataStr) {
  const blob = window.atob(dataStr) // Base64 string converted to a char array
  const fLen = blob.length / Float32Array.BYTES_PER_ELEMENT // How many floats can be made, but be even
  const dView = new DataView(new ArrayBuffer(Float32Array.BYTES_PER_ELEMENT)) // ArrayBuffer/DataView to convert 4 bytes into 1 float.
  const fAry = new Float32Array(fLen) // Final Output at the correct size
  let p = 0 // Position

  for (let j = 0; j < fLen; j++) {
    p = j * 4
    dView.setUint8(0, blob.charCodeAt(p))
    dView.setUint8(1, blob.charCodeAt(p + 1))
    dView.setUint8(2, blob.charCodeAt(p + 2))
    dView.setUint8(3, blob.charCodeAt(p + 3))
    fAry[j] = dView.getFloat32(0, true)
  }
  return fAry
}

class SegyRep {
  constructor (socketUrl) {
    this.socketUrl = socketUrl

    this.segyInfo = null
    this.traceCount = null
    this.sampleCount = null
    // this.traces = []
    this.tracePromises = new Map()

    this.socket = new WebSocket(this.socketUrl)

    this.readyPromise = new Promise((resolve, reject) => {
      this.readyPromiseResolve = resolve
      this.readyPromiseReject = reject
    })

    this.socket.onopen = () => this._onOpen()
    this.socket.onclose = (event) => this._onClose(event)
    this.socket.onmessage = (resp) => this._onMessage(resp)
    this.socket.onerror = (e) => this._onError(e)

    this._errorObservers = []
    this._readyObservers = []
  }

  getInfo () {
    return this.segyInfo
  }

  getTraceCount () {
    return this.segyInfo.traceCount
  }

  getSampleCount () {
    return this.segyInfo.sampleCount
  }

  getSampleInterval () {
    return this.segyInfo.sampleInterval
  }

  getMinmaxNorm () {
    return this.segyInfo.minmaxNorm
  }

  getTrace (traceIndex) {
    if (!this.tracePromises.has(traceIndex)) {
      let traceResolv = null
      let traceReject = null
      const tracePromise = new Promise((resolve, reject) => {
        traceResolv = resolve
        traceReject = reject
      })
      this.tracePromises.set(traceIndex, { promise: tracePromise, resolve: traceResolv, reject: traceReject })
      this.socket.send(JSON.stringify({ command: 'getTrace', traceIndex: traceIndex }))
      return tracePromise
    } else {
      return this.tracePromises.get(traceIndex).promise
    }
  }

  isTraceCached (traceIndex) {
    return false
  }

  addErrorObserver (obs) {
    this._errorObservers.push(obs)
  }

  removeErrorObserver (obs) {
    this._errorObservers = this._errorObservers.filter(
      (o) => {
        if (o !== obs) return true
        return false
      }
    )
  }

  _notifyErrorObservers (message) {
    for (const errObs of this._errorObservers) {
      errObs(message)
    }
  }

  getReadyPromise () {
    return this.readyPromise
  }

  addReadyObserver (obs) {
    this._readyObservers.push(obs)
  }

  removeReadyObserver (obs) {
    this._readyObservers = this._readyObservers.filter(
      (o) => {
        if (o !== obs) return true
        return false
      }
    )
  }

  _notifyReadyObservers () {
    for (const errObs of this._readyObservers) {
      errObs()
    }
  }

  _onOpen () {
    console.log('Successfully Connected')
    this.socket.send(JSON.stringify({ command: 'getInfo' }))
  }

  _onClose (event) {
    console.log('Socket Closed Connection: ', event)
  }

  _onError (error) {
    window.hmmErr = error
    const errMsg = 'Socket Error: ' + error
    this.readyPromiseReject(errMsg)
    this._notifyErrorObservers(errMsg)
    console.log('Socket Error: ', error)
  };

  _onReady () {
    this.readyPromiseResolve()
    this._notifyReadyObservers()
  }

  _onMessage (resp) {
    const data = JSON.parse(resp.data)
    if ('error' in data) {
      this._notifyErrorObservers(data.error)
    } else if ('command' in data && data.command === 'getTrace') {
      const trace = base64ToFloatArray(data.trace)
      const traceIndex = data.traceIndex
      const resolve = this.tracePromises.get(traceIndex).resolve
      this.tracePromises.delete(traceIndex)
      resolve(trace)
    } else if ('command' in data && data.command === 'getInfo') {
      this.segyInfo = data
      this.traceCount = data.traceCount
      this.sampleCount = data.sampleCount

      this._onReady()
    }
  }
};

class CachedSegyRep extends SegyRep {
  constructor (socketUrl) {
    super(socketUrl)

    this.traces = []

    const oldOnReady = super._onReady

    super._onReady = () => {
      this.traces.length = this.getTraceCount()

      oldOnReady.apply(this)
    }
  }

  isTraceCached (traceIndex) {
    if (traceIndex >= this.getTraceCount()) return false

    const trace = this.traces[traceIndex]
    return trace != null
  }

  getTrace (traceIndex) {
    const trace = this.traces[traceIndex]
    if (trace == null) {
      const tracePromise = super.getTrace(traceIndex)
      tracePromise.then((data) => { this.traces[traceIndex] = data })
      return tracePromise
    } else {
      return trace
    }
  }
};

class CachedStack2dRep extends CachedSegyRep {
  // constructor (socketUrl) {
  //   super(socketUrl)
  // }
};

export function getSeismic2dStackSegyRep (stackId) {
  return new CachedStack2dRep(`ws://${window.location.hostname}:8080/api/seismic2d-stack-segy/${stackId}`)
}
