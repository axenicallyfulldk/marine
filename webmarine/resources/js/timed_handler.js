// Class implements object that calls given handler if event has not been
// triggered for some time
export class TimedHandler {
    constructor (subHandler, delay = 500) {
      this.timerId = null
      this.subHandler = subHandler
      this.delay = delay

      let that = this

      this.handler = function (event) {
        if (that.timerId != null) clearTimeout(that.timerId)
        that.timerId = setTimeout(that.subHandler, that.delay, event)
      }
    }

    getHandler () {
      return this.handler
    }

    setDelay (delay) {
      this.delay = delay
    }

    setSubHandler (sh) {
      this.subHandler = sh
    }
}
