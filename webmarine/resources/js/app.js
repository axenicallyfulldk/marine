/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import GeoMap from './components/GeoMap.vue'
import StackViewer from './components/StackViewer.vue'
import InterpretedModelViewer from './components/InterpretedModelViewer.vue'
import StatisticsBySurveys from './components/StatisticsBySssMbesSurveys.vue'
import StatisticsByMosaics from './components/StatisticsByMosaics.vue'
import SurveyMosaicMap from './components/SurveyMosaicMap.vue'

import BatchImporter from './components/BatchImporter.vue'

import BatchModelImporter from './components/BatchModelImporter.vue'

import { getInterpretedModelRep } from './repositories/interpreted_model_rep'

import { getHazardTypeRep } from './repositories/hazard_type_rep'

import VueRouter from 'vue-router'
import MapView from './components/Map.vue'

require('./bootstrap')

require('./third_party/leaflet.contextmenu')

window.Vue = require('vue')
window.getInterpretedModelRep = getInterpretedModelRep
window.getHazardTypeRep = getHazardTypeRep

// Since #app_continer doesn't exist on all pages,
// we have to check that it exists.
if (document.getElementById('survey_mosaic_map') != null) {
  const Viewer = Vue.extend(SurveyMosaicMap)
  // let Viewer = Vue.extend(StatisticsBySurveys);
  new Viewer({
    el: '#survey_mosaic_map',
    propsData: { zone: 11 }
  })
}

if (document.getElementById('surv_stat') != null) {
  const Viewer = Vue.extend(StatisticsBySurveys)
  new Viewer({
    el: '#surv_stat'
  })
}

if (document.getElementById('mosaic_stat') != null) {
  const Viewer = Vue.extend(StatisticsByMosaics)
  new Viewer({
    el: '#mosaic_stat'
  })
}

if (document.getElementById('sssmbes_batch_importer') != null) {
  const Viewer = Vue.extend(BatchImporter)
  new Viewer({
    el: '#sssmbes_batch_importer',
    propsData: { reqUrl: '/api/import-sssmbes' }
  })
}

if (document.getElementById('seismic_batch_importer') != null) {
  const Viewer = Vue.extend(BatchImporter)
  new Viewer({
    el: '#seismic_batch_importer',
    propsData: { reqUrl: '/api/import-seismic' }
  })
}

if (document.getElementById('model_batch_importer') != null) {
  const Viewer = Vue.extend(BatchModelImporter)
  // let Viewer = Vue.extend(StatisticsBySurveys);
  new Viewer({
    el: '#model_batch_importer',
  })
}

Vue.use(VueRouter)

if (document.getElementById('seismic_stack_viewer') != null) {
  const Viewer = Vue.extend(StackViewer)
  const router = new VueRouter({
    routes: [
      {
        path: '/:stackId',
        component: Viewer,
        name: 'stack',
        props: (route) => { return { seismic2dStackId: Number.parseInt(route.params.stackId) } }
      }
    ]
  })

  const app = new Vue({
    router
  }).$mount('#seismic_stack_viewer')
}

if (document.getElementById('interpreted_model_viewer') != null) {
  const Viewer = Vue.extend(InterpretedModelViewer)
  const router = new VueRouter({
    routes: [
      {
        path: '/:modelId',
        component: Viewer,
        name: 'model',
        props: (route) => { return { modelId: Number.parseInt(route.params.modelId) } }
      }
    ]
  })

  const app = new Vue({
    router
  }).$mount('#interpreted_model_viewer')
}

if (document.getElementById('entity_map') != null) {
  const Map = Vue.extend(MapView)

  const router = new VueRouter({
    routes: [
      { path: '/:zoneId', component: Map, name: 'zone', props: (route) => { return { zoneId: Number.parseInt(route.params.zoneId) } } },
      { path: '/', component: Map, props: (route) => { return { zoneId: 0 } } }
    ]
  })

  const app = new Vue({
    router
  }).$mount('#entity_map')
}
