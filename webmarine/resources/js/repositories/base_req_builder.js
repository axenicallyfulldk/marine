export function filterToParameter (filters, filterName) {
  if (filterName in filters && filters[filterName] != null) {
    return filterName + '=' + filters[filterName]
  }
  return null
}

export function filterToParameters (filters) {
  const params = []
  for (const param in filters) {
    if (filters[param] != null) params.push(filterToParameter(filters, param))
  }
  return params
}

export class BaseReqBuilder {
  constructor (baseUrl) {
    this.baseUrl = baseUrl
  }

  getById (objId) {
    return this.baseUrl + '/' + objId
  }

  getList (filters) {
    let req = this.baseUrl
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }
}
