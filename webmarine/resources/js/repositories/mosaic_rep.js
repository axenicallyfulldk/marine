import { BaseRep } from './base_rep'
import { BaseReqBuilder, filterToParameters } from './base_req_builder'
import axios from 'axios'

class MosaicReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getUniques (filters) {
    let req = '/api/mosaic-unique-properties'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }

  getFileDownloadLink (id) {
    return `${this.baseUrl}/${id}/file`
  }

  getArchDownloadLink (id) {
    return `${this.baseUrl}/${id}/arch`
  }
}

// Implements repository for working with mosaics
class MosaicRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }

  async getUniques (filters) {
    const req = mosaicReqBuilder.getUniques(filters)
    const response = await axios.get(req)
    return response.data
  }
}

const mosaicReqBuilder = new MosaicReqBuilder('/api/mosaics')
export function getMosaicReqBuilder () {
  return mosaicReqBuilder
}

// Returns repository
let mosaicRep = null
export function getMosaicRep () {
  if (mosaicRep == null) mosaicRep = new MosaicRep(getMosaicReqBuilder())

  return mosaicRep
}
