import { filterToParameters } from './base_req_builder'
import axios from 'axios'

class StatReqBuilder {
  constructor (baseUrl) {
    this.baseUrl = baseUrl
  }

  getSssMbesSurveysByYear (filters) {
    let req = this.baseUrl + '/sssmbes-surveys/years'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }

  getSssMbesSurveysByType (filters) {
    let req = this.baseUrl + '/sssmbes-surveys/types'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }

  getSssMbesSurveysByTools (filters) {
    let req = this.baseUrl + '/sssmbes-surveys/tools'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }

  getSssMbesSurveysByDirection (filters) {
    let req = this.baseUrl + '/sssmbes-surveys/directions'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }

  getMosaicsByYear (filters) {
    let req = this.baseUrl + '/mosaics/years'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }
}

// Implements repository for getting statistics
class StatRep {
  constructor (reqBuilder) {
    this.reqBuilder = reqBuilder
  }

  async getSssMbesSurveysByYear (filters) {
    const req = this.reqBuilder.getSssMbesSurveysByYear(filters)
    const response = await axios.get(req)
    return response.data
  }

  async getSssMbesSurveysByType (filters) {
    const req = this.reqBuilder.getSssMbesSurveysByType(filters)
    const response = await axios.get(req)
    return response.data
  }

  async getSssMbesSurveysByTools (filters) {
    const req = this.reqBuilder.getSssMbesSurveysByTools(filters)
    const response = await axios.get(req)
    return response.data
  }

  async getSssMbesSurveysByDirection (filters) {
    const req = this.reqBuilder.getSssMbesSurveysByDirection(filters)
    const response = await axios.get(req)
    return response.data
  }

  async getMosaicsByYear (filters) {
    const req = this.reqBuilder.getMosaicsByYear(filters)
    const response = await axios.get(req)
    return response.data
  }
}

const statReqBuilder = new StatReqBuilder('/api/statistics')

let statRep = null
export function getStatRep () {
  if (statRep == null) statRep = new StatRep(statReqBuilder)

  return statRep
}
