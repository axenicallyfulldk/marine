import { BaseRep } from './base_rep'
import { BaseReqBuilder } from './base_req_builder'

class Seismic3dStackReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getFileDownloadLink (id) {
    return `${this.baseUrl}/${id}/file`
  }
}

class Seismic3dStackRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }
}

const seismic3dStackReqBuilder = new Seismic3dStackReqBuilder('/api/seismic3d-stacks')
export function getSeismic3dStackReqBuilder () {
  return seismic3dStackReqBuilder
}

let seismic3dStackRep = null
export function getSeismic3dStackRep () {
  if (seismic3dStackRep == null) seismic3dStackRep = new Seismic3dStackRep(getSeismic3dStackReqBuilder())

  return seismic3dStackRep
}
