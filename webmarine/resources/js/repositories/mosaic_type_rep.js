import { BaseReqBuilder } from './base_req_builder'
import { BaseSmallSyncedRep } from './base_rep'

class MosaicTypeRep extends BaseSmallSyncedRep {
  constructor (reqBuilder) {
    super(reqBuilder)

    this.nameObjMap = new Map()
    this.keyObjMap = new Map()

    this._readyResolveMosaicTypeRep = null
    this._readyRejectMosaicTypeRep = null
    this._readyPromiseMosaicTypeRep = new Promise((resolve, reject) => {
      this._readyResolveMosaicTypeRep = resolve
      this._readyRejectMosaicTypeRep = reject
    })

    super.getReadyPromise().then(() => {
      for (const obj of this.idObjMap.values()) {
        this.nameObjMap.set(obj.name, obj)
        this.keyObjMap.set(obj.key, obj)
      }
      this._readyResolveMosaicTypeRep(true)
    }).catch(err => this._readyRejectMosaicTypeRep(err))
  }

  getReadyPromise () {
    return this._readyPromiseMosaicTypeRep
  }

  getByName (name) {
    return this.nameObjMap.get(name)
  }

  getByKey (key) {
    return this.keyObjMap.get(key)
  }
}

let typeRep = null
export function getMosaicTypeRep () {
  if (typeRep != null) return typeRep
  typeRep = new MosaicTypeRep(new BaseReqBuilder('/api/mosaic-types'))
  return typeRep
}
