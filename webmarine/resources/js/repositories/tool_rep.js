import { BaseReqBuilder } from './base_req_builder'
import { BaseSmallSyncedRep } from './base_rep'

class ToolRep extends BaseSmallSyncedRep {
  constructor (reqBuilder) {
    super(reqBuilder)

    this.nameObjMap = new Map()

    this._readyResolveToolRep = null
    this._readyRejectToolRep = null
    this._readyPromiseToolRep = new Promise((resolve, reject) => {
      this._readyResolveToolRep = resolve
      this._readyRejectToolRep = reject
    })

    super.getReadyPromise().then(() => {
      for (const obj of this.idObjMap.values()) {
        this.nameObjMap.set(obj.name, obj)
      }
      this._readyResolveToolRep(true)
    }).catch(err => this._readyRejectToolRep(err))
  }

  getReadyPromise () {
    return this._readyPromiseToolRep
  }

  getByName (name) {
    return this.nameObjMap.get(name)
  }
}

let toolRep = null
export function getSssMbesToolRep () {
  if (toolRep != null) return toolRep
  toolRep = new ToolRep(new BaseReqBuilder('/api/tools'))
  return toolRep
}
