import axios from 'axios'

export class BaseRep {
  constructor (reqBuilder) {
    this.reqBuilder = reqBuilder
  }

  async getById (objectId) {
    const req = this.reqBuilder.getById(objectId)
    const response = await axios.get(req)
    return response.data
  }

  async getList (filters) {
    const req = this.reqBuilder.getList(filters)
    const response = await axios.get(req)
    return response.data
  }
}

export class BaseSmallSyncedRep {
  constructor (reqBuilder) {
    this.reqBuilder = reqBuilder
    this.idObjMap = new Map()

    this._readyResolve = null
    this._readyReject = null
    this._readyPromise = new Promise((resolve, reject) => {
      this._readyResolve = resolve
      this._readyReject = reject
    })

    axios.get(this.reqBuilder.getList()).then(data => {
      for (const obj of data.data) {
        this.idObjMap.set(obj.id, obj)
      }
      this._readyResolve(true)
    }).catch(err => this._readyReject(err))
  }

  getReadyPromise () {
    return this._readyPromise
  }

  getById (objId) {
    return this.idObjMap.get(objId)
  }

  getList () {
    return Array.from(this.idObjMap.values())
  }

  getIds () {
    return Array.from(this.idObjMap.keys())
  }
}
