import { BaseReqBuilder } from './base_req_builder'
import { BaseSmallSyncedRep } from './base_rep'

class HazardTypeRep extends BaseSmallSyncedRep {
  constructor (reqBuilder) {
    super(reqBuilder)

    this.keyObjMap = new Map()

    this._readyResolveHazardTypeRep = null
    this._readyRejectHazardTypeRep = null
    this._readyPromiseHazardTypeRep = new Promise((resolve, reject) => {
      this._readyResolveHazardTypeRep = resolve
      this._readyRejectHazardTypeRep = reject
    })

    super.getReadyPromise().then(() => {
      for (const hazard of this.idObjMap.values()) {
        this.keyObjMap.set(hazard.key, hazard)
      }
      this._readyResolveHazardTypeRep(true)
    }).catch(err => this._readyRejectHazardTypeRep(err))
  }

  getReadyPromise () {
    return this._readyPromiseHazardTypeRep
  }

  getByKey (objKey) {
    return this.keyObjMap.get(objKey)
  }
}

let hazardTypeRep = null
export function getHazardTypeRep () {
  if (hazardTypeRep != null) return hazardTypeRep
  hazardTypeRep = new HazardTypeRep(new BaseReqBuilder('/api/hazard-types'))
  return hazardTypeRep
}
