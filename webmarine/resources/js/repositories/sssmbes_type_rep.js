import { BaseReqBuilder } from './base_req_builder'
import { BaseSmallSyncedRep } from './base_rep'

class SssMbesTypeRep extends BaseSmallSyncedRep {
  constructor (reqBuilder) {
    super(reqBuilder)

    this.nameObjMap = new Map()
    this.keyObjMap = new Map()

    this._readyResolveSssMbesTypeRep = null
    this._readyRejectSssMbesTypeRep = null
    this._readyPromiseSssMbesTypeRep = new Promise((resolve, reject) => {
      this._readyResolveSssMbesTypeRep = resolve
      this._readyRejectSssMbesTypeRep = reject
    })

    super.getReadyPromise().then(() => {
      for (const obj of this.idObjMap.values()) {
        this.nameObjMap.set(obj.name, obj)
        this.keyObjMap.set(obj.key, obj)
      }
      this._readyResolveSssMbesTypeRep(true)
    }).catch(err => this._readyRejectSssMbesTypeRep(err))
  }

  getReadyPromise () {
    return this._readyPromiseSssMbesTypeRep
  }

  getByName (name) {
    return this.nameObjMap.get(name)
  }

  getByKey (key) {
    return this.keyObjMap.get(key)
  }
}

let typeRep = null
export function getSssMbesTypeRep () {
  if (typeRep != null) return typeRep
  typeRep = new SssMbesTypeRep(new BaseReqBuilder('/api/survey-types'))
  return typeRep
}
