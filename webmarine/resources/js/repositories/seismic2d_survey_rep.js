import { BaseRep } from './base_rep'
import { BaseReqBuilder } from './base_req_builder'

class Seismic2dSurveyReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getArchDownloadLink (id) {
    return `${this.baseUrl}/${id}/arch`
  }
}

class Seismic2dSurveyRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }
}

const seismic2dSurveyReqBuilder = new Seismic2dSurveyReqBuilder('/api/seismic2d-surveys')

export function getSeismic2dSurveyReqBuilder () {
  return seismic2dSurveyReqBuilder
}

let seismic2dSurveyRep = null
export function getSeismic2dSurveyRep () {
  if (seismic2dSurveyRep == null) {
    seismic2dSurveyRep = new Seismic2dSurveyRep(getSeismic2dSurveyReqBuilder())
  }

  return seismic2dSurveyRep
}
