import { BaseRep } from './base_rep'
import { BaseReqBuilder } from './base_req_builder'

class InterpretedModelReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getBackgroundDownloadLink (id) {
    return `${this.baseUrl}/${id}/background`
  }
}

class InterpretedModelRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }
}

let reqBuilder = null
export function getInterpretedModelReqBuilder () {
  if (reqBuilder == null) reqBuilder = new InterpretedModelReqBuilder('/api/interpreted-models')

  return reqBuilder
}

let modelRep = null
export function getInterpretedModelRep () {
  if (modelRep != null) return modelRep

  modelRep = new InterpretedModelRep(getInterpretedModelReqBuilder())
  return modelRep
}
