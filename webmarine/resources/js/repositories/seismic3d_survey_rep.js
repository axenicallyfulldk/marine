import { BaseRep } from './base_rep'
import { BaseReqBuilder } from './base_req_builder'

class Seismic3dSurveyReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getArchDownloadLink (id) {
    return `${this.baseUrl}/${id}/arch`
  }
}

class Seismic3dSurveyRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }
}

const seismic3dSurveyReqBuilder = new Seismic3dSurveyReqBuilder('/api/seismic3d-surveys')

export function getSeismic3dSurveyReqBuilder () {
  return seismic3dSurveyReqBuilder
}

let seismic3dSurveyRep = null
export function getSeismic3dSurveyRep () {
  if (seismic3dSurveyRep == null) seismic3dSurveyRep = new Seismic3dSurveyRep(getSeismic3dSurveyReqBuilder())

  return seismic3dSurveyRep
}
