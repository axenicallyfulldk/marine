import LRUCache from 'lru-cache'

import { BaseRep } from './base_rep'
import { BaseReqBuilder } from './base_req_builder'

import axios from 'axios'

class ZoneReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getRoot () {
    return this.baseUrl + '/root'
  }
}

class ZoneRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }

  async getRoot () {
    const req = zoneReqBuilder.getRoot()
    const response = await axios.get(req)
    return response.data
  }
}

// Caching wrap for zone repository
class CachedZoneRep extends ZoneRep {
  constructor (reqBuilder, cacheSize = 50) {
    super(reqBuilder)
    this.zoneCache = new LRUCache(cacheSize)
  }

  async getById (zoneId) {
    let zone = this.zoneCache.get(zoneId)
    if (zone === undefined) {
      zone = super.getById(zoneId)
      this.zoneCache.set(zoneId, zone)
      return zone
    }
    return zone
  }
}

const zoneReqBuilder = new ZoneReqBuilder('/api/zones')
export function getZoneReqBuilder () {
  return zoneReqBuilder
}

let zoneRep = null
export function getZoneRep () {
  if (zoneRep != null) return zoneRep

  zoneRep = new CachedZoneRep(getZoneReqBuilder())
  return zoneRep
}
