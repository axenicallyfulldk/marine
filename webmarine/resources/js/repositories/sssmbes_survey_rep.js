import { BaseRep } from './base_rep'
import { BaseReqBuilder, filterToParameters } from './base_req_builder'
import axios from 'axios'

class SssMbesSurveyReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getUniques (filters) {
    let req = '/api/sssmbes-survey-unique-properties'
    if (filters != null) {
      const params = filterToParameters(filters)
      if (params.length) req += '?' + params.join('&')
    }
    return req
  }

  getFileDownloadLink (id) {
    return `${this.baseUrl}/${id}/file`
  }

  getArchDownloadLink (id) {
    return `${this.baseUrl}/${id}/arch`
  }
}

class SssMbesSurveyRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }

  async getUniques (filters) {
    const req = this.reqBuilder.getUniques(filters)
    const response = await axios.get(req)
    return response.data
  }
}

const sssMbesSurveyReqBuilder = new SssMbesSurveyReqBuilder('/api/sssmbes-surveys')
export function getSssMbesSurveyReqBuilder () {
  return sssMbesSurveyReqBuilder
}

let sssMbesSurveyRep = null
export function getSssMbesSurveyRep () {
  if (sssMbesSurveyRep == null) sssMbesSurveyRep = new SssMbesSurveyRep(getSssMbesSurveyReqBuilder())

  return sssMbesSurveyRep
}
