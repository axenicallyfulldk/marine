import { BaseRep } from './base_rep'
import { BaseReqBuilder } from './base_req_builder'

class Seismic2dStackReqBuilder extends BaseReqBuilder {
//   constructor (baseUrl) {
//     super(baseUrl)
//   }

  getFileDownloadLink (id) {
    return `${this.baseUrl}/${id}/file`
  }
}

class Seismic2dStackRep extends BaseRep {
//   constructor (reqBuilder) {
//     super(reqBuilder)
//   }
}

const seismic2dStackReqBuilder = new Seismic2dStackReqBuilder('/api/seismic2d-stacks')
export function getSeismic2dStackReqBuilder () {
  return seismic2dStackReqBuilder
}

let seismic2dStackRep = null
export function getSeismic2dStackRep () {
  if (seismic2dStackRep == null) seismic2dStackRep = new Seismic2dStackRep(getSeismic2dStackReqBuilder())

  return seismic2dStackRep
}
