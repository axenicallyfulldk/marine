const Colors = [
  '#AA0DFE',
  '#3283FE',
  '#85660D',
  '#782AB6',
  '#565656',
  '#1C8356',
  '#16FF32',
  '#F7E1A0',
  '#E2E2E2',
  '#1CBE4F',
  '#C4451C',
  '#DEA0FD',
  '#FE00FA',
  '#325A9B',
  '#FEAF16',
  '#F8A19F',
  '#90AD1C',
  '#F6222E',
  '#1CFFCE',
  '#2ED9FF',
  '#B10DA1',
  '#C075A6',
  '#FC1CBF',
  '#B00068',
  '#FBE426',
  '#FA0087'
]

// Class that associate colors with given ids
class ColorMap {
  constructor (ids) {
    this.map = new Map()
    ids.sort()
    for (let i = 0; i < ids.length; ++i) {
      this.map.set(ids[i], Colors[i % Colors.length])
    }
  }

  get (id) {
    return this.map.get(id)
  }
}

// Returns color map that associated colors with given ids
export function createColorMap (ids) {
  return new ColorMap(ids)
}

class SeismicResolutionColorMap {
  constructor () {
    this.colorMap = {
      1: '#07D8F0',
      2: '#2AB3E4',
      3: '#4E8DD8',
      4: '#7168CB',
      5: '#9442BF',
      6: '#FF0AEC'
    }
  }

  getColorById (id) {
    return this.colorMap[id]
  }
}

export function getSeismicResolutionColorMap () {
  return new SeismicResolutionColorMap()
}

export const EntityType = Object.freeze({
  unknown: 0,
  zone: 1,
  sssSurvey: 2,
  mbesSurvey: 3,
  mosaic: 4,
  seismic2dSurvey: 5,
  seismic3dSurvey: 6,
  interpretedModel: 7
})

function doesObjHaveProperty (obj, propertyName) {
  return Object.prototype.hasOwnProperty.call(obj, propertyName)
}

export function getEntityType (entity) {
//   if (entity.hasOwnProperty('streamer')) {
  if (doesObjHaveProperty(entity, 'streamer')) {
    if (doesObjHaveProperty(entity, 'distBetweenStreamers')) {
      return EntityType.seismic3dSurvey
    } else {
      return EntityType.seismic2dSurvey
    }
  } else if (doesObjHaveProperty(entity, 'type')) {
    if (doesObjHaveProperty(entity, 'mainTool')) return EntityType.mosaic

    if (entity.type === 'sss') {
      return EntityType.sssSurvey
    } else {
      return EntityType.mbesSurvey
    }
  } else if (doesObjHaveProperty(entity, 'code')) {
    return EntityType.zone
  } else if (doesObjHaveProperty(entity, 'hasBackground')) {
    return EntityType.interpretedModel
  }

  return EntityType.unknown
}
