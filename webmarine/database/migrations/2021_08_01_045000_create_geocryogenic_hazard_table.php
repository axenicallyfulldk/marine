<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateGeocryogenicHazardTable extends Migration
{
    public static function getHazardGroupTableName(){
        return \App\Tables::GEOCRYOGENIC_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardTableName(){
        return \App\Tables::GEOCRYOGENIC_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hgtn = static::getHazardGroupTableName();
        $htn = static::getHazardTableName();
        $httn = static::getHazardTypeTableName();
        $ztn = static::getZoneTableName();
        Schema::create($hgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create($htn, function (Blueprint $table) use($hgtn, $httn, $ztn) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($hgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($hgtn);
            $table->foreignId(FK::get($ztn))->nullable()->references('id')->comment('Идентификатор зоны')->on($ztn);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');

            $table->float('permafrost_rocks_thickness')->nullable()->comment('Мощность ММП (мерзлотные)');
            $table->float('active_layer_thickness')->nullable()->comment('Мощность деятельного слоя (мерзлотные)');
            $table->float('temperature')->nullable()->comment('Температура (мерзлотные, постмерзлотные)');
            $table->string('ice_content', 200)->nullable()->comment('Структура льда (мерзлотные)');
            $table->float('homogeneity')->nullable()->comment('Степень однородности (мерзлотные)');
            $table->float('intencity')->nullable()->comment('Интенсивность оттаивания, интенсивность выделения (мерзлотные)');
            $table->float('area')->nullable()->comment('Площадь распространения (мерзлотные)');
            $table->float('thickness')->nullable()->comment('Мощность (постмерзлотные)');
            $table->float('gas_saturation')->nullable()->comment('Газонасыщеннсть (постмерзлотные)');
            $table->float('concentration')->nullable()->comment('Концентрация (постмерзлотные)');
            $table->string('composition', 200)->nullable()->comment('Состав (постмерзлотные)');
            $table->float('volume')->nullable()->comment('Объём образования (постмерзлотные)');
            $table->float('depth')->nullable()->comment('Глубина образования (постмерзлотные)');

            // $table->float('')->nullable()->comment('');
            // $table->smallInteger('')->nullable()->comment('');

            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $htn = static::getHazardTableName();
        $hgtn = static::getHazardGroupTableName();
        Schema::dropIfExists($htn);
        Schema::dropIfExists($hgtn);
    }
}
