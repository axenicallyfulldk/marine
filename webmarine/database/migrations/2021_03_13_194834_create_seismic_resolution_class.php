<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeismicResolutionClass extends Migration
{

    private static function getSeismicResolutionClassTableName(){
        return \App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE;
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seismicResolutionTypeTableName = static::getSeismicResolutionClassTableName();

        Schema::create($seismicResolutionTypeTableName, function(Blueprint $table){
            $table->id();
            $table->string('name', 100);
            $table->string('key', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $seismicResolutionTypeTableName = static::getSeismicResolutionClassTableName();

        Schema::dropIfExists($seismicResolutionTypeTableName);
    }
}
