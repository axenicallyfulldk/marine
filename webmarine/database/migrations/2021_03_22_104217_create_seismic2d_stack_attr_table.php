<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismic2dStackAttrTable extends Migration
{

    private static function getSeismic2dStackAttrTableName(){
        return \App\Tables::SEISMIC2D_STACK_ATTR_TABLE;
    }

    private static function getSeismic2dStackTableName(){
        return \App\Tables::SEISMIC2D_STACK_TABLE;
    }

    private static function getSeismicStackAttrTypeTableName(){
        return \App\Tables::SEISMIC_STACK_ATTR_TYPE_TABLE;
    }

    private static function getHorizonTableName(){
        return \App\Tables::HORIZON_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seismic2dStackAttrTableName = static::getSeismic2dStackAttrTableName();
        $seismicStackAttrTypeTableName = static::getSeismicStackAttrTypeTableName();
        $seismic2dStackTableName = static::getSeismic2dStackTableName();
        $horizonTableName = static::getHorizonTableName();

        Schema::create($seismic2dStackAttrTableName, function (Blueprint $table)
                use($seismic2dStackTableName, $seismicStackAttrTypeTableName, $horizonTableName) {
            $table->id();
            $table->foreignId(FK::get($seismic2dStackTableName))->references('id')->on($seismic2dStackTableName);
            $table->foreignId(FK::get($seismicStackAttrTypeTableName))->references('id')->on($seismicStackAttrTypeTableName);
            
            $table->foreignId('horizon_1')->references('id')->on($horizonTableName);
            $table->foreignId('horizon_2')->references('id')->on($horizonTableName);

            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();
            $table->string('comment', 1024);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $seismic2dStackAttrTableName = static::getSeismic2dStackAttrTableName();
        Schema::dropIfExists($seismic2dStackAttrTableName);
    }
}
