<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVesselTable extends Migration
{

    private function getVesselTableName(){
        return \App\Tables::VESSEL_TABLE;
    }

    private function getUserTableName(){
        return \App\Tables::USER_TABLE;
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $vesselTableName = $this->getVesselTableName();
        $userTableName = $this->getUserTableName();
        Schema::create($vesselTableName, function (Blueprint $table) use($userTableName) {
            $table->id();
            $table->string('name', 128);
            $table->string('ais_request', 128)->nullable();
            $table->string('home_port', 128)->nullable();
            $table->string('shipowner', 128)->nullable();
            $table->string('imo', 16)->nullable();
            $table->string('mmsi', 16)->nullable();
            $table->string('callsign', 32)->nullable();
            $table->double('length')->nullable();
            $table->double('width')->nullable();
            $table->double('drought')->nullable();
            $table->double('displacement')->nullable();
            $table->double('speed_kn')->nullable();
            $table->date('build_date')->nullable();

            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);
            
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->getVesselTableName());
    }
}
