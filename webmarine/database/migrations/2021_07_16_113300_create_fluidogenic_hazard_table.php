<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;


class CreateFluidogenicHazardTable extends Migration
{

    static public function getFluidogenicHazardGroupTableName(){
        return \App\Tables::FLUIDOGENIC_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    static public function getFluidogenicHazardTableName(){
        return \App\Tables::FLUIDOGENIC_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fhtn = static::getFluidogenicHazardTableName();
        $fhgtn = static::getFluidogenicHazardGroupTableName();
        $httn = static::getHazardTypeTableName();
        $zoneTableName = static::getZoneTableName();
        Schema::create($fhtn, function (Blueprint $table) use($fhgtn, $httn, $zoneTableName) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($fhgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($fhgtn);
            $table->foreignId(FK::get($zoneTableName))->nullable()->references('id')->comment('Идентификатор зоны')->on($zoneTableName);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');
            $table->float('concentration')->nullable()->comment('Концентрация');
            $table->float('area')->nullable()->comment('Площадь скоплений, размер зоны (АВПД), размер области (газопроявления)');
            $table->float('volume')->nullable()->comment('Объём скоплений');
            $table->string('composition')->nullable()->comment('Состав флюидов');
            $table->float('depth')->nullable()->comment('Глубина залегани источника');
            $table->float('thickness')->nullable()->comment('Мощность ');
            $table->float('pressure')->nullable()->comment('Давление');
            $table->float('temperature')->nullable()->comment('Температура');
            $table->float('abnormality_coefficient')->nullable()->comment('Коэффициент аномальности');
            $table->boolean('regularity')->nullable()->comment('Непрырвность');
            $table->float('column_height')->nullable()->comment('Высота столба');
            $table->enum('floor_division', \App\Models\FluidogenicHazard::FLOOR_DIVISION)->nullable()->comment('Категорий крупных зон профиля океанического дна');
            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $fhtn = static::getFluidogenicHazardTableName();
        Schema::dropIfExists($fhtn);
    }
}
