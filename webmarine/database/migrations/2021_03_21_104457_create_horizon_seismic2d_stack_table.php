<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHorizonSeismic2dStackTable extends Migration
{
    private static function getHorizonSeismic2dStackTableName(){
        return \App\Tables::SEISMIC2D_HORIZON_TABLE;
    }

    private static function getHorizonTableName(){
        return \App\Tables::HORIZON_TABLE;
    }

    private static function getSeismic2dStackTableName(){
        return \App\Tables::SEISMIC2D_STACK_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $horTableName = static::getHorizonTableName();
        $horSeismic2dStackTableName = static::getHorizonSeismic2dStackTableName();
        $seismic2dTableName = static::getSeismic2dStackTableName();

        Schema::create($horSeismic2dStackTableName, function (Blueprint $table)
                use($horTableName, $seismic2dTableName) {
            $table->id();
            $table->foreignId(FK::get($horTableName))->references('id')->on($horTableName);
            $table->foreignId(FK::get($seismic2dTableName))->references('id')->on($seismic2dTableName);
            $table->jsonb('coords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horSeismic2dStackTableName = static::getHorizonSeismic2dStackTableName();
        Schema::dropIfExists($horSeismic2dStackTableName);
    }
}
