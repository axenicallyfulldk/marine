<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaultRiskDegreeTable extends Migration
{
    protected static function getFaultRiskDegreeTableName(){
        return \App\Tables::FAULT_RISK_DEGREE_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faultRiskDegreeTableName = static::getFaultRiskDegreeTableName();
        Schema::create($faultRiskDegreeTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $faultRiskDegreeTableName = static::getFaultRiskDegreeTableName();
        Schema::dropIfExists($faultRiskDegreeTableName);
    }
}
