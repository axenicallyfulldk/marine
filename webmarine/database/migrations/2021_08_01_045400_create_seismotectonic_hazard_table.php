<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismotectonicHazardTable extends Migration
{
    public static function getHazardGroupTableName(){
        return \App\Tables::SEISMOTECTONIC_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardTableName(){
        return \App\Tables::SEISMOTECTONIC_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hgtn = static::getHazardGroupTableName();
        $htn = static::getHazardTableName();
        $httn = static::getHazardTypeTableName();
        $ztn = static::getZoneTableName();
        Schema::create($hgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create($htn, function (Blueprint $table) use($hgtn, $httn, $ztn) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($hgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($hgtn);
            $table->foreignId(FK::get($ztn))->nullable()->references('id')->comment('Идентификатор зоны')->on($ztn);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');

            $table->float('intensity')->nullable()->comment('Интенсивность');
            $table->float('dist_form_epicenter')->nullable()->comment('Удалённость от эпицентра');
            $table->float('affected_area')->nullable()->comment('Зона поражения');
            $table->float('abs_displacement_values')->nullable()->comment('Абсолютная величина перемещения');
            $table->float('propogation_speed')->nullable()->comment('Скорость распространения');
            $table->float('propogation_area')->nullable()->comment('Площадь распространения');

            // $table->float('')->nullable()->comment('');
            // $table->smallInteger('')->nullable()->comment('');
            // $table->string('', 400)->nullable()->comment('');

            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $htn = static::getHazardTableName();
        $hgtn = static::getHazardGroupTableName();
        Schema::dropIfExists($htn);
        Schema::dropIfExists($hgtn);
    }
}
