<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateGeomorphologicalHazardTable extends Migration
{
    public static function getHazardGroupTableName(){
        return \App\Tables::GEOMORPHOLOGICAL_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardTableName(){
        return \App\Tables::GEOMORPHOLOGICAL_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hgtn = static::getHazardGroupTableName();
        $htn = static::getHazardTableName();
        $httn = static::getHazardTypeTableName();
        $ztn = static::getZoneTableName();
        Schema::create($hgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create($htn, function (Blueprint $table) use($hgtn, $httn, $ztn) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($hgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($hgtn);
            $table->foreignId(FK::get($ztn))->nullable()->references('id')->comment('Идентификатор зоны')->on($ztn);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');
            $table->float('speed')->nullable()->comment('Скорость (литодинамические, гравитационные)');
            $table->float('area')->nullable()->comment('Площадь распространения (литодинамические, гравитационные)');
            $table->smallInteger('heterogenity')->nullable()->comment('Cтепень неоднородности (литодинамические, морфологические)');
            $table->float('slope')->nullable()->comment('Уклон (гравитационные, морфологические)');
            $table->float('volume')->nullable()->comment('Объём (литодинамические, гравитационные)');
            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $htn = static::getHazardTableName();
        $hgtn = static::getHazardGroupTableName();
        Schema::dropIfExists($htn);
        Schema::dropIfExists($hgtn);
    }
}
