<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismic2dStackTable extends Migration
{

    private static function getSurveyTableName(){
        return \App\Tables::SEISMIC2D_SURVEY_TABLE;
    }

    private static function getStackTableName(){
        return \App\Tables::SEISMIC2D_STACK_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $surveyTableName = static::getSurveyTableName();
        $stackTableName = static::getStackTableName();

        Schema::create($stackTableName, function (Blueprint $table) use($surveyTableName) {
            $table->id();

            $table->foreignId(FK::get($surveyTableName))->references('id')->on($surveyTableName);
            $table->string('name');
            $table->string('alias')->nullable();
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();

            $table->boolean('is_migrated')->nullable();
            $table->boolean('is_ampl_corrected')->nullable();
            $table->boolean('is_deconvolved')->nullable();

            $table->integer('cdp_offset')->nullable();
            $table->integer('cdp_size')->nullable();
            $table->integer('cdp_start')->nullable();
            $table->integer('cdp_step')->nullable();
            $table->integer('cdp_end')->nullable();
            $table->float('cdp_distance')->nullable();

            $table->float('minmax_norm')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $stackTableName = static::getStackTableName();
        Schema::dropIfExists($stackTableName);
    }
}
