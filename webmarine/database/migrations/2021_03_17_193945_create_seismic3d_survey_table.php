<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismic3dSurveyTable extends Migration
{
    private static function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    private static function getTripTableName(){
        return \App\Tables::TRIP_TABLE;
    }

    private static function getSurveyTableName(){
        return \App\Tables::SEISMIC3D_SURVEY_TABLE;
    }

    private static function getUserTableName(){
        return \App\Tables::USER_TABLE;
    }

    private static function getSurveyArchFileTableName(){
        return \App\Tables::SEISMIC3D_SURVEY_ARCHIVE_FILES_TABLE;
    }

    private static function getSourceTableName(){
        return \App\Tables::SEISMIC_SOURCE_TABLE;
    }

    private static function getStreamerTableName(){
        return \App\Tables::STREAMER_TABLE;
    }

    private static function getFieldTableName(){
        return \App\Tables::FIELD_TABLE;
    }

    private static function getRecieverTableName(){
        return \App\Tables::SEISMIC_RECEIVER_TABLE;
    }

    private static function getSeismicResolutionClassTableName(){
        return \App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $zoneTableName = static::getZoneTableName();
        $zoneTableId = FK::get($zoneTableName);

        $tripTableName = static::getTripTableName();
        $tripTableId = FK::get($tripTableName);

        $surveyArchFileTableName = static::getSurveyArchFileTableName();
        $surveyArchFileTableId = FK::get($surveyArchFileTableName);

        $seismicResolutionTypeTableName = static::getSeismicResolutionClassTableName();
        $seismicResolutionTypeTableId = FK::get($seismicResolutionTypeTableName);

        $surveyTableName = static::getSurveyTableName();
        $surveyTableId = FK::get($surveyTableName);

        $sourceTableName = static::getSourceTableName();
        $sourceTableId = FK::get($sourceTableName);

        $streamerTableName = static::getStreamerTableName();
        $streamerTableId = FK::Get($streamerTableName);

        $fieldTableName = static::getFieldTableName();
        $fieldTableId = FK::get($fieldTableName);

        $userTableName = static::getUserTableName();
        $userTableId = FK::get($userTableName);

        Schema::create($surveyTableName, function (Blueprint $table)
            use($zoneTableName, $zoneTableId,
                $tripTableName, $tripTableId,
                $sourceTableName, $sourceTableId,
                $streamerTableName, $streamerTableId,
                $fieldTableName, $fieldTableId,
                $seismicResolutionTypeTableName, $seismicResolutionTypeTableId,
                $userTableName) {

            $table->id();
            $table->uuid('unique_id')->default(DB::raw('uuid_generate_v4()'));
            $table->string('name');
            $table->string('alias')->nullable();
            $table->geometry('area');
            $table->enum('direction', \App\Models\Seismic3dSurvey::DIRECTION);
            $table->date('start_date');
            // $table->string('file_path', 200);
            // $table->bigInteger('file_size')->nullable();
            $table->string('arch_path', 200)->nullable();
            $table->bigInteger('arch_size')->nullable();
            $table->foreignId($zoneTableId)->references('id')->on($zoneTableName);
            $table->foreignId($fieldTableId)->nullable()->references('id')->on($fieldTableName);
            $table->foreignId($tripTableId)->references('id')->on($tripTableName);

            $table->foreignId($sourceTableId)->references('id')->on($sourceTableName);
            $table->smallInteger('src_group_count')->default(1);
            $table->smallInteger('src_per_group')->default(1);
            
            $table->foreignId($streamerTableId)->nullable()->references('id')->on($streamerTableName);
            $table->boolean('is_towed')->default(true);
            $table->float('streamer_count')->nullable();
            $table->float('dist_between_streamers')->nullable();

            $table->foreignId(FK::get($seismicResolutionTypeTableName))->nullable()->references('id')->on($seismicResolutionTypeTableName);    
            
            $table->string('customer', 100)->nullable();
            $table->string('operator', 100)->nullable();

            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);

            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            $table->unique([FK::get($zoneTableName), 'name']);
        });

        Schema::create($surveyArchFileTableName, function(Blueprint $table)
            use($surveyTableName, $surveyTableId){

            $table->id();
            $table->foreignId($surveyTableId)->references('id')->on($surveyTableName);
            $table->string('name', 100);
            $table->string('description', 200);

            $table->timestamps();
        });

        Schema::create($surveyTableName.'_permission', function(Blueprint $table) use($surveyTableName, $surveyTableId, $userTableName, $userTableId){
            $table->id();
            $table->foreignId($surveyTableId)->references('id')->on($surveyTableName);
            $table->foreignId($userTableId)->references('id')->on($userTableName);
            $table->smallInteger('can_see')->default(0);
            $table->smallInteger('can_download')->default(0);
            $table->smallInteger('can_edit')->default(0);

            $table->unique([$surveyTableId, $userTableId]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $surveyArchFileTableName = $this->getSurveyArchFileTableName();
        $surveyTableName = $this->getSurveyTableName();
        $seismicResolutionTypeTableName = static::getSeismicResolutionClassTableName();

        Schema::dropIfExists($surveyArchFileTableName);
        Schema::dropIfExists($surveyTableName.'_permission');
        Schema::dropIfExists($surveyTableName);
    }
}
