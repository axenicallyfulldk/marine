<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFluidogenicHazardGroupTable extends Migration
{

    static public function getFluidogenicHazardGroupTableName(){
        return \App\Tables::FLUIDOGENIC_HAZARD_GROUP_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $fhgtn = static::getFluidogenicHazardGroupTableName();
        Schema::create($fhgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $fhgtn = static::getFluidogenicHazardGroupTableName();
        Schema::dropIfExists($fhgtn);
    }
}
