<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorizonTypeTable extends Migration
{

    private static function getHorizonTypeTableName(){
        return \App\Tables::HORIZON_TYPE_TABLE;
    }
        
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //ММП - многолетние мёрзлые породы
        //Газовая труба
        //Газовый сип
        //Покмарк
        //
        //

        $horTypeTableName = static::getHorizonTypeTableName();

        Schema::create($horTypeTableName, function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horTypeTableName = static::getHorizonTypeTableName();
        Schema::dropIfExists($horTypeTableName);
    }
}
