<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHydrologicalHazardTable extends Migration
{
    public static function getHazardGroupTableName(){
        return \App\Tables::HYDROLOGICAL_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardTableName(){
        return \App\Tables::HYDROLOGICAL_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hgtn = static::getHazardGroupTableName();
        $htn = static::getHazardTableName();
        $httn = static::getHazardTypeTableName();
        $ztn = static::getZoneTableName();
        Schema::create($hgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create($htn, function (Blueprint $table) use($hgtn, $httn, $ztn) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($hgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($hgtn);
            $table->foreignId(FK::get($ztn))->nullable()->references('id')->comment('Идентификатор зоны')->on($ztn);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');

            $table->float('speed')->nullable()->comment('Скорость (волновые, приливные, цунами)');
            $table->smallInteger('repeatability')->nullable()->comment('Повторяемость (волновые, приливные, цунами)');

            $table->float('periodicity')->nullable()->comment('Периодичность (волновые, приливные)');
            $table->float('height')->nullable()->comment('Высота (волновы, приливные)');
            $table->float('temperature')->nullable()->comment('Температура (температурные)');
            $table->float('duration')->nullable()->comment('Продолжительность (температурные)');
            $table->float('area')->nullable()->comment('Площадь (цунами)');
            $table->float('wave_speed')->nullable()->comment('Скорость волны (цунами)');
            $table->float('max_vert_runup')->nullable()->comment('Максимальная величина заплеска (цунами)');
            $table->float('h100')->nullable()->comment('Максимум вертикального заплеска за 100 лет');
            $table->float('h50')->nullable()->comment('Максимум вертикального заплеска за 50 лет');
            $table->smallInteger('thread_degree')->nullable()->comment('Цунамеопасность (цунами)');
            $table->enum('generator', ['землетрясение', 'обвал', 'оползень', 'вулкан', 'землетрясени+обвал', 'землетрясение+оползень'])->nullable()->comment('Прицины (цунами)');
            $table->dateTime('spc_alarm_time')->nullable()->comment('СПЦ время (цунами)');
            $table->float('time_of_reaching')->nullable()->comment('Время добегания волны судна (цунами)');
            $table->float('time_to_safe_zone')->nullable()->comment('Время до безопасной зоны (цунами)');

            // $table->float('')->nullable()->comment('');
            // $table->smallInteger('')->nullable()->comment('');

            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $htn = static::getHazardTableName();
        $hgtn = static::getHazardGroupTableName();
        Schema::dropIfExists($htn);
        Schema::dropIfExists($hgtn);
    }
}
