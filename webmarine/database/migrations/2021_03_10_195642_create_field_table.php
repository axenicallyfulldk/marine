<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldTable extends Migration
{

    private static function getFieldTableName(){
        return \App\Tables::FIELD_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fieldTableName = static::getFieldTableName();
        
        Schema::create($fieldTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name', 200);
            $table->geometry('area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $fieldTableName = static::getFieldTableName();

        Schema::dropIfExists($fieldTableName);
    }
}
