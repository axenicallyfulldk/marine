<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeismicStreamerTable extends Migration
{

    private static function getStreamerTableName(){
        return \App\Tables::STREAMER_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $streamerTableName = static::getStreamerTableName();

        Schema::create($streamerTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->float('receiver_dist');
            $table->float('length');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $streamerTableName = static::getStreamerTableName();
        
        Schema::dropIfExists($streamerTableName);
    }
}
