<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaultGroupTypeTable extends Migration
{

    private static function getFaultGroupTypeTable(){
        return \App\Tables::FAULT_GROUP_TYPE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faultGroupTypeTableName = static::getFaultGroupTypeTable();

        Schema::create($faultGroupTypeTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $faultGroupTypeTableName = static::getFaultGroupTypeTable();
        Schema::dropIfExists($faultGroupTypeTableName);
    }
}
