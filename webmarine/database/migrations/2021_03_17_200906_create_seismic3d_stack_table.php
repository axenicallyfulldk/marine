<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismic3dStackTable extends Migration
{
    private static function getSurveyTableName(){
        return \App\Tables::SEISMIC3D_SURVEY_TABLE;
    }

    private static function getStackTableName(){
        return \App\Tables::SEISMIC3D_STACK_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $surveyTableName = static::getSurveyTableName();
        $stackTableName = static::getStackTableName();

        Schema::create($stackTableName, function (Blueprint $table) use($surveyTableName) {
            $table->id();

            $table->foreignId(FK::get($surveyTableName))->references('id')->on($surveyTableName);
            $table->string('name');
            $table->string('alias')->nullable();
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();

            $table->boolean('is_migrated')->nullable();
            $table->boolean('is_ampl_corrected')->nullable();
            $table->boolean('is_deconvolved')->nullable();

            // $table->integer('inline_start')->nullable();
            // $table->integer('inline_step')->nullable();
            // $table->integer('inline_end')->nullable();
            $table->integer('inline_offset')->nullable();
            $table->integer('inline_size')->nullable();
            $table->float('inline_distance')->nullable();

            $table->integer('xline_offset')->nullable();
            $table->integer('xline_size')->nullable();
            $table->float('xline_distance')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $stackTableName = static::getStackTableName();
        Schema::dropIfExists($stackTableName);
    }
}
