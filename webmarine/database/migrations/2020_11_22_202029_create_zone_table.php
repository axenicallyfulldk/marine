<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



class CreateZoneTable extends Migration
{
    private function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    private function getZoneTypeTableName(){
        return \App\Tables::ZONE_TABLE."_type";
    }

    private function getZonePermTableName(){
        return \App\Tables::ZONE_TABLE."_permission";
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $zoneTableName = $this->getZoneTableName();
        $zoneTableForeignKey = $zoneTableName.'_id';

        $zoneTypeTableName =  $this->getZoneTypeTableName();
        $zoneTypeTableForeignKey = $zoneTypeTableName.'_id';

        $zonePermTableName = $this->getZonePermTableName();

        $userTableName = \App\Tables::USER_TABLE;
        $userTableForeignKey = $userTableName.'_id';

        Schema::create($zoneTypeTableName, function(Blueprint $table){
            $table->id();
            $table->string('name');
        });

        Schema::create($zoneTableName, function (Blueprint $table) use($userTableName) {
            $table->id();
            $table->uuid('unique_id')->default(DB::raw('uuid_generate_v4()'));
            $table->string('name');
            $table->string('code', 10);
            $table->geometry('area');
            $table->morphs('zonable');
            $table->year('default_year')->nullable();
            
            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);
            
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });

        Schema::table($zoneTableName, function (Blueprint $table) use($zoneTypeTableName, $zoneTypeTableForeignKey, $zoneTableName) {
            $table->foreignId('parent_zone_id')->nullable()->references('id')->on($zoneTableName);
            // $table->foreignId($zoneTypeTableForeignKey)->references('id')->on($zoneTypeTableName);
        });

        Schema::create('zone_permission', function(Blueprint $table) use($zoneTableName, $zoneTableForeignKey, $userTableName, $userTableForeignKey){
            $table->foreignId($zoneTableForeignKey)->references('id')->on($zoneTableName);
            $table->foreignId($userTableForeignKey)->references('id')->on($userTableName);
            // $table->boolean('can_see')->default(false);
            $table->boolean('can_edit')->default(false);

            $table->primary(array($zoneTableForeignKey, $userTableForeignKey));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $zoneTableName = $this->getZoneTableName();
        $zoneTypeTableName =  $this->getZoneTypeTableName();
        $zonePermTableName = $this->getZonePermTableName();

        Schema::dropIfExists($zonePermTableName);
        Schema::dropIfExists($zoneTableName);
        Schema::dropIfExists($zoneTypeTableName);
    }
}
