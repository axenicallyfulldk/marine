<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHorizonSeismic3dStackAttrTable extends Migration
{

    private static function getHorizonSeismic3dStackAttrTableName(){
        return \App\Tables::HORIZON_SEISMIC3D_STACK_ATTR_TABLE;
    }

    private static function getHorizonSeismic3dStackTableName(){
        return \App\Tables::SEISMIC3D_HORIZON_TABLE;
    }

    private static function getHorizonAttrTypeTableName(){
        return \App\Tables::HORIZON_ATTRIBUTE_TYPE_TABLE;
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $horSeismic3dStackAttrTableName = $this->getHorizonSeismic3dStackAttrTableName();
        $horSeismic3dStackTableName = $this->getHorizonSeismic3dStackTableName();
        $horAttrTypeTableName = $this->getHorizonAttrTypeTableName();

        Schema::create($horSeismic3dStackAttrTableName, function (Blueprint $table) use($horSeismic3dStackTableName, $horAttrTypeTableName) {
            $table->id();
            $table->foreignId(FK::get($horSeismic3dStackTableName))->references('id')->on($horSeismic3dStackTableName);
            $table->foreignId(FK::get($horAttrTypeTableName))->references('id')->on($horAttrTypeTableName);
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horSeismic3dStackAttrTableName = $this->getHorizonSeismic3dStackAttrTableName();
        Schema::dropIfExists($horSeismic3dStackAttrTableName);
    }
}
