<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorizonAttrTypeTable extends Migration
{

    private static function getHorizonAttrTypeTable(){
        return \App\Tables::HORIZON_ATTRIBUTE_TYPE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $horAttrTypeTableName = static::getHorizonAttrTypeTable();

        Schema::create($horAttrTypeTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horAttrTypeTableName = static::getHorizonAttrTypeTable();
        Schema::dropIfExists($horAttrTypeTableName);
    }
}
