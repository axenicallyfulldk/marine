<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateGlacialHazardTable extends Migration
{

    static public function getGlacialHazardTableName(){
        return \App\Tables::GLACIAL_HAZARD_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    static public function getGlacialHazardGroupTableName(){
        return \App\Tables::GLACIAL_HAZARD_GROUP_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $glacialHazardTableName = static::getGlacialHazardTableName();
        $hazardTypeTableName = static::getHazardTypeTableName();
        $glacialHazardGroupTableName = static::getGlacialHazardGroupTableName();
        $zoneTableName = static::getZoneTableName();
        Schema::create($glacialHazardTableName, function (Blueprint $table) use($hazardTypeTableName, $glacialHazardGroupTableName, $zoneTableName) {
            $table->id();
            $table->foreignId(FK::get($hazardTypeTableName))->references('id')->comment('Идентификатор типа опасности')->on($hazardTypeTableName);
            $table->foreignId(FK::get($glacialHazardGroupTableName))->nullable()->references('id')->comment('Идентификатор группы')->on($glacialHazardGroupTableName);
            $table->foreignId(FK::get($zoneTableName))->nullable()->references('id')->comment('Идентификатор зоны')->on($zoneTableName);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');
            $table->float('depth')->comment('Глубина борозды')->nullable();
            $table->float('width')->comment('Ширина борозды')->nullable();
            $table->float('area')->comment('Площадь воздействия')->nullable();
            $table->float('intensity')->comment('Интенсивность воздействия')->nullable();
            $table->float('furrow_system_width')->comment('')->nullable();
            $table->float('side_width')->comment('Ширина бортика в системе борозд')->nullable();
            $table->float('orientation')->comment('Ориентировка борозды')->nullable();
            $table->float('volume')->comment('Объём переносимого материала (ледовый разнос)')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->string('comment', 1000)->comment('Комментарий')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $glacialHazardTableName = static::getGlacialHazardTableName();
        Schema::dropIfExists($glacialHazardTableName);
    }
}
