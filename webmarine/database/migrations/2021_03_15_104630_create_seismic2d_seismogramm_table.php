<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismic2dSeismogrammTable extends Migration
{

    private static function getSeismogramTableName(){
        return \App\Tables::SEISMIC2D_SEISMOGRAM_TABLE;
    }

    private static function getSurveyTableName(){
        return \App\Tables::SEISMIC2D_SURVEY_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seismogramTableName = static::getSeismogramTableName();
        $surveyTableName = static::getSurveyTableName();

        Schema::create($seismogramTableName, function (Blueprint $table) use($surveyTableName) {
            $table->id();

            $table->foreignId(FK::get($surveyTableName))->references('id')->on($surveyTableName);
            $table->string('name');
            $table->string('alias')->nullable();
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();

            $table->integer('sp_start');
            $table->integer('sp_step');
            $table->integer('sp_end');
            $table->float('sp_distance');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $seismogramTableName = static::getSeismogramTableName();
        Schema::dropIfExists($seismogramTableName);
    }
}
