<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripTable extends Migration
{

    private function getTripTableName(){
        return \App\Tables::TRIP_TABLE;
    }

    private function getVesselTableName(){
        return \App\Tables::VESSEL_TABLE;
    }

    private function getUserTableName(){
        return \App\Tables::USER_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tripTableName = $this->getTripTableName();
        $vesselTableName = $this->getVesselTableName();
        $userTableName = $this->getUserTableName();

        Schema::create($tripTableName, function (Blueprint $table) use($vesselTableName, $userTableName) {
            $table->id();
            $table->foreignId($vesselTableName.'_id')->references('id')->on($vesselTableName);
            $table->datetime('start')->nullable();
            // $table->integer('ios10')->nullable();
            // $table->double('swh10')->nullable();
            // $table->double('mean_pitch')->nullable();
            // $table->double('std_pitch')->nullable();
            // $table->double('mean_roll')->nullable();
            // $table->double('std_roll')->nullable();
            // $table->double('mean_heave')->nullable();
            // $table->double('std_heave')->nullable();
            // $table->double('cc_roll')->nullable();
            // $table->boolean('flcurs')->nullable();
            // $table->double('vc')->nullable();
            // $table->double('vuh_prog')->nullable();
            // $table->double('vuh_fact')->nullable();

            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);
            
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tripTableName = $this->getTripTableName();
        Schema::dropIfExists($tripTableName);
    }
}
