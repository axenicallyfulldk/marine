<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHorizonSeismic2dStackAttrTable extends Migration
{

    private static function getHorizonSeismic2dStackAttrTableName(){
        return \App\Tables::HORIZON_SEISMIC2D_STACK_ATTR_TABLE;
    }

    private static function getHorizonSeismic2dStackTableName(){
        return \App\Tables::SEISMIC2D_HORIZON_TABLE;
    }

    private static function getHorizonAttrTypeTableName(){
        return \App\Tables::HORIZON_ATTRIBUTE_TYPE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $horSeismic2dStackAttrTableName = $this->getHorizonSeismic2dStackAttrTableName();
        $horSeismic2dStackTableName = $this->getHorizonSeismic2dStackTableName();
        $horAttrTypeTableName = $this->getHorizonAttrTypeTableName();

        Schema::create($horSeismic2dStackAttrTableName, function (Blueprint $table) use($horSeismic2dStackTableName, $horAttrTypeTableName) {
            $table->id();
            $table->foreignId(FK::get($horSeismic2dStackTableName))->references('id')->on($horSeismic2dStackTableName);
            $table->foreignId(FK::get($horAttrTypeTableName))->references('id')->on($horAttrTypeTableName);
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horSeismic2dStackAttrTableName = $this->getHorizonSeismic2dStackAttrTableName();
        Schema::dropIfExists($horSeismic2dStackAttrTableName);
    }
}
