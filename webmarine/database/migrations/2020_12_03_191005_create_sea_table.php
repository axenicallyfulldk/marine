<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeaTable extends Migration
{
    private function getSeaTableName(){
        return \App\Tables::SEA_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seaTableName = $this->getSeaTableName();
        Schema::create($seaTableName, function (Blueprint $table) {
            $table->id();
            $table->string('location_type')->nullable();
            $table->string('morphological_type')->nullable();
            $table->string('tectonic_conidtions')->nullable();
            $table->string('climate')->nullable();
            $table->string('shelf_type')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $seaTableName = $this->getSeaTableName();
        Schema::dropIfExists($seaTableName);
    }
}
