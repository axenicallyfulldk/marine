<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosaicTable extends Migration
{

    private function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    private function getMosaicTypeTableName(){
        return \App\Tables::MOSAIC_TYPE_TABLE;
    }

    private function getMosaicTableName(){
        return \App\Tables::MOSAIC_TABLE;
    }

    private function getUserTableName(){
        return \App\Tables::USER_TABLE;
    }

    private function getMosaicArchFileTableName(){
        return \App\Tables::MOSAIC_ARCHIVE_FILES_TABLE;
    }

    private function getToolTableName(){
        return \App\Tables::TOOL_TABLE;
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $zoneTableName = $this->getZoneTableName();
        $zoneTableId = $zoneTableName.'_id';

        $mosaicArchFileTableName = $this->getMosaicArchFileTableName();
        $mosaicArchFileTableId = $mosaicArchFileTableName.'_id';

        $mosaicTypeTableName = $this->getMosaicTypeTableName();
        $mosaicTypeTableId = $mosaicTypeTableName.'_id';

        $mosaicTableName = $this->getMosaicTableName();
        $mosaicTableId = $mosaicTableName.'_id';

        $userTableName = $this->getUserTableName();
        $userTableId = $userTableName.'_id';

        $toolTableName = $this->getToolTableName();

        Schema::create($mosaicTypeTableName, function(Blueprint $table){
            $table->id();
            $table->string('name', 40);
            $table->string('key', 10)->unique();
            $table->bigInteger('map_color')->nullable();
            $table->timestamps();
        });

        Schema::create($mosaicTableName, function (Blueprint $table)
            use($zoneTableName, $zoneTableId, $mosaicTypeTableName, $mosaicTypeTableId, $userTableName, $toolTableName) {
            $table->id();
            $table->uuid('unique_id')->default(DB::raw('uuid_generate_v4()'));
            $table->string('name'); 
            $table->geometry('area');
            $table->enum('input_direction', \App\Models\Mosaic::INPUT_DIRECTION);
            $table->foreignId($mosaicTypeTableId)->references('id')->on($mosaicTypeTableName);
            $table->foreignId('main_tool_id')->references('id')->on($toolTableName);
            $table->date('creation_date');
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();
            $table->string('arch_path', 200)->nullable();
            $table->bigInteger('arch_size')->nullable();
            $table->foreignId($zoneTableId)->references('id')->on($zoneTableName);

            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);

            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            $table->unique([$zoneTableName.'_id', 'name']);
        });

        Schema::create($mosaicArchFileTableName, function(Blueprint $table)
            use($mosaicTableName, $mosaicTableId){

            $table->id();
            $table->foreignId($mosaicTableId)->references('id')->on($mosaicTableName);
            $table->string('name', 100);
            $table->string('description', 200);
            $table->timestamps();
        });

        Schema::create($mosaicTableName.'_permission', function(Blueprint $table) use($mosaicTableName, $mosaicTableId, $userTableName, $userTableId){
            $table->id();
            $table->foreignId($mosaicTableId)->references('id')->on($mosaicTableName);
            $table->foreignId($userTableId)->references('id')->on($userTableName);
            $table->smallInteger('can_see')->default(0);
            $table->smallInteger('can_download')->default(0);
            $table->smallInteger('can_edit')->default(0);

            $table->unique([$mosaicTableId, $userTableId]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $mosaicArchFileTableName = $this->getMosaicArchFileTableName();
        $mosaicTableName = $this->getMosaicTableName();
        $mosaicTypeTableName = $this->getMosaicTypeTableName();
        Schema::dropIfExists($mosaicArchFileTableName);
        Schema::dropIfExists($mosaicTableName.'_permission');
        Schema::dropIfExists($mosaicTableName);
        Schema::dropIfExists($mosaicTypeTableName);
    }
}
