<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismicSourceTable extends Migration
{

    static function getSeismicSourceTypeTable(){
        return \App\Tables::SEISMIC_SOURCE_TYPE_TABLE;
    }

    static function getSeismicSourceTable(){
        return \App\Tables::SEISMIC_SOURCE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seismicSourceTypeTable = static::getSeismicSourceTypeTable();
        $seismicSourceTypeTableId = FK::get($seismicSourceTypeTable);
        $seismicSourceTable = static::getSeismicSourceTable();

        Schema::create($seismicSourceTypeTable, function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->timestamps();
        });

        Schema::create($seismicSourceTable, function(Blueprint $table)
                use($seismicSourceTypeTable, $seismicSourceTypeTableId){
            $table->id();
            $table->string('name', 200);
            $table->foreignId($seismicSourceTypeTableId)->references('id')->on($seismicSourceTypeTable);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $seismicSourceTypeTable = static::getSeismicSourceTypeTable();
        $seismicSourceTable = static::getSeismicSourceTable();

        Schema::dropIfExists($seismicSourceTable);
        Schema::dropIfExists($seismicSourceTypeTable);
    }
}
