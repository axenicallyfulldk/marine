<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnogenicHazardGroupTable extends Migration
{

    public static function getTechnogenicHazardGroupTableName(){
        return \App\Tables::TECHNOGENIC_HAZARD_GROUP_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $thgtn = static::getTechnogenicHazardGroupTableName();
        Schema::create($thgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $thgtn = static::getTechnogenicHazardGroupTableName();
        Schema::dropIfExists($thgtn);
    }
}
