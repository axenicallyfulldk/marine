<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateSeismic3dStackAttrTable extends Migration
{

    private static function getSeismic3dStackAttrTableName(){
        return \App\Tables::SEISMIC3D_STACK_ATTR_TABLE;
    }

    private static function getSeismic3dStackTableName(){
        return \App\Tables::SEISMIC3D_STACK_TABLE;
    }

    private static function getSeismicStackAttrTypeTableName(){
        return \App\Tables::SEISMIC_STACK_ATTR_TYPE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $seismic3dStackAttrTableName = static::getSeismic3dStackAttrTableName();
        $seismicStackAttrTypeTableName = static::getSeismicStackAttrTypeTableName();
        $seismic3dStackTableName = static::getSeismic3dStackTableName();

        Schema::create($seismic3dStackAttrTableName, function (Blueprint $table)
                use($seismic3dStackTableName, $seismicStackAttrTypeTableName) {
            $table->id();
            $table->foreignId(FK::get($seismic3dStackTableName))->references('id')->on($seismic3dStackTableName);
            $table->foreignId(FK::get($seismicStackAttrTypeTableName))->references('id')->on($seismicStackAttrTypeTableName);
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $seismic3dStackAttrTableName = static::getSeismic3dStackAttrTableName();
        Schema::dropIfExists($seismic3dStackAttrTableName);
    }
}
