<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSssMbesSurveyTable extends Migration
{
    private function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    private function getTripTableName(){
        return \App\Tables::TRIP_TABLE;
    }

    private function getSurveyTypeTableName(){
        return \App\Tables::SURVEY_TYPE_TABLE;
    }

    private function getSurveyTableName(){
        return \App\Tables::SSSMBES_SURVEY_TABLE;
    }

    private function getToolTableName(){
        return \App\Tables::TOOL_TABLE;
    }

    private function getUserTableName(){
        return \App\Tables::USER_TABLE;
    }

    private function getSurveyArchFileTableName(){
        return \App\Tables::SSSMBES_SURVEY_ARCHIVE_FILES_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $zoneTableName = $this->getZoneTableName();
        $zoneTableId = $zoneTableName.'_id';

        $tripTableName = $this->getTripTableName();
        $tripTableId = $tripTableName.'_id';

        $toolTableName = $this->getToolTableName();
        $toolTableId = $toolTableName.'_id';

        $surveyTypeTableName = $this->getSurveyTypeTableName();
        $surveyTypeTableId = $surveyTypeTableName.'_id';

        $surveyArchFileTableName = $this->getSurveyArchFileTableName();
        $surveyArchFileTableId = $surveyArchFileTableName.'_id';

        $surveyTableName = $this->getSurveyTableName();
        $surveyTableId = $surveyTableName.'_id';

        $userTableName = $this->getUserTableName();
        $userTableId = $userTableName.'_id';

        Schema::create($surveyTypeTableName, function(Blueprint $table){
            $table->id();
            $table->string('name', 40);
            $table->string('key', 10)->unique();
            $table->bigInteger('map_color')->nullable();
            $table->timestamps();
        });

        Schema::create($surveyTableName, function (Blueprint $table)
            use($zoneTableName, $zoneTableId, $surveyTypeTableName, $surveyTypeTableId,
                $tripTableName, $tripTableId, $toolTableName, $toolTableId, $userTableName) {
            $table->id();
            $table->uuid('unique_id')->default(DB::raw('uuid_generate_v4()'));
            $table->string('name');
            $table->geometry('area');
            $table->enum('direction', \App\Models\SssMbesSurvey::DIRECTION);
            $table->foreignId($surveyTypeTableId)->references('id')->on($surveyTypeTableName);
            $table->date('start_date');
            $table->string('file_path', 200);
            $table->bigInteger('file_size')->nullable();
            $table->string('arch_path', 200)->nullable();
            $table->bigInteger('arch_size')->nullable();
            $table->foreignId($zoneTableId)->references('id')->on($zoneTableName);
            $table->foreignId($tripTableId)->references('id')->on($tripTableName);
            $table->foreignId($toolTableId)->references('id')->on($toolTableName);

            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);

            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            $table->unique([$zoneTableName.'_id', 'name']);
        });

        Schema::create($surveyArchFileTableName, function(Blueprint $table)
            use($surveyTableName, $surveyTableId){

            $table->id();
            $table->foreignId($surveyTableId)->references('id')->on($surveyTableName);
            $table->string('name', 100);
            $table->string('description', 200);

            $table->timestamps();
        });

        Schema::create($surveyTableName.'_permission', function(Blueprint $table) use($surveyTableName, $surveyTableId, $userTableName, $userTableId){
            $table->id();
            $table->foreignId($surveyTableId)->references('id')->on($surveyTableName);
            $table->foreignId($userTableId)->references('id')->on($userTableName);
            $table->smallInteger('can_see')->default(0);
            $table->smallInteger('can_download')->default(0);
            $table->smallInteger('can_edit')->default(0);

            $table->unique([$surveyTableId, $userTableId]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $surveyArchFileTableName = $this->getSurveyArchFileTableName();
        $surveyTableName = $this->getSurveyTableName();
        $surveyTypeTableName = $this->getSurveyTypeTableName();
        Schema::dropIfExists($surveyArchFileTableName);
        Schema::dropIfExists($surveyTableName.'_permission');
        Schema::dropIfExists($surveyTableName);
        Schema::dropIfExists($surveyTypeTableName);
    }
}
