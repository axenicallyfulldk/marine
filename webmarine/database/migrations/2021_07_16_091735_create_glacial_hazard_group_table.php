<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlacialHazardGroupTable extends Migration
{
    static public function getGlacialHazardGroupTableName(){
        return \App\Tables::GLACIAL_HAZARD_GROUP_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $glacialHazardGroupTableName = static::getGlacialHazardGroupTableName();
        Schema::create($glacialHazardGroupTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $glacialHazardGroupTableName = static::getGlacialHazardGroupTableName();
        Schema::dropIfExists($glacialHazardGroupTableName);
    }
}
