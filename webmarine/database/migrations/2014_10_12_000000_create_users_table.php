<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userTableName = Tables::USER_TABLE;

        $adminGroupId = Config::get('auth.default_groups_id.admin');
        $moderatorGroupId = Config::get('auth.default_groups_id.moderator');
        $gnosticUserGroupId = Config::get('auth.default_groups_id.gnostic_user');
        $agnosticUserGroupId = Config::get('auth.default_groups_id.agnostic_user');
        
        Schema::create($userTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            
            $table->boolean('edit_tool')->nullable();
            $table->boolean('edit_vessel')->nullable();
            // $table->boolean('see_zone')->nullable();
            $table->boolean('edit_zone')->nullable();

            $table->boolean('sssmbes_survey_permission')->nullable();
            $table->boolean('see_sssmbes_survey')->nullable();
            $table->boolean('download_sssmbes_survey')->nullable();
            $table->boolean('edit_sssmbes_survey')->nullable();

            $table->boolean('seismic2d_survey_permission')->nullable();
            $table->boolean('see_seismic2d_survey')->nullable();
            $table->boolean('download_seismic2d_survey')->nullable();
            $table->boolean('edit_seismic2d_survey')->nullable();

            $table->boolean('seismic3d_survey_permission')->nullable();
            $table->boolean('see_seismic3d_survey')->nullable();
            $table->boolean('download_seismic3d_survey')->nullable();
            $table->boolean('edit_seismic3d_survey')->nullable();

            $table->boolean('mosaic_permission')->nullable();
            $table->boolean('see_mosaic')->nullable();
            $table->boolean('download_mosaic')->nullable();
            $table->boolean('edit_mosaic')->nullable();

            $table->boolean('model_permission')->nullable();
            $table->boolean('see_model')->nullable();
            $table->boolean('download_model')->nullable();
            $table->boolean('edit_model')->nullable();
            
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table($userTableName, function (Blueprint $table) use($userTableName){
            $table->foreignId('group_id')->nullable()->references('id')->on($userTableName);
        });

        //Reserve first 50 ids
        DB::update(DB::raw("alter sequence ".$userTableName."_id_seq restart with 50"));

        // Insert users groups
        DB::table($userTableName)->insert(
            [
                // Create admin group
                [
                    'id' => $adminGroupId,
                    'name' => 'admin_group','email' => 'admin_group@marine.marine',
                    'password' => Hash::make('admin_group_pass'),

                    'edit_tool' => true,
                    'edit_vessel' => true,
                    // 'see_zone' => true,
                    'edit_zone' => true,

                    'sssmbes_survey_permission' => true,
                    'see_sssmbes_survey' => true,
                    'download_sssmbes_survey' => true,
                    'edit_sssmbes_survey' => true,

                    'seismic2d_survey_permission' => true,
                    'see_seismic2d_survey' => true,
                    'download_seismic2d_survey' => true,
                    'edit_seismic2d_survey' => true,

                    'seismic3d_survey_permission' => true,
                    'see_seismic3d_survey' => true,
                    'download_seismic3d_survey' => true,
                    'edit_seismic3d_survey' => true,

                    'mosaic_permission' => true,
                    'see_mosaic' => true,
                    'download_mosaic' => true,
                    'edit_mosaic' => true,

                    'model_permission' => true,
                    'see_model' => true,
                    'download_model' => true,
                    'edit_model' => true,
                ],
                // Create moderator group
                [
                    'id' => $moderatorGroupId,
                    'name' => 'moderator_group',
                    'email' => 'moderator_group@marine.marine',
                    'password' => Hash::make('moderator_pass'),
                    
                    'edit_tool' => true,
                    'edit_vessel' => true,
                    // 'see_zone' => true,
                    'edit_zone' => true,

                    'sssmbes_survey_permission' => true,
                    'see_sssmbes_survey' => true,
                    'download_sssmbes_survey' => true,
                    'edit_sssmbes_survey' => true,

                    'seismic2d_survey_permission' => true,
                    'see_seismic2d_survey' => true,
                    'download_seismic2d_survey' => true,
                    'edit_seismic2d_survey' => true,

                    'seismic3d_survey_permission' => true,
                    'see_seismic3d_survey' => true,
                    'download_seismic3d_survey' => true,
                    'edit_seismic3d_survey' => true,

                    'mosaic_permission' => true,
                    'see_mosaic' => true,
                    'download_mosaic' => true,
                    'edit_mosaic' => true,

                    'model_permission' => true,
                    'see_model' => true,
                    'download_model' => true,
                    'edit_model' => true,
                ],
                // Create gnostic_user group
                [
                    'id' => $gnosticUserGroupId,
                    'name' => 'gnostic_group',
                    'email' => 'gnostic_group@marine.marine',
                    'password' => Hash::make('user_pass'),

                    'edit_tool' => false,
                    'edit_vessel' => false,
                    // 'see_zone' => true,
                    'edit_zone' => false,

                    'sssmbes_survey_permission' => false,
                    'see_sssmbes_survey' => true,
                    'download_sssmbes_survey' => true,
                    'edit_sssmbes_survey' => false,

                    'seismic2d_survey_permission' => false,
                    'see_seismic2d_survey' => true,
                    'download_seismic2d_survey' => true,
                    'edit_seismic2d_survey' => false,

                    'seismic3d_survey_permission' => false,
                    'see_seismic3d_survey' => true,
                    'download_seismic3d_survey' => true,
                    'edit_seismic3d_survey' => false,

                    'mosaic_permission' => false,
                    'see_mosaic' => true,
                    'download_mosaic' => true,
                    'edit_mosaic' => false,

                    'model_permission' => false,
                    'see_model' => true,
                    'download_model' => true,
                    'edit_model' => false,
                ],
                // Create agnostic_user group
                [
                    'id' => $agnosticUserGroupId,
                    'name' => 'agnostic_group',
                    'email' => 'agnostic_group@marine.marine',
                    'password' => Hash::make('user_pass'),

                    'edit_tool' => false,
                    'edit_vessel' => false,
                    // 'see_zone' => false,
                    'edit_zone' => false,

                    'sssmbes_survey_permission' => false,
                    'see_sssmbes_survey' => false,
                    'download_sssmbes_survey' => false,
                    'edit_sssmbes_survey' => false,

                    'seismic2d_survey_permission' => false,
                    'see_seismic2d_survey' => false,
                    'download_seismic2d_survey' => false,
                    'edit_seismic2d_survey' => false,

                    'seismic3d_survey_permission' => false,
                    'see_seismic3d_survey' => false,
                    'download_seismic3d_survey' => false,
                    'edit_seismic3d_survey' => false,

                    'mosaic_permission' => false,
                    'see_mosaic' => false,
                    'download_mosaic' => false,
                    'edit_mosaic' => false,

                    'model_permission' => false,
                    'see_model' => false,
                    'download_model' => false,
                    'edit_model' => false,
                ]
            ]
        );

        //Insert admin users
        DB::table($userTableName)->insert(
            [
                ['name' => 'nav', 'email' => 'nav@marine.marine', 'password' => Hash::make('nav'), 'group_id' => $adminGroupId],
                ['name' => 'kaa', 'email' => 'kaa@marine.marine', 'password' => Hash::make('kaa'), 'group_id' => $adminGroupId],
                ['name' => 'agnostic', 'email' => 'agnostic@marine.marine', 'password' => Hash::make('agnostic'), 'group_id' => $agnosticUserGroupId],
                ['name' => 'gnostic', 'email' => 'gnostic@marine.marine', 'password' => Hash::make('gnostic'), 'group_id' => $gnosticUserGroupId],
                ['name' => 'moderator', 'email' => 'moderator@marine.marine', 'password' => Hash::make('moderator'), 'group_id' => $moderatorGroupId]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $userTableName = \App\Tables::USER_TABLE;

        Schema::dropIfExists($userTableName);
    }
}
