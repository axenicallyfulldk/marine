<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateFaultTable extends Migration
{

    static private function getFaultTypeTableName(){
        return \App\Tables::FAULT_TYPE_TABLE;
    }

    static private function getFaultTableName(){
        return \App\Tables::FAULT_TABLE;
    }

    static private function getFaultActivityDegreeTableName(){
        return \App\Tables::FAULT_ACTIVITY_DEGREE_TABLE;
    }

    static private function getFaultRiskDegreeTableName(){
        return \App\Tables::FAULT_RISK_DEGREE_TABLE;
    }

    static private function getFaultGroupTableName(){
        return \App\Tables::FAULT_GROUP_TABLE;
    }

    static private function getFaultSeismic2dStackTableName(){
        return \App\Tables::SEISMIC2D_FAULT_TABLE;
    }

    static private function getSeismic2dStackTableName(){
        return \App\Tables::SEISMIC2D_STACK_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faultActivityDegreeTableName = static::getFaultActivityDegreeTableName();
        $faultRiskDegreeTableName = static::getFaultRiskDegreeTableName();
        $faultTypeTableName = static::getFaultTypeTableName();
        $faultTableName = static::getFaultTableName();
        $faultGroupTableName = static::getFaultGroupTableName();
        $faultStackTableName = static::getFaultSeismic2dStackTableName();
        $seismic2dStackTableName = static::getSeismic2dStackTableName();

        Schema::create($faultTypeTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name', 200);
            $table->timestamps();
        });

        Schema::create($faultTableName, function (Blueprint $table) use($faultTypeTableName,
                    $faultActivityDegreeTableName, $faultRiskDegreeTableName, $faultGroupTableName) {
            $table->id();
            $table->string('name', 200);
            $table->string('description', 400)->nullable();
            $table->foreignId(FK::get($faultTypeTableName))->nullable()->references('id')->on($faultTypeTableName);
            $table->foreignId(FK::get($faultActivityDegreeTableName))->nullable()->references('id')->on($faultActivityDegreeTableName);
            $table->foreignId(FK::get($faultRiskDegreeTableName))->nullable()->references('id')->on($faultRiskDegreeTableName);
            $table->foreignId(FK::get($faultGroupTableName))->nullable()->references('id')->on($faultGroupTableName);
            $table->float('throw')->nullable()->description('vertical displacement');
            $table->float('heave')->nullable()->description('horizontal displacement');
            $table->float('dip')->nullable();
            $table->timestamps();
        });

        Schema::create($faultStackTableName, function (Blueprint $table)
            use($faultTableName, $seismic2dStackTableName) {
            $table->id();

            $table->foreignId(FK::get($faultTableName))->references('id')->on($faultTableName);
            $table->foreignId(FK::get($seismic2dStackTableName))->references('id')->on($seismic2dStackTableName);
            $table->jsonb('coords');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $faultTypeTableName = static::getFaultTypeTableName();
        $faultTableName = static::getFaultTableName();
        $faultStackTableName = static::getFaultSeismic2dStackTableName();

        Schema::dropIfExists($faultStackTableName);
        Schema::dropIfExists($faultTableName);
        Schema::dropIfExists($faultTypeTableName);
    }
}
