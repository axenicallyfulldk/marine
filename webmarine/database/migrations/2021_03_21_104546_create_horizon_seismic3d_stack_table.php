<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHorizonSeismic3dStackTable extends Migration
{

    private static function getHorizonSeismic3dStackTableName(){
        return \App\Tables::SEISMIC3D_HORIZON_TABLE;
    }

    private static function getHorizonTableName(){
        return \App\Tables::HORIZON_TABLE;
    }

    private static function getSeismic3dStackTableName(){
        return \App\Tables::SEISMIC3D_STACK_TABLE;
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $horTableName = static::getHorizonTableName();
        $horSeismic3dStackTableName = static::getHorizonSeismic3dStackTableName();
        $seismic3dTableName = static::getSeismic3dStackTableName();

        Schema::create($horSeismic3dStackTableName, function (Blueprint $table)
                use($horTableName, $seismic3dTableName) {
            $table->id();
            $table->foreignId(FK::get($horTableName))->references('id')->on($horTableName);
            $table->foreignId(FK::get($seismic3dTableName))->references('id')->on($seismic3dTableName);
            // $table->string('file_path', 200);
            // $table->bigInteger('file_size')->nullable();
            $table->jsonb('coords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horSeismic3dStackTableName = static::getHorizonSeismic3dStackTableName();

        Schema::dropIfExists($horSeismic3dStackTableName);
    }
}
