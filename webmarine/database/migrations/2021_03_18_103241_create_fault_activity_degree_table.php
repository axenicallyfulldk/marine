<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaultActivityDegreeTable extends Migration
{

    protected static function getFaultActivityDegreeTableName(){
        return \App\Tables::FAULT_ACTIVITY_DEGREE_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faultActivityDegreeTableName = static::getFaultActivityDegreeTableName();
        Schema::create($faultActivityDegreeTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $faultActivityDegreeTableName = static::getFaultActivityDegreeTableName();
        Schema::dropIfExists($faultActivityDegreeTableName);
    }
}
