<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHazardTypeTable extends Migration
{

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardClassTableName(){
        return \App\Tables::HAZARD_CLASS_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hazardClassTableName = static::getHazardClassTableName();
        $hazardTypeTableName = static::getHazardTypeTableName();
        Schema::create($hazardTypeTableName, function (Blueprint $table) use($hazardClassTableName){
            $table->id();
            $table->string('name')->comment('Имя типа опасностей');
            $table->foreignId(FK::get($hazardClassTableName))->references('id')->comment('Идентификатор класс опасностей к которой принадлежит тип')->on($hazardClassTableName);
            $table->string('key')->comment('Текстовый идентфикатор типа');
            $table->bigInteger('color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $hazardTypeTableName = static::getHazardTypeTableName();
        Schema::dropIfExists($hazardTypeTableName);
    }
}
