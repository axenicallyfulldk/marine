<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateAtmosphericHazardTable extends Migration
{
    public static function getHazardGroupTableName(){
        return \App\Tables::ATMOSPHERIC_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardTableName(){
        return \App\Tables::ATMOSPHERIC_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hgtn = static::getHazardGroupTableName();
        $htn = static::getHazardTableName();
        $httn = static::getHazardTypeTableName();
        $ztn = static::getZoneTableName();
        Schema::create($hgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create($htn, function (Blueprint $table) use($hgtn, $httn, $ztn) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($hgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($hgtn);
            $table->foreignId(FK::get($ztn))->nullable()->references('id')->comment('Идентификатор зоны')->on($ztn);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');

            $table->float('speed')->nullable()->comment('Скорость (ветровые, температурные)');
            $table->float('periodicity')->nullable()->comment('Периодичность (ветровые, электромагнитные)');
            $table->smallInteger('repeatability')->nullable()->comment('Повторяемость (ветровые)');
            $table->float('direction')->nullable()->comment('Направленность (ветровые)');
            $table->float('amount')->nullable()->comment('Количество осадков (снего-дождевые)');
            $table->float('frequency')->nullable()->comment('Частота (снего-дождевые)');
            $table->float('intensity')->nullable()->comment('Интенсивность (обледенения) (температурные, электромагнитные)');
            $table->float('temperature')->nullable()->comment('Температура (температурные)');
            $table->float('voltage')->nullable()->comment('Напряжение (электромагнитные)');

            // $table->float('')->nullable()->comment('');
            // $table->smallInteger('')->nullable()->comment('');

            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $htn = static::getHazardTableName();
        $hgtn = static::getHazardGroupTableName();
        Schema::dropIfExists($htn);
        Schema::dropIfExists($hgtn);
    }
}
