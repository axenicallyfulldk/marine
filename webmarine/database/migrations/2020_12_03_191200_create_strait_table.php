<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStraitTable extends Migration
{

    private function getStraitTableName(){
        return \App\Tables::STRAIT_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $straitTableName = $this->getStraitTableName();
        Schema::create($straitTableName, function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $straitTableName = $this->getStraitTableName();
        Schema::dropIfExists($straitTableName);
    }
}
