<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHazardClassTable extends Migration
{

    public static function getHazardClassTableName(){
        return \App\Tables::HAZARD_CLASS_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hazardClassTableName = static::getHazardClassTableName();
        Schema::create($hazardClassTableName, function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Имя класса опасностей');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $hazardClassTableName = static::getHazardClassTableName();
        Schema::dropIfExists($hazardClassTableName);
    }
}
