<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateTechnogenicHazardTable extends Migration
{
    public static function getTechnogenicHazardGroupTableName(){
        return \App\Tables::TECHNOGENIC_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getTechnogenicHazardTableName(){
        return \App\Tables::TECHNOGENIC_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $thgtn = static::getTechnogenicHazardGroupTableName();
        $thtn = static::getTechnogenicHazardTableName();
        $httn = static::getHazardTypeTableName();
        $zoneTableName = static::getZoneTableName();
        Schema::create($thtn, function (Blueprint $table) use($thgtn, $httn, $zoneTableName) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($thgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($thgtn);
            $table->foreignId(FK::get($zoneTableName))->nullable()->references('id')->comment('Идентификатор зоны')->on($zoneTableName);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');
            $table->float('max_length')->comment('Максимальная длина объекта')->nullable();
            $table->float('max_width')->comment('Максимальная ширина объекта')->nullable();
            $table->float('max_height')->comment('Максимальная высота объекта')->nullable();
            $table->float('volume')->comment('Максимальная высота объекта')->nullable();
            $table->string('composition')->comment('Состав захороненного объекта')->nullable();
            $table->smallInteger('preservation_degree')->comment('Степень сохранности')->nullable();
            $table->smallInteger('aggressiveness')->comment('Агрессивность')->nullable();
            $table->float('heat_flux')->comment('Величина теплового потока')->nullable();
            $table->float('temperature')->comment('Температура')->nullable();
            $table->float('intensity')->comment('Интенсивность воздействия')->nullable();
            $table->float('load')->comment('Нагрузка')->nullable();
            $table->float('repeatability')->comment('Повторяемость')->nullable();
            $table->float('destruction_degree')->comment('Степень разрушения объекта')->nullable();
            $table->float('decomposition_rate')->comment('Степень разложения объекта')->nullable();
            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $thtn = static::getTechnogenicHazardTableName();
        Schema::dropIfExists($thtn);
    }
}
