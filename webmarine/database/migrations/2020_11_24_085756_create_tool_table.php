<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToolTable extends Migration
{
    function getToolTableName(){
        return \App\Tables::TOOL_TABLE;
    }

    private function getUserTableName(){
        return \App\Tables::USER_TABLE;
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $toolTableName = $this->getToolTableName();
        $userTableName = $this->getUserTableName();
        Schema::create($toolTableName, function (Blueprint $table) use($userTableName) {
            $table->id();
            $table->string('model', 64);
            $table->string('site', 128)->nullable();
            $table->string('brand', 128)->nullable();
            $table->string('code', 128)->nullable();
            $table->string('sb_mb', 8)->nullable();
            $table->string('type1', 8)->nullable();
            $table->string('type2', 8)->nullable();
            $table->double('op_freq1')->nullable();
            $table->double('op_freq2')->nullable();
            $table->double('op_depth')->nullable();
            $table->double('op_max_op_range')->nullable();
            $table->double('weight_in_air')->nullable();
            $table->double('weight_in_water')->nullable();
            $table->integer('xtf_id')->nullable();
            $table->bigInteger('map_color')->nullable();

            $table->foreignId('creator_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('updater_id')->nullable()->references('id')->on($userTableName);
            $table->foreignId('deleter_id')->nullable()->references('id')->on($userTableName);
            
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });

        $uknownToolId = Config::get('app.default_tools_id.unknown');
        $mixedToolId = Config::get('app.default_tools_id.mixed');

        //Reserve first 5 ids
        DB::update(DB::raw("alter sequence ".$toolTableName."_id_seq restart with 5"));

        // Insert pseudo tools
        DB::table($toolTableName)->insert(
            [
                // Create unknown tool
                [
                    'id' => $uknownToolId,
                    'model' => 'Неизвестный',
                ],
                // Create mixed tool
                [
                    'id' => $mixedToolId,
                    'model' => 'Интегрированный',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $toolTableName = $this->getToolTableName();
        Schema::dropIfExists($toolTableName);
    }
}
