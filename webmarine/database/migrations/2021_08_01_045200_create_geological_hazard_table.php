<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateGeologicalHazardTable extends Migration
{
    public static function getHazardGroupTableName(){
        return \App\Tables::GEOLOGICAL_HAZARD_GROUP_TABLE;
    }

    static public function getHazardTypeTableName(){
        return \App\Tables::HAZARD_TYPE_TABLE;
    }

    public static function getHazardTableName(){
        return \App\Tables::GEOLOGICAL_HAZARD_TABLE;
    }

    static public function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $hgtn = static::getHazardGroupTableName();
        $htn = static::getHazardTableName();
        $httn = static::getHazardTypeTableName();
        $ztn = static::getZoneTableName();
        Schema::create($hgtn, function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create($htn, function (Blueprint $table) use($hgtn, $httn, $ztn) {
            $table->id();
            $table->foreignId(FK::get($httn))->references('id')->comment('Идентификатор типа опасности')->on($httn);
            $table->foreignId(FK::get($hgtn))->nullable()->references('id')->comment('Идентификатор группы')->on($hgtn);
            $table->foreignId(FK::get($ztn))->nullable()->references('id')->comment('Идентификатор зоны')->on($ztn);
            $table->string('name')->nullable()->comment('Текстовый идентификатор опасноти');

            $table->float('inclusion_size')->nullable()->comment('Размер включений (валунные отложения)');
            $table->string('inclusion_composition', 400)->nullable()->comment('Состав включений (валунные отложения)');
            $table->float('thickness')->nullable()->comment('Мощность (валунные отложения, иинсгт, спссг, спсссг)');
            $table->float('density')->nullable()->comment('Плотность (валенные отложения)');
            $table->smallInteger('bodenklasse')->nullable()->comment('Категория разрабатываемости');
            $table->float('e')->nullable()->comment('Модуль Юнга (валунные отложения, иинсгт, спссг, спсссг)');
            $table->float('c')->nullable()->comment('Модуль общей деформации (валунные отложения, иинсгт, спссг, спсссг)');
            $table->float('fi')->nullable()->comment('Угол внутреннего трения (валунные отложения, иинсгт, спссг, спсссг)');
            $table->string('composition', 400)->nullable()->comment('Состав (иинсгт)');
            $table->float('area_of_extent')->nullable()->comment('Площадь развития (иинсгт)');
            $table->float('porosity')->nullable()->comment('Пористость (спссг, спсссг)');
            $table->smallInteger('drillability_index')->nullable()->comment('Категория буримости (спссг, спсссг)');
            $table->float('moisture_capacity')->nullable()->comment('Влагопоглащаемость (спсссг)');
            $table->float('length')->nullable()->comment('Длина отложения');
            $table->float('width')->nullable()->comment('Ширина отложения ');
            $table->float('height')->nullable()->comment('Высота отложения');
            $table->float('min_depth')->nullable()->comment('Минимальная глубина палеовреза');
            $table->float('max_depth')->nullable()->comment('Максимальная глубина палеовреза');
            $table->float('major_axis_azimuth')->nullable()->comment('Азимут большой оси структуры');

            // $table->float('')->nullable()->comment('');
            // $table->smallInteger('')->nullable()->comment('');
            // $table->string('', 400)->nullable()->comment('');

            $table->string('comment', 1000)->comment('Комментарий')->nullable();
            $table->geometry('shape')->comment('Географическая область опасности');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $htn = static::getHazardTableName();
        $hgtn = static::getHazardGroupTableName();
        Schema::dropIfExists($htn);
        Schema::dropIfExists($hgtn);
    }
}
