<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeismicStackAttrTypeTable extends Migration
{

    private static function getSeismicStackAttrTypeTableName(){
        return \App\Tables::SEISMIC_STACK_ATTR_TYPE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ssattn = static::getSeismicStackAttrTypeTableName();

        Schema::create($ssattn, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            // $table->enum('group', \App\Models\SeismicStackAttrType::GROUPS);
            $table->string('comment', 1024);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $ssattn = static::getSeismicStackAttrTypeTableName();
        Schema::dropIfExists($ssattn);
    }
}
