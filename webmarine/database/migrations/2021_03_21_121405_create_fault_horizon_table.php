<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateFaultHorizonTable extends Migration
{

    static private function getFaultTableName(){
        return \App\Tables::FAULT_TABLE;
    }

    private static function getHorizonTableName(){
        return \App\Tables::HORIZON_TABLE;
    }

    private static function getFaultHorizonTableName(){
        return \App\Tables::FAULT_HORIZON_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faultHorizonTable = static::getFaultHorizonTableName();
        $faultTableName = static::getFaultTableName();
        $horizonTableName = static::getHorizonTableName();
        Schema::create($faultHorizonTable, function (Blueprint $table) use($horizonTableName, $faultTableName) {
            $table->id();
            $table->foreignId(FK::get($horizonTableName))->references('id')->on($horizonTableName);
            $table->foreignId(FK::get($faultTableName))->references('id')->on($faultTableName);
            $table->jsonb('coords');
            $table->timestamps();
            // $table->unique([FK::get($horizonTableName), FK::get($faultTableName)]);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $faultHorizonTable = static::getFaultHorizonTableName();
        Schema::dropIfExists($faultHorizonTable);
    }
}
