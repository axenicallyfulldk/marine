<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateHorizonTable extends Migration
{

    private static function getHorizonTypeTableName(){
        return \App\Tables::HORIZON_TYPE_TABLE;
    }

    private static function getHorizonTableName(){
        return \App\Tables::HORIZON_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $horTypeTableName = static::getHorizonTypeTableName();
        $horizonTableName = static::getHorizonTableName();
        Schema::create($horizonTableName, function (Blueprint $table) use($horTypeTableName){
            $table->id();
            $table->string('name');
            $table->foreignId(FK::get($horTypeTableName))->nullable()->references('id')->on($horTypeTableName);
            $table->boolean('is_top')->nullable();
            $table->integer('hor_risk_degree')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $horizonTableName = static::getHorizonTableName();
        Schema::dropIfExists($horizonTableName);
    }
}
