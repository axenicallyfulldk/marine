<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateFaultGroupTable extends Migration
{

    private static function getFaultGroupTableName(){
        return \App\Tables::FAULT_GROUP_TABLE;
    }

    private static function getFaultGroupTypeTableName(){
        return \App\Tables::FAULT_GROUP_TYPE_TABLE;
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $faultGroupTypeTableName = static::getFaultGroupTypeTableName();
        $faultGroupTableName = static::getFaultGroupTableName();

        Schema::create($faultGroupTableName, function (Blueprint $table) use($faultGroupTypeTableName) {
            $table->id();
            $table->string("name");
            $table->foreignId(FK::get($faultGroupTypeTableName))->nullable()->references('id')->on($faultGroupTypeTableName);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $faultGroupTableName = static::getFaultGroupTableName();
        Schema::dropIfExists($faultGroupTableName);
    }
}
