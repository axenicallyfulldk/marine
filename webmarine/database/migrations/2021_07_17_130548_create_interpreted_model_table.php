<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Helpers\FK;

class CreateInterpretedModelTable extends Migration
{

    public static function getInterpretedModelTableName(){
        return \App\Tables::INTERPRETED_MODEL_TABLE;
    }

    public static function getInterpretedModelObjectTableName(){
        return \App\Tables::INTERPRETED_MODEL_OBJECT_TABLE;
    }

    public static function getMosaicTableName(){
        return \App\Tables::MOSAIC_TABLE;
    }

    private static function getZoneTableName(){
        return \App\Tables::ZONE_TABLE;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $imtn = static::getInterpretedModelTableName();
        $imotn = static::getInterpretedModelObjectTableName();
        $mtn = static::getMosaicTableName();

        $zoneTableName = static::getZoneTableName();

        Schema::create($imtn, function (Blueprint $table) use($mtn, $zoneTableName){
            $table->id();
            $table->string('name');
            $table->foreignId(FK::get($zoneTableName))->references('id')->on($zoneTableName);
            // $table->foreignId(FK::get($mtn))->nullable()->references('id')->comment('Идентификатор мозаики')->on($mtn);
            $table->string('file_path', 200)->nullable();
            $table->bigInteger('file_size')->nullable();
            $table->date('creation_date');
            $table->string('comment')->nullable();
            $table->geometry('shape');
            $table->timestamps();
        });

        Schema::create($imotn, function (Blueprint $table) use($imtn){
            $table->id();
            $table->foreignId(FK::get($imtn))->references('id')->comment('Идентификатор модели')->on($imtn);
            $table->morphs('object');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $imtn = static::getInterpretedModelTableName();
        $imotn = static::getInterpretedModelObjectTableName();
        Schema::dropIfExists($imotn);
        Schema::dropIfExists($imtn);
    }
}
