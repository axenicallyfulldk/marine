<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBayTable extends Migration
{
    private function getBayTableName(){
        return \App\Tables::BAY_TABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $bayTableName = $this->getBayTableName();
        Schema::create($bayTableName, function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $bayTableName = $this->getBayTableName();
        Schema::dropIfExists($bayTableName);
    }
}
