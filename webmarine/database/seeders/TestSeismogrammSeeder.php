<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Seismic2dSeismogramm;
use App\Models\Zone;
use App\Models\Vessel;
use App\Models\SeismicResolutionClass;
use App\Models\SeismicSource;
use App\Models\SeismicStreamer;

class TestSeismogrammSeeder extends Seeder
{

    public static function createOrGetSeismogramm($name, $alias, $survey, $segyPath, $sp){
        $seismogramm = $survey->byNameOrAliasSubstr($name)->first();
        if(is_null($seismogramm)){
            $seismogramm = new Seismic2dSeismogramm();
        }

        $seismogramm->name = $name;
        $seismogramm->alias = $alias;
        $seismogramm->survey()->associate($survey);
        $seismogramm->file_path = $segyPath;
        $seismogramm->sp_start = $sp['sp_start'];
        $seismogramm->sp_step = $sp['sp_step'];
        $seismogramm->sp_end = $sp['sp_end'];
        $seismogramm->sp_distance = $sp['sp_distance'];


        $seismogramm->save();
        return $seismogramm;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testZone = ZonesSeeder::createOrGetZone("Test", "T", null, Zone::ZONE_CLASSES['ocean_basin'], \DB::raw("'POLYGON((1 1, 1 2, 2 2, 2 1, 1 1))'"), null);

        $vessel = Vessel::first();
        $source = SeismicSource::first();
        $streamer = SeismicStreamer::first();
        $resolClass = SeismicResolutionClass::findByName(\mb_strtolower('Unknown'))->first();

        $survey = Seismic2dStackSeeder::createOrGetSurvey("Test", "Test", \DB::raw("'POLYGON((1 1, 1 2, 2 2, 2 1, 1 1))'"), "L", null, null, $testZone, null, $vessel, $source, 1, 1, $streamer, false, $resolClass, "2021-09-15", null, null);


        $seismogrammFiles = [
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_be_I1.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_be_I2.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_be_I4.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_be_R4IBM.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_be_R4IEEE.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_le_I1.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_le_I2.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_le_I4.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_le_R4IBM.sgy",
            "GS2021/T/L1/2021/SEIS2D/sin10&30Hz_le_R4IEEE.sgy",
        ];

        $sp = ['sp_start'=>1, 'sp_end'=>1, 'sp_step'=>1, 'sp_distance'=>1];

        foreach($seismogrammFiles as $sf){
            $name = static::getFileName($sf);
            static::createOrGetSeismogramm($name, null, $survey, $sf, $sp);
        }
    }

    private static function getFileName($fileName){
        $path_parts = pathinfo($fileName);
        return $path_parts['filename'];
    }
}
