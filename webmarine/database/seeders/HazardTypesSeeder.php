<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\HazardType;
use App\Models\HazardClass;
use App\Helpers\ColorIntConverter;

class HazardTypesSeeder extends Seeder
{

    public static function createOrGetHazardType($class, string $name, string $key, $colorStr = null){
        $type = HazardType::findByName($name)->first();
        if(is_null($type)) $type = new HazardType();
        $type->name = $name;
        $type->class()->associate($class);
        $type->key = $key;
        if(!is_null($colorStr)) $type->color = ColorIntConverter::strToInt($colorStr);
        $type->save();
        return $type;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            HazardClassesSeeder::class,
        ]);

        $data = [
            ['class' => "Ледовые", 'name' => "Донная экзарация", 'key' => "extraction", 'color' => "65655EFF"],
            ['class' => "Ледовые", 'name' => "Намерзание и термоабразия", 'key' => "thermal_abrasion", 'color' => "7D80DAFF"],
            ['class' => "Ледовые", 'name' => "Ледовый разнос", 'key' => "ice_rafting", 'color' => "B0A3D4FF"],
            ['class' => "Ледовые", 'name' => "Локальные неровности дна", 'key' => "curve_seabottom", 'color' => "CFFFBAFF"],
            ['class' => "Геоморфологические", 'name' => "Литодинамические", 'key' => "lythodynamic", 'color' => "CEBACFFF"],
            ['class' => "Геоморфологические", 'name' => "Гравитационные", 'key' => "gravity", 'color' => "C6AFB1FF"],
            ['class' => "Геоморфологические", 'name' => "Морфологические", 'key' => "morphological", 'color' => "FF6F59FF"],
            ['class' => "Гидрологические", 'name' => "Волновые", 'key' => "wave", 'color' => "EF3054FF"],
            ['class' => "Гидрологические", 'name' => "Приливные", 'key' => "tidal", 'color' => "170312FF"],
            ['class' => "Гидрологические", 'name' => "Температурные", 'key' => "hydrological_temperature", 'color' => "33032FFF"],
            ['class' => "Гидрологические", 'name' => "Цунами", 'key' => "tsunami", 'color' => "531253FF"],
            ['class' => "Атмосферные", 'name' => "Ветровые", 'key' => "wind", 'color' => "FB6107FF"],
            ['class' => "Атмосферные", 'name' => "Снего-дождевые", 'key' => "rainfall_and_snowfall", 'color' => "F3DE2CFF"],
            ['class' => "Атмосферные", 'name' => "Температурные", 'key' => "atmospheric_temperature", 'color' => "7CB518FF"],
            ['class' => "Атмосферные", 'name' => "Электромагнитные", 'key' => "electromagnetics", 'color' => "5C8001FF"],
            ['class' => "Геокриогенные и посткриогенные", 'name' => "Мерзлотные", 'key' => "permafrost", 'color' => "FBB02DFF"],
            ['class' => "Геокриогенные и посткриогенные", 'name' => "Постмерзлотные", 'key' => "post-perfmafrost", 'color' => "1B065EFF"],
            ['class' => "Геокриогенные и посткриогенные", 'name' => "Криогенные и посткриогенные образования", 'key' => "cryogenic_soil", 'color' => "BAF2FFFF"],
            ['class' => "Геологические", 'name' => "Валунные отложения", 'key' => "boulder_beds", 'color' => "FF47DAFF"],
            ['class' => "Геологические", 'name' => "Слабые глинистые отложения или грунты со специфическими свойствами", 'key' => "soft_sediments", 'color' => "FF87ABFF"],
            ['class' => "Геологические", 'name' => "Инверсионное или неоднородное строение грунтовой толщи", 'key' => "inhomogeneous", 'color' => "AA4465FF"],
            ['class' => "Геологические", 'name' => "Специфические по составу скальные грунты", 'key' => "rock_soils_specific_in_composition", 'color' => "2D3142FF"],
            ['class' => "Геологические", 'name' => "Специфические по строению, структуре скальные грунты", 'key' => "structure-specific_rock_soils", 'color' => "27187EFF"],
            ['class' => "Геологические", 'name' => "Отложения, заполняющие палеоврезы и палеодолины", 'key' => "buried_paleovalleys_beds", 'color' => "758BFDFF"],
            ['class' => "Геологические", 'name' => "Смена литологии донных грунтов", 'key' => "seabottom_litochange", 'color' => "FFB8FEFF"],
            ['class' => "Флюидогенные", 'name' => "Мелкозалегающий газ", 'key' => "shallow_gas", 'color' => "AEB8FEFF"],
            ['class' => "Флюидогенные", 'name' => "Глубинный газ", 'key' => "deep-lying_gas", 'color' => "F1F2F6FF"],
            ['class' => "Флюидогенные", 'name' => "АВПД флюида", 'key' => "AHRP", 'color' => "FF8600FF"],
            ['class' => "Флюидогенные", 'name' => "Газогидраты и кристаллогидраты", 'key' => "clathrates", 'color' => "84E296FF"],
            ['class' => "Флюидогенные", 'name' => "Газонасыщенные осадки", 'key' => "gas_shows", 'color' => "4DCCBDFF"],
            ['class' => "Флюидогенные", 'name' => "Зона фокусированной разгрузки флюида", 'key' => "fluid_flowout", 'color' => "FFFEABAFF"],
            ['class' => "Сейсмотектонические", 'name' => "Землетрясения", 'key' => "earthquakes", 'color' => "FF8484FF"],
            ['class' => "Сейсмотектонические", 'name' => "Разрывные нарушения", 'key' => "warping_movements", 'color' => "FF5D73FF"],
            ['class' => "Техногенные", 'name' => "Захороненные опасные объекты", 'key' => "buried_dangerous", 'color' => "9F7E69FF"],
            ['class' => "Техногенные", 'name' => "Тепловое и температурное воздействие", 'key' => "thermal_effect", 'color' => "23CE6BFF"],
            ['class' => "Техногенные", 'name' => "Механическое воздействие", 'key' => "mechanical_action", 'color' => "898952FF"],
            ['class' => "Техногенные", 'name' => "Химическое воздействие", 'key' => "chemical_action", 'color' => "FFAD69FF"],
        ];

        foreach($data as $typeData){
            $class = HazardClass::findByName($typeData['class'])->first();
            if(is_null($class)){
                echo('Unknown hazard class "'.$typeData['class'].'". Skipping "'.$typeData['name']."\" type\n");
                continue;
            }
            static::createOrGetHazardType($class, $typeData['name'], $typeData['key'], $typeData['color']);
        }
    }
}
