<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MosaicType;
use App\Helpers\ColorIntConverter;

class MosaicTypesSeeder extends Seeder
{

    private function createOrGetType($name, $key, $colorStr = null){
        $mosaicTypeQuery = MosaicType::where('key', $key);
        if($mosaicTypeQuery->count()){
            $mosaicType = $mosaicTypeQuery->first();
        }else{
            $mosaicType = new MosaicType(['name' => $name, 'key' => $key]);
            if(!is_null($colorStr)) $mosaicType->map_color = ColorIntConverter::strToInt($colorStr);
            $mosaicType->save();
        }
        return $mosaicType;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOrGetType('ГЛБО', 'sss', '#36A2EBFF');
        $this->createOrGetType('МЛЭ', 'mbes', '#FF6384FF');
        $this->createOrGetType('МЛЭ+ГЛБО', 'sssmbes', '#AF8DD3FF');
    }
}
