<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SurveyType;
use App\Models\Vessel;
use App\Models\Tool;
use App\Models\Trip;
use App\Models\SssMbesSurvey;
use App\Models\Zone;
use App\Helpers\CsvReader;

use Illuminate\Support\Facades\Storage;

class SssMbesSurveysSeeder2 extends Seeder
{

    public static function createOrGetSurvey(string $name, $type, $zone, $date, $direction, $vessel, $tool, $area, $filePath, $fileSize, $archPath, $archSize){
        $survey = SssMbesSurvey::findByName($name)->first();
        if(is_null($survey)) $survey = new SssMbesSurvey();

        $survey->name = $name;

        $survey->type()->associate($type);
        $survey->parent()->associate($zone);
        $survey->start_date = $date;
        $survey->direction = $direction;

        if(is_null($survey->trip)){
            $trip = new Trip();
            $trip->vessel()->associate($vessel);
            $trip->save();
            $survey->trip()->associate($trip);
        } else {
            $survey->trip->vessel()->associate($vessel);
            $survey->trip->save();
        }
        
        $survey->tool()->associate($tool);

        $survey->area = $area;

        $survey->arch_path = $archPath;
        $survey->arch_size = $archSize;

        $survey->file_path = $filePath;
        $survey->file_size = $fileSize;
        
        $survey->save();
    }


    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    private static function parsePolygon(string $polygonStr, $sep, $invert = false){
        $polygonStr = \str_replace($sep, " ", trim($polygonStr));
        $polygonStr = \str_replace("\r", "", $polygonStr);
        $points = explode("\n", $polygonStr);
        if($points[0] != $points[count($points)-1]){
            array_push($points, $points[0]);
        }
        if($invert){
            for($i=0;$i<count($points);$i+=1){
                $point = explode(" ",$points[$i]);
                $points[$i] = $point[1]." ".$point[0];
            }
        }
        // print_r($points);
        $pp = implode(",", $points);
        $pp = sprintf("ST_GeometryFromText('POLYGON((%s))')", $pp);
        // echo($pp."\n");
        return \DB::raw($pp);
    }


    private static function extractZone(string $path){
        $codes = preg_split('/(\/|\\|)/', $path);
        $index = 1;
        $zone = Zone::query()->root()->byCode($codes[0])->first();
        if(is_null($zone)){
            $index = 2;
            $zone = Zone::query()->root()->byCode($codes[1])->first();
            if(is_null($zone)){
                echo("Failed code ".$codes[1]."\n");
                return null;
            }
        }

        for(;$index < count($codes); ++$index){
            if(preg_match("/^L\d+$/",$codes[$index])) break;
            $zone = $zone->children()->byCode($codes[$index])->first();
            if(is_null($zone)) break;
        }
        
        return $zone;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ZonesSeeder::class,
            TestToolsSeeder::class,
            TestVesselsSeeder::class,
            SurveyTypesSeeder::class
        ]);

        $surveysData = CsvReader::read(dirname(__FILE__).'/sssmbes_surveys2.csv', ',');

        $dataFolder = Storage::disk('data')->path(\Config::get('app.data_folder'));
        foreach($surveysData as $survData){            
            $surveyPath = static::joinPaths($survData['path'], $survData['survey file']);
            $fullSurveyPath = static::joinPaths($dataFolder, $surveyPath);
            if(!\file_exists($fullSurveyPath)){
                echo("Survey file doesn't exist! Skipping \"".$fullSurveyPath."\" survey\n");
                continue;
            }
            $surveySize = filesize($fullSurveyPath);

            // Check existencs of area file
            $pathToArea = static::joinPaths($dataFolder, $survData['path'], $survData['polygon file']);
            if(!\file_exists($pathToArea)){
                echo("Area file doesn't exist! Skipping \"".$survData['name']."\" survey\n");
                continue;
            }

            // Extracting zone
            $zone = static::extractZone($survData['path']);
            if(is_null($zone)){
                echo($survData['path']."\n");
                echo("Failed to find zone! Skipping \"".$survData['name']."\" survey\n");
                continue;
            }

            $polygon = static::parsePolygon(\file_get_contents($pathToArea), ",", true);

            // Check existens of archive file and get its size
            $archPath = null;
            $archSize = null;
            if(!empty($survData['arch file'])){
                $archPath = static::joinPaths($dataFolder, $survData['arch file']);
                $fullArchPath = static::joinPaths($dataFolder, $archPath);
                if(!\file_exists($fullArchPath)){
                    echo("Path to archive is incorrect! Skipping \"".$survData['name']."\" survey\n");
                    continue;
                }
                $archSize = filesize($fullArchPath);
            }

            $type = SurveyType::findByName($survData["type"])->first();

            $vessel = Vessel::findByName($survData["vessel"])->first();
            $tool = Tool::findByName($survData["tool"])->first();
            static::createOrGetSurvey($survData["name"], $type, $zone, $survData["date"], $survData["direction"], $vessel, $tool, $polygon, $surveyPath, $surveySize, $archPath, $archSize);
        }
    }
}
