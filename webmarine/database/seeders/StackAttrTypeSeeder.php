<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Helpers\CsvReader;

use App\Models\SeismicStackAttrType;

class StackAttrTypeSeeder extends Seeder
{

    public static function createOrGetStackAttrType(string $name){
        $hat = SeismicStackAttrType::findByName($name)->first();
        if(is_null($hat)) $hat = new SeismicStackAttrType();

        $hat->name = $name;
        $hat->save();

        return $hat;
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $satsData = CsvReader::read(dirname(__FILE__).'/stack_attr_types.csv', ',');
        foreach($satsData as $satData){
            static::createOrGetStackAttrType($satData['name']);
        }
    }
}
