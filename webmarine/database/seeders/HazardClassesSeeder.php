<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\HazardClass;

class HazardClassesSeeder extends Seeder
{

    public static function createOrGetHazardClass(string $name){
        $class = HazardClass::findByName($name)->first();
        if(is_null($class)) $class = new HazardClass();
        $class->name = $name;
        $class->save();
        return $class;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Ледовые'],
            ['name' => 'Геоморфологические'],
            ['name' => 'Гидрологические'],
            ['name' => 'Атмосферные'],
            ['name' => 'Геокриогенные и посткриогенные'],
            ['name' => 'Геологические'],
            ['name' => 'Флюидогенные'],
            ['name' => 'Сейсмотектонические'],
            ['name' => 'Техногенные'],
        ];

        foreach($data as $classData){
            static::createOrGetHazardClass($classData['name']);
        }
    }
}
