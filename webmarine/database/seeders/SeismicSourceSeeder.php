<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SeismicSourceType;
use App\Models\SeismicSource;

use App\Helpers\FK;

class SeismicSourceSeeder extends Seeder
{
    private static function isEqualTypes($type1, $type2){
        if(is_integer($type1) && is_integer($type2)){
            return $type1 == $type2;
        }

        if(\is_integer($type1) && $type2 instanceof SeismicSourceType){
            return $type1 == $type2->id;
        }

        if($type1 instanceof SeismicSourceType && $type2 instanceof SeismicSourceType){
            return $type1->id == $type2->id;
        }

        throw new Exception('Incorrect type of input arguments $type1 and $type2 have to be integers or instances of SeismicSourceType');
    }

    public static function createOrGetSeismicSource($name, $type){
        $lowName = mb_strtolower($name);
        $seismicQuery = SeismicSource::whereRaw('lower(name) like (?)', ["$lowName"]);
        if($seismicQuery->count()){
            $source = $seismicQuery->first();
            if($source->name != $name || !static::isEqualTypes($source->type, $type)){
                echo('Updating "'.$source->name.'" source!\n');
                $source->name = $name;
                $source->type()->associate($type);
                $source->save();
            }
        } else {
            $source = new SeismicSource();
            $source->name = $name;
            $source->type()->associate($type);
            $source->save();
        }
        return $source;
    }

    private static function readCsv(string $csvFile, string $sep = ","){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 10000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][mb_strtolower($header[$c])] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            SeismicSourceTypesSeeder::class
        ]);

        $streamersData = static::readCsv(dirname(__FILE__).'/seismic_sources.csv');

        foreach($streamersData as $streamerData){
            $sourceType = SeismicSourceType::findByName($streamerData['type'])->first();
            if(is_null($sourceType)){
                echo('Unknown source type "'.$streamerData['type'].'". Skipping "'.$streamerData['name']."\" source\n");
                continue;
            }
            // echo($streamerData['name']."\n");
            static::createOrGetSeismicSource($streamerData['name'], $sourceType);
        }
    }
}
