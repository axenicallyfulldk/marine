<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SeismicSourceType;

class TestSeismicSourceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'Бумер'],
            ['name' => 'Элекроискровые'],
            ['name' => 'Высокоразрешающая сейсморазведка'],
            ['name' => 'Сверхвысокоразрешающая сейсморазведка'],
            ['name' => 'Кувалда'],
        ];

        foreach($types as $type){
            if(SeismicSourceType::where('name', $type['name'])->count() == 0){
                $sft = new SeismicSourceType();
                $sft->name = $type['name'];
                $sft->save();
            }
        }
    }

    
}
