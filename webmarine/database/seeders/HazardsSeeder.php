<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\HazardType;

use App\Helpers\CsvReader;

class HazardsSeeder extends Seeder
{

    public static function addHazard($name, $typeKey, $wkt){
        $hazardType = HazardType::findByKey($typeKey)->first();
        if(is_null($hazardType)){
            return null;
        }

        $hazard = new ($hazardType->getModel())();
        $hazard->name = $name;
        $hazard->type()->associate($hazardType);
        $newWkt = static::checkWktAndReturn($wkt);
        // echo($newWkt."\n");
        $hazard->shape = \DB::raw("'".$newWkt."'");
        $hazard->save();

        return $hazard;
    }

    private static function checkWktAndReturn($wkt){
        if(\str_starts_with($wkt, "POLYGON")){
            // echo("It is polygon!\n");
            $start = substr($wkt, strpos($wkt, "((")+2);
            // echo("Start: ".$start."\n\n");
            $pointsStr = substr($start, 0, strpos($start, "))"));
            // echo("Points: ".$pointsStr."\n\n");
            $points = explode(",", $pointsStr);
            if($points[0] == end($points)) return $wkt;

            // echo("not equal\n");

            array_push($points, $points[0]);

            $newPointsStr = \implode(",", $points);

            return "POLYGON ((".$newPointsStr."))";
        } else {
            return $wkt;
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            HazardTypesSeeder::class,
        ]);

        $csvFilename = '/home/aggravator/Documents/WebMarine/others/outed2/hazards.csv';

        $hazards = CsvReader::read($csvFilename);

        foreach($hazards as $hazard){
            static::addHazard($hazard['name'], $hazard['type'], $hazard['shape']);
        }
    }
}
