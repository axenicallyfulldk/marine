import sys, argparse, pathlib, os, re

def flushHorizon(outputFile, horizonName, segyName, points):
    if (horizonName is None) or (segyName is None):
        return
    pointStr = '[['+'],['.join([','.join(map(str,i)) for i in points])+']]'
    outputFile.write('{};{};{}\n'.format(horizonName, segyName, pointStr))

horizonRegexp = r'^PROFILE\s+([\w\-\d]+)\s'
pointRegexp1 = r'^\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([a-zA-Z]\w+)\s+$'
pointRegexp2 = r'^\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([a-zA-Z]\w+)\s+$'


parser = argparse.ArgumentParser(description='Process horizon files.')
parser.add_argument('-f', type = pathlib.Path, default = "horizons", help='path to folder with fault files')
parser.add_argument('-o', type = argparse.FileType('w'), default = 'horizons.csv', help = 'output file')

args = parser.parse_args()

horizonFolderPath = args.f

outputFile = args.o

outputFile.write('Horizon;Segy;Points\n')
faultFiles = os.listdir(horizonFolderPath)
for ff in faultFiles:
    fullFaultPath = os.path.join(horizonFolderPath, ff)
    faultName = None
    segyName = None
    points = []

    for line in open(fullFaultPath, "r").readlines():
        headerMatch = re.search(horizonRegexp, line)
        if headerMatch :
            flushHorizon(outputFile, faultName, segyName, points)
            faultName = headerMatch.group(1)
            segyName = None
            points = []
            continue

        dataMatch = re.search(pointRegexp1, line)
        if dataMatch:
            time = dataMatch.group(3)
            trace = dataMatch.group(5)
            newSegyName = dataMatch.group(10)
            if newSegyName != segyName :
                flushHorizon(outputFile, faultName, segyName, points)
                segyName = newSegyName
                points = [[float(trace), float(time)]]
                continue
            else:
                points.append([float(trace), float(time)])
        else:
            dataMatch = re.search(pointRegexp2, line)
            if dataMatch:
                time = dataMatch.group(4)
                trace = dataMatch.group(6)
                newSegyName = dataMatch.group(11)
                if newSegyName != segyName :
                    flushHorizon(outputFile, faultName, segyName, points)
                    segyName = newSegyName
                    points = [[float(trace), float(time)]]
                    continue
                else:
                    points.append([float(trace), float(time)])
            
