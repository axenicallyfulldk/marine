<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SeismicSourceType;

class SeismicSourceTypesSeeder extends Seeder
{

    public static function createOrGetType($name){
        $query = SeismicSourceType::findByName($name);
        if($query->count()){
            $type = $query->first();
        }else{
            $type = new SeismicSourceType(['name' => $name]);
            $type->save();
        }
        return $type;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        static::createOrGetType("электроискровой");
        static::createOrGetType("электродинамический");
        static::createOrGetType("пневматический");
    }
}
