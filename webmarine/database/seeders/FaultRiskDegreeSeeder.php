<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FaultRiskDegree;

class FaultRiskDegreeSeeder extends Seeder
{
    private static function createOrGetFaultRiskDegree($riskName, $minScore, $maxScore){
        $lowerRiskName = mb_strtolower($riskName);
        $frdQuery = FaultRiskDegree::findByName($lowerRiskName);
        if($frdQuery->count()){
            $frd = $frdQuery->first();
        }else{
            $frd = new FaultRiskDegree([
                'name' => $riskName,
            ]);
            $frd->save();
        }
        $frd->min = $minScore;
        $frd->max = $maxScore;
        $frd->save();
        return $frd;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faultRiskDegrees = [
            ['name' => 'незначительная', 'min' => 0, 'max' => 2],
            ['name' => 'низкая', 'min' => 3, 'max' => 4],
            ['name' => 'средняя', 'min' => 5, 'max' => 8],
            ['name' => 'высокая', 'min' => 9, 'max' => 42],
        ];

        foreach($faultRiskDegrees as $frd){
            $this->createOrGetFaultRiskDegree($frd['name'], $frd['min'], $frd['max']);
        }
    }
}
