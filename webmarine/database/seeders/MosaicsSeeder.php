<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Mosaic;
use App\Models\MosaicType;
use App\Models\Vessel;
use App\Models\Tool;
use App\Models\Trip;
use App\Models\Zone;
use App\Helpers\CsvReader;

use Illuminate\Support\Facades\Storage;

class MosaicsSeeder extends Seeder
{
    public static function createOrGetMosaic(string $name, $type, $zone, $date, $direction, $tool, $area, $filePath, $fileSize, $archPath, $archSize){
        $mosaic = Mosaic::findByName($name)->first();
        if(is_null($mosaic)) $mosaic = new Mosaic();

        $mosaic->name = $name;

        $mosaic->type()->associate($type);
        $mosaic->parent()->associate($zone);
        $mosaic->creation_date = $date;
        $mosaic->input_direction = $direction;
        
        $mosaic->mainTool()->associate($tool);

        $mosaic->area = $area;

        $mosaic->arch_path = $archPath;
        $mosaic->arch_size = $archSize;

        $mosaic->file_path = $filePath;
        $mosaic->file_size = $fileSize;
        
        $mosaic->save();
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    private static function parsePolygon(string $polygonStr, $sep){
        $polygonStr = \str_replace($sep, " ", trim($polygonStr));
        $polygonStr = \str_replace("\r", "", $polygonStr);
        $points = explode("\n", $polygonStr);
        if($points[0] != $points[count($points)-1]){
            array_push($points, $points[0]);
        }
        $pp = implode(",", $points);
        $pp = sprintf("ST_GeometryFromText('POLYGON((%s))')", $pp);
        return \DB::raw($pp);
    }


    private static function extractZone(string $path){
        $codes = preg_split('/(\/|\\|)/', $path);
        $index = 1;
        $zone = Zone::query()->root()->byCode($codes[0])->first();
        if(is_null($zone)){
            $index = 2;
            $zone = Zone::query()->root()->byCode($codes[1])->first();
            if(is_null($zone)){
                echo("Failed code ".$codes[1]."\n");
                return null;
            }
        }

        for(;$index < count($codes); ++$index){
            if(preg_match("/^L\d+$/",$codes[$index])) break;
            $zone = $zone->children()->byCode($codes[$index])->first();
            if(is_null($zone)) break;
        }
        
        return $zone;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TestToolsSeeder::class,
            ZonesSeeder::class,
            MosaicTypesSeeder::class
        ]);

        $mosaicsData = CsvReader::read(dirname(__FILE__).'/mosaics.csv', ',');

        $dataFolder = Storage::disk('data')->path(\Config::get('app.data_folder'));
        foreach($mosaicsData as $mosaicData){            
            $mosaicPath = static::joinPaths($mosaicData['path'], $mosaicData['mosaic file']);
            $fullMosaicPath = static::joinPaths($dataFolder, $mosaicPath);
            if(!\file_exists($fullMosaicPath)){
                echo("Mosaic file doesn't exist! Skipping \"".$fullMosaicPath."\" mosaic\n");
                continue;
            }
            $surveySize = filesize($fullMosaicPath);

            // Check existencs of area file
            $pathToArea = static::joinPaths($dataFolder, $mosaicData['path'], $mosaicData['polygon file']);
            if(!\file_exists($pathToArea)){
                echo("Area file doesn't exist! Skipping \"".$mosaicData['name']."\" survey\n");
                continue;
            }

            $polygon = static::parsePolygon(\file_get_contents($pathToArea), ",");

            // Extracting zone
            $zone = static::extractZone($mosaicData['path']);
            if(is_null($zone)){
                echo($mosaicData['path']."\n");
                echo("Failed to find zone! Skipping \"".$mosaicData['name']."\" survey\n");
                continue;
            }

            // Check existens of archive file and get its size
            $archPath = null;
            $archSize = null;
            if(!empty($mosaicData['arch file'])){
                $archPath = static::joinPaths($mosaicData['path'], $mosaicData['arch file']);
                $fullArchPath = static::joinPaths($dataFolder, $archPath);
                if(!\file_exists($fullArchPath)){
                    echo("Path to archive is incorrect! Skipping \"".$mosaicData['name']."\" mosaic\n");
                    continue;
                }
                $archSize = filesize($fullArchPath);
            }

            $type = MosaicType::findByName($mosaicData["type"])->first();

            $tool = Tool::findByName($mosaicData["tool"])->first();
            static::createOrGetMosaic($mosaicData["name"], $type, $zone, $mosaicData["date"], $mosaicData["direction"], $tool, $polygon, $mosaicPath, $surveySize, $archPath, $archSize);
        }
    }
}
