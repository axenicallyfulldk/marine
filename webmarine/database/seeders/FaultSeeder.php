<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Fault;
use App\Models\FaultType;
use App\Models\Seismic2dStack;
use App\Models\Seismic2dFault;

use App\Helpers\FK;

class FaultSeeder extends Seeder
{

    public static function createOrGetFault($faultName, $segyStack, $points){
        $lowName = mb_strtolower($faultName);
        $fault = Fault::findByName($lowName)->first();
        if(is_null($fault)){
            $fault = new Fault();
            $fault->name = $faultName;
            $fault->save();
        }
        
        $proj = $fault->stacks2d()->where(FK::get(\App\Tables::SEISMIC2D_STACK_TABLE))->first();
        if(is_null($proj)){
            $proj = new Seismic2dFault();
            $proj->stack()->associate($segyStack);
            $proj->fault()->associate($fault);
        }
        // $proj->projection = \json_decode($points);
        $proj->coords = $points;
        $proj->save();
        return $fault;
    }

    private static function readCsv(string $csvFile, string $sep = ";"){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 100000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 100000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][mb_strtolower($header[$c])] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FaultTypesSeeder::class,
            FaultActivityDegreeSeeder::class,
            FaultRiskDegreeSeeder::class,
            // Seismic2dStackSeeder::class
        ]);

        $faultsData = static::readCsv(dirname(__FILE__).'/faults.csv');

        foreach($faultsData as $faultData){
            $segyNameLower = \mb_strtolower($faultData['segy']);
            $stack = Seismic2dStack::findByName($segyNameLower)->first();
            if(is_null($stack)){
                $stack = Seismic2dStack::findByAlias($segyNameLower)->first();
            }
            
            if(is_null($stack)){
                echo('Unknown stack "'.$faultData['segy'].'". Skipping "'.$faultData['fault']."\" fault for this stack\n");
                continue;
            }
            static::createOrGetFault($faultData['fault'], $stack, $faultData['points']);
        }
    }
}
