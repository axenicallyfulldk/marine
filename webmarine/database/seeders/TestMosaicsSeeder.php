<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Zone;
use App\Models\Mosaic;
use App\Models\MosaicType;
use App\Models\MosaicArchFile;

use App\Helpers\FK;

class TestMosaicsSeeder extends Seeder
{

    private static function createOrGetMosaic($name, $type, $direction, $zoneId, $toolId, $date, $area){
        $loweredName = mb_strtolower($name);
        $mosaicQuery = Mosaic::whereRaw('lower(name) like (?)', ["$loweredName"]);
        if($mosaicQuery->count()){
            $mosaic = $mosaicQuery->first();
        } else {
            $mosaic = new Mosaic([
                'name' => $name,
                'area' => $area,
                'input_direction' => $direction,
                FK::get(\App\Tables::MOSAIC_TYPE_TABLE) => $type,
                'main_tool_id' => $toolId,
                'creation_date' => $date,
                FK::get(\App\Tables::ZONE_TABLE) => $zoneId,
                'file_path' => 'file/path',
                'file_size' => static::randomMosaicFileSize(),
                'arch_path' => 'arch/path.rar',
                'arch_size' => static::randomMosaicArchSize(),
            ]);
            $mosaic->save();

            $faker = \Faker\Factory::create();
            
            for($i = 0; $i < $faker->numberBetween(1,4); $i+=1){
                $file = new MosaicArchFile([
                    FK::get(\App\Tables::MOSAIC_TABLE) => $mosaic->id,
                    'name' => $faker->words($nb = 1, true),
                    'description' => $faker->sentence(4, true)
                ]);
                $file->save();
            }
        }
        return $mosaic;
    }


    private static function readCsv(string $csvFile){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, ",");
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][$header[$c]] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }

    private static function parsePolygon(string $polygonStr){
        $points = explode(",", $polygonStr);
        if($points[0] != $points[count($points)-1]){
            array_push($points, $points[0]);
        }
        return \DB::raw('ST_GeometryFromText(\'POLYGON(('.implode(",",$points).'))\')');
    }

    private static function randomMosaicFileSize(){
        $faker = \Faker\Factory::create();
        $minSize = 40*1024*1024; // Min size is 40 MB
        $maxSize = 20*1024*1024*1024; // Max size is 20 GB
        return $faker->numberBetween($minSize,$maxSize);
    }


    private static function randomMosaicArchSize(){
        $faker = \Faker\Factory::create();
        $minSize = 40*1024*1024; // Min size is 40 MB
        $maxSize = 20*1024*1024*1024; // Max size is 4 GB
        return $faker->numberBetween($minSize,$maxSize);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TestToolsSeeder::class,
            ZonesSeeder::class,
            MosaicTypesSeeder::class
        ]);

        $greatSalma = Zone::whereRaw('lower(name) like (?)',["%салма%"])->first();

        $mosaicsData = static::readCsv(dirname(__FILE__).'/velikaya_salma_mosaic_data.csv');

        $sssType = MosaicType::where('key','sss')->first();
        $mbesType = MosaicType::where('key', 'mbes')->first();

        $tools = \App\Models\Tool::all()->toArray();

        foreach($mosaicsData as $mosaicData){
            $name = $mosaicData['name'];
            // Parse mosaic type
            $mosaicType = NULL;
            switch(mb_strtolower($mosaicData['mosaic_type'])){
                case "глбо":
                    $mosaicType = $sssType;
                break;
                case "млэ":
                    $mosaicType = $mbesType;
                break;
            }
            // Parse direction
            $direction = in_array(\mb_strtoupper($mosaicData['direction']), ["L", "X", "XL"]) ? \mb_strtoupper($mosaicData['direction']) : NULL;
            $direction = str_replace('XL', 'D', $direction);

            $date = new \DateTime($mosaicData['date']);

            $area = static::parsePolygon($mosaicData['polygon']);

            if(is_null($name) || is_null($mosaicType) || is_null($direction) || is_null($date) || is_null($area)){
                print($name." mosaic has been skipped\n");
                continue;
            }

            $toolId = $tools[array_rand($tools)]["id"];
            
            static::createOrGetMosaic($name, $mosaicType->id, $direction, $greatSalma->id, $toolId, $date, $area);
        }
    }
}
