<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SurveyType;
use App\Helpers\ColorIntConverter;

class SurveyTypesSeeder extends Seeder
{

    private function createOrGetSurveyType($name, $key, $colorStr = null){
        $surveyTypeQuery = SurveyType::where('key', $key);
        if($surveyTypeQuery->count()){
            $surveyType = $surveyTypeQuery->first();
        }else{
            $surveyType = new SurveyType(['name' => $name, 'key' => $key]);
            if(!is_null($colorStr)) $surveyType->map_color = ColorIntConverter::strToInt($colorStr);
            $surveyType->save();
        }
        return $surveyType;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOrGetSurveyType('ГЛБО', 'sss', '#36A2EBFF');
        $this->createOrGetSurveyType('МЛЭ', 'mbes', '#FF6384FF');
    }
}
