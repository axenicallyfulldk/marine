<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

use App\Models\Trip;
use App\Models\Seismic3dSurvey;
use App\Models\Seismic3dStack;
use App\Models\SeismicSource;
use App\Models\SeismicStreamer;
use App\Models\SeismicResolutionClass;
use App\Models\Zone;
use App\Models\Field;
use App\Models\Vessel;

use App\Helpers\LatLongDistance;

use App\Helpers\SegyFile;

class Seismic3dStackSeeder extends Seeder
{

    private static function createOrGetSurvey($name, $alias, $area, $direction,
        $archPath, $archSize, $zone, $field, $vessel, $source, $srcGroupCount, $srcPerGroup,
        $streamer, $streamersCount, $distBetwennStreamers,  $isTowed, $resolClass, $startDate, $customer, $operator){
            $survey = Seismic3dSurvey::findByName(\mb_strtolower($name))->first();
            if(is_null($survey)){
                $survey = new Seismic3dSurvey(['name'=>$name]);
            }

            $survey->alias = $alias;
            $survey->area = $area;
            $survey->direction = $direction;
            $survey->arch_path = $archPath;
            $survey->arch_size = $archSize;
            $survey->zone()->associate($zone);
            $survey->field()->associate($field);

            if(is_null($survey->trip)){
                $trip = new Trip();
                $trip->vessel()->associate($vessel);
                $trip->save();
                $survey->trip()->associate($trip);
            }
            $survey->trip->vessel()->associate($vessel);
            $survey->trip->save();

            $survey->source()->associate($source);
            $survey->src_group_count = $srcGroupCount;
            $survey->src_per_group = $srcPerGroup;

            $survey->streamer()->associate($streamer);
            $survey->dist_between_streamers = $distBetwennStreamers;
            $survey->streamer_count = $streamersCount;
            $survey->is_towed = $isTowed;

            $survey->resolution()->associate($resolClass);

            $survey->start_date = $startDate;

            $survey->customer = $customer;
            $survey->operator = $operator;

            $survey->save();

            return $survey;
    }

    private static function createOrGetStack($name, $alias, $survey, $filePath, $fileSize,
            $isMigrated, $isAmplCorrected, $isDeconvolved,
            $inlineOffset, $inlineSize, $inlineDist, $xlineOffset, $xlineSize, $xlineDist){
        $stack = Seismic3dStack::findByName(\mb_strtolower($name))->first();
        if(is_null($stack)){
            $stack = new Seismic3dStack(['name'=>$name]);
        }

        $stack->name = $name;
        $stack->alias = $alias;
        $stack->survey()->associate($survey);
        $stack->file_path = $filePath;
        $stack->file_size = $fileSize;

        $stack->is_migrated = $isMigrated;
        $stack->is_ampl_corrected = $isAmplCorrected;
        $stack->is_deconvolved = $isDeconvolved;

        $stack->inline_offset = $inlineOffset;
        $stack->inline_size = $inlineSize;
        $stack->inline_distance = $inlineDist;

        $stack->xline_offset = $xlineOffset;
        $stack->xline_size = $xlineSize;
        $stack->xline_distance = $xlineDist;

        $stack->save();
        return $stack;
    }

    private static function readCsv(string $csvFile, string $sep = ','){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 10000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][mb_strtolower($header[$c])] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }


    private static function parseCsvPolyline(string $polylineStr, string $sep = ",", $swapCoords = false){
        $swapFunc = function($pointStr)use($sep){
            $arrr = explode($sep, trim($pointStr, " \n\r\t\0"));
            return [$arrr[1], $arrr[0]];
        };

        $splitFunc = function($pointStr)use($sep){
            $arrr = explode($sep, trim($pointStr, " \n\r\t\0"));
            return [$arrr[0], $arrr[1]];
        };

        if($swapCoords){
            $points = array_map($swapFunc, explode("\n", trim($polylineStr, " \n\r\t\0")));
        }else{
            $points = array_map($splitFunc, explode("\n", trim($polylineStr, " \n\r\t\0")));
        }
        return $points;
    }
    
    private static function parseCsvPolygon(string $polylineStr, string $sep = ",", $swapCoords = false){
        $swapFunc = function($pointStr)use($sep){
            $arrr = explode($sep, trim($pointStr, " \n\r\t\0"));
            return [$arrr[1], $arrr[0]];
        };

        $splitFunc = function($pointStr)use($sep){
            $arrr = explode($sep, trim($pointStr, " \n\r\t\0"));
            return [$arrr[0], $arrr[1]];
        };

        if($swapCoords){
            $points = array_map($swapFunc, explode("\n", trim($polylineStr, " \n\r\t\0")));
        }else{
            $points = array_map($splitFunc, explode("\n", trim($polylineStr, " \n\r\t\0")));
        }

        if(reset($points) != end($points)){
            array_push($points, $points[0]);
        }
        return $points;
    }

    private static function findMissingFields($csvData){
        $missingFields = [];

        static $requiredFields = ['name', 'date', 'direction', 'source',
            'streamer', 'streamer_count', 'distance_between_streamers', 'is_towed', 'src_group_count', 'src_per_group',
            'resolution_class', 'inline_offset', 'inline_size', 'xline_size', 'xline_size', 'path', 'survey_file', 'area_file'];
        $sample = $csvData[0];
        foreach($requiredFields as $rf){
            if(!array_key_exists($rf, $sample)){
                array_push($missingFields, $rf);
            }
        }
        return $missingFields;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    private static function findMinMaxStepTraceFieldValue($segyFile, int $offset, int $size){
        $vals = $segyFile->getUintTraceHeaderFieldValues($offset, $size);
        $diffs = [];
        for($i = 0; $i < count($vals)-1; ++$i){
            array_push($diffs, $vals[$i+1]-$vals[$i]);
        }

        return [min($vals), max($vals), array_sum($diffs)/count($diffs)];
    }

    private static function arr_diff($arr, $func = null){
        $res = [];
        $prevEl = reset($arr);
        $el = next($arr);
        while($el){
            if(is_null($func)){
                array_push($res, $el-$prevEl);
            }else{
                array_push($res, $func($prevEl, $el));
            }
            
            $prevEl = $el;
            $el = next($arr);
        }
        return $res;
    }

    private static function pointDist($p1, $p2){
        return sqrt(($p1[0]-$p2[0])*($p1[0]-$p2[0]) + ($p1[1]-$p2[1])*($p1[1]-$p2[1]));
    }

    private static function getLineDist($inlines, $xlines, $xy){
        $inlineStat = [];
        $xlineStat = [];
        foreach(array_map(null, $inlines, $xlines, $xy) as $ixxy){
            if(!isset($inlineStat[$ixxy[0]])){
                $inlineStat[$ixxy[0]] = [[$ixxy[1], $ixxy[2]]];
            }else{
                array_push($inlineStat[$ixxy[0]], [$ixxy[1], $ixxy[2]]);
            }

            if(!isset($xlineStat[$ixxy[1]])){
                $xlineStat[$ixxy[1]] = [[$ixxy[0], $ixxy[2]]];
            }else{
                array_push($xlineStat[$ixxy[1]], [$ixxy[0], $ixxy[2]]);
            }
        }

        $cmp = function($v1, $v2){
            if($v1 > $v2) return 1;
            if($v2 > $v1) return -1;
            return 0;
        };

        foreach($inlineStat as $v){
            usort($v, $cmp);
        }

        foreach($xlineStat as $v){
            usort($v, $cmp);
        }

        $diff = function($v1, $v2){
            return static::pointDist($v1[1], $v2[1]);
        };

        foreach($inlineStat as $k => $v){
            $inlineStat[$k] = static::arr_diff($v, $diff);
        }

        foreach($xlineStat as $k => $v){
            $xlineStat[$k] = static::arr_diff($v, $diff);
        }

        $minInlineDist = PHP_FLOAT_MAX;
        $minXlineDist = PHP_FLOAT_MAX;
        
        foreach($inlineStat as $k => $v){
            $minInlineDist = min($minInlineDist, min($v));
        }

        foreach($xlineStat as $k => $v){
            if(count($v)) $minXlineDist = min($minXlineDist, min($v));
        }

        return [$minXlineDist, $minInlineDist];
    }

    private static function getInlineXlineInfo($segyFile, int $inlineOffset, int $inlineSize, int $xlineOffset, int $xlineSize){
        $pointsNum = 2000;
        $traceOffset = max(0, $segyFile->getTraceCount() - $pointsNum);

        $inlines1 = $segyFile->getUintTraceHeaderFieldValues($inlineOffset, $inlineSize, 0, $pointsNum);
        $inlines2 = $segyFile->getUintTraceHeaderFieldValues($inlineOffset, $inlineSize, $traceOffset, $pointsNum);
        $inlines = array_merge($inlines1,$inlines2);

        $xlines1 = $segyFile->getUintTraceHeaderFieldValues($xlineOffset, $xlineSize, 0, $pointsNum);
        $xlines2 = $segyFile->getUintTraceHeaderFieldValues($xlineOffset, $xlineSize, $traceOffset, $pointsNum);
        $xlines = array_merge($xlines1,$xlines2);

        $x1 = $segyFile->getUintTraceHeaderFieldValues(73, 4, 0, $pointsNum);
        $x2 = $segyFile->getUintTraceHeaderFieldValues(73, 4, $traceOffset, $pointsNum);

        $y1 = $segyFile->getUintTraceHeaderFieldValues(77, 4, 0, $pointsNum);
        $y2 = $segyFile->getUintTraceHeaderFieldValues(73, 4, $traceOffset, $pointsNum);

        $xy1 = array_map(null, $x1, $y1);
        $xy2 = array_map(null, $x2, $y2);
        $xy = array_merge($xy1, $xy2);

        $inlineMin = min(min($inlines1), min($inlines2));
        $inlineMax = max(max($inlines1), max($inlines2));
        $xlineMin = min(min($xlines1), min($xlines2));
        $xlineMax = max(max($xlines1), max($xlines2));

        $uinlines = array_unique($inlines);
        asort($uinlines);
        $inlineStep = min(static::arr_diff($uinlines));

        $uxlines = array_unique($xlines);
        asort($uxlines);
        $xlineStep = min(static::arr_diff($uxlines));

        $lineDist = static::getLineDist($inlines, $xlines, $xy);

        return [
            'inlineMin' => $inlineMin,
            'inlineMax' => $inlineMax,
            'inlineStep' => $inlineStep,
            'inlineDist' => $lineDist[0],
            'xlineMin' => $xlineMin,
            'xlineMax' => $xlineMax,
            'xlineStep' => $xlineStep,
            'xlineDist' => $lineDist[1],
        ];
    }

    private static function extractZone(string $path){
        $codes = preg_split('/(\/|\\|)/', $path);
        $index = 1;
        $zone = Zone::query()->root()->byCode($codes[0])->first();
        if(is_null($zone)){
            $index = 2;
            $zone = Zone::query()->root()->byCode($codes[1])->first();
            if(is_null($zone)){
                echo("Failed code ".$codes[1]."\n");
                return null;
            }
        }

        for(;$index < count($codes); ++$index){
            if(preg_match("/^L\d+$/",$codes[$index])) break;
            $zone = $zone->children()->byCode($codes[$index])->first();
            if(is_null($zone)) break;
        }
        
        return $zone;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ZonesSeeder::class,
            SeismicSourceSeeder::class,
            StreamerSeeder::class,
            TestVesselsSeeder::class,
            SeismicResolutionClassesSeeder::class,
            FieldSeeder::class
        ]);

        $stacksData = static::readCsv(dirname(__FILE__).'/seismic3d_surveys.csv');

        $missingFields = static::findMissingFields($stacksData);

        if(count($missingFields)){
            echo("Fields: ".implode(",", $missingFields)." are missing!\n");
            return;
        }

        // $dataFolder = \Config::get('app.data_folder');
        // $dataFolder = "/media/aggravator/Seagate Backup Plus Drive/WebMarine/data";
        // $dataFolder = Storage::disk('data')->getAdapter()->getPathPrefix();
        $dataFolder = Storage::disk('data')->path(\Config::get('app.data_folder'));

        foreach($stacksData as $sd){
            // Check that segy file exists and get segy size and trace count
            $segyPath = static::joinPaths($sd['path'], $sd['survey_file']);
            $fullSegyPath = static::joinPaths($dataFolder, $segyPath);
            if(!\file_exists($fullSegyPath)){
                echo("Survey file doesn't exist! Skipping \"".$sd['name']."\" survey\n");
                // echo("Survey file doesn't exist! Skipping \"".$fullSegyPath."\" survey\n");
                continue;
            }
            
            $segySize = filesize($fullSegyPath);
            
            $segyFile = new SegyFile($fullSegyPath, false);
            $traceCount = $segyFile->getTraceCount();

            // Check existencs of area file
            $pathToArea = static::joinPaths($dataFolder, $sd['path'], $sd['area_file']);
            if(!\file_exists($pathToArea)){
                echo("Area file doesn't exist! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            // Check existens of archive file and get its size
            $archPath = null;
            $archSize = null;
            if(!empty($sd['arch_file'])){
                $archPath = static::joinPaths($dataFolder, $sd['arch_file']);
                $fullArchPath = static::joinPaths($dataFolder, $archPath);
                if(!\file_exists($fullArchPath)){
                    echo("Path to archive is incorrect! Skipping \"".$sd['name']."\" survey\n");
                    continue;
                }
                $archSize = filesize($fullArchPath);
            }

            // Get fields
            $name = $sd['name'];
            $alias = !empty($sd['alias']) ? $sd['alias'] : null;
            $date = gmdate("Y-m-d", \strtotime($sd['date']));
            $direction = $sd['direction'];

            // Extracting zone
            $zone = static::extractZone($sd['path']);
            if(is_null($zone)){
                echo($sd['path']."\n");
                echo("Failed to find zone! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            // Extracting polyline
            $polylinePoints = static::parseCsvPolygon(\file_get_contents($pathToArea), ";", true);
            $polyline =  \DB::raw('ST_GeometryFromText(\'POLYGON(('.implode(",",array_map(function($el){return implode(" ",$el);},$polylinePoints)).'))\')');
            
            $field = !empty($sd['field'])  ? Field::findByName(\mb_strtolower($sd['field']))->first() : null;

            // Getting fields relative to seismic source
            $source = SeismicSource::findByName(\mb_strtolower($sd['source']))->first();
            // if(is_null($source)){
            //     echo("Seismic source '".$sd['source']."' is not found! Skipping \"".$sd['name']."\" survey\n");
            //     continue;
            // }
            $srcGroupCount = $sd['src_group_count'];
            $srcPerGroup = $sd['src_per_group'];

            // Getting fields relative to seismic streamer
            $streamer = SeismicStreamer::findByName(\mb_strtolower($sd['streamer']))->first();
            // if(is_null($streamer)){
            //     echo("Seismic streamer is not found! Skipping \"".$sd['name']."\" survey\n");
            //     continue;
            // }
            $streamersCount = !empty($sd['streamer_count']) ? $sd['streamer_count'] : null;
            $distBetwennStreamers = !empty($sd['distance_between_streamers']) ? $sd['distance_between_streamers'] : null;
            $isTowed = $sd['is_towed'];


            $resolClass = SeismicResolutionClass::findByName(\mb_strtolower($sd['resolution_class']))->first();
            if(is_null($resolClass)){
                $resolClass = SeismicResolutionClass::findByKey($sd['resolution_class'])->first();
            }
            // if(is_null($resolClass)){
            //     echo("Resolution class is not found! Skipping \"".$sd['name']."\" survey\n");
            //     continue;
            // }

            $isMigrated = !empty($sd['migrated']) || $sd['migrated'] == '0' ? $sd['migrated'] : null;
            $isAmplCorrected = !empty($sd['ampl_correction']) || $sd['ampl_correction'] == '0' ? $sd['ampl_correction'] : null;
            $isDeconvolved = !empty($sd['deconvolution']) || $sd['deconvolution'] == '0' ? $sd['deconvolution'] : null;

            $vessel = Vessel::findByName(\mb_strtolower($sd['vessel']))->first();
            if(is_null($vessel)){
                echo("Vesel '".$sd['vessel']."' is not found! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            $customer = array_key_exists('customer', $sd) ? $sd['customer'] : null;
            $operator = array_key_exists('operator', $sd) ? $sd['operator'] : null;

            // $inlineMinMaxStep = static::findMinMaxStepTraceFieldValue($segyFile, $sd['inline_offset'], $sd['inline_size']);
            // $xlineMinMaxStep = static::findMinMaxStepTraceFieldValue($segyFile, $sd['xline_offset'], $sd['xline_size']);
            $lineStat = static::getInlineXlineInfo($segyFile, $sd['inline_offset'], $sd['inline_size'], $sd['xline_offset'], $sd['xline_size']);

            $fp = reset($polylinePoints);
            $lp = end($polylinePoints);
            $cdpDist = LatLongDistance::vincentyGreatCircleDistance($fp[1], $fp[0], $lp[1], $lp[0])/($traceCount-1);

            $survey = static::createOrGetSurvey($name, null, $polyline, $direction, $archPath, $archSize, $zone,
                $field, $vessel, $source, $srcGroupCount, $srcPerGroup,
                $streamer, $streamersCount, $distBetwennStreamers, $isTowed, $resolClass, $date, $customer, $operator);

            $stack = static::createOrGetStack($name, $alias, $survey, $segyPath, $segySize,
                $isMigrated, $isAmplCorrected, $isDeconvolved,
                $sd['inline_offset'], $sd['inline_size'], $lineStat['inlineDist'], $sd['xline_offset'], $sd['xline_size'], $lineStat['xlineDist']);
        }
        
    }
}
