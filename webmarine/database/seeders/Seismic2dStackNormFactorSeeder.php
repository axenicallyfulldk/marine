<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Helpers\DataSystem;

use App\Models\Seismic2dStack;

use App\Jobs\CalculateStackNormFactor;

class Seismic2dStackNormFactorSeeder extends Seeder
{

    private $normCalculator;

    private function calculateNorm($stack){
        $path = $stack->getAbsoluteDataFilePath();
        $absPath = DataSystem::getPath($path);
        // $absPath = $stack->getAbsoluteDataFilePath();
        // echo($absPath);
        if(\file_exists($absPath)){
            $output = [];
            $resultCode = 0;

            $execCommand = sprintf("%s -t minmax -s \"%s\"\n", $this->normCalculator, $absPath);
            // echo($execCommand);
            exec($execCommand, $output, $resultCode);
            if($resultCode == 0) {
                return floatval($output[0]);
            }
        }
    }

    private static function joinPaths(...$paths) {
        $fpaths = array_filter($paths, function($el){return $el != null;});
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $fpaths));
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->normCalculator = "python3 ";
        $this->normCalculator .= static::joinPaths(base_path(), "scripts/segy_normalization_calculator.py");

        $stacks = Seismic2dStack::all();
        foreach($stacks as $stack){
            if(is_null($stack->minmax_norm)){
                // CalculateStackNormFactor::dispatch($stack);
                $norm = static::calculateNorm($stack);
                echo("Stack ".$stack->name." has norm ".$norm."\n");
                $stack->minmax_norm = $norm;
                $stack->save();
            }
        }
    }
}
