<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FaultActivityDegree;

class FaultActivityDegreeSeeder extends Seeder
{
    private static function createOrGetFaultActivityDegree($degreeName, $minScore, $maxScore){
        $lowerDegreeName = mb_strtolower($degreeName);
        $fadQuery = FaultActivityDegree::findByName($lowerDegreeName);
        if($fadQuery->count()){
            $fad = $fadQuery->first();
        }else{
            $fad = new FaultActivityDegree([
                'name' => $degreeName,
            ]);
            $fad->save();
        }
        $fad->min = $minScore;
        $fad->max = $maxScore;
        $fad->save();
        return $fad;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faultActivityDegrees = [
            ['name' => 'слабая', 'min' => 1, 'max' => 5],
            ['name' => 'средняя', 'min' => 6, 'max' => 10],
            ['name' => 'повышенная', 'min' => 11, 'max' => 20],
            ['name' => 'высокая', 'min' => 21, 'max' => 30],
            ['name' => 'аномально высокая', 'min' => 31, 'max' => null],
        ];

        foreach($faultActivityDegrees as $fad){
            $this->createOrGetFaultActivityDegree($fad['name'], $fad['min'], $fad['max']);
        }
    }
}
