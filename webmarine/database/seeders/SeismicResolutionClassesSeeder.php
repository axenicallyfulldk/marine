<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SeismicResolutionClass;

class SeismicResolutionClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['name' => 'Unknown', 'key' => 'unknown'],
            ['name' => 'Глубинно сейсмическое зондирование', 'key' => null],
            ['name' => 'Разведочная/нефтегазовая сейсморазведка', 'key' => null],
            ['name' => 'Высокоразрешающая сейсморазведка', 'key' => 'HR'],
            ['name' => 'Сверхвысокоразрешающая сейсморазведка', 'key' => 'VHR'],
            ['name' => 'Ультравысокоразрешающая сейсморазведка', 'key' => 'UHR'],
        ];

        foreach($types as $type){
            $srQuery = SeismicResolutionClass::where('name', $type['name']);
            if($srQuery->count() == 0){
                $sft = new SeismicResolutionClass();
                $sft->name = $type['name'];
                $sft->key = $type['key'];
                $sft->save();
            }else {
                $resType = $srQuery->first();
                if($resType->key != $type['key']){
                    echo("Updating seismic resolution type \"".$resType->name."\"\n");
                    $resType->key = $type['key'];
                    $resType->save();
                }
            }
        }
    }
}
