<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Helpers\CsvReader;

use App\Models\HorizonAttrType;

class HorizonAttrTypeSeeder extends Seeder
{

    public static function createOrGetHorizonAttrType(string $name){
        $hat = HorizonAttrType::findByName($name)->first();
        if(is_null($hat)) $hat = new HorizonAttrType();

        $hat->name = $name;
        $hat->save();

        return $hat;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hatsData = CsvReader::read(dirname(__FILE__).'/horizon_attr_types.csv', ',');
        foreach($hatsData as $hatData){
            static::createOrGetHorizonAttrType($hatData['name']);
        }
    }
}
