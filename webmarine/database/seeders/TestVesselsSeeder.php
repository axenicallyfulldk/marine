<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vessel;

class TestVesselsSeeder extends Seeder
{

    private static function createOrGetVessel($vesselName, $imo = null){
        $lowerVesselName = mb_strtolower($vesselName);
        $vesselQuery = Vessel::whereRaw('lower(name) like (?)', ["%$lowerVesselName%"]);
        if($vesselQuery->count()){
            $vessel = $vesselQuery->first();
        }else{
            $vessel = new Vessel([
                'name' => $vesselName,
                'imo' => $imo
            ]);
            $vessel->save();
        }
        return $vessel;
    }

    private static function readCsv(string $csvFile, string $sep = ","){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 10000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++){
                    $res[$row][$header[$c]] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->createOrGetVessel('Профессор Зенкевич');
        // $this->createOrGetVessel('Профессор Куренцов', '7406136');
        // $this->createOrGetVessel('Диабаз', '8138671');
        // $this->createOrGetVessel('Геофизик', '8138798');

        $streamersData = static::readCsv(dirname(__FILE__).'/vessels.csv');

        foreach($streamersData as $streamerData){
            static::createOrGetVessel($streamerData['name'], $streamerData['imo']);
        }
    }
}
