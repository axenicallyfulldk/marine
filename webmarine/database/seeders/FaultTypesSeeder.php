<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\FaultType;

class FaultTypesSeeder extends Seeder
{

    static public function createOrGetFaultType($faultTypeName){
        $ft = FaultType::findByName(\mb_strtolower($faultTypeName));
        if(is_null($ft)){
            $ft = new FaultType();
            $ft->name = $faultTypeName;
            $ft->save();
        }
        return $ft;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faultTypes = [
            ['name' => 'сброс'],
            ['name' => 'взброс'],
            ['name' => 'сдвиг'],
            ['name' => 'сброс-сдвиг'],
            ['name' => 'взброс-сдвиг'],
            ['name' => 'шарнирный'],
            ['name' => 'лестрический'],
            ['name' => 'цилиндрический']
        ];

        foreach($faultTypes as $faultType){
            static::createOrGetFaultType($faultType['name']);
        }
    }
}
