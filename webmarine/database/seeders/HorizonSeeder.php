<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Horizon;
use App\Models\FaultType;
use App\Models\Seismic2dStack;
use App\Models\Seismic2dHorizon;

use App\Helpers\FK;

class HorizonSeeder extends Seeder
{

    public static function createOrGetHorizon($horizonName, $segyStack, $points){
        $lowName = mb_strtolower($horizonName);
        $horizon = Horizon::findByName($lowName)->first();
        if(is_null($horizon)){
            $horizon = new Horizon();
            $horizon->name = $horizonName;
            $horizon->save();
        }
        
        $proj = $horizon->seismic2dHorizons()->where(FK::get(\App\Tables::SEISMIC2D_STACK_TABLE),$segyStack->id)->first();
        if(is_null($proj)){
            $proj = new Seismic2dHorizon();
            $proj->stack()->associate($segyStack);
            $proj->horizon()->associate($horizon);
        }
        // $proj->projection = \json_decode($points);
        $proj->coords = $points;
        $proj->save();
        return $horizon;
    }

    private static function readCsv(string $csvFile, string $sep = ";"){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 100000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 100000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][mb_strtolower($header[$c])] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // FaultTypesSeeder::class,
            // FaultActivityDegreeSeeder::class,
            // FaultRiskDegreeSeeder::class,
            // Seismic2dStackSeeder::class
        ]);

        $faultsData = static::readCsv(dirname(__FILE__).'/horizons.csv');

        foreach($faultsData as $faultData){
            $segyNameLower = \mb_strtolower($faultData['segy']);
            $stack = Seismic2dStack::findByName($segyNameLower)->first();
            if(is_null($stack)){
                $stack = Seismic2dStack::findByAlias($segyNameLower)->first();
            }
            
            if(is_null($stack)){
                echo('Unknown stack "'.$faultData['segy'].'". Skipping "'.$faultData['horizon']."\" horizon for this stack\n");
                continue;
            }
            static::createOrGetHorizon($faultData['horizon'], $stack, $faultData['points']);
        }
    }
}
