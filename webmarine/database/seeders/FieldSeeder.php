<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Field;

class FieldSeeder extends Seeder
{

    public static function createOrGetField($fieldName){
        $lowerFieldName = mb_strtolower($fieldName);
        $fieldQuery = Field::whereRaw('lower(name) like (?)', ["%$lowerFieldName%"]);
        if($fieldQuery->count()){
            $field = $fieldQuery->first();
        }else{
            $field = new Field([
                'name' => $fieldName
            ]);
            $field->save();
        }
        return $field;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fields = [
            ["name" => "Ленинградское"],
            ["name" => "Айяшское"],
            ["name" => "Лисянский"],
            ["name" => "Ледовое"],
            ["name" => "Демидовский"]
        ];

        foreach($fields as $field){
            static::createOrGetField($field['name']);
        }
    }
}
