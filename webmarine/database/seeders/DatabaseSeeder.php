<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            ZonesSeeder::class,
            SurveyTypesSeeder::class,
            MosaicTypesSeeder::class,
            SeismicSourceTypesSeeder::class,
            SeismicSourceSeeder::class,
            SeismicResolutionClassesSeeder::class,
            UsersSeeder::class,
            HazardTypesSeeder::class,
            // TestSssMbesSurveysSeeder::class,
            // TestMosaicsSeeder::class,

            // SssMbesSurveysSeeder::class,
            // SssMbesSurveysSeeder2::class,
            // MosaicsSeeder::class,
            // MosaicArchSeeder::class,
            // Seismic2dStackSeeder::class,
            // Seismic3dStackSeeder::class,
            // FaultSeeder::class,
            // HorizonSeeder::class,
        ]);
        
    }
}
