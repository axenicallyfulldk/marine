<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Zone;
use App\Models\SurveyType;
use App\Models\SssMbesSurvey;
use App\Models\SssMbesSurveyArchFile;
use App\Models\Trip;
use App\Models\Vessel;
use App\Models\Tool;

use App\Helpers\FK;

class TestSssMbesSurveysSeeder extends Seeder
{

    private static function createOrGetSurvey($name, $survTypeId, $direction, $zoneId, $vesselId, $toolId, $startDate, $polygon){
        $loweredName = mb_strtolower($name);
        $surveyQuery = SssMbesSurvey::whereRaw('lower(name) like (?)', ["$loweredName"]);
        if($surveyQuery->count()){
            $survey = $surveyQuery->first();
        } else {
            $trip = new Trip([FK::get(\App\Tables::VESSEL_TABLE) => $vesselId]);
            $trip->save();

            // $polygon = \DB::raw("'POINT(1 1)'");

            $survey = new SssMbesSurvey([
                'name' => $name,
                'area' => $polygon,
                'direction' => $direction,
                FK::get(\App\Tables::SURVEY_TYPE_TABLE) => $survTypeId,
                'start_date' => $startDate,
                FK::get(\App\Tables::ZONE_TABLE) => $zoneId,
                FK::get(\App\Tables::TRIP_TABLE) => $trip->id,
                FK::get(\App\Tables::TOOL_TABLE) => $toolId,
                'file_path' => 'file/path',
                'file_size' => static::randomSurveyFileSize(),
                'arch_path' => 'arch/path.rar',
                'arch_size' => static::randomSurveyArchSize(),
            ]);
            $survey->save();

            $faker = \Faker\Factory::create();
            
            for($i = 0; $i < $faker->numberBetween(1,4); $i+=1){
                $file = new SssMbesSurveyArchFile([
                    FK::get(\App\Tables::SSSMBES_SURVEY_TABLE) => $survey->id,
                    'name' => $faker->words($nb = 1, true),
                    'description' => $faker->sentence(4, true)
                ]);
                $file->save();
            }
        }
        return $survey;
    }

    private static function readCsv(string $csvFile){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, ",");
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                // echo "<p> $num полей в строке $row: <br /></p>\n";
                for ($c=0; $c < $num; $c++) {
                    $res[$row][$header[$c]] = $data[$c];
                    // echo $data[$c] . "<br />\n";
                }
                $row++;
                
            }
            fclose($handle);
        }
        return $res;
    }

    private static function parsePolygon(string $polygonStr){
        $points = explode(",", $polygonStr);
        if($points[0] != $points[count($points)-1]){
            array_push($points, $points[0]);
        }
        return \DB::raw('ST_GeometryFromText(\'POLYGON(('.implode(",",$points).'))\')');
    }

    private static function randomSurveyFileSize(){
        $faker = \Faker\Factory::create();
        $minSize = 40*1024*1024; // Min size is 40 MB
        $maxSize = 20*1024*1024*1024; // Max size is 20 GB
        return $faker->numberBetween($minSize,$maxSize);
    }


    private static function randomSurveyArchSize(){
        $faker = \Faker\Factory::create();
        $minSize = 40*1024*1024; // Min size is 40 MB
        $maxSize = 20*1024*1024*1024; // Max size is 4 GB
        return $faker->numberBetween($minSize,$maxSize);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ZonesSeeder::class,
            TestToolsSeeder::class,
            TestVesselsSeeder::class,
            SurveyTypesSeeder::class
        ]);

        $sssType = SurveyType::where('key','sss')->first();
        $mbesType = SurveyType::where('key', 'mbes')->first();

        $greatSalma = Zone::whereRaw('lower(name) like (?)',["%салма%"])->first();

        $profZenkevich = Vessel::whereRaw('lower(name) like (?) or lower(name) like (?)', ["%зенкевич%","%зинкевич%"])->first();

        $klein = Tool::where('brand', 'Klein')->first();
        $c3d = Tool::where('brand', 'Benthos')->first();

        $surveysData = static::readCsv(dirname(__FILE__).'/velikaya_salma_survey_data.csv');

        foreach($surveysData as $surveyData){
            $name = $surveyData['name'];
            // Parse survey type
            $surveyType = NULL;
            switch(mb_strtolower($surveyData['survey_type'])){
                case "глбо":
                    $surveyType = $sssType;
                break;
                case "млэ":
                    $surveyType = $mbesType;
                break;
            }
            // Parse direction
            $direction = in_array(\mb_strtoupper($surveyData['direction']), ["L", "X", "XL"]) ? \mb_strtoupper($surveyData['direction']) : NULL;
            $direction = str_replace('XL', 'D', $direction);
            // Parse vessel
            $vessel = NULL;
            if(\str_contains(\mb_strtolower($surveyData['vessel']),'нкевич')) $vessel = $profZenkevich;
            // Parse tool
            $tool = NULL;
            if(\str_contains(mb_strtolower($surveyData['tool']),'klein')) $tool = $klein;
            if(\str_contains(mb_strtolower($surveyData['tool']),'c3d')) $tool = $c3d;

            $date = new \DateTime($surveyData['date']);

            $polygon = static::parsePolygon($surveyData['polygon']);

            if(is_null($name) || is_null($surveyType) || is_null($direction) || is_null($vessel) || is_null($tool) || is_null($date) || is_null($polygon)){
                print($name." survey has been skipped\n");
                continue;
            }
            
            static::createOrGetSurvey($name, $surveyType->id, $direction, $greatSalma->id, $vessel->id, $tool->id, $date, $polygon);
        }
    }
}
