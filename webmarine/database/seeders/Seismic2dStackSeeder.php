<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

use App\Models\Trip;
use App\Models\Seismic2dSurvey;
use App\Models\Seismic2dStack;
use App\Models\SeismicSource;
use App\Models\SeismicStreamer;
use App\Models\SeismicResolutionClass;
use App\Models\Zone;
use App\Models\Field;
use App\Models\Vessel;

use App\Helpers\LatLongDistance;

use App\Helpers\SegyFile;

class Seismic2dStackSeeder extends Seeder
{

    public static function createOrGetSurvey($name, $alias, $area, $direction,
        $archPath, $archSize, $zone, $field, $vessel, $source, $srcGroupCount, $srcPerGroup,
        $streamer, $isTowed, $resolClass, $startDate, $customer, $operator){
            $survey = Seismic2dSurvey::findByName(\mb_strtolower($name))->first();
            if(is_null($survey)){
                $survey = new Seismic2dSurvey(['name'=>$name]);
            }

            $survey->alias = $alias;
            $survey->area = $area;
            $survey->direction = $direction;
            $survey->arch_path = $archPath;
            $survey->arch_size = $archSize;
            $survey->zone()->associate($zone);
            $survey->field()->associate($field);

            if(is_null($survey->trip)){
                $trip = new Trip();
                $trip->vessel()->associate($vessel);
                $trip->save();
                $survey->trip()->associate($trip);
            }
            $survey->trip->vessel()->associate($vessel);
            $survey->trip->save();

            $survey->source()->associate($source);
            $survey->src_group_count = $srcGroupCount;
            $survey->src_per_group = $srcPerGroup;

            $survey->streamer()->associate($streamer);
            $survey->is_towed = $isTowed;

            $survey->resolution()->associate($resolClass);

            $survey->start_date = $startDate;

            $survey->customer = $customer;
            $survey->operator = $operator;

            $survey->save();

            return $survey;
    }

    private static function createOrGetStack($name, $alias, $survey, $filePath, $fileSize,
            $isMigrated, $isAmplCorrected, $isDeconvolved,
            $cdpOffset, $cdpSize, $cdpStart, $cdpEnd, $cdpStep, $cdpDistance){
        $stack = Seismic2dStack::findByName(\mb_strtolower($name))->first();
        if(is_null($stack)){
            $stack = new Seismic2dStack(['name'=>$name]);
        }

        $stack->name = $name;
        $stack->alias = $alias;
        $stack->survey()->associate($survey);
        $stack->file_path = $filePath;
        $stack->file_size = $fileSize;

        $stack->is_migrated = $isMigrated;
        $stack->is_ampl_corrected = $isAmplCorrected;
        $stack->is_deconvolved = $isDeconvolved;

        $stack->cdp_offset = $cdpOffset;
        $stack->cdp_size = $cdpSize;
        $stack->cdp_start = $cdpStart;
        $stack->cdp_end = $cdpEnd;
        $stack->cdp_step = $cdpStep;
        $stack->cdp_distance = $cdpDistance;

        $stack->save();
        return $stack;
    }

    private static function readCsv(string $csvFile, string $sep = ','){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 10000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][mb_strtolower($header[$c])] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }


    private static function parseCsvPolyline(string $polylineStr, string $sep = ",", $swapCoords = false){
        $swapFunc = function($pointStr)use($sep){
            $arrr = explode($sep, trim($pointStr, " \n\r\t\0"));
            return [$arrr[1], $arrr[0]];
        };

        $splitFunc = function($pointStr)use($sep){
            $arrr = explode($sep, trim($pointStr, " \n\r\t\0"));
            return [$arrr[0], $arrr[1]];
        };

        if($swapCoords){
            $points = array_map($swapFunc, explode("\n", trim($polylineStr, " \n\r\t\0")));
        }else{
            $points = array_map($splitFunc, explode("\n", trim($polylineStr, " \n\r\t\0")));
        }
        return $points;
        // return \DB::raw('ST_GeometryFromText(\'LINESTRING('.implode(",",$points).')\')');
    }

    

    private static function findMissingFields($csvData){
        $missingFields = [];
        static $requiredFields = ['name', 'date', 'direction', 'source',
            'streamer', 'is_towed', 'src_group_count', 'src_per_group',
            'resolution_class', 'cdp_offset', 'cdp_size', 'path', 'survey_file', 'area_file'];
        $sample = $csvData[0];
        foreach($requiredFields as $rf){
            if(!array_key_exists($rf, $sample)){
                array_push($missingFields, $rf);
            }
        }
        return $missingFields;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    private static function findMinMaxStepTraceFieldValue($segyFile, int $offset, int $size){
        $vals = $segyFile->getUintTraceHeaderFieldValues($offset, $size);
        $diffs = [];
        for($i = 0; $i < count($vals)-1; ++$i){
            array_push($diffs, $vals[$i+1]-$vals[$i]);
        }

        return [min($vals), max($vals), array_sum($diffs)/count($diffs)];
    }

    private static function extractZone(string $path){
        $codes = preg_split('/(\/|\\|)/', $path);
        $index = 1;
        $zone = Zone::query()->root()->byCode($codes[0])->first();
        if(is_null($zone)){
            $index = 2;
            $zone = Zone::query()->root()->byCode($codes[1])->first();
            if(is_null($zone)){
                echo("Failed code ".$codes[1]."\n");
                return null;
            }
        }

        for(;$index < count($codes); ++$index){
            if(preg_match("/^L\d+$/",$codes[$index])) break;
            $zone = $zone->children()->byCode($codes[$index])->first();
            if(is_null($zone)) break;
        }
        
        return $zone;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ZonesSeeder::class,
            SeismicSourceSeeder::class,
            StreamerSeeder::class,
            TestVesselsSeeder::class,
            SeismicResolutionClassesSeeder::class,
            FieldSeeder::class
        ]);

        $stacksData = static::readCsv(dirname(__FILE__).'/seismic_surveys.csv');

        $missingFields = static::findMissingFields($stacksData);

        if(count($missingFields)){
            echo("Fields: ".implode(",", $missingFields)." are missing!\n");
            return;
        }

        // $dataFolder = \Config::get('app.data_folder');
        // $dataFolder = "/media/aggravator/Seagate Backup Plus Drive/WebMarine/data";
        // $dataFolder = Storage::disk('data')->getAdapter()->getPathPrefix();
        $dataFolder = Storage::disk('data')->path(\Config::get('app.data_folder'));

        foreach($stacksData as $sd){
            // Check that segy file exists and get segy size and trace count
            $segyPath = static::joinPaths($sd['path'], $sd['survey_file']);
            $fullSegyPath = static::joinPaths($dataFolder, $segyPath);
            if(!\file_exists($fullSegyPath)){
                // echo("Survey file doesn't exist! Skipping \"".$sd['name']."\" survey\n");
                echo("Survey file doesn't exist! Skipping \"".$fullSegyPath."\" survey\n");
                continue;
            }
            
            $segySize = filesize($fullSegyPath);
            
            $segyFile = new SegyFile($fullSegyPath, false);
            $traceCount = $segyFile->getTraceCount();

            // Check existencs of area file
            $pathToArea = static::joinPaths($dataFolder, $sd['path'], $sd['area_file']);
            if(!\file_exists($pathToArea)){
                echo("Area file doesn't exist! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            // Check existens of archive file and get its size
            $archPath = null;
            $archSize = null;
            if(!empty($sd['arch_file'])){
                $archPath = static::joinPaths($dataFolder, $sd['arch_file']);
                $fullArchPath = static::joinPaths($dataFolder, $archPath);
                if(!\file_exists($fullArchPath)){
                    echo("Path to archive is incorrect! Skipping \"".$sd['name']."\" survey\n");
                    continue;
                }
                $archSize = filesize($fullArchPath);
            }

            // Get fields
            $name = $sd['name'];
            $alias = !empty($sd['alias']) ? $sd['alias'] : null;
            $date = gmdate("Y-m-d", \strtotime($sd['date']));
            $direction = $sd['direction'];

            // Extracting zone
            $zone = static::extractZone($sd['path']);
            if(is_null($zone)){
                echo($sd['path']."\n");
                echo("Failed to find zone! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            // Extracting polyline
            $polylinePoints = static::parseCsvPolyline(\file_get_contents($pathToArea), ";", true);
            $polyline =  \DB::raw('ST_GeometryFromText(\'LINESTRING('.implode(",",array_map(function($el){return implode(" ", $el);},$polylinePoints)).')\')');
            
            $field = !empty($sd['field'])  ? Field::findByName(\mb_strtolower($sd['field']))->first() : null;

            // Getting fields relative to seismic source
            $source = SeismicSource::findByName(\mb_strtolower($sd['source']))->first();
            if(is_null($source)){
                echo("Seismic source '".$sd['source']."' is not found! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }
            $srcGroupCount = $sd['src_group_count'];
            $srcPerGroup = $sd['src_per_group'];

            // Getting fields relative to seismic streamer
            $streamer = SeismicStreamer::findByName(\mb_strtolower($sd['streamer']))->first();
            if(is_null($streamer)){
                echo("Seismic streamer is not found! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }
            $isTowed = $sd['is_towed'];


            $resolClass = SeismicResolutionClass::findByName(\mb_strtolower($sd['resolution_class']))->first();
            if(is_null($resolClass)){
                $resolClass = SeismicResolutionClass::findByKey($sd['resolution_class'])->first();
            }
            if(is_null($resolClass)){
                echo("Resolution class is not found! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            $isMigrated = !empty($sd['migrated']) || $sd['migrated'] == '0' ? $sd['migrated'] : null;
            $isAmplCorrected = !empty($sd['ampl_correction']) || $sd['ampl_correction'] == '0' ? $sd['ampl_correction'] : null;
            $isDeconvolved = !empty($sd['deconvolution']) || $sd['deconvolution'] == '0' ? $sd['deconvolution'] : null;

            $vessel = Vessel::findByName(\mb_strtolower($sd['vessel']))->first();
            if(is_null($vessel)){
                echo("Vesel '".$sd['vessel']."' is not found! Skipping \"".$sd['name']."\" survey\n");
                continue;
            }

            $customer = array_key_exists('customer', $sd) ? $sd['customer'] : null;
            $operator = array_key_exists('operator', $sd) ? $sd['operator'] : null;

            $cdpMinMaxStep = static::findMinMaxStepTraceFieldValue($segyFile, $sd['cdp_offset'], $sd['cdp_size']);

            $fp = reset($polylinePoints);
            $lp = end($polylinePoints);
            $cdpDist = LatLongDistance::vincentyGreatCircleDistance($fp[1], $fp[0], $lp[1], $lp[0])/($traceCount-1);

            $survey = static::createOrGetSurvey($name, null, $polyline, $direction, $archPath, $archSize, $zone,
                $field, $vessel, $source, $srcGroupCount, $srcPerGroup,
                $streamer, $isTowed, $resolClass, $date, $customer, $operator);

            $stack = static::createOrGetStack($name, $alias, $survey, $segyPath, $segySize,
                $isMigrated, $isAmplCorrected, $isDeconvolved,
                $sd['cdp_offset'], $sd['cdp_size'], $cdpMinMaxStep[0], $cdpMinMaxStep[1], $cdpMinMaxStep[2], $cdpDist);
        }
        
    }
}
