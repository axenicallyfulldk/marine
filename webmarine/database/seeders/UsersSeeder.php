<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Services\UserService;
use App\Models\User;
use App\Models\Group;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'galaev', 'email' => 'galaev@marine.marine', 'password' => '8^QJk2', 'role' => 'moderator'],
            ['name' => 'tokarev', 'email' => 'tokarev@marine.marine', 'password' => '1872', 'role' => 'moderator'],
        ];

        foreach($users as $user){
            $u = User::where('email', $user['email'])->first();
            if(is_null($u)){
                $u = new User();
            }

            $u->name = $user['name'];
            $u->email = $user['email'];
            $u->password = \Hash::make($user['password']);
            $u->group()->associate(Group::get($user['role']));
            $u->save();
        }
    }
}
