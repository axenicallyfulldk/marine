import sys, argparse, pathlib, os, re

def flushFault(outputFile, faultName, segyName, points):
    if (faultName is None) or (segyName is None):
        return
    pointStr = '[['+'],['.join([','.join(map(str,i)) for i in points])+']]'
    outputFile.write('{};{};{}\n'.format(faultName, segyName, pointStr))

faultRegexp = r'^PROFILE\s+([\w\-\d]+)\s'
pointRegexp1 = r'^\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([a-zA-Z]\w+)\s+$'
pointRegexp2 = r'^\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+([-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?)\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+[-+]?[0-9]+[\.]?[0-9]*([eE][-+]?[0-9]+)?\s+([a-zA-Z]\w+)\s+$'


parser = argparse.ArgumentParser(description='Process fault files.')
parser.add_argument('-f', type = pathlib.Path, default = "faults", help='path to folder with fault files')
parser.add_argument('-o', type = argparse.FileType('w'), default = 'faults.csv', help = 'output file')

args = parser.parse_args()

# faultFolderPath = args.f
faultFolderPath = '/home/aggravator/Documents/WebMarine/backend/webmarine/database/seeders/faults'

# outputFile = args.o
outputFile = open('/home/aggravator/Documents/WebMarine/backend/webmarine/database/seeders/faults.csv','w')

outputFile.write('Fault;Segy;Points\n')
faultFiles = os.listdir(faultFolderPath)
for ff in faultFiles:
    fullFaultPath = os.path.join(faultFolderPath, ff)
    faultName = None
    segyName = None
    points = []

    for line in open(fullFaultPath, "r").readlines():
        headerMatch = re.search(faultRegexp, line)
        if headerMatch :
            flushFault(outputFile, faultName, segyName, points)
            faultName = headerMatch.group(1)
            segyName = None
            points = []
            continue

        dataMatch = re.search(pointRegexp1, line)
        if dataMatch:
            time = dataMatch.group(3)
            trace = dataMatch.group(5)
            newSegyName = dataMatch.group(10)
            if newSegyName != segyName :
                flushFault(outputFile, faultName, segyName, points)
                segyName = newSegyName
                points = [[float(trace), float(time)]]
                continue
            else:
                points.append([float(trace), float(time)])
        else:
            dataMatch = re.search(pointRegexp2, line)
            if dataMatch:
                time = dataMatch.group(4)
                trace = dataMatch.group(6)
                newSegyName = dataMatch.group(11)
                if newSegyName != segyName :
                    flushFault(outputFile, faultName, segyName, points)
                    segyName = newSegyName
                    points = [[float(trace), float(time)]]
                    continue
                else:
                    points.append([float(trace), float(time)])
            
