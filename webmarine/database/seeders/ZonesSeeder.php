<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Zone;
use App\Models\OceanBasin;
use App\Models\Sea;
use App\Models\Bay;
use App\Models\Strait;

class ZonesSeeder extends Seeder
{
    public static function createOrGetZone($zoneName, $zoneCode, $parent, $zoneClass, $polygon, $year){
        $lowerZoneName = mb_strtolower($zoneName);
        if(is_null($polygon)) $polygon = \DB::raw("'POINT(1 1)'");
        $zoneQuery = Zone::whereRaw('lower(name) like (?)', ["%$lowerZoneName%"])->where('zonable_type', $zoneClass);
        // $zoneCount = $zoneQuery->count();

        $zone = $zoneQuery->first();
        if(is_null($zone)){
            $zoneObj = new $zoneClass();
            $zone = new Zone();
        } else {
            $zoneObj = $zone->zonable;
        }

        $zoneObj->save();

        $zone->name = $zoneName;
        $zone->code = $zoneCode;
        $zone->area = $polygon;
        $zone->default_year = $year;
        $zone->parent()->associate($parent);
        $zone->zonable()->associate($zoneObj);
        
        $zone->save();

        return $zone;
    }

    private static function readCsv(string $csvFile){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, ",");
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][$header[$c]] = $data[$c];
                }
                $row++;
                
            }
            fclose($handle);
        }
        return $res;
    }

    private static function parsePolygon(string $polygonStr){
        $points = explode(",", $polygonStr);
        if($points[0] != $points[count($points)-1]){
            array_push($points, $points[0]);
        }
        return \DB::raw('ST_GeometryFromText(\'POLYGON(('.implode(",",$points).'))\')');
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $arctic = TestZones::createOrGetZone("Северный Ледовитый океан", "A", NULL, OceanBasin::class);

        // $whiteSea = TestZones::createOrGetZone("Белое море", "W", $arctic, Sea::class);

        // $kandBay = TestZones::createOrGetZone("Кандалакшский залив", "BK", $whiteSea, Bay::class);

        // $dvinBay = TestZones::createOrGetZone("Двинская губа", "BD", $whiteSea, Bay::Class);

        // TestZones::createOrGetZone("Колвицкая губа", "BK", $kandBay, Bay::class);
        // TestZones::createOrGetZone("Ругозерская губа", "BR", $kandBay, Bay::class);
        // TestZones::createOrGetZone("Чёрная губа", "BB", $kandBay, Bay::class);
        // TestZones::createOrGetZone("Губа Кив", "BKV", $kandBay, Bay::class);
        
        // $velSalmaStrait = TestZones::createOrGetZone("Пролив Великая Салма", "SVS", $kandBay, Strait::class);

        $zonesData = static::readCsv(dirname(__FILE__).'/zone_data.csv');
        foreach($zonesData as $zoneData){
            $name = $zoneData['name'];
            $code = $zoneData['code'];
            $zoneClass= Zone::ZONE_CLASSES[$zoneData['type']];
            $parentZone = empty($zoneData['parent_name']) ? null : Zone::query()->containsString($zoneData['parent_name'])->first();
            $polygon = trim($zoneData['polygon']) == '' ? null : static::parsePolygon($zoneData['polygon']);
            static::createOrGetZone($name, $code, $parentZone, $zoneClass, $polygon, 2016);
        }
    }
}
