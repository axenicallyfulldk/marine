<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tool;

use App\Helpers\ColorIntConverter;

class TestToolsSeeder extends Seeder
{
    private static function createOrGetTool($toolModel, $toolBrand, $toolColor = null){
        $lowerToolModel = mb_strtolower($toolModel);
        $lowerToolBrand = mb_strtolower($toolBrand);
        $toolQuery = Tool::whereRaw('lower(model) like (?) and lower(brand) like (?)', ["%$lowerToolModel%", "%$lowerToolBrand%"]);
        $tool = $toolQuery->first();
        if(is_null($tool)) $tool = new Tool();
        $tool->model = $toolModel;
        $tool->brand = $toolBrand;
        if(!is_null($toolColor)) $tool->map_color = ColorIntConverter::strToInt($toolColor);
        $tool->save();
        return $tool;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOrGetTool('3900','Klein', '#DAFF00FF');
        $this->createOrGetTool('C3D', 'Benthos', '#FFDD00FF');
    }
}
