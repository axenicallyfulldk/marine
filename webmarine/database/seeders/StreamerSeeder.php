<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SeismicStreamer;

class StreamerSeeder extends Seeder
{

    public static function createOrGetStreamer($name, $length, $receiverDist){
        $lowName = mb_strtolower($name);
        $seismicQuery = SeismicStreamer::whereRaw('lower(name) like (?)', ["$lowName"]);
        if($seismicQuery->count()){
            $streamer = $seismicQuery->first();
            if($streamer->name != $name || $streamer->length != $length || $streamer->receiver_dist != $receiverDist){
                echo('Updating "'.$streamer->name.'" streamer!'."\n");
                $streamer->name = $name;
                $streamer->length = $length;
                $streamer->receiver_dist = $receiverDist;
                $streamer->save();
            }
        } else {
            $streamer = new SeismicStreamer(['name' => $name, 'length' => $length, 'receiver_dist' => $receiverDist]);
            $streamer->save();
        }
        return $streamer;
    }

    private static function readCsv(string $csvFile, string $sep = ","){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 10000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 10000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++){
                    $res[$row][$header[$c]] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $streamersData = static::readCsv(dirname(__FILE__).'/streamers.csv');

        foreach($streamersData as $streamerData){
            static::createOrGetStreamer($streamerData['name'], $streamerData['length'], $streamerData['receiver_dist']);
        }
    }
}
