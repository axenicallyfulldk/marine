<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Helpers\CsvReader;
use App\Models\Mosaic;
use App\Models\MosaicArchFile;

use Illuminate\Support\Facades\Storage;

class MosaicArchSeeder extends Seeder
{

    public static function createOrGetFile($mosaic, $name, $desc){
        $file = $mosaic->archFiles()->withName($name)->first();
        if(is_null($file)) $file = new MosaicArchFile();
        $file->name = $name;
        $file->mosaic()->associate($mosaic);
        $file->description = $desc;
        $file->save();
        return $file;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MosaicsSeeder::class
        ]);

        $filesData = CsvReader::read(dirname(__FILE__).'/mosaic_files.csv', ',');

        foreach($filesData as $fileData){        
            $mosaicName = $fileData["name"];
            $mosaic = Mosaic::findByName($mosaicName)->first();
            if(is_null($mosaic)){
                echo(sprintf("Failed to find mosaic %s! Skipping file %s\n", $mosaicName, $fileData['file']));
                continue;
            }

            static::createOrGetFile($mosaic, $fileData['file'], $fileData['description']);
        }
    }
}
