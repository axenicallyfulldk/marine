<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\InterpretedModel;

use App\Models\HazardType;

use App\Models\Modelable;

use App\Models\Zone;

use App\Helpers\CsvReader;

use App\Helpers\WktStringFormatter;

class TestInterpretedModelSeeder extends Seeder
{

    public static function addHazard($name, $typeKey, $wkt){
        $hazardType = HazardType::findByKey($typeKey)->first();
        if(is_null($hazardType)){
            return null;
        }

        //
        $typeClass = $hazardType->getModel();
        $hazard = $typeClass::findByName($name)->first();
        if(is_null($hazard)){
            $hazard = new $typeClass();
        }

        $hazard->name = $name;
        $hazard->type()->associate($hazardType);
        $newWkt = static::checkWktAndReturn($wkt);
        // echo($newWkt."\n");
        $hazard->shape = \DB::raw("'".$newWkt."'");
        $hazard->save();

        return $hazard;
    }

    public static function connectHazardAndModel($model, $hazard){
        $obj = Modelable::where('interpreted_model_id', $model->id)->
            where('object_type', get_class($hazard))->
            where('object_id', $hazard->id)->first();
        if(is_null($obj)){
            $obj = new Modelable();
            $obj->model()->associate($model);
            $obj->modelable()->associate($hazard);
            $obj->save();
        }
        return $obj;
    }

    private static function checkWktAndReturn($wkt){
        return WktStringFormatter::formatWktString($wkt);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            HazardTypesSeeder::class,
        ]);

        $modelPolygon = "'POLYGON ((144.19035527202385 51.38902732635528, 144.25540943653823 51.407389469055914, 144.2706930563811 51.386390532070834, 144.3992677608802 51.428073237352244, 144.38278286885583 51.450383648738715, 144.51077221396042 51.489444063169856, 144.52471805362586 51.469710890243796, 144.6086914700917 51.494446168429626, 144.75618680827955 51.30840991662052, 144.6682767761521 51.28138729180887, 144.64926762746438 51.3033969075324, 144.5291353751278 51.2655145882026, 144.54542180096402 51.242347840758384, 144.42458470116836 51.2006027635034, 144.40503812353722 51.225526783139145, 144.33852075441098 51.20267278805819, 144.19035527202385 51.38902732635528))'";
        $model1 = InterpretedModel::findByName("model1")->first();
        if(is_null($model1)){
            $model1 = new InterpretedModel();
            $model1->name = "model1";
            $model1->shape = \DB::raw($modelPolygon);
            $model1->parent()->associate(Zone::deduceZone('P/O'));
            $model1->creation_date = "2021-09-01";
            $model1->file_path = "GS2021/P/O/L3/2020/MODEL/ok_background.tif";
            $model1->save();
            $model1 = $model1->fresh();
        }

        $model1->name = "model1";
        $model1->shape = \DB::raw($modelPolygon);
        $model1->parent()->associate(Zone::deduceZone('P/O'));
        $model1->creation_date = "2021-09-01";
        $model1->file_path = "GS2021/P/O/L3/2020/MODEL/ok_background.tif";

        $model1->save();

        // Add hazards
        foreach(CsvReader::read(dirname(__FILE__).'/test_hazards.csv', ",") as $hazard){
            $hazard = static::addHazard($hazard['name'], $hazard['type'], $hazard['shape']);
            static::connectHazardAndModel($model1, $hazard);
        }
    }
}
