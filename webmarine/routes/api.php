<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Illuminate\Routing\Middleware\SubstituteBindings;

use \App\Models\Zone;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::group(['middleware' => ['api', 'auth']], function () {
    //Zone endpoints
    Route::get('zones', 'App\Http\Controllers\ZoneController@index');
    Route::get('zones/root', 'App\Http\Controllers\ZoneController@root');
    Route::get('zones/{zone}', 'App\Http\Controllers\ZoneController@show');
    Route::post('zones', 'App\Http\Controllers\ZoneController@store');
    Route::patch('zones/{zone}', 'App\Http\Controllers\ZoneController@update');
    Route::delete('zones/{zone}', 'App\Http\Controllers\ZoneController@destroy');

    //Tool endpoints
    Route::get('tools', 'App\Http\Controllers\ToolController@index');
    Route::get('tools/{tool}', 'App\Http\Controllers\ToolController@show');
    Route::post('tools', 'App\Http\Controllers\ToolController@store');
    Route::patch('tools/{tool}', 'App\Http\Controllers\ToolController@update');
    Route::delete('tools/{tool}', 'App\Http\Controllers\ToolController@destroy');

    //Vessel endpoints
    Route::get('vessels', 'App\Http\Controllers\VesselController@index');
    Route::get('vessels/{vessel}', 'App\Http\Controllers\VesselController@show');
    Route::post('vessels', 'App\Http\Controllers\VesselController@store');
    Route::patch('vessels/{vessel}', 'App\Http\Controllers\VesselController@update');
    Route::delete('vessels/{vessel}', 'App\Http\Controllers\VesselController@destroy');

    // SSS and MBES survey endpoints
    Route::get('sssmbes-surveys', 'App\Http\Controllers\SssMbesSurveyController@index');
    Route::get('sssmbes-surveys/{survey}', 'App\Http\Controllers\SssMbesSurveyController@show');
    Route::get('sssmbes-surveys/{survey}/arch', 'App\Http\Controllers\SssMbesSurveyController@downloadArchive');
    Route::get('sssmbes-surveys/{survey}/file', 'App\Http\Controllers\SssMbesSurveyController@downloadFile');
    Route::post('sssmbes-surveys', 'App\Http\Controllers\SssMbesSurveyController@store');
    Route::patch('sssmbes-surveys/{survey}', 'App\Http\Controllers\SssMbesSurveyController@update');
    Route::delete('sssmbes-surveys/{survey}', 'App\Http\Controllers\SssMbesSurveyController@destroy');

    Route::get('survey-types', 'App\Http\Controllers\SurveyTypeController@index');


    Route::get('seismic-sources', 'App\Http\Controllers\SeismicSourceController@index');
    Route::get('seismic-sources/{seismicSource}', 'App\Http\Controllers\SeismicSourceController@show');


    Route::get('seismic-streamers', 'App\Http\Controllers\SeismicStreamerController@index');
    Route::get('seismic-streamers/{seismicStreamer}', 'App\Http\Controllers\SeismicStreamerController@show');

    //Seismic 2D survey endpoints
    Route::get('seismic2d-surveys', 'App\Http\Controllers\Seismic2dSurveyController@index');
    Route::get('seismic2d-surveys/{survey}', 'App\Http\Controllers\Seismic2dSurveyController@show');
    Route::get('seismic2d-surveys/{survey}/arch', 'App\Http\Controllers\Seismic2dSurveyController@downloadArchive');
    Route::post('seismic2d-surveys', 'App\Http\Controllers\Seismic2dSurveyController@store');
    Route::patch('seismic2d-surveys/{survey}', 'App\Http\Controllers\Seismic2dSurveyController@update');
    Route::delete('seismic2d-surveys/{survey}', 'App\Http\Controllers\Seismic2dSurveyController@destroy');

    // End points for stacks of 2D survyes
    Route::get('seismic2d-stacks', 'App\Http\Controllers\Seismic2dStackController@index');
    Route::get('seismic2d-stacks/{stack}', 'App\Http\Controllers\Seismic2dStackController@show');
    Route::get('seismic2d-stacks/{stack}/file', 'App\Http\Controllers\Seismic2dStackController@download');
    Route::post('seismic2d-stacks', 'App\Http\Controllers\Seismic2dStackController@store');
    Route::patch('seismic2d-stacks/{stack}', 'App\Http\Controllers\Seismic2dStackController@update');
    Route::delete('seismic2d-stacks/{stack}', 'App\Http\Controllers\Seismic2dStackController@destroy');

    // End points for seismogramms of 2D survyes
    Route::get('seismic2d-seismogramms', 'App\Http\Controllers\Seismic2dSeismogrammController@index');
    Route::get('seismic2d-seismogramms/{seismogramm}', 'App\Http\Controllers\Seismic2dSeismogrammController@show');
    Route::get('seismic2d-seismogramms/{seismogramm}/file', 'App\Http\Controllers\Seismic2dSeismogrammController@download');
    Route::post('seismic2d-seismogramms', 'App\Http\Controllers\Seismic2dSeismogrammController@store');
    Route::patch('seismic2d-seismogramms/{seismogramm}', 'App\Http\Controllers\Seismic2dSeismogrammController@update');
    Route::delete('seismic2d-seismogramms/{seismogramm}', 'App\Http\Controllers\Seismic2dSeismogrammController@destroy');


    //Seismic 3D survey endpoints
    Route::get('seismic3d-surveys', 'App\Http\Controllers\Seismic3dSurveyController@index');
    Route::get('seismic3d-surveys/{survey}', 'App\Http\Controllers\Seismic3dSurveyController@show');
    Route::get('seismic3d-surveys/{survey}/arch', 'App\Http\Controllers\Seismic3dSurveyController@downloadArchive');
    Route::post('seismic3d-surveys', 'App\Http\Controllers\Seismic3dSurveyController@store');
    Route::patch('seismic3d-surveys/{survey}', 'App\Http\Controllers\Seismic3dSurveyController@update');
    Route::delete('seismic3d-surveys/{survey}', 'App\Http\Controllers\Seismic3dSurveyController@destroy');

    // End points for stacks of 3D survyes
    Route::get('seismic3d-stacks', 'App\Http\Controllers\Seismic3dStackController@index');
    Route::get('seismic3d-stacks/{stack}', 'App\Http\Controllers\Seismic3dStackController@show');
    Route::get('seismic3d-stacks/{stack}/file', 'App\Http\Controllers\Seismic3dStackController@download');
    Route::post('seismic3d-stacks', 'App\Http\Controllers\Seismic3dStackController@store');
    Route::patch('seismic3d-stacks/{stack}', 'App\Http\Controllers\Seismic3dStackController@update');
    Route::delete('seismic3d-stacks/{stack}', 'App\Http\Controllers\Seismic3dStackController@destroy');
    
    Route::get('sssmbes-survey-unique-properties', 'App\Http\Controllers\SssMbesSurveyController@unique');

    // Endpoints for special permission on surveys for users
    Route::group(['middleware' => \App\Http\Middleware\CheckSurveyPermission::class], function(){
        Route::get('sssmbes-survey-user-permissions', 'App\Http\Controllers\SssMbesSurveyUserPermissionController@index');
        Route::get('sssmbes-survey-user-permissions/{survey}-{user}', 'App\Http\Controllers\SssMbesSurveyUserPermissionController@show');
        Route::post('sssmbes-survey-user-permissions', 'App\Http\Controllers\SssMbesSurveyUserPermissionController@store');
        Route::patch('sssmbes-survey-user-permissions/{survey}-{user}', 'App\Http\Controllers\SssMbesSurveyUserPermissionController@update');
        Route::delete('sssmbes-survey-user-permissions/{survey}-{user}', 'App\Http\Controllers\SssMbesSurveyUserPermissionController@destroy');

        Route::get('sssmbes-survey-group-permissions', 'App\Http\Controllers\SssMbesSurveyGroupPermissionController@index');
        Route::get('sssmbes-survey-group-permissions/{survey}-{group}', 'App\Http\Controllers\SssMbesSurveyGroupPermissionController@show');
        Route::post('sssmbes-survey-group-permissions', 'App\Http\Controllers\SssMbesSurveyGroupPermissionController@store');
        Route::patch('sssmbes-survey-group-permissions/{survey}-{group}', 'App\Http\Controllers\SssMbesSurveyGroupPermissionController@update');
        Route::delete('sssmbes-survey-group-permissions/{survey}-{group}', 'App\Http\Controllers\SssMbesSurveyGroupPermissionController@destroy');
    });


    Route::get('mosaics', 'App\Http\Controllers\MosaicController@index');
    Route::get('mosaics/{mosaic}', 'App\Http\Controllers\MosaicController@show');
    Route::get('mosaics/{mosaic}/arch', 'App\Http\Controllers\MosaicController@downloadArchive');
    Route::get('mosaics/{mosaic}/file', 'App\Http\Controllers\MosaicController@downloadFile');
    Route::post('mosaics', 'App\Http\Controllers\MosaicController@store');
    Route::patch('mosaics/{mosaic}', 'App\Http\Controllers\MosaicController@update');
    Route::delete('mosaics/{mosaic}', 'App\Http\Controllers\MosaicController@destroy');

    Route::get('mosaic-types', 'App\Http\Controllers\MosaicTypeController@index');

    Route::get('mosaic-unique-properties', 'App\Http\Controllers\MosaicController@unique');

    // Endpoints for special permission on mosaics for users
    Route::group(['middleware' => \App\Http\Middleware\CheckMosaicPermission::class], function(){
        Route::get('mosaic-user-permissions', 'App\Http\Controllers\MosaicUserPermissionController@index');
        Route::get('mosaic-user-permissions/{mosaic}-{user}', 'App\Http\Controllers\MosaicUserPermissionController@show');
        Route::post('mosaic-user-permissions', 'App\Http\Controllers\MosaicUserPermissionController@store');
        Route::patch('mosaic-user-permissions/{mosaic}-{user}', 'App\Http\Controllers\MosaicUserPermissionController@update');
        Route::delete('mosaic-user-permissions/{mosaic}-{user}', 'App\Http\Controllers\MosaicUserPermissionController@destroy');

        Route::get('mosaic-group-permissions', 'App\Http\Controllers\MosaicGroupPermissionController@index');
        Route::get('mosaic-group-permissions/{mosaic}-{group}', 'App\Http\Controllers\MosaicGroupPermissionController@show');
        Route::post('mosaic-group-permissions', 'App\Http\Controllers\MosaicGroupPermissionController@store');
        Route::patch('mosaic-group-permissions/{mosaic}-{group}', 'App\Http\Controllers\MosaicGroupPermissionController@update');
        Route::delete('mosaic-group-permissions/{mosaic}-{group}', 'App\Http\Controllers\MosaicGroupPermissionController@destroy');
    });

    Route::get('interpreted-models', 'App\Http\Controllers\InterpretedModelController@index');
    Route::get('interpreted-models/{model}', 'App\Http\Controllers\InterpretedModelController@show');
    Route::get('interpreted-models/{model}/background', 'App\Http\Controllers\InterpretedModelController@background');

    Route::get('hazard-types', 'App\Http\Controllers\HazardTypeController@index');
    Route::get('hazard-types/{hazardType}', 'App\Http\Controllers\HazardTypeController@show');

    Route::get('users', 'App\Http\Controllers\UserController@index');
    Route::get('users/me', 'App\Http\Controllers\UserController@me');
    Route::get('users/{user}', 'App\Http\Controllers\UserController@show');
    Route::post('users', 'App\Http\Controllers\UserController@store');
    Route::patch('users/{user}', 'App\Http\Controllers\UserController@update');
    Route::delete('users/{user}', 'App\Http\Controllers\UserController@destroy');

    Route::get('groups', 'App\Http\Controllers\GroupController@index');
    Route::get('groups/my', 'App\Http\Controllers\GroupController@my');
    Route::get('groups/{group}', 'App\Http\Controllers\GroupController@show');
    Route::post('groups', 'App\Http\Controllers\GroupController@store');
    Route::patch('groups/{group}', 'App\Http\Controllers\GroupController@update');
    Route::delete('groups/{group}', 'App\Http\Controllers\GroupController@destroy');

    Route::get('statistics/sssmbes-surveys/years', 'App\Http\Controllers\StatController@surveyYears');
    Route::get('statistics/sssmbes-surveys/types', 'App\Http\Controllers\StatController@surveyTypes');
    Route::get('statistics/sssmbes-surveys/tools', 'App\Http\Controllers\StatController@surveyTools');
    Route::get('statistics/sssmbes-surveys/directions', 'App\Http\Controllers\StatController@surveyDirections');

    Route::get('statistics/mosaics/years', 'App\Http\Controllers\StatController@mosaicYears');
    Route::get('statistics/mosaics/types', 'App\Http\Controllers\StatController@mosaicTypes');
    Route::get('statistics/mosaics/tools', 'App\Http\Controllers\StatController@mosaicTools');
    Route::get('statistics/mosaics/directions', 'App\Http\Controllers\StatController@mosaicDirections');


    Route::get('import-sssmbes', 'App\Http\Controllers\BatchImportController@sssmbesImport');
    Route::get('import-seismic', 'App\Http\Controllers\BatchImportController@seismicImport');

    Route::post('import-models', 'App\Http\Controllers\BatchInterpretedModelController@import');
// });
