<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Web
Route::group(['middleware' => ['auth']], function () {
	Route::get('/home/WhiteSea', function () {
		return view('WhiteSea');
	});
	Route::get('/home/WhiteSea/KandalakshaBay', function () {
		return view('KandalakshaBay');
	});
	Route::get('/home/WhiteSea/KandalakshaBay/BBS', function () {
		return view('BBS');
	});
	Route::get('/home/WhiteSea/KandalakshaBay/BBSm', function () {
		return view('BBSm');
	});
	Route::get('/home/WhiteSea/KandalakshaBay/BBSall', function () {
		return view('BBSall');
	});

	Route::get('/', function () {
		return view('map');
	})->name('home');

	Route::get('/seismic2d-stack-viewer', function(){
		return view('stack_viewer');
	})->name('seismic2d-stack-viewer');

	Route::get('/interpreted-model-viewer', function(){
		return view('interpreted_model_viewer');
	})->name('interpreted-model-viewer');

	// Route::get('/seismic2d-stack-viewer/{any}', function(){
	// 	return view('stack_viewer');
	// })->name('seismic2d-stack-viewer')->where('any', '.*');

	Route::get('/survey-stat', function () {
		return view('survey_stat');
	})->name('survey-stat');

	Route::get('/mosaic-stat', function () {
		return view('mosaic_stat');
	})->name('mosaic-stat');

	Route::get('/sssmbes-batch-import', function () {
		return view('sssmbes_batch_import');
	})->name('sssmbes-batch-import'); 

	Route::get('/seismic-batch-import', function () {
		return view('seismic_batch_import');
	})->name('seismic-batch-import'); 

	Route::get('/map', function () {
		return view('map');
	})->name('map');

	Route::get('/model-batch-import', function () {
		return view('model_batch_import');
	})->name('model-batch-import');
});
