<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        if ($request->ajax() || $request->wantsJson()) {
            return response()->json(
                          $this->getJsonMessage($e), 
                          $this->getExceptionHTTPStatusCode($e)
                        );
        }
        return parent::render($request, $e);
    }


    protected function getJsonMessage($e){
        // You may add in the code, but it's duplication

        if($e instanceof \Illuminate\Validation\ValidationException){
            return $this->getJsonMessageForValidation($e);
        }

        if($e instanceof \Illuminate\Database\QueryException){
            return [
                'status' => 500,
                'errors' => [['title' => 'Unknown server exception', 'placement' => 'general']],
            ];
        }

        return [
            'status' => $this->getExceptionHTTPStatusCode($e),
            'errors' => [['title' => $this->getTitle($e), 'placement' => 'general']],
            'kjg' => get_class($e)
         ];
        
    }

    protected function getJsonMessageForValidation($e){
        $errors = array();
        foreach($e->errors() as $field=>$message){
            array_push($errors, ['title' => $message[0], 'placement' => 'field', 'field' => $field]);
        }

        return [
            'status' => 400,
            'errors' => $errors,
        ];
    }

    protected function getTitle($e){
        if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException){
            return "Object not found";
        }

        if($e instanceof \Illuminate\Validation\ValidationException){
            return $e->errors();
        }

        return $e->getMessage();
    }

    protected function getExceptionHTTPStatusCode($e){
        // Not all Exceptions have a http status code
        // We will give Error 500 if none found
        if($e instanceof \Illuminate\Auth\AuthenticationException){
            return 401;
        }

        if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException){
            return 404;
        }

        return method_exists($e, 'getStatusCode') ? 
                         $e->getStatusCode() : 500;
    }
}
