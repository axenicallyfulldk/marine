<?php
namespace App;

class Tables{
    const USER_TABLE = 'user';
    const GROUP_TABLE = 'user';

    const OCEAN_BASIN_TABLE = 'ocean_basin';
    const SEA_TABLE = 'sea';
    const BAY_TABLE = 'bay';
    const STRAIT_TABLE = 'strait';
    const ZONE_TABLE = 'zone';
    const ZONE_PERM_TABLE = 'zone_permission';

    const FIELD_TABLE = 'field';

    const VESSEL_TABLE = 'vessel';

    const TRIP_TABLE = 'trip';
    // const SEISMIC2D_TRIP_TABLE = 'seismic2d_trip';

    const TOOL_TABLE = 'tool';

    const STREAMER_TABLE = 'seismic_streamer';

    const SURVEY_TYPE_TABLE = 'survey_type';

    const SSSMBES_SURVEY_PERM_TABLE = 'sssmbes_survey_permission';
    const SSSMBES_SURVEY_ARCHIVE_FILES_TABLE = 'sssmbes_survey_arch_file';
    const SSSMBES_SURVEY_TABLE = 'sssmbes_survey';

    const SEISMIC_RESOLUTION_CLASS_TABLE = 'seismic_resolution_class';

    const SEISMIC_SOURCE_TYPE_TABLE = 'seismic_source_type';
    const SEISMIC_SOURCE_TABLE = 'seismic_source';
    // const SEISMIC_RECEIVER_TABLE = 'seismic_receiver';

    const SEISMIC2D_SURVEY_PERM_TABLE = 'seismic2d_survey_permission';
    const SEISMIC2D_SURVEY_ARCHIVE_FILES_TABLE = 'seismic2d_survey_arch_file';
    const SEISMIC2D_SURVEY_TABLE = 'seismic2d_survey';

    const SEISMIC2D_SEISMOGRAM_TABLE = 'seismic2d_seismogram';

    const SEISMIC2D_STACK_TABLE = 'seismic2d_stack';

    CONST SEISMIC_STACK_ATTR_TYPE_TABLE = 'seismic_stack_attr_type';
    const SEISMIC2D_STACK_ATTR_TABLE = 'seismic2d_stack_attr';

    const SEISMIC3D_SURVEY_PERM_TABLE = 'seismic3d_survey_permission';
    const SEISMIC3D_SURVEY_ARCHIVE_FILES_TABLE = 'seismic3d_survey_arch_file';
    const SEISMIC3D_SURVEY_TABLE = 'seismic3d_survey';

    const SEISMIC3D_STACK_TABLE = 'seismic3d_stack';

    const SEISMIC3D_STACK_ATTR_TABLE = 'seismic3d_stack_attribute';

    const HORIZON_TYPE_TABLE = 'horizon_type';
    const HORIZON_TABLE = 'horizon';

    const SEISMIC2D_HORIZON_TABLE = 'horizon_seismic2d_stack';
    const SEISMIC3D_HORIZON_TABLE = 'horizon_seismic3d_stack';

    const HORIZON_ATTRIBUTE_TYPE_TABLE = 'horizon_attr_type';
    const HORIZON_SEISMIC2D_STACK_ATTR_TABLE = 'horizon_seismic2d_stack_attr';
    const HORIZON_SEISMIC3D_STACK_ATTR_TABLE = 'horizon_seismic3d_stack_attr';

    const FAULT_RISK_DEGREE_TABLE = 'fault_risk_degree';
    const FAULT_ACTIVITY_DEGREE_TABLE = 'fault_activity_degree';
    const FAULT_TYPE_TABLE = 'fault_type';
    const FAULT_TABLE = 'fault';

    const FAULT_GROUP_TYPE_TABLE = 'fault_group_type';
    const FAULT_GROUP_TABLE = 'fault_group';

    const FAULT_GROUP_FAULT_TABLE = 'fault_group_fault';

    const SEISMIC2D_FAULT_TABLE = 'fault_seismic2d_stack';

    const FAULT_HORIZON_TABLE = 'fault_horizon';
    
    const MOSAIC_TYPE_TABLE = 'mosaic_type';
    const MOSAIC_PERM_TABLE = 'mosaic_permission';
    const MOSAIC_ARCHIVE_FILES_TABLE = 'mosaic_arch_file';
    const MOSAIC_TABLE = 'mosaic';

    const HAZARD_CLASS_TABLE = 'hazard_class';
    const HAZARD_TYPE_TABLE = 'hazard_type';
    const GLACIAL_HAZARD_TABLE = 'glacial_hazard';
    const GLACIAL_HAZARD_GROUP_TABLE = 'glacial_hazard_group';

    const FLUIDOGENIC_HAZARD_TABLE = 'fluidogenic_hazard';
    const FLUIDOGENIC_HAZARD_GROUP_TABLE = 'fluidogenic_hazard_group';

    const TECHNOGENIC_HAZARD_TABLE = 'technogenic_hazard';
    const TECHNOGENIC_HAZARD_GROUP_TABLE = 'technogenic_hazard_group';

    const GEOMORPHOLOGICAL_HAZARD_TABLE = 'geomorphological_hazard';
    const GEOMORPHOLOGICAL_HAZARD_GROUP_TABLE = 'geomorphological_hazard_group';

    const HYDROLOGICAL_HAZARD_TABLE = 'hydrological_hazard';
    const HYDROLOGICAL_HAZARD_GROUP_TABLE = 'hydrological_hazard_group';

    const ATMOSPHERIC_HAZARD_TABLE = 'atmospheric_hazard';
    const ATMOSPHERIC_HAZARD_GROUP_TABLE = 'atmospheric_hazard_group';

    const GEOCRYOGENIC_HAZARD_TABLE = 'geocryogenic_hazard';
    const GEOCRYOGENIC_HAZARD_GROUP_TABLE = 'geocryogenic_hazard_group';

    const GEOLOGICAL_HAZARD_TABLE = 'geological_hazard';
    const GEOLOGICAL_HAZARD_GROUP_TABLE = 'geological_hazard_group';

    const SEISMOTECTONIC_HAZARD_TABLE = 'seismotectonic_hazard';
    const SEISMOTECTONIC_HAZARD_GROUP_TABLE = 'seismotectonic_hazard_group';

    const INTERPRETED_MODEL_TABLE = 'interpreted_model';
    const INTERPRETED_MODEL_OBJECT_TABLE = 'interpreted_model_object';
}