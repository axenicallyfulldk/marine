<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InterpretedModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'hasBackground' => !is_null($this->file_path),
            'backgroundSize' => $this->file_size,
            'area' => new GeometryResource($this->shape),
            'creationDate' => $this->creation_date,
            'hazards' => HazardResource::collection($this->hazards)
        ];
    }
}
