<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            // 'email' => $this->email,
            'editTool' => $this->editToolsByDefault(),
            'editVessel' => $this->editVesselsByDefault(),
            'editZone' => $this->editZonesByDefault(),
            'seeSssMbesSurvey' => $this->seeSssMbesSurveysByDefault(),
            'downloadSssMbesSurvey' => $this->downloadSssMbesSurveysByDefault(),
            'editSssMbesSurvey' => $this->editSssMbesSurveysByDefault(),
            'seeMosaic' => $this->seeMosaicsByDefault(),
            'downloadMosaic' => $this->downloadMosaicsByDefault(),
            'editMosaic' => $this->editMosaicsByDefault(),
            'seeSeismic2dSurvey' => $this->seeSeismic2dSurveysByDefault(),
            'downloadSeismic2dSurvey' => $this->downloadSeismic2dSurveysByDefault(),
            'editSeismic2dSurvey' => $this->editSeismic2dSurveysByDefault(),
            'seeSeismic3dSurvey' => $this->seeSeismic3dSurveysByDefault(),
            'downloadSeismic3dSurvey' => $this->downloadSeismic3dSurveysByDefault(),
            'editSeismic3dSurvey' => $this->editSeismic3dSurveysByDefault(),
        ];
    }
}
