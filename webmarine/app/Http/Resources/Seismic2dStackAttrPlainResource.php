<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Seismic2dStackAttrPlainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'stack' => $this->{FK::get(\App\Tables::SEISMIC2D_STACK_TABLE)},
            'type' => $this->type->name,
            'horizon1' => $this->horizon_1,
            'horizon2' => $this->horizon_2,
            'fileSize' =>  $this->when($this->isDownloadable(), $this->file_size),
            'comment' => $this->comment,
        ];
    }
}
