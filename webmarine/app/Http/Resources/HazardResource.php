<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class HazardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->resource instanceof \App\Models\Modelable){
            return (new HazardResource($this->resource->modelable))->toArray($request);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->{FK::get(\App\Tables::HAZARD_TYPE_TABLE)},
            'shape' => new GeometryResource($this->shape),
        ];
    }
}
