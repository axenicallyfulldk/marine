<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class Seismic2dSurveyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fieldName = null;
        if(!is_null($this->{FK::get(\App\Tables::FIELD_TABLE)})){
            $fieldName = $this->field->name;
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'parentZone' => $this->zone_id,
            'field' => $this->when(!is_null($this->{FK::get(\App\Tables::FIELD_TABLE)}), $fieldName),
            'direction' => $this->direction,
            'trip' => new TripPlainResource($this->trip),
            // 'source' => $this->source->name,
            'source' => new SeismicSourceResource($this->source),
            'srcGroupCount' => $this->src_group_count,
            'srcPerGroup' => $this->src_per_group,
            'streamer' => $this->streamer->name,
            'streamerCount' => $this->streamer_count,
            'isTowed' => $this->is_towed,
            // 'resolution' => $this->resolution->name,
            'resolution' => new SeismicResolutionResource($this->resolution),
            'seismogramms' => $this->when($this->isDownloadable(), Seismic2dSeismogrammPlainResource::collection($this->seismogramms)),
            'stacks' => $this->when($this->isDownloadable(), Seismic2dStackPlainResource::collection($this->stacks)),
            'archFiles' => SurveyFileResource::collection($this->archFiles),
            // 'area' => \geoPHP::load($this->area)->asArray(),
            // 'area' => \geoPHP::load($this->area)->out('json'),
            'area' => new GeometryResource($this->area),
            'customer' => $this->customer,
            'operator' => $this->operator,
            'startDate' => $this->start_date,
            'archSize' => $this->when($this->isDownloadable(), $this->arch_size),
            'isEditable' => $this->isEditable(),
            'isDownloadable' => $this->isDownloadable()
        ];
    }
}
