<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SssMbesSurveyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parentZone' => new ZonePlainResource($this->parent),
            'direction' => $this->direction,
            'type' => $this->type->key,
            'trip' => new TripPlainResource($this->trip),
            'tool' => new ToolResource($this->tool),
            'archFiles' => SurveyFileResource::collection($this->archFiles),
            // 'area' => \geoPHP::load($this->area)->asArray(),
            // 'area' => \geoPHP::load($this->area)->out('json'),
            'area' => new GeometryResource($this->area),
            'startDate' => $this->start_date,
            'fileSize' => $this->when($this->isDownloadable(), $this->file_size),
            'archSize' => $this->when($this->isDownloadable(), $this->arch_size),
            'isEditable' => $this->isEditable(),
            'isDownloadable' => $this->isDownloadable()
        ];
    }
}
