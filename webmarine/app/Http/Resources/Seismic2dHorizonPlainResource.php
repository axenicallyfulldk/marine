<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use \App\Helpers\FK;

class Seismic2dHorizonPlainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->horizon->name,
            'horizon' => $this->{FK::get(\App\Tables::HORIZON_TABLE)},
            'stack' => $this->{FK::get(\App\Tables::SEISMIC2D_STACK_TABLE)},
            'coords' => json_decode($this->coords)
        ];
    }
}
