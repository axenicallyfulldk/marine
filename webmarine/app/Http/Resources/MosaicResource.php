<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MosaicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $area = \geoPHP::load($this->area);
        $areaGeos = $area->getGeos();
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parentZone' => $this->zone_id,
            'inputDirection' => $this->input_direction,
            'type' => $this->type->key,
            'mainTool' => new ToolResource($this->mainTool),
            'archFiles' => MosaicFileResource::collection($this->archFiles),
            // 'area' => \geoPHP::load($this->area)->asArray(),
            // 'area' => \geoPHP::load($this->area)->getGeoInterface(),
            'area' => new GeometryResource($this->area),
            'creationDate' => $this->creation_date,
            'fileSize' => $this->when($this->isDownloadable(), $this->file_size),
            'archSize' => $this->when($this->isDownloadable(), $this->arch_size),
            'isEditable' => $this->isEditable(),
            'isDownloadable' => $this->isDownloadable()
        ];
    }
}
