<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class Seismic2dStackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'survey' => new Seismic2dSurveySuperPlainResource($this->survey),
            'attributes' => Seismic2dStackAttrPlainResource::collection($this->attributes),
            'faults' => Seismic2dFaultPlainResource::collection($this->faults),
            'horizons' => Seismic2dHorizonPlainResource::collection($this->horizons),
            'fileSize' => $this->when($this->isDownloadable(), $this->file_size),
            'isMigrated' => $this->is_migrated,
            'isAmplCorrected' => $this->is_ampl_corrected,
            'isDeconvolved' => $this->is_deconvolved,
            'cdpOffset' => $this->cdp_offset,
            'cdpSize' => $this->cdp_size,
            'cdpStart' => $this->cdp_start,
            'cdpStep' => $this->cdp_step,
            'cdpEnd' => $this->cdp_end,
            'cdpDistance' => $this->cdp_distance,
            'isDownloadable' => $this->isDownloadable()
        ];
    }
}
