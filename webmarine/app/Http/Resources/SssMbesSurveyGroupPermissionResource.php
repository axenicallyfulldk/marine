<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SssMbesSurveyGroupPermissionResource extends JsonResource
{
    const surveyIdField = FK::get(\App\Tables::SSSMBES_SURVEY_TABLE);
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'group' => $this->user_id,
            // 'survey' => $this->survey_id,
            'survey' => $this->{\App\Helpers\FK::get(\App\Tables::SSSMBES_SURVEY_TABLE)},
            'see' => $this->see,
            'download' => $this->download,
            'edit' => $this->edit
        ];
    }
}
