<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VesselResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'homePort' => $this->home_port,
            'shipowner' => $this->shipowner,
            'imo' => $this->imo,
            'mmsi' => $this->mmsi,
            'callsign' => $this->callsign,
            'length' => $this->length,
            'width' => $this->width,
            'drought' => $this->drought,
            'displacement' => $this->displacement,
            'speedKn' => $this->speed_kn,
            'buildDate' => $this->build_date
        ];
    }
}
