<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ToolCountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'tool' => $this->when(isset($this->tool), new ToolResource($this->tool ?? null)),
            'main_tool' => $this->when(isset($this->main_tool), new ToolResource($this->main_tool ?? null)),
            'count' => $this->count
        ];
    }
}
