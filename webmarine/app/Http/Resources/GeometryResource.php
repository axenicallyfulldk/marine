<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GeometryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $area =  \geoPHP::load($this->resource);
        $res = [
            'type' => $area->geometryType(),
            'coordinates' => $area->asArray(),
        ];
        return $res;
    }
}
