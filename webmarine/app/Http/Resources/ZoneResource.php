<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ZoneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'parentZone' => new ZonePlainResource($this->parent),
            'name' => $this->name,
            'code' => $this->code,
            'type' => $this->type,
            // 'area' => \geoPHP::load($this->area)->asArray(),
            // 'area' => \geoPHP::load($this->area)->out('json'),
            'area' => new GeometryResource($this->area),
            'subzones' => ZonePlainResource::collection($this->children),
            'sssMbesSurveys' => SssMbesSurveyResource::collection($this->visibleSssMbesSurveys()->get()),
            'seismic2dSurveys' => Seismic2dSurveyPlainResource::collection($this->visibleSeismic2dSurveys()->get()),
            'seismic3dSurveys' => Seismic3dSurveyPlainResource::collection($this->visibleSeismic3dSurveys()->get()),
            'mosaics' => MosaicResource::collection($this->visibleMosaics),
            'interpretedModels' => InterpretedModelPlainResource::collection($this->visibleInterpretedModels),
            'defaultYear' => $this->default_year
        ];
    }
}
