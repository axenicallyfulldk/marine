<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SssMbesSurveyUserPermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => $this->user_id,
            'survey' => $this->survey_id,
            'see' => $this->see,
            'download' => $this->download,
            'edit' => $this->edit
        ];
    }
}
