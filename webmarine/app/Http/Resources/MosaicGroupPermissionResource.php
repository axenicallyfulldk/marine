<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MosaicGroupPermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'group' => $this->user_id,
            'mosaic' => $this->mosaic_id,
            'see' => $this->see,
            'download' => $this->download,
            'edit' => $this->edit
        ];
    }
}
