<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class Seismic2dSeismogrammPlainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'survey' => $this->{FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE)},
            'fileSize' => $this->when($this->survey->isDownloadable(), $this->file_size),
            'spStart' => $this->sp_start,
            'spStep' => $this->sp_step,
            'spEnd' => $this->sp_end,
            'spDistance' => $this->sp_distance,
        ];
    }
}
