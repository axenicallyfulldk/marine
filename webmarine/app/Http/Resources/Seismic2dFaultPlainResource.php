<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class Seismic2dFaultPlainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->fault->name,
            'fault' => $this->{FK::get(\App\Tables::FAULT_TABLE)},
            'stack' => $this->{FK::get(\App\Tables::SEISMIC2D_STACK_TABLE)},
            'coords' => json_decode($this->coords),
        ];
    }
}
