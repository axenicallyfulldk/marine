<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class Seismic3dStackPlainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'survey' => $this->{FK::get(\App\Tables::SEISMIC3D_SURVEY_TABLE)},
            'fileSize' => $this->when($this->survey->isDownloadable(), $this->file_size),
            'isMigrated' => $this->is_migrated,
            'isAmplCorrected' => $this->is_ampl_corrected,
            'isDeconvolved' => $this->is_deconvolved,
            'inlineOffset' => $this->inline_offset,
            'inlineSize' => $this->inline_size,
            'inlineDistance' => $this->inline_distance,
            'xlineOffset' => $this->xline_offset,
            'xlineSize' => $this->xline_size,
            'xlineDistance' => $this->xline_distance
        ];
    }
}
