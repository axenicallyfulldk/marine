<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DirectionCountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'direction' => $this->when(isset($this->direction), $this->direction ?? null),
            'input_direction' => $this->when(isset($this->input_direction), $this->input_direction ?? null),
            'count' => $this->count
        ];
    }
}
