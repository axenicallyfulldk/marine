<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Helpers\FK;

class Seismic3dSurveyPlainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'parentZone' => $this->zone_id,
            // 'field' => $this->when(!is_null($this->{FK::get(\App\Tables::FIELD_TABLE)}), $this->field->name),
            'field' => $this->{FK::get(\App\Tables::FIELD_TABLE)},
            'direction' => $this->direction,
            // 'trip' => new TripPlainResource($this->trip),
            'trip' => $this->{FK::get(\App\Tables::TRIP_TABLE)},
            // 'source' => $this->source->name,
            'source' => new SeismicSourceResource($this->source),
            'srcGroupCount' => $this->src_group_count,
            'srcPerGroup' => $this->src_per_group,
            'streamer' => is_null($this->streamer) ? null : $this->streamer->name,
            'streamerCount' => $this->streamer_count,
            'distBetweenStreamers' => $this->dist_between_streamers,
            'isTowed' => $this->is_towed,
            'resolution' => new SeismicResolutionResource($this->resolution),
            'stacks' => $this->when($this->isDownloadable(), Seismic3dStackPlainResource::collection($this->stacks)),
            // 'resolution' => $this->resolution->name,
            // 'area' => \geoPHP::load($this->area)->asArray(),
            // 'area' => \geoPHP::load($this->area)->out('json'),
            'area' => new GeometryResource($this->area),
            'customer' => $this->customer,
            'operator' => $this->operator,
            'startDate' => $this->start_date,
            'archSize' => $this->when($this->isDownloadable(), $this->arch_size),
            'isEditable' => $this->isEditable(),
            'isDownloadable' => $this->isDownloadable()
        ];
    }
}
