<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CheckSurveyPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $curUser = \Auth::user();

        if(!$curUser->workWithSssMbesSurveysPermissions()){
            throw new AccessDeniedHttpException("You have no right work with permissions!");
        }
        
        return $next($request);
    }
}
