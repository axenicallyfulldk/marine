<?php

namespace App\Http\Controllers;

use App\Helpers\DataSystem;
use Illuminate\Http\Request;

use App\Helpers\FolderSynchronizer;
use App\Services\InterpretedModelBatchImportService;

class BatchInterpretedModelController extends Controller
{
    public function import(Request $req){
        $req->validate([
            'file' => 'required'
        ]);

        $file = $req->file('file');

        $idf = env('IMPORT_DATA_FOLDER', '');
        if($idf){
            FolderSynchronizer::copy($idf, DataSystem::getDataPath());
        }

        $bis = new InterpretedModelBatchImportService();

        $oldFilename = $file->getPathname();
        $newFilename = $oldFilename.".xlsx";

        rename($oldFilename, $newFilename);
        
        $bis->importFromExcel($newFilename);

        rename($newFilename, $oldFilename);
    }
}
