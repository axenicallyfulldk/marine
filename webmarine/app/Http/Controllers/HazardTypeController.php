<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\HazardTypeResource;

use App\Models\HazardType;

class HazardTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return HazardTypeResource::collection(HazardType::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HazardType  $hazardType
     * @return \Illuminate\Http\Response
     */
    public function show(HazardType  $hazardType)
    {
        return new HazardTypeResource($hazardType);
    }
}
