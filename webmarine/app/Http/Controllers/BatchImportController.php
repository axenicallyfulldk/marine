<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Services\BatchImportService;

class BatchImportController extends Controller
{

    public function __construct(){
        $this->batchImportService = new BatchImportService();
        set_time_limit(8000000);
    }

    /**
     * Imports sss and mbes surveys and mosaics
     *
     * @return \Illuminate\Http\Response
     */
    public function sssmbesImport(Request $request){
        $this->batchImportService->importSssMbesData();
    }

    /**
     * Imports seismic 2d survey, seismic 3d survey, seismic 2d stack, seismic 3d stack
     *
     * @return \Illuminate\Http\Response
     */
    public function seismicImport(Request $request){
        $this->batchImportService->importSeismicData();
    }

    
}