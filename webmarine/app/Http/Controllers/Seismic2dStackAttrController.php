<?php

namespace App\Http\Controllers;

use App\Models\Seismic2dStackAttr;
use Illuminate\Http\Request;

class Seismic2dStackAttrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic2dStackAttr  $attr
     * @return \Illuminate\Http\Response
     */
    public function show(Seismic2dStackAttr $attr)
    {
        if(!$attr->isVisible())
            throw new NotFoundHttpException("Attribute doesn't exist");

        
        return new Seismic2dStackResource($stack);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic2dStackAttr  $attr
     * @return \Illuminate\Http\Response
     */
    public function download(Seismic2dStackAttr $attr){
        if(!$attr->isVisible())
            throw new NotFoundHttpException("Attribute doesn't exist");

        if(!$attr->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download attribute");

        $path = $attr->getAbsoluteFilePath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = array('Content-Type: application/unknown',);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Seismic2dStackAttr  $seismic2dStackAttr
     * @return \Illuminate\Http\Response
     */
    public function edit(Seismic2dStackAttr $seismic2dStackAttr)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seismic2dStackAttr  $seismic2dStackAttr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seismic2dStackAttr $seismic2dStackAttr)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seismic2dStackAttr  $seismic2dStackAttr
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seismic2dStackAttr $seismic2dStackAttr)
    {
        //
    }
}
