<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Group;

use Illuminate\Http\Request;

use App\Http\Resources\UserPlainResource;
use App\Http\Resources\UserResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $user = \Auth::user();
        if(!($user->isAdmin() || $user->isModerator()))
            throw new AccessDeniedHttpException('You have no right to get users list.');

        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = User::query();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return UserPlainResource::collection($query->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $curUser = \Auth::user();

        if(is_null($user->group_id))
            throw new NotFoundHttpException("User doesn't exist");

        
        if($curUser->isAdmin()){
            
            return new UserResource($user);
        }
        else{
            
            return new UserResource($user);
        }
    }

    /**
     * Display current user info.
     *
     * @return \Illuminate\Http\Response
     */
    public function me(Request $request)
    {
        
        return new UserResource(\Auth::user());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $curUser = \Auth::user();
        if(!$curUser->isAdmin())
            throw new AccessDeniedHttpException('You have no right to create user.');
        
        $data = [];
        if(count($request->json()->all()))
            $data = $request->json()->all();

        $rules = [
            'name' => ['required', 'min:3', 'max:255'],
            'email' => ['required', 'email', 'unique:'.\App\Tables::USER_TABLE.',email'],
            'password' => ['required', 'min:3'],
            'group' => ['required', 'exists:'.\App\Tables::GROUP_TABLE.',id'],
            'editTool' => ['nullable', 'boolean'],
            'editVessel' => ['nullable', 'boolean'],
            'editZone' => ['nullable', 'boolean'],
            'seeSurvey' => ['nullable', 'boolean'],
            'downloadSurvey' => ['nullable', 'boolean'],
            'editSurvey' => ['nullable', 'boolean'],
            'seeMosaic' => ['nullable', 'boolean'],
            'downloadMosaic' => ['nullable', 'boolean'],
            'editMosaic' => ['nullable', 'boolean'],
        ];      
        
        $request->validate($rules);

        $group = Group::find($data['group']);
        if(!is_null($group->group_id)){
            throw ValidationException::withMessages([
                'group' => ['Group doesn\'t exist'],
                ]);
        }

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = \Hash::make($data['password']);
        $user->group()->associate($data['group']);
        if(isset($data['editTool'])) $user->edit_tool = $data['editTool'];
        if(isset($data['editVessel'])) $user->edit_vessel = $data['editVessel'];
        if(isset($data['editZone'])) $user->edit_zone = $data['editZone'];
        if(isset($data['seeSurvey'])) $user->see_sssmbes_survey = $data['seeSurvey'];
        if(isset($data['downloadSurvey'])) $user->download_sssmbes_survey = $data['downloadSurvey'];
        if(isset($data['editSurvey'])) $user->edit_sssmbes_survey = $data['editSurvey'];
        if(isset($data['seeMosaic'])) $user->see_mosaic = $data['seeMosaic'];
        if(isset($data['downloadMosaic'])) $user->download_mosaic = $data['downloadMosaic'];
        if(isset($data['editMosaic'])) $user->edit_mosaic = $data['editMosaic'];
        $user->save();

        
        return new UserResource($user->fresh());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $curUser = \Auth::user();
        if(!$curUser->isAdmin())
            throw new AccessDeniedHttpException('You have no right to update user.');

        if(is_null($user->group_id))
            throw new NotFoundHttpException("User doesn't exist");
        
        $data = [];
        if(count($request->json()->all()))
            $data = $request->json()->all();

        $rules = [
            'name' => ['nullable', 'min:3', 'max:255'],
            'password' => ['nullable', 'min:3'],
            'group' => ['nullable', 'exists:'.\App\Tables::GROUP_TABLE.',id'],
            'editTool' => ['nullable', 'boolean'],
            'editVessel' => ['nullable', 'boolean'],
            'editZone' => ['nullable', 'boolean'],
            'seeSurvey' => ['nullable', 'boolean'],
            'downloadSurvey' => ['nullable', 'boolean'],
            'editSurvey' => ['nullable', 'boolean'],
            'seeMosaic' => ['nullable', 'boolean'],
            'downloadMosaic' => ['nullable', 'boolean'],
            'editMosaic' => ['nullable', 'boolean'],
        ];      
        
        $request->validate($rules);

        // Check that id of group is indeed group id (not a user id).
        if(isset($data['group'])){
            $group = Group::find($data['group']);
            if(!is_null($group->group_id)){
                throw ValidationException::withMessages([
                    'group' => ['Group doesn\'t exist'],
                    ]);
            }
        }
        
        // Check email
        if(isset($data['email'])){
            if(\DB::table(\App\Tables::USER_TABLE)->where('email', $data['email'])->where('id', '<>', $user->id)->count()){
                throw ValidationException::withMessages([
                    'email' => ['Email has been already taken.'],
                    ]);
            }
        }
        
        if(isset($data['name'])) $user->name = $data['name'];
        if(isset($data['email'])) $user->email = $data['email'];
        if(isset($data['password'])) $user->password = \Hash::make($data['password']);
        if(isset($data['group'])) $user->group()->associate($data['group']);
        if(isset($data['editTool'])) $user->edit_tool = $data['editTool'];
        if(isset($data['editVessel'])) $user->edit_vessel = $data['editVessel'];
        if(isset($data['editZone'])) $user->edit_zone = $data['editZone'];
        if(isset($data['seeSurvey'])) $user->see_sssmbes_survey = $data['seeSurvey'];
        if(isset($data['downloadSurvey'])) $user->download_sssmbes_survey = $data['downloadSurvey'];
        if(isset($data['editSurvey'])) $user->edit_sssmbes_survey = $data['editSurvey'];
        if(isset($data['seeMosaic'])) $user->see_mosaic = $data['seeMosaic'];
        if(isset($data['downloadMosaic'])) $user->download_mosaic = $data['downloadMosaic'];
        if(isset($data['editMosaic'])) $user->edit_mosaic = $data['editMosaic'];
        $user->save();

        
        return new UserResource($user->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $curUser = \Auth::user();
        if(!$curUser->isAdmin()){
            throw new AccessDeniedHttpException('You have no right to delete user.');
        }

        $user->delete();

        return \Response::make("", 204);
    }

}
