<?php

namespace App\Http\Controllers;

use App\Models\Seismic2dStack;
use Illuminate\Http\Request;

use App\Http\Resources\Seismic2dStackPlainResource;
use App\Http\Resources\Seismic2dStackResource;

use Illuminate\Support\Facades\Storage;

class Seismic2dStackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'name' => ['nullable'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $query = Seismic2dStack::visible()->
            byNameOrAliasSubstr($request->name);
        
        // $query->with('survey');
        
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        // echo($query->toSql()."\n\n\n");

        
        return Seismic2dStackPlainResource::collection($query->get());
        // return $query->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic2dStack  $stack
     * @return \Illuminate\Http\Response
     */
    public function show(Seismic2dStack $stack)
    {
        if(!$stack->isVisible())
            throw new NotFoundHttpException("Stack doesn't exist");

        
        return new Seismic2dStackResource($stack);
    }


    public function download(Seismic2dStack $stack){
        if(!$stack->isVisible())
            throw new NotFoundHttpException("Stack doesn't exist");

        if(!$stack->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download stack data");

        $path = $stack->getAbsoluteFilePath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = array('Content-Type: application/unknown',);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seismic2dStack  $stack
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seismic2dStack $stack)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seismic2dStack  $stasck
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seismic2dStack $stack)
    {
        //
    }
}
