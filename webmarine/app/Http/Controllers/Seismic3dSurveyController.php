<?php

namespace App\Http\Controllers;

use App\Models\Seismic3dSurvey;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\Seismic3dSurveyPlainResource;
use App\Http\Resources\Seismic3dSurveyResource;

use Illuminate\Support\Facades\Storage;

class Seismic3dSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['nullable', Rule::in(array_values(Seismic3dSurvey::DIRECTION))],
            'source' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);


        $query = Seismic3dSurvey::visible()->
            inYear($request->year)->
            withDirection($request->direction)->
            inZone($request->parent)->
            byNameOrAliasSubstr($request->name)->
            bySource($request->source);
        
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        // $query->with(['trip', 'trip.vessel', 'type', 'tool', 'archFiles']);

        
        return Seismic3dSurveyPlainResource::collection($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic3dSurvey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Seismic3dSurvey $survey)
    {
        if(!$survey->isVisible())
            throw new NotFoundHttpException("Survey doesn't exist");

        
        return new Seismic3dSurveyResource($survey);
    }


    public function downloadArchive(Seismic2dSurvey $survey){
        if(!$survey->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download survey data");
        
        if($survey->is_deleted)
            throw new NotFoundHttpException("Archive doesn't exist");

        $path = $survey->getAbsoluteArchPath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("Archive doesn't exist");
        }

        $extToMime = ['zip' => 'application/zip', 'rar' => 'application/x-rar'];

        $mimeType = $extToMime[pathinfo($path, PATHINFO_EXTENSION)];
        if(is_null($mimeType)) $mimeType = 'application/unknown';

        $headers = array('Content-Type: '.$mimeType,);
        
        // return \Response::download($path, basename($path), $headers);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seismic3dSurvey  $seismic3dSurvey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seismic3dSurvey $seismic3dSurvey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seismic3dSurvey  $seismic3dSurvey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seismic3dSurvey $seismic3dSurvey)
    {
        //
    }
}
