<?php

namespace App\Http\Controllers;

use App\Models\Tool;
use Illuminate\Http\Request;

use App\Http\Resources\ToolResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = Tool::visibleTools();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return ToolResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        if(!$user->editToolsByDefault())
            throw new AccessDeniedHttpException('You have no right to create tools.');

        $data = [];
        if(count($request->json()->all()))
            $data = $request->json()->all();
        
        $rules = [
            'brand' => 'required|max:64',
            'model' => 'required|max:128'
        ];

        $request->validate($rules);

        $tool = new Tool();
        $tool->brand = $data['brand'];
        $tool->model = $data['model'];
        $tool->save();

        
        return new ToolResource($tool->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function show(Tool $tool)
    {
        
        return new ToolResource($tool);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function edit(Tool $tool)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tool $tool)
    {
        $user = \Auth::user();
        if(!$user->editToolsByDefault())
            throw new AccessDeniedHttpException('You have no right to edit tools.');

        $data = [];
        if(count($request->json()->all()))
            $data = $request->json()->all();
        
        $rules = [
            'brand' => 'nullable|max:64',
            'model' => 'nullable|max:128'
        ];

        $request->validate($rules);

        if(isset($data['brand'])) $tool->brand = $data['brand'];
        if(isset($data['model'])) $tool->model = $data['model'];
        $tool->save();

        
        return new ToolResource($tool->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tool  $tool
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tool $tool)
    {
        $user = \Auth::user();
        if(!$user->editToolsByDefault()){
            throw new AccessDeniedHttpException('You have no right to delete tools.');
        }
        $tool->is_deleted = true;
        $tool->save();

        return \Response::make("", 204);
    }
}
