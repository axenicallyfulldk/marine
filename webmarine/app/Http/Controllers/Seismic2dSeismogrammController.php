<?php

namespace App\Http\Controllers;

use App\Models\Seismic2dSeismogramm;
use Illuminate\Http\Request;

use App\Http\Resources\Seismic2dSeismogrammPlainResource;
use App\Http\Resources\Seismic2dSeismogrammResource;

use Illuminate\Support\Facades\Storage;

class Seismic2dSeismogrammController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'name' => ['nullable'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $query = Seismic2dSeismogramm::visible()->
            byNameOrAliasSubstr($request->name);
        
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        return Seismic2dSeismogrammPlainResource::collection($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic2dSeismogramm  $seismogramm
     * @return \Illuminate\Http\Response
     */
    public function show(Seismic2dSeismogramm $seismogramm)
    {
        if(!$seismogramm->isVisible())
            throw new NotFoundHttpException("Seismogramm doesn't exist");

        
        return new Seismic2dSeismogrammResource($seismogramm);
    }


    public function download(Seismic2dSeismogramm $seismogramm){
        if(!$seismogramm->isVisible())
            throw new NotFoundHttpException("Seismogramm doesn't exist");

        if(!$seismogramm->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download seismogramm");

        $path = $seismogramm->getAbsoluteFilePath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = array('Content-Type: application/unknown',);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seismic2dSeismogramm  $seismogramm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seismic2dSeismogramm $seismogramm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seismic2dSeismogramm  $seismogram
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seismic2dSeismogramm $seismogram)
    {
        //
    }
}
