<?php

namespace App\Http\Controllers;

use App\Models\Group;

use Illuminate\Http\Request;

use App\Http\Resources\GroupResource;
use App\Http\Resources\GroupPlainResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $user = \Auth::user();
        if(!$user->isAdmin() || $user->isModerator())
            throw new AccessDeniedHttpException('You have no right to get groups list.');

        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);
    
        $query = Group::query();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        return GroupPlainResource::collection($query->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $curUser = \Auth::user();

        if(!is_null($group->group_id))
            throw new NotFoundHttpException("Group doesn't exist");

        
        if($curUser->isAdmin()){
            return new GroupResource($group);
        }
        else{
            return new GroupPlainResource($group);
        }
    }

    /**
     * Display current user info.
     *
     * @return \Illuminate\Http\Response
     */
    public function my(Request $request)
    {
        return new GroupResource(\Auth::user()->group);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $user = \Auth::user();
        if(!$user->isAdmin())
            throw new AccessDeniedHttpException('You have no right to create group.');

        $data = [];
            if(count($request->json()->all()))
                $data = $request->json()->all();

        $rules = [
            'name' => ['required', 'min:3', 'max:255'],
            'email' => ['required', 'email'],
            'editTool' => ['required', 'boolean'],
            'editVessel' => ['required', 'boolean'],
            'editZone' => ['required', 'boolean'],
            'seeSurvey' => ['required', 'boolean'],
            'downloadSurvey' => ['required', 'boolean'],
            'editSurvey' => ['required', 'boolean'],
            'seeMosaic' => ['required', 'boolean'],
            'downloadMosaic' => ['required', 'boolean'],
            'editMosaic' => ['required', 'boolean'],
        ];

        $request->validate($rules);

        $group = new Group();
        $group->name = $data['name'];
        $group->email = $data['email'];
        $group->password = \Hash::make(generateRandomString(30));
        $group->edit_tool = $data['editTool'];
        $group->edit_vessel = $data['editVessel'];
        $group->edit_zone = $data['editZone'];
        $group->see_sssmbes_survey = $data['seeSurvey'];
        $group->download_sssmbes_survey = $data['downloadSurvey'];
        $group->edit_sssmbes_survey = $data['editSurvey'];
        $group->see_mosaic = $data['seeMosaic'];
        $group->download_mosaic = $data['downloadMosaic'];
        $group->edit_mosaic = $data['editMosaic'];
        $group->save();

        return new GroupResource($group->fresh());
    }
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group){
        $user = \Auth::user();
        if(!$user->isAdmin())
            throw new AccessDeniedHttpException('You have no right to edit group.');
        
        if(!is_null($group->group_id))
            throw new NotFoundHttpException("Group doesn't exist");

        $data = [];
            if(count($request->json()->all()))
                $data = $request->json()->all();

        $rules = [
            'name' => ['nullable', 'min:3', 'max:255'],
            'email' => ['nullable', 'email'],
            'editTool' => ['nullable', 'boolean'],
            'editVessel' => ['nullable', 'boolean'],
            'editZone' => ['nullable', 'boolean'],
            'seeSurvey' => ['nullable', 'boolean'],
            'downloadSurvey' => ['nullable', 'boolean'],
            'editSurvey' => ['nullable', 'boolean'],
            'seeMosaic' => ['nullable', 'boolean'],
            'downloadMosaic' => ['nullable', 'boolean'],
            'editMosaic' => ['nullable', 'boolean'],
        ];

        $request->validate($rules);

        if(isset($data['name'])) $group->name = $data['name'];
        if(isset($data['email'])) $group->email = $data['email'];
        if(isset($data['editTool'])) $group->edit_tool = $data['editTool'];
        if(isset($data['editVessel'])) $group->edit_vessel = $data['editVessel'];
        if(isset($data['editZone'])) $group->edit_zone = $data['editZone'];
        if(isset($data['seeSurvey'])) $group->see_sssmbes_survey = $data['seeSurvey'];
        if(isset($data['downloadSurvey'])) $group->download_sssmbes_survey = $data['downloadSurvey'];
        if(isset($data['editSurvey'])) $group->edit_sssmbes_survey = $data['editSurvey'];
        if(isset($data['seeMosaic'])) $group->see_mosaic = $data['seeMosaic'];
        if(isset($data['downloadMosaic'])) $group->download_mosaic = $data['downloadMosaic'];
        if(isset($data['editMosaic'])) $group->edit_mosaic = $data['editMosaic'];
        $group->save();

        return new GroupResource($group->fresh());
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $curUser = \Auth::user();
        if(!$curUser->isAdmin()){
            throw new AccessDeniedHttpException('You have no right to delete group.');
        }

        $group->delete();

        return \Response::make("", 204);
    }
}
