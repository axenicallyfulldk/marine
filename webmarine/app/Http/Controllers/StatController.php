<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use App\Models\SssMbesSurvey;
use App\Models\SurveyType;
use App\Models\Zone;
use App\Models\Mosaic;
use App\Models\MosaicType;
use App\Models\Tool;
use App\Http\Resources\YearCountResource;
use App\Http\Resources\TypeCountResource;
use App\Http\Resources\ToolCountResource;
use App\Http\Resources\DirectionCountResource;

use App\Helpers\FK;

class StatController extends Controller
{
    //

    public function all()
    {
    }

    public function surveyYears(Request $request){
        $request->validate([
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer'],
            'direction' => ['nullable', Rule::in(array_values(SssMbesSurvey::DIRECTION))],
            'type' => ['nullable', 'integer_or_string'],
            'tool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $typeId = $request['type'];
        if(!is_null($typeId) && !is_numeric($typeId)){
            $typeObj = SurveyType::findByKey($typeId)->first();
            if(is_null($typeObj)){
                throw new HttpException(400, "Survey type '".$request['type']."' doesn't exist");
            }
            $typeId = $typeObj->id;
        }

        $res = SssMbesSurvey::getStatisticsByYear();
        if (!is_null($request['name'])){
            $res->whereRaw('lower(name) where (?)', ["%".\mb_strtolower($request['name'])."%"]);
        }

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        if(!is_null($request['direction'])){
            $res->where('direction', $request['direction']);
        }

        if(!is_null($typeId)){
            $res->where(FK::get(\App\Tables::SURVEY_TYPE_TABLE), $typeId);
        }

        if(!is_null($request['tool'])){
            $res->where(FK::get(\App\Tables::TOOL_TABLE), $request['tool']);
        }

        if(!is_null($request['limit'])){
            $res->take($request['limit']);
        }

        if(!is_null($request['offset'])){
            $res->take($request['offset']);
        }

        
        return YearCountResource::collection($res->get());
    }

    public function surveyTypes(Request $request){
        $request->validate([
            'parent' => ['nullable', 'integer'],
        ]);

        $res = SssMbesSurvey::getStatisticsByType();

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        $res = $res->get();

        $typeIds = array_keys($res->keyBy(FK::get(\App\Tables::SURVEY_TYPE_TABLE))->toArray());
        $types = SurveyType::whereIn('id', $typeIds)->get()->keyBy('id');

        foreach($res as $td){
            $td->type = $types[$td->survey_type_id];
        }

        
        return TypeCountResource::collection($res);
    }

    public function surveyTools(Request $request){
        $request->validate([
            'parent' => ['nullable', 'integer'],
        ]);

        $res = SssMbesSurvey::getStatisticsByTool();

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        $res = $res->get();

        $toolIds = array_keys($res->keyBy(FK::get(\App\Tables::TOOL_TABLE))->toArray());
        $tools = Tool::whereIn('id', $toolIds)->get()->keyBy('id');

        foreach($res as $td){
            $td->tool = $tools[$td->tool_id];
        }

        
        return ToolCountResource::collection($res);
    }

    public function surveyDirections(Request $request){
        $request->validate([
            'parent' => ['nullable', 'integer'],
        ]);

        $res = SssMbesSurvey::getStatisticsByDirection();

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        
        return DirectionCountResource::collection($res->get());
    }

    public function mosaicYears(Request $request){
        $request->validate([
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer'],
            'inputDirection' => ['nullable', Rule::in(array_values(Mosaic::INPUT_DIRECTION))],
            'type' => ['nullable', 'exists:'.\App\Tables::MOSAIC_TYPE_TABLE.',key'],
            'mainTool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'limit' => ['nullable', 'integer', 'min:0']
        ]);

        $res = Mosaic::getStatisticsByYear();
        if (!is_null($request['name'])){
            $res->whereRaw('lower(name) where (?)', ["%".\mb_strtolower($request['name'])."%"]);
        }

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        if(!is_null($request['inputDirection'])){
            $res->where('input_direction', $request['inputDirection']);
        }

        if(!is_null($request['type'])){
            $res->where(FK::get(\App\Tables::MOSAIC_TYPE_TABLE), MosaicType::where('key', $request['type'])->first()->id);
        }

        if(!is_null($request['mainTool'])){
            $res->where('main_'.FK::get(\App\Tables::TOOL_TABLE), $request['mainTool']);
        }

        if(!is_null($request['limit'])){
            $res->take($request['limit']);
        }

        if(!is_null($request['offset'])){
            $res->take($request['offset']);
        }

        
        return YearCountResource::collection($res->get());
    }

    public function mosaicTypes(Request $request){
        $request->validate([
            'parent' => ['nullable', 'integer'],
        ]);

        $res = Mosaic::getStatisticsByType();

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        $res = $res->get();

        $typeIds = array_keys($res->keyBy(FK::get(\App\Tables::MOSAIC_TYPE_TABLE))->toArray());
        $types = MosaicType::whereIn('id', $typeIds)->get()->keyBy('id');

        foreach($res as $td){
            $td->type = $types[$td->mosaic_type_id];
        }

        
        return TypeCountResource::collection($res);
    }

    public function mosaicTools(Request $request){
        $request->validate([
            'parent' => ['nullable', 'integer'],
        ]);

        $res = Mosaic::getStatisticsByTool();

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        $res = $res->get();

        $toolIds = array_keys($res->keyBy('main_tool_id')->toArray());
        $tools = Tool::whereIn('id', $toolIds)->get()->keyBy('id');

        foreach($res as $td){
            $td->main_tool = $tools[$td->main_tool_id];
        }

        
        return ToolCountResource::collection($res);
    }

    public function mosaicDirections(Request $request){
        $request->validate([
            'parent' => ['nullable', 'integer'],
        ]);

        $res = Mosaic::getStatisticsByDirection();

        if(!is_null($request['parent'])){
            $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($request['parent']));
        }

        
        return DirectionCountResource::collection($res->get());
    }
}
