<?php

namespace App\Http\Controllers;

use App\Models\SeismicStreamer;
use Illuminate\Http\Request;

use App\Http\Resources\SeismicStreamerResource;

class SeismicStreamerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = SeismicStreamer::query();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return SeismicStreamerResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SeismicStreamer  $seismicStreamer
     * @return \Illuminate\Http\Response
     */
    public function show(SeismicStreamer $seismicStreamer)
    {
        
        return new SeismicStreamerResource($seismicStreamer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SeismicStreamer  $seismicStreamer
     * @return \Illuminate\Http\Response
     */
    public function edit(SeismicStreamer $seismicStreamer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SeismicStreamer  $seismicStreamer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeismicStreamer $seismicStreamer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SeismicStreamer  $seismicStreamer
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeismicStreamer $seismicStreamer)
    {
        //
    }
}
