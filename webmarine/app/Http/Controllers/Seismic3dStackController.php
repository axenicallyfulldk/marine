<?php

namespace App\Http\Controllers;

use App\Models\Seismic3dStack;
use Illuminate\Http\Request;

use App\Http\Resources\Seismic3dStackPlainResource;
use App\Http\Resources\Seismic3dStackResource;

use Illuminate\Support\Facades\Storage;

class Seismic3dStackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'name' => ['nullable'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $query = Seismic3dStack::visible()->
            byNameOrAliasSubstr($request->name);
        
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return Seismic3dStackPlainResource::collection($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic3dStack  $stack
     * @return \Illuminate\Http\Response
     */
    public function show(Seismic3dStack $stack)
    {
        if(!$stack->isVisible())
            throw new NotFoundHttpException("Stack doesn't exist");

        
        return new Seismic3dStackResource($stack);
    }


    public function download(Seismic3dStack $stack){
        if(!$stack->isVisible())
            throw new NotFoundHttpException("Stack doesn't exist");

        if(!$stack->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download stack data");

        $path = $stack->getAbsoluteFilePath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = array('Content-Type: application/unknown',);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seismic2dStack  $stack
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seismic2dStack $stack)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seismic2dStack  $stasck
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seismic2dStack $stack)
    {
        //
    }
}
