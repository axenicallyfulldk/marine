<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Models\User;
use App\Models\SssMbesSurvey;

use App\Models\SssMbesSurveyPermission;

use App\Http\Resources\SssMbesSurveyGroupPermissionResource;

use App\Helpers\FK;

class SssMbesSurveyGroupPermissionController extends Controller
{
    /**
     * Display a listing of the user permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'survey' => ['nullable', 'exists:'.\App\Tables::SSSMBES_SURVEY_TABLE.',id'],
            'group' => ['nullable', 'exists:'.\App\Tables::USER_TABLE.',id,group_id,NULL'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = SssMbesSurveyPermission::forGroup($request->group)->onSurvey($request->survey);
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return SssMbesSurveyGroupPermissionResource::collection($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'survey' => [
                'required',
                'exists:'.\App\Tables::SSSMBES_SURVEY_TABLE.',id',
                Rule::unique(\App\Tables::SSSMBES_SURVEY_PERM_TABLE, FK::get(\App\Tables::SSSMBES_SURVEY_TABLES))->where(function ($query) use($request) {
                    return $query->where(FK::get(\App\Tables::USER_TABLE), $request->group);
                })
            ],
            // Check that it is really a group and not a user
            'group' => ['required', 'exists:'.\App\Tables::GROUP_TABLE.',id,group_id,NULL'],
            'see' => ['nullable', 'boolean'],
            'download' => ['nullable', 'boolean'],
            'edit' => ['nullable', 'boolean'],
        );

        $request->validate($rules);

        $sp = new SssMbesSurveyPermission();
        $sp->user()->associate($request->group);
        $sp->survey()->associate($request->survey);
        if(isset($request->see)) $sp->can_see = $request->see ? 100 : -100;
        if(isset($request->download)) $sp->can_download = $request->download ? 100 : -100;
        if(isset($request->edit)) $sp->can_edit = $request->edit ? 100 : -100;
        $sp->save();

        
        return new SssMbesSurveyGroupPermissionResource($sp);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SssMbesSurvey $survey
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(SssMbesSurvey $survey, Group $group)
    {
        $res = SssMbesSurveyPermission::forGroup($group->id)->onSurvey($survey->id)->first();
        if(is_null($res)){
            throw new NotFoundHttpException("There is no such permission");
        }

        
        return new SssMbesSurveyGroupPermissionResource($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SssMbesSurvey $survey
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SssMbesSurvey $survey, Group $group)
    {
        $rules = array(
            'see' => ['nullable', 'boolean'],
            'download' => ['nullable', 'boolean'],
            'edit' => ['nullable', 'boolean'],
        );

        $request->validate($rules);
        
        $resQuery = SssMbesSurveyPermission::query()->where(
            [FK::get(\App\Tables::SSSMBES_SURVEY_TABLE) => $survey->id, FK::get(\App\Tables::GROUP_TABLE) => $user->id]
        );
        
        $sp = $resQuery->first();
        if(is_null($sp)){
            throw new NotFoundHttpException("There is no such permission");
        }

        if(isset($request->see)) $sp->can_see = $request->see ? 100 : -100;
        else if($request->has('see')) $sp->can_see = 0;

        if(isset($request->download)) $sp->can_download = $request->download ? 100 : -100;
        else if($request->has('download')) $sp->can_download1 = 0;

        if(isset($request->edit)) $sp->can_edit = $request->edit ? 100 : -100;
        else if($request->has('edit')) $sp->can_edit = 0;
        $sp->save();

        
        return new SssMbesSurveyGroupPermissionResource($sp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SssMbesSurvey $survey
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(SssMbesSurvey $survey, Group $group)
    {
        $resQuery = SssMbesSurveyPermission::query()->where(
            [FK::get(\App\Tables::SSSMBES_SURVEY_TABLE) => $survey->id, FK::get(\App\Tables::GROUP_TABLE) => $group->id]
        );
        
        $res = $resQuery->first();
        if(is_null($res)){
            throw new NotFoundHttpException("There is no such permission");
        }

        $resQuery->delete();

        return \Response::make("", 204);
    }
}
