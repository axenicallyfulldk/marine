<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Mosaic;
use App\Models\MosaicType;
use App\Models\MosaicArchFile;
use App\Models\Tool;


use App\Http\Resources\MosaicResource;
use App\Http\Resources\MosaicTypeResource;
use App\Http\Resources\ToolResource;
use App\Http\Resources\ArrayResource;

use Illuminate\Validation\Rule;
use App\Helpers\PolygonParser;
use App\Helpers\FK;
use App\Models\Zone;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Illuminate\Validation\ValidationException;

use Illuminate\Support\Facades\Storage;

use App\Services\MosaicService;

class MosaicController extends Controller
{


    public function __construct(){
        $this->mosaicService = new MosaicService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'inputDirection' => ['nullable', Rule::in(array_values(Mosaic::INPUT_DIRECTION))],
            'type' => ['nullable', 'exists:'.\App\Tables::MOSAIC_TYPE_TABLE.',key'],
            'mainTool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $typeId = $request['type'];
        if(!is_null($typeId)){
            $typeId = MosaicType::findByKey($typeId)->first()->id;
        }

        $query = Mosaic::visibleMosaics()->
            inYear($request->year)->
            withInputDirection($request->inputDirection)->
            inZone($request->parent)->
            containsString($request->name)->
            ofType($typeId)->
            withMainTool($request->mainTool);

        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        $query->with(['archFiles', 'type', 'mainTool']);

        return MosaicResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return new MosaicResource($this->mosaicService->add($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mosaic  $mosaic
     * @return \Illuminate\Http\Response
     */
    public function show(Mosaic $mosaic)
    {
        if(!$mosaic->isVisible())
            throw new HttpException(404, "Mosaic doesn't exist");

        return new MosaicResource($mosaic);
    }


    public function downloadArchive(Mosaic $mosaic){
        if(!$mosaic->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download mosaic data");
        
        if($mosaic->is_deleted)
            throw new NotFoundHttpException("Archive doesn't exist");

        $path = $mosaic->getAbsoluteArchPath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("Archive doesn't exist");
        }

        $extToMime = ['zip' => 'application/zip', 'rar' => 'application/x-rar'];

        $mimeType = $extToMime[pathinfo($path, PATHINFO_EXTENSION)];
        if(is_null($mimeType)) $mimeType = 'application/unknown';

        $headers = array('Content-Type: '.$mimeType,);
        
        // return \Response::download($path, basename($path), $headers);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }


    public function downloadFile(Mosaic $mosaic){
        if(!$mosaic->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download mosaic data");

        if($mosaic->is_deleted)
            throw new NotFoundHttpException("File doesn't exist");

        $path = $mosaic->getAbsoluteFilePath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = array('Content-Type: application/unknown',);
        
        // return \Response::download($path, basename($path), $headers);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mosaic  $mosaic
     * @return \Illuminate\Http\Response
     */
    public function edit(Mosaic $mosaic)
    {
        //
    }

    /**
     * Update the specified mosaic in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mosaic  $mosaic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mosaic $mosaic)
    {
        return new MosaicResource($this->mosaicService->patch($request->all(), $mosaic));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mosaic  $mosaic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mosaic $mosaic)
    {
        $this->mosaicService->destroy($mosaic);
        return \Response::make("", 204);
    }

    /**
     * Returns years, directions, tools and types that are used in
     * visible for user mosaics.
     *
     * @return \Illuminate\Http\Response
     */
    public function unique(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'inputDirection' => ['nullable', Rule::in(array_values(Mosaic::INPUT_DIRECTION))],
            'type' => ['nullable', 'exists:'.\App\Tables::MOSAIC_TYPE_TABLE.',key'],
            'mainTool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id']
        ]);

        $applyFilters = function($req, $res){
            if (!is_null($req['year'])){
                $res->whereYear('creation_date', $req['year']);
            }

            if (!is_null($req['name'])){
                $res->whereRaw('lower(name) where (?)', ["%".\mb_strtolower($req['name'])."%"]);
            }
    
            if(!is_null($req['parent'])){
                $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($req['parent']));
            }
    
            if(!is_null($req['inputDirection'])){
                $res->where('input_direction', $req['inputDirection']);
            }

            $typeId = $req['type'];
            if(!is_null($typeId)){
                $typeId = MosaicType::findByKey($typeId)->first()->id;
            }
    
            if(!is_null($typeId)){
                $res->where(FK::get(\App\Tables::MOSAIC_TYPE_TABLE), $typeId);
            }    

            if(!is_null($req['mainTool'])){
                $res->where('main_tool_id', $req['mainTool']);
            }

            return $res;
        };

        $years = $applyFilters($request, Mosaic::getUniqueYears())->get();
        $directions = $applyFilters($request, Mosaic::getUniqueInputDirections())->get();
        $tools = Tool::whereIn('id', $applyFilters($request, Mosaic::getUniqueMainToolIds()))->get();
        $types = MosaicType::whereIn('id', $applyFilters($request, Mosaic::getUniqueTypeIds()))->get();

        return [
            'years' => new ArrayResource($years),
            'inputDirections' => new ArrayResource($directions),
            'mainTools' => ToolResource::collection($tools),
            'types' => MosaicTypeResource::collection($types)
        ];
    }
}
