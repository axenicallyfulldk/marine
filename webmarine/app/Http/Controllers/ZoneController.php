<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use App\Models\Zone;
use Illuminate\Http\Request;

use App\Http\Resources\ZonePlainResource;
use App\Http\Resources\ZoneResource;

use App\Services\ZoneService;

use App\Helpers\PolygonParser;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Illuminate\Support\Facades\Storage;

class ZoneController extends Controller
{

    private $zoneService;

    public function __construct(){
        $this->zoneService = new ZoneService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $request->validate([
            'type' => ['nullable', Rule::in(array_keys(Zone::ZONE_CLASSES))],
            'name' => ['nullable'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $query = Zone::query()->
            ofType($request->type)->
            containsString($request->name);

        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        $query->with([
            'children',
            'visibleSssMbesSurveys', 'visibleSssMbesSurveys.tool',
            'visibleSeismic2dSurveys', 'visibleSeismic2dSurveys.source', 'visibleSeismic2dSurveys.stacks', 'visibleSeismic2dSurveys.seismogramms', 'visibleSeismic2dSurveys.resolution',
            'visibleSeismic3dSurveys',
            'mosaics']);
            
            
        return ZonePlainResource::collection($query->get());
    }


    public function root(){
        return ZonePlainResource::collection(Zone::root()->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return new ZoneResource($this->zoneService->add($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        return new ZoneResource($zone);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(Zone $zone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
        
        return new ZoneResource($this->zoneService->patch($request->all(), $zone));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone)
    {
        $this->zoneService->destroy($zone);
        return \Response::make("", 204);
    }
}
