<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\SssMbesSurvey;
use App\Models\SurveyType;
use App\Models\SurveyArchFile;
use App\Models\Tool;
use App\Models\Trip;
use App\Models\Zone;

use App\Http\Requests\StoreSssMbesSurvey;
use App\Http\Requests\UpdateSssMbesSurvey;


use App\Http\Resources\SssMbesSurveyResource;
use App\Http\Resources\SurveyTypeResource;
use App\Http\Resources\ToolResource;
use App\Http\Resources\ArrayResource;

use App\Helpers\PolygonParser;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Illuminate\Validation\ValidationException;

use Illuminate\Support\Facades\Storage;

use App\Helpers\FK;

use App\Services\SssMbesSurveyService;

class SssMbesSurveyController extends Controller
{


    public function __construct(){
        $this->sssMbesSurveyService = new SssMbesSurveyService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['nullable', Rule::in(array_values(SssMbesSurvey::DIRECTION))],
            'type' => ['nullable', 'exists:'.\App\Tables::SURVEY_TYPE_TABLE.',key'],
            'tool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);

        $typeId = $request['type'];
        if(!is_null($typeId)) $typeId = SurveyType::findByKey($typeId)->first()->id;

        $query = SssMbesSurvey::visible()->
            inYear($request->year)->
            withDirection($request->direction)->
            inZone($request->parent)->
            containsString($request->name)->
            ofType($typeId)->
            byTool($request->tool);
        
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        $query->with(['trip', 'trip.vessel', 'type', 'tool', 'archFiles']);

        
        return SssMbesSurveyResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        return new SssMbesSurveyResource($this->sssMbesSurveyService->add($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SssMbesSurvey $survey
     * @return \Illuminate\Http\Response
     */
    public function show(SssMbesSurvey $survey)
    {
        if(!$survey->isVisible())
            throw new NotFoundHttpException("Survey doesn't exist");

        
        return new SssMbesSurveyResource($survey);
    }

    public function downloadArchive(SssMbesSurvey $survey){
        if(!$survey->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download survey data");
        
        if($survey->is_deleted)
            throw new NotFoundHttpException("Archive doesn't exist");

        $path = $survey->getAbsoluteArchPath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("Archive doesn't exist");
        }

        $extToMime = ['zip' => 'application/zip', 'rar' => 'application/x-rar'];

        $mimeType = $extToMime[pathinfo($path, PATHINFO_EXTENSION)];
        if(is_null($mimeType)) $mimeType = 'application/unknown';

        $headers = array('Content-Type: '.$mimeType,);
        
        // return \Response::download($path, basename($path), $headers);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    public function downloadFile(SssMbesSurvey $survey){
        if(!$survey->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download survey data");

        $path = $survey->getAbsoluteFilePath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = array('Content-Type: application/unknown',);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SssMbesSurvey $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(SssMbesSurvey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SssMbesSurvey $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SssMbesSurvey $survey)
    {
        
        return new SssMbesSurveyResource($this->sssMbesSurveyService->patch($request->all(), $survey));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SssMbesSurvey $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(SssMbesSurvey $survey)
    {
        $this->sssMbesSurveyService->destroy($survey);

        return \Response::make("", 204);
    }


    /**
     * Returns years, directions, tools and types that are used in
     * visible for user surveys.
     *
     * @return \Illuminate\Http\Response
     */
    public function unique(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['nullable', Rule::in(array_values(SssMbesSurvey::DIRECTION))],
            'type' => ['nullable', 'integer_or_string'],
            'tool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id']
        ]);

        $applyFilters = function($req, $res){
            if (!is_null($req['year'])){
                $res->whereYear('start_date', $req['year']);
            }

            if (!is_null($req['name'])){
                $res->whereRaw('lower(name) where (?)', ["%".\mb_strtolower($req['name'])."%"]);
            }
    
            if(!is_null($req['parent'])){
                $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($req['parent']));
            }
    
            if(!is_null($req['direction'])){
                $res->where('direction', $req['direction']);
            }

            $typeId = $req['type'];
            if(!is_null($typeId) && !is_numeric($typeId)){
                $typeObj = SurveyType::findByKey($typeId)->first();
                if(is_null($typeObj)){
                    throw new HttpException(400, "Survey type '".$req['type']."' doesn't exist");
                }
                $typeId = $typeObj->id;
            }
    
            if(!is_null($typeId)){
                $res->where(FK::get(\App\Tables::SURVEY_TYPE_TABLE), $typeId);
            }    

            if(!is_null($req['tool'])){
                $res->where('tool', $req['tool']);
            }

            return $res;
        };

        $years = $applyFilters($request, SssMbesSurvey::getUniqueYears())->get();
        $directions = $applyFilters($request, SssMbesSurvey::getUniqueDirections())->get();
        $tools = Tool::whereIn('id', $applyFilters($request, SssMbesSurvey::getUniqueToolIds()))->get();
        $types = SurveyType::whereIn('id', $applyFilters($request, SssMbesSurvey::getUniqueTypeIds()))->get();

        return [
            'years' => new ArrayResource($years),
            'directions' => new ArrayResource($directions),
            'tools' => ToolResource::collection($tools),
            'types' => SurveyTypeResource::collection($types)];
    }

    private $sssMbesSurveyService;
}
