<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\Seismic2dSurvey;
use App\Models\Seismic2dSurveyArchFile;
use App\Models\Trip;

// use App\Http\Requests\StoreSeismic2dSurvey;
// use App\Http\Requests\UpdateSeismic2dSurvey;

use App\Http\Resources\Seismic2dSurveyPlainResource;
use App\Http\Resources\Seismic2dSurveyResource;
use App\Http\Resources\ToolResource;
use App\Http\Resources\ArrayResource;

use App\Helpers\LineParser;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Illuminate\Validation\ValidationException;

use Illuminate\Support\Facades\Storage;

use App\Helpers\FK;

class Seismic2dSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['nullable', Rule::in(array_values(Seismic2dSurvey::DIRECTION))],
            'source' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        ]);


        $query = Seismic2dSurvey::visible()->
            inYear($request->year)->
            withDirection($request->direction)->
            inZone($request->parent)->
            byNameOrAliasSubstr($request->name)->
            bySource($request->source);
        
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        $query->with(['resolution', 'trip', 'trip.vessel', 'source', 'streamer', 'seismogramms', 'stacks']);

        // echo($query->toSql()."\n\n");

        
        return Seismic2dSurveyPlainResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSeismic2dSurvey $request)
    {
        // $trip = new Trip();
        // $trip->vessel()->associate($request->vessel);
        // $trip->save();

        // $survey = new Seismic2dSurvey();
        // $survey->name = $request->name;
        // $survey->area = \DB::raw("'".LineParser::parse($request->area)->toEKWT()."'");
        // $survey->parent()->associate($request->parent);
        // $survey->direction = $request->direction;
        // $survey->type()->associate(SurveyType::query()->where('key', $request->type)->get()[0]);
        // $survey->tool()->associate($request->tool);
        // $survey->trip()->associate($trip);
        // $survey->start_date = $request->startDate;
        // $survey->is_deleted = false;
        // $survey->file_path = $request->filePath;
        // $survey->file_size = filesize($survey->getAbsoluteFilePath());
        // $survey->arch_path = $request->archPath;
        // $survey->arch_size = filesize($survey->getAbsoluteArchPath());
        // $survey->save();

        // if(isset($request->archFiles)){
        //     foreach($request->archFiles as $file){
        //         $archFile = new SurveyArchFile();
        //         $archFile->survey()->associate($survey);
        //         $archFile->name = $file['name'];
        //         $archFile->description = $file['description'];
        //         $archFile->save();
        //     }
        // }
        
        // 
        // return new Seismic2dSurveyResource($survey->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seismic2dSurvey $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Seismic2dSurvey $survey)
    {
        if(!$survey->isVisible())
            throw new NotFoundHttpException("Survey doesn't exist");

        
        return new Seismic2dSurveyResource($survey);
    }

    public function downloadArchive(Seismic2dSurvey $survey){
        if(!$survey->isDownloadable())
            throw new AccessDeniedHttpException("You have no permission to download survey data");
        
        if($survey->is_deleted)
            throw new NotFoundHttpException("Archive doesn't exist");

        $path = $survey->getAbsoluteArchPath();

        if(!Storage::disk('data')->exists($path)){
            throw new NotFoundHttpException("Archive doesn't exist");
        }

        $extToMime = ['zip' => 'application/zip', 'rar' => 'application/x-rar'];

        $mimeType = $extToMime[pathinfo($path, PATHINFO_EXTENSION)];
        if(is_null($mimeType)) $mimeType = 'application/unknown';

        $headers = array('Content-Type: '.$mimeType,);
        
        // return \Response::download($path, basename($path), $headers);
        return Storage::disk('data')->download($path, basename($path), $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Seismic2dSurvey $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(Seismic2dSurvey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seismic2dSurvey $zone
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSeismic2dSurvey $request, Seismic2dSurvey $survey)
    {
        if($survey->is_deleted)
            throw new NotFoundHttpException("Survey doesn't exist.");

        if(isset($request->name)) $survey->name = $request->name;
        if(isset($request->area)) $survey->area = \DB::raw("'".LineParser::parse($request->area)->toEKWT()."'");
        if(isset($request->parent)) $survey->parent()->associate($request->parent);
        if(isset($request->direction)) $survey->direction = $request->direction;
        if(isset($request->type)) $survey->type()->associate(SurveyType::query()->where('key', $request->type)->get()[0]);
        if(isset($request->tool)) $survey->tool()->associate($request->tool);
        if(isset($request->vessel)) $survey->trip->vessel()->associate($request->vessel);
        if(isset($request->startDate)) $survey->start_date = $request->startDate;
        if(isset($request->filePath)){
            $survey->file_path = $request->filePath;
            $survey->file_size = filesize($survey->getAbsoluteFilePath());
        }
        if(isset($request->archPath)){
            $survey->arch_path = $request->archPath;
            $survey->arch_size = filesize($survey->getAbsoluteArchPath());
        }
        if(isset($request->archFiles)){
            $survey->archFiles()->delete();
            foreach($request->archFiles as $file){
                $archFile = new SurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }
        $survey->trip->save();
        $survey->save();
        
        
        return new Seismic2dSurveyResource($survey->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seismic2dSurvey $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seismic2dSurvey $survey)
    {
        $user = \Auth::user();
        if(!$survey->isEditable($user))
            throw AccessDeniedHttpException("You have no permission to delete this survey.");
        $survey->delete();

        return \Response::make("", 204);
    }


    /**
     * Returns years, directions, tools and types that are used in
     * visible for user surveys.
     *
     * @return \Illuminate\Http\Response
     */
    public function unique(Request $request)
    {
        $request->validate([
            'year' => ['nullable', 'integer'],
            'name' => ['nullable'],
            'parent' => ['nullable', 'integer', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['nullable', Rule::in(array_values(Seismic2dSurvey::DIRECTION))],
            'type' => ['nullable', 'integer_or_string'],
            'tool' => ['nullable', 'integer', 'exists:'.\App\Tables::TOOL_TABLE.',id']
        ]);

        $applyFilters = function($req, $res){
            if (!is_null($req['year'])){
                $res->whereYear('start_date', $req['year']);
            }

            if (!is_null($req['name'])){
                $res->whereRaw('lower(name) where (?)', ["%".\mb_strtolower($req['name'])."%"]);
            }
    
            if(!is_null($req['parent'])){
                $res->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($req['parent']));
            }
    
            if(!is_null($req['direction'])){
                $res->where('direction', $req['direction']);
            }

            $typeId = $req['type'];
            if(!is_null($typeId) && !is_numeric($typeId)){
                $typeObj = SurveyType::findByKey($typeId)->first();
                if(is_null($typeObj)){
                    throw new HttpException(400, "Survey type '".$req['type']."' doesn't exist");
                }
                $typeId = $typeObj->id;
            }
    
            if(!is_null($typeId)){
                $res->where(FK::get(\App\Tables::SURVEY_TYPE_TABLE), $typeId);
            }    

            if(!is_null($req['tool'])){
                $res->where('tool', $req['tool']);
            }

            return $res;
        };

        $years = $applyFilters($request, Seismic2dSurvey::getUniqueYears())->get();
        $directions = $applyFilters($request, Seismic2dSurvey::getUniqueDirections())->get();
        $tools = Tool::whereIn('id', $applyFilters($request, Seismic2dSurvey::getUniqueToolIds()))->get();
        $types = SurveyType::whereIn('id', $applyFilters($request, Seismic2dSurvey::getUniqueTypeIds()))->get();

        return [
            'years' => new ArrayResource($years),
            'directions' => new ArrayResource($directions),
            'tools' => ToolResource::collection($tools),
            'types' => SurveyTypeResource::collection($types)];
    }
}
