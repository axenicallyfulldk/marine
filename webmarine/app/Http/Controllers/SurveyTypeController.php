<?php

namespace App\Http\Controllers;

use App\Models\SurveyType;
use Illuminate\Http\Request;

use App\Http\Resources\SurveyTypeResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SurveyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = SurveyType::query();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return SurveyTypeResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SurveyType  $surveyType
     * @return \Illuminate\Http\Response
     */
    public function show(SurveyType $surveyType)
    {
        
        return new SurveyTypeResource($surveyType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SurveyType  $surveyType
     * @return \Illuminate\Http\Response
     */
    public function edit(SurveyType $surveyType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SurveyType  $surveyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SurveyType $surveyType)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SurveyType  $surveyType
     * @return \Illuminate\Http\Response
     */
    public function destroy(SurveyType $surveyType)
    {
    }
}
