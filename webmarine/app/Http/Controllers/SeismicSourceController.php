<?php

namespace App\Http\Controllers;

use App\Models\SeismicSource;
use Illuminate\Http\Request;

use App\Http\Resources\SeismicSourceResource;

class SeismicSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = SeismicSource::query();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        $query->with('type');

        
        return SeismicSourceResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SeismicSource  $seismicSource
     * @return \Illuminate\Http\Response
     */
    public function show(SeismicSource $seismicSource)
    {
        
        return new SeismicSourceResource($seismicSource);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SeismicSource  $seismicSource
     * @return \Illuminate\Http\Response
     */
    public function edit(SeismicSource $seismicSource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SeismicSource  $seismicSource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeismicSource $seismicSource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SeismicSource  $seismicSource
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeismicSource $seismicSource)
    {
        //
    }
}
