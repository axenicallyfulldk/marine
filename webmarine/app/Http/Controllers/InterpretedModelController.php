<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\InterpretedModel;

use App\Http\Resources\InterpretedModelPlainResource;
use App\Http\Resources\InterpretedModelResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Illuminate\Validation\ValidationException;

use Illuminate\Support\Facades\Storage;

use App\Helpers\DataSystem;

class InterpretedModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return InterpretedModelPlainResource::collection(InterpretedModel::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InterpretedModel  $model
     * @return \Illuminate\Http\Response
     */
    public function show(InterpretedModel $model)
    {
        $model = InterpretedModel::where('id', $model->id)->with(['hazards.modelable'])->first();
        return new InterpretedModelResource($model);
    }

    public function background(InterpretedModel $model){
        // if(!$mosaic->isDownloadable())
        //     throw new AccessDeniedHttpException("You have no permission to download mosaic data");

        // if($mosaic->is_deleted)
        //     throw new NotFoundHttpException("File doesn't exist");

        if(is_null($model->file_path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $path = $model->file_path;

        if(!DataSystem::fileExistsInData($path)){
            throw new NotFoundHttpException("File doesn't exist");
        }

        $headers = ['Content-Type' => 'application/x-geotiff',];

        return DataSystem::downloadFromDataByteRange($path, basename($path), $headers);

        $relPath = DataSystem::getRelDataPath($path);

        $filestream = new \App\Http\Responses\S3FileStream($relPath, 'data');
        return $filestream->output();

        
        
        return DataSystem::downloadFromData($path, basename($path), $headers);
    }
}
