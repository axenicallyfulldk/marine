<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Models\User;
use App\Models\Mosaic;

use App\Models\MosaicPermission;

use App\Http\Resources\MosaicUserPermissionResource;

use App\Helpers\FK;

class MosaicUserPermissionController extends Controller
{
    /**
     * Display a listing of the user permission.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'mosaic' => ['nullable', 'exists:'.\App\Tables::MOSAIC_TABLE.',id'],
            'user' => ['nullable', 'exists:'.\App\Tables::USER_TABLE.',id,group_id,NOT_NULL'],
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = MosaicPermission::forUser($request->user)->onMosaic($request->mosaic);
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        return MosaicUserPermissionResource::collection($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'mosaic' => [
                'required',
                'exists:'.\App\Tables::MOSAIC_TABLE.',id',
                Rule::unique(\App\Tables::MOSAIC_PERM_TABLE, FK::get(\App\Tables::MOSAIC_TABLE))->where(function ($query) use($request) {
                    return $query->where(FK::get(\App\Tables::USER_TABLE), $request->user);
                })
            ],
            // Check that it is really a user and not a group
            'user' => ['required', 'exists:'.\App\Tables::USER_TABLE.',id,group_id,NOT_NULL'],
            'see' => ['nullable', 'boolean'],
            'download' => ['nullable', 'boolean'],
            'edit' => ['nullable', 'boolean'],
        );

        $request->validate($rules);

        $sp = new MosaicPermission();
        $sp->user_id = $request->user;
        $sp->mosaic_id = $request->mosaic;
        if(isset($request->see)) $sp->can_see = $request->see ? 100 : -100;
        if(isset($request->download)) $sp->can_download = $request->download ? 100 : -100;
        if(isset($request->edit)) $sp->can_edit = $request->edit ? 100 : -100;
        $sp->save();

        return new MosaicUserPermissionResource($sp);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mosaic  $mosaic
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Mosaic $mosaic, User $user)
    {
        $res = MosaicPermission::forUser($user->id)->onMosaic($mosaic->id)->first();
        if(is_null($res)){
            throw new NotFoundHttpException("There is no such permission");
        }

        return new MosaicUserPermissionResource($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mosaic  $mosaic
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mosaic $mosaic, User $user)
    {
        $rules = array(
            'see' => ['nullable', 'boolean'],
            'download' => ['nullable', 'boolean'],
            'edit' => ['nullable', 'boolean'],
        );

        $request->validate($rules);
        
        $resQuery = MosaicPermission::query()->where(
            [FK::get(\App\Tables::MOSAIC_TABLE) => $mosaic->id, FK::get(\App\Tables::USER_TABLE) => $user->id]
        );
        
        $sp = $resQuery->first();
        if(is_null($sp)){
            throw new NotFoundHttpException("There is no such permission");
        }

        if(isset($request->see)) $sp->can_see = $request->see ? 100 : -100;
        else if($request->has('see')) $sp->can_see = 0;

        if(isset($request->download)) $sp->can_download = $request->download ? 100 : -100;
        else if($request->has('download')) $sp->can_download1 = 0;

        if(isset($request->edit)) $sp->can_edit = $request->edit ? 100 : -100;
        else if($request->has('edit')) $sp->can_edit = 0;
        $sp->save();

        return new MosaicUserPermissionResource($sp);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mosaic  $mosaic
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mosaic $mosaic, User $user)
    {
        $resQuery = MosaicPermission::query()->where(
            [FK::get(\App\Tables::MOSAIC_TABLE) => $mosaic->id, FK::get(\App\Tables::USER_TABLE) => $user->id]
        );
        
        $res = $resQuery->first();
        if(is_null($res)){
            throw new NotFoundHttpException("There is no such permission");
        }

        $resQuery->delete();

        return \Response::make("", 204);
    }
}
