<?php

namespace App\Http\Controllers;

use App\Models\MosaicType;
use Illuminate\Http\Request;

use App\Http\Resources\MosaicTypeResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MosaicTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = MosaicType::query();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        return MosaicTypeResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MosaicType  $mosaicType
     * @return \Illuminate\Http\Response
     */
    public function show(MosaicType $mosaicType)
    {
        return new MosaicTypeResource($mosaicType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MosaicType  $mosaicType
     * @return \Illuminate\Http\Response
     */
    public function edit(MosaicType $mosaicType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MosaicType  $mosaicType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MosaicType $mosaicType)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MosaicType  $mosaicType
     * @return \Illuminate\Http\Response
     */
    public function destroy(MosaicType $mosaicType)
    {
    }
}
