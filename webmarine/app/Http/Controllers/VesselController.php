<?php

namespace App\Http\Controllers;

use App\Models\Vessel;
use Illuminate\Http\Request;

use App\Http\Resources\VesselResource;
use App\Http\Resources\VesselPlainResource;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;



class VesselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = array(
            'limit' => ['nullable', 'integer', 'min:0'],
            'offset' => ['nullable', 'integer', 'min:0']
        );

        $request->validate($rules);

        $query = Vessel::visibleVessels();
        if(isset($request->limit)) $query->take($request->limit);
        if(isset($request->offset)) $query->offset($request->offset);

        
        return VesselPlainResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        if(!$user->editVesselsByDefault())
            throw new AccessDeniedHttpException('You have no right to create vessels.');


        $data = [];
        if(count($request->json()->all()))
            $data = $request->json()->all();
        
        $rules = [
            'name' => ['required', 'max:128'],
            'homePort' => ['nullable', 'max:128'],
            'shipowner' => ['nullable', 'max:128'],
            'imo' => ['nullable', 'regex:/^\d{7}$/i'],
            'mmsi' => ['nullable', 'regex:/^\d{6}$/i'],
            'callsign' => ['nullable', 'max:128'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'drought' => ['nullable', 'numeric'],
            'displacement' => ['nullable', 'numeric'],
            'speedKn' => ['nullable', 'numeric'],
            'buildDate' => ['nullable', 'date']
        ];

        $request->validate($rules);

        $getValue = function(array $data, string $key){
            return isset($data[$key]) ? $data[$key] : null;
        };

        $vessel = new Vessel();
        $vessel->name = $data['name'];
        $vessel->home_port = $getValue($data, 'homePort');
        $vessel->shipowner = $getValue($data, 'shipowner');
        $vessel->imo = $getValue($data, 'imo');
        $vessel->mmsi = $getValue($data, 'mmsi');
        $vessel->callsign = $getValue($data, 'callsign');
        $vessel->length = $getValue($data, 'length');
        $vessel->width = $getValue($data, 'width');
        $vessel->drought = $getValue($data, 'drought');
        $vessel->displacement = $getValue($data, 'displacement');
        $vessel->speed_kn = $getValue($data, 'speedKn');
        $vessel->build_date = $getValue($data, 'buildDate');

        $vessel->save();

        
        return new VesselResource($vessel->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vessel  $vessel
     * @return \Illuminate\Http\Response
     */
    public function show(Vessel $vessel)
    {
        
        return new VesselResource($vessel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vessel  $vessel
     * @return \Illuminate\Http\Response
     */
    public function edit(Vessel $vessel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vessel  $vessel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vessel $vessel)
    {
        $user = \Auth::user();
        if(!$user->editVesselsByDefault())
            throw new AccessDeniedHttpException('You have no right to edit vessels.');

        $data = [];
        if(count($request->json()->all()))
            $data = $request->json()->all();
        
        $rules = [
            'name' => ['nullable', 'max:128'],
            'homePort' => ['nullable', 'max:128'],
            'shipowner' => ['nullable', 'max:128'],
            'imo' => ['nullable', 'regex:/^\d{7}$/i'],
            'mmsi' => ['nullable', 'regex:/^\d{6}$/i'],
            'callsign' => ['nullable', 'max:128'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'drought' => ['nullable', 'numeric'],
            'displacement' => ['nullable', 'numeric'],
            'speedKn' => ['nullable', 'numeric'],
            'buildDate' => ['nullable', 'date']
        ];

        $request->validate($rules);

        $getValue = function(array $data, string $key){
            return isset($data[$key]) ? $data[$key] : null;
        };

        if(isset($data['name'])) $vessel->name = $getValue($data, 'name');
        $vessel->home_port = $getValue($data, 'homePort');
        $vessel->shipowner = $getValue($data, 'shipowner');
        $vessel->imo = $getValue($data, 'imo');
        $vessel->mmsi = $getValue($data, 'mmsi');
        $vessel->callsign = $getValue($data, 'callsign');
        $vessel->length = $getValue($data, 'length');
        $vessel->width = $getValue($data, 'width');
        $vessel->drought = $getValue($data, 'drought');
        $vessel->displacement = $getValue($data, 'displacement');
        $vessel->speed_kn = $getValue($data, 'speedKn');
        $vessel->build_date = $getValue($data, 'buildDate');

        $vessel->save();

        
        return new VesselResource($vessel->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vessel  $vessel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vessel $vessel)
    {
        $user = \Auth::user();
        if(!$user->editVesselsByDefault()){
            throw new HttpException(403, "You have no right to delete vessels.");
        }
        $vessel->is_deleted = true;
        $vessel->save();

        return \Response::make("", 204);
    }
}
