<?php

namespace App\Http\Requests;

use App\Models\SssMbesSurvey;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

use App\Helpers\FK;

class StoreSssMbesSurvey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return $user->editSssMbesSurveysByDefault();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this;
        return [
            'name' => ['required', 'max:255', Rule::unique(\App\Tables::SSSMBES_SURVEY_TABLE, 'name')->where(function ($query) use($request) {
                return $query->where(FK::get(\App\Tables::ZONE_TABLE), $request->parent);
            })],
            'area' => ['required', new \App\Rules\Polygon()],
            'parent' => ['required', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['required', Rule::in(array_values(SssMbesSurvey::DIRECTION))],
            'type' => ['required', 'exists:'.\App\Tables::SURVEY_TYPE_TABLE.',key'],
            'startDate' => ['required', 'date'],
            'filePath' => ['required', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][\w\d\.]*)*$/i',
                new \App\Rules\FileExists(\Config::get('app.data_folder'))],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][\w\d\.]*)*$/i',
                new \App\Rules\FileExists(\Config::get('app.data_folder'))],
            'archFiles' => ['nullable', new \App\Rules\ArchiveFiles()],
            'vessel' => ['required', 'exists:'.\App\Tables::VESSEL_TABLE.',id'],
            'tool' => ['required', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
        ];
    }
}
