<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

use App\Helpers\FK;

class UpdateSssMbesSurvey extends StoreSssMbesSurvey
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return parent::authorize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $prules = parent::rules();
        // Replace reuired rules by nullable.
        foreach($prules as $field => $rules){
            $prules[$field] = str_replace(['required'], ['nullable'], $rules);
        }
        $prules['name'] = ['nullable', 'max:255'];

        $request = $this;
        // If user wants to change survey's name or survey's zone we have
        // to check that this name-zone pair are not taken yet.
        // Don't check if user don't change name or zone.
        if(!isset($request->name) && !isset($request->parent)) return $prules;

        $survey = $this->route('survey');
        // Don't check if new name, zone pair is similar to original.
        if(isset($request->name) && isset($request->parent) &&
            $request->name == $survey->name && $request->parent == $survey->zone_id){
            return $prules;
        }

        if(isset($request->name)){
            $zone = isset($request->parent) ? $request->parent : $survey->zone_id;
            array_push($prules['name'], Rule::unique(\App\Tables::SSSMBES_SURVEY_TABLE, 'name')->where(function ($query) use($zone) {
                return $query->where(FK::get(\App\Tables::ZONE_TABLE), $zone);
            }));
        }

        if(isset($request->parent)){
            $name = isset($request->name) ? $request->name : $survey->name;
            array_push($prules['parent'], Rule::unique(\App\Tables::SSSMBES_SURVEY_TABLE, FK::get(\App\Tables::ZONE_TABLE))->where(function ($query) use($name) {
                return $query->where('name', $name);
            }));
        }

        return $prules;
    }
}
