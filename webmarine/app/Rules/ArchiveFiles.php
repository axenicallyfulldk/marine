<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ArchiveFiles implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $archFileIndex = 0;
        foreach($value as $file){
            $archFileIndex += 1;
            if(!\is_array($file)) {
                $this->errMessage = "File description with index ".$archFileIndex." is not an array";
                return false;
            }

            if(!isset($file['name'])){
                $this->errMessage = "File ".$archFileIndex." doesn't contain name";
                return false;
            }

            if(!isset($file['description'])){
                $this->errMessage = "File ".$archFileIndex." doesn't contain description";
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errMessage;
    }

    private $errMessage = "";
}
