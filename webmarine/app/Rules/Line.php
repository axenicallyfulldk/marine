<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Line implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($pointSep = '/\n+/')
    {
        $this->pointSep = $pointSep;
    }

    protected $pointSep;

    protected $line=0;

    protected $hasEnoughPoints = true;


    public function setPointSep($pointSep){
        $this->pointSep = $pointSep;
    }

    /**
     * Determine if the validation rule passes.
     * 
     *      *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->line = 0;
        $this->hasEnoughPoints = true;
        $points = preg_split($this->pointSep, trim($value));
        $firstPoint = null;
        $lastPoint = null;
        foreach($points as $point){
            $this->line += 1;
            $latLong = preg_split('/\s+/', trim($point));
            
            if(count($latLong) != 2) return false;
            if(!is_numeric($latLong[0])) return false;
            if(!is_numeric($latLong[1])) return false;
            if(floatval($latLong[0]) < -90.0000000000001 || floatval($latLong[0]) > 90.0000000000001) return false;
            if(floatval($latLong[1]) < -180.0000000000001 || floatval($latLong[1]) > 180.0000000000001) return false;
            
            if($this->line == 1) $firstPoint = $latLong;
            $lastPoint = $latLong;
        }
        // if($this->line <= 2 || ($this->line == 3 && $firstPoint == $lastPoint)) {
        if($this->line < 2){
            $this->hasEnoughPoints = false;
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if($this->hasEnoughPoints == false)
            return "Line has to contain at least two point";
            
        return "Line string is incorrect, error in line $this->line";
    }
}
