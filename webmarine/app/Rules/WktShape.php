<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Helpers\WktValidator;

class WktShape implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->vv = new WktValidator();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $res = $this->vv->isValidString($value);
        $this->message = $this->vv->getMessage();
        return $res;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
