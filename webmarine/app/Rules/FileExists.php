<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FileExists implements Rule
{
    private $basePath;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $base = null)
    {
        $this->basePath = $base;
        if(substr($this->basePath, -1) != DIRECTORY_SEPARATOR){
            $this->basePath .= DIRECTORY_SEPARATOR;
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $file = $value;
        if(is_null($this->basePath) || $this->basePath == ''){
            $file = ltrim($file, '/');
        }

        $file = $this->basePath.rtrim($file, '/');

        return \file_exists($file);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'File doesn\'t exist';
    }
}
