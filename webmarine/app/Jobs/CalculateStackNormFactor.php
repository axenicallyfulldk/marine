<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Helpers\DataSystem;

class CalculateStackNormFactor implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stack;
    protected $normCalculator;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($stack)
    {
        $this->stack = $stack;

        $this->normCalculator = "python3 ";
        $this->normCalculator .= static::joinPaths(base_path(), "scripts/segy_normalization_calculator.py");
    }

    public function uniqueId()
    {
        return $this->stack->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->stack->getAbsoluteDataFilePath();
        $absPath = DataSystem::getPath($path);
        $execCommand = sprintf("%s -t minmax -s \"%s\"\n", $this->normCalculator, $absPath);
        // echo($execCommand);
        $resultCode = 0;
        $output = [];
        exec($execCommand, $output, $resultCode);
        if($resultCode == 0) {
            $this->stack->minmax_norm = floatval($output[0]);
            $this->stack->save();
        }
    }

    private static function joinPaths(...$paths) {
        $fpaths = array_filter($paths, function($el){return $el != null;});
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $fpaths));
    }
}
