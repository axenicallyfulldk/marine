<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Helpers\DataSystem;
use App\Helpers\SegyFile;
use App\Helpers\LatLongDistance;

class CalculateStackCdpInfo implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $stack;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($stack)
    {
        $this->stack = $stack;
    }

    public function uniqueId()
    {
        return $this->stack->id;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stack = $this->stack;

        $segyFilePath = DataSystem::getDataPath($stack->file_path);

        $rangeStep = static::findCdpRangeAndStep($segyFilePath, $stack->cdp_offset, $stack->cdp_size);
        $stack->cdp_start = $rangeStep[0];
        $stack->cdp_end = $rangeStep[1];
        $stack->cdp_step = $rangeStep[2];

        $polylinePoints = \geoPHP::load($stack->survey->area)->asArray();
        $fp = reset($polylinePoints);
        $lp = end($polylinePoints);
        $segyFile = new SegyFile($segyFilePath, false);
        $cdpDist = LatLongDistance::vincentyGreatCircleDistance($fp[1], $fp[0], $lp[1], $lp[0])/($segyFile->getTraceCount()-1);

        $stack->cdp_distance = $cdpDist;
    }

    private static function findCdpRangeAndStep($segyPath, $cdpOffset, $cdpSize){
        $segyFile = new SegyFile($segyPath, false);
        $vals = $segyFile->getUintTraceHeaderFieldValues($cdpOffset, $cdpSize);
        $diffs = [];
        for($i = 0; $i < count($vals)-1; ++$i){
            array_push($diffs, $vals[$i+1]-$vals[$i]);
        }

        return [min($vals), max($vals), array_sum($diffs)/count($diffs)];
    }

    
}
