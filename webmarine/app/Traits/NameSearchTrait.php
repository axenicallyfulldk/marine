<?php

namespace App\Traits;

trait NameSearchTrait {

    public static function findByName($name){
        return static::query()->byName($name);
    }

    public function scopeByName($query, $name = null)
    {
        if(is_null($name)) return $query;
        $name = \mb_strtolower($name);
        return $query->whereRaw('lower(name) like (?)',["$name"]);
    }

    public static function findByNameSubstr($name){
        return static::query()->byNameSubstr($name);
    }

    public function scopeByNameSubstr($query, $name = null){
        if(is_null($name)) return $query;
        $name = \mb_strtolower($name);
        return $query->whereRaw('lower(name) like (?)',["%$name%"]);
    }
}