<?php
namespace App\Traits;

use \App\Models\User;

/**
 * Adds info about user who created, updated or soft-deleted model.
 */
trait CreateUpdateUserRecorder
{
    private static function isItSoftDelete($model){
        if(isset($model->is_deleted)){
            if ($model->is_deleted == true && $model->getOriginal('is_deleted') == false) {
                return true;
            }
        }
        return false;
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            $user = \Auth::user();
            if(!is_null($user)) $model->creator_id = $user->id;
        });

        static::updating(function($model){
            $user = \Auth::user();
            if(!is_null($user)){
                if(static::isItSoftDelete($model)) $model->deleter_id = $user->id; 
                else $model->updater_id = $user->id;
            }
        });
    }
}