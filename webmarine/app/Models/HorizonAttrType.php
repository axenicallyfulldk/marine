<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HorizonAttrType extends Model
{
    use HasFactory;

    protected $table = \App\Tables::HORIZON_ATTRIBUTE_TYPE_TABLE;

    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }

    public static function findByNameSubstr($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }
}
