<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreateUpdateUserRecorder;

use App\Helpers\FK;

class Vessel extends Model
{
    use HasFactory;
    use CreateUpdateUserRecorder;

    protected $table = \App\Tables::VESSEL_TABLE;

    protected $guarded = ['id']; 

    protected $hidden = ['created_at', 'updated_at'];

    public static function visibleVessels(){
        return static::query()->where('is_deleted', false);
    }

    public function trips(){
        return $this->hasMany(\App\Models\Trip::class, FK::get(\App\Tables::VESSEL_TABLE));
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    private static $storeRules = [
        'name' => ['required', 'max:128'],
        'homePort' => ['nullable', 'max:128'],
        'shipowner' => ['nullable', 'max:128'],
        'imo' => ['nullable', 'regex:/^\d{7}$/i'],
        'mmsi' => ['nullable', 'regex:/^\d{6}$/i'],
        'callsign' => ['nullable', 'max:128'],
        'length' => ['nullable', 'numeric'],
        'width' => ['nullable', 'numeric'],
        'drought' => ['nullable', 'numeric'],
        'displacement' => ['nullable', 'numeric'],
        'speedKn' => ['nullable', 'numeric'],
        'buildDate' => ['nullable', 'date']
    ];

    public static function getStoreRules(){
        return static::$storeRules;
    }

    private static $patchRules = [
        'name' => ['nullable', 'max:128'],
        'homePort' => ['nullable', 'max:128'],
        'shipowner' => ['nullable', 'max:128'],
        'imo' => ['nullable', 'regex:/^\d{7}$/i'],
        'mmsi' => ['nullable', 'regex:/^\d{6}$/i'],
        'callsign' => ['nullable', 'max:128'],
        'length' => ['nullable', 'numeric'],
        'width' => ['nullable', 'numeric'],
        'drought' => ['nullable', 'numeric'],
        'displacement' => ['nullable', 'numeric'],
        'speedKn' => ['nullable', 'numeric'],
        'buildDate' => ['nullable', 'date']
    ];

    public static function getPatchRules(){
        return static::$patchRules;
    }
}
