<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;
use App\Traits\NameSearchTrait;

class GeologicalHazard extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::GEOLOGICAL_HAZARD_TABLE;

    public function type(){
        return $this->belongsTo(\App\Models\HazardType::class, FK::get(\App\Tables::HAZARD_TYPE_TABLE));
    }

    public static function findByType($type){
        return static::query()->byType($type);
    }

    public function scopeByType($query, $type = null)
    {
        if(is_null($type)) return $query;
        return $query->where(FK::get(\App\Tables::HAZARD_TYPE_TABLE), $type);
    }
}
