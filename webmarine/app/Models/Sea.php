<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sea extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SEA_TABLE;

    public const name = "sea";

    public function zone(){
        return $this->morphOne('Zone', 'zonable');
    }
}
