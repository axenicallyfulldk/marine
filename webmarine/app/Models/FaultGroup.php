<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaultGroup extends Model
{
    use HasFactory;

    protected $table = \App\Tables::FAULT_GROUP_TABLE;


    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    public function type(){
        return $this->belongsTo(\App\Models\FaultGroupType::class, FK::get(\App\Tables::FAULT_GROUP_TYPE_TABLE));
    }

    public function faults(){
        return $this->hasMany(\App\Models\Fault::class, FK::get(\App\Tables::FAULT_GROUP_TABLE));
    }
}
