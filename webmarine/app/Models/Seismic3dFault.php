<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Seismic3dFault extends Model
{
    use HasFactory;

    protected $table = \App\Tables::FAULT_SEISMIC3D_STACK_TABLE;

    public function fault(){
        return $this->belongsTo(\App\Models\Fault::class, FK::get(\App\Tables::FAULT_TABLE));
    }

    public function stack(){
        return $this->belongsTo(\App\Models\Seismic3dStack::class, FK::get(\App\Tables::SEISMIC3D_STACK_TABLE));
    }
}
