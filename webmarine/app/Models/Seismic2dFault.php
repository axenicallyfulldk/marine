<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Seismic2dFault extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SEISMIC2D_FAULT_TABLE;

    public function fault(){
        return $this->belongsTo(\App\Models\Fault::class, FK::get(\App\Tables::FAULT_TABLE));
    }

    public function stack(){
        return $this->belongsTo(\App\Models\Seismic2dStack::class, FK::get(\App\Tables::SEISMIC2D_STACK_TABLE));
    }
}
