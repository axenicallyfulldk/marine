<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaultRiskDegree extends Model
{
    use HasFactory;

    protected $table = \App\Tables::FAULT_RISK_DEGREE_TABLE;

    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    public function faults(){
        return $this->hasMany(\App\Models\Fault::class, FK::get(\App\Tables::FAULT_RISK_DEGREE_TABLE));
    }
}
