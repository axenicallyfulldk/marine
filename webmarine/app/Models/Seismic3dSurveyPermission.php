<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Seismic3dSurveyPermission extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SEISMIC3D_SURVEY_PERM_TABLE;

    public function user(){
        return $this->belongsTo(\App\Models\User::class, FK::get(\App\Tables::USER_TABLE));
    }

    public function group(){
        return $this->belongsTo(\App\Models\Group::class, FK::get(\App\Tables::GROUP_TABLE));
    }

    public function survey(){
        return $this->belongsTo(\App\Models\Seismic3dSurvey::class, FK::get(\App\Tables::SEISMIC3D_SURVEY_TABLE));
    }

    public function getSeeAttribute(){
        if($this->can_see == 0) return null;
        return $this->can_see > 0 ? true : false;
    }

    public function getEditAttribute(){
        if($this->can_edit == 0) return null;
        return $this->can_edit > 0 ? true : false;
    }

    public function getDownloadAttribute(){
        if($this->can_download == 0) return null;
        return $this->can_download > 0 ? true : false;
    }

    public function scopeForUser($query, $userId)
    {
        $query->whereIn('user_id', function($query){
            $query->select('id')
            ->from(with(new User)->getTable())
            ->whereNotNull('group_id');
        });

        if(is_null($userId)) return $query;
        return $query->where('user_id', $userId);
    }

    public function scopeForGroup($query, $groupId)
    {
        $query->whereIn('user_id', function($query){
            $query->select('id')
            ->from(with(new User)->getTable())
            ->whereNull('group_id');
        });

        if(is_null($groupId)) return $query;
        return $query->where('user_id', $groupId);
    }

    public function scopeOnSurvey($query, $surveyId)
    {
        if(is_null($surveyId)) return $query;
        return $query->where(FK::get(\App\Tables::SEISMIC3D_SURVEY_TABLE), $surveyId);
    }
}
