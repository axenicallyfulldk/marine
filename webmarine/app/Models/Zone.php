<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreateUpdateUserRecorder;

use Illuminate\Support\Facades\Auth;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

use App\Helpers\FK;

use App\Traits\NameSearchTrait;

class Zone extends Model
{
    use HasFactory;
    use CreateUpdateUserRecorder;
    use NameSearchTrait;

    protected $table = \App\Tables::ZONE_TABLE;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'zonable_id',
        'zonable_type',
        'parent_zone_id',
        'created_at',
        'updated_at',
        'unique_id'
    ];

    protected $appends = ['type', 'parentZone'];

    private static function userParamToUserId($user){
        if(is_int($user)) return $user;
        if(is_array($user) && array_key_exists ('id', $user)){
            return intval($user['id']);
        }
        if(\is_object($user) && property_exists($user, 'id')){
            return intval($user->id);
        }
        if(property_exists($user, 'attributes') && array_key_exists('id', $user->attributes)){
            return intval($user->id);
        }
        
        return NULL;
    }

    const ZONE_CLASSES = [
        OceanBasin::name => OceanBasin::class,
        Sea::name => Sea::class,
        Bay::name => Bay::class,
        Strait::name => Strait::class
    ];

    private static function zoneTypeToZonableType($zoneType){
        if(is_null($zoneType)) return NULL;
        if(array_key_exists($zoneType, Zone::ZONE_CLASSES)) return Zone::ZONE_CLASSES[$zoneType];
        if(in_array($zoneType, Zone::ZONE_CLASSES)) return $zoneType;
        return NULL;
    }

    public static function all($columns = ['*']){
        $query = static::query();
        return $query->get(is_array($columns) ? $columns : func_get_args());
    }

    public function zonable(){
        return $this->morphTo();
    }

    public function getTypeAttribute(){
        return $this->zonable_type::name;
    }

    public function getParentZoneAttribute(){
        return $this->parent_zone_id;
    }

    /**
     * Returns users with specific permissions on zones.
     */
    public function permissions()
    {
        return $this->belongsToMany(\App\Models\User::class, \App\Tables::ZONE_PERM_TABLE)->withPivot('can_see', 'can_edit');
    }

    /**
     * Returns permission of concrete user on zones.
     */
    public function userPermission($user)
    {
        $userId = $this->userParamToUserId($user);
        return $this->belongsToMany(\App\Models\User::class, \App\Tables::ZONE_PERM_TABLE)->wherePivot('user_id',$userId);
    }

    
    /**
     * Returns children zones
     */
    public function children(){
        return $this->hasMany(\App\Models\Zone::class, 'parent_zone_id');
    }

    /**
     * Returns parent zone
     */
    public function parent(){
        return $this->belongsTo(\App\Models\Zone::class,'parent_zone_id');
    }

    /**
     * Returns sss and mbes surveys that belongs to zone
     */
    public function sssMbesSurveys(){
        return $this->hasMany(\App\Models\SssMbesSurvey::class, FK::get(\App\Tables::ZONE_TABLE));
    }

    /**
     * Returns seismic 2D surveys that belongs to zone
     */
    public function seismic2dSurveys(){
        return $this->hasMany(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::ZONE_TABLE));
    }


    /**
     * Returns seismic 3D surveys that belong to zone
     */
    public function seismic3dSurveys(){
        return $this->hasMany(\App\Models\Seismic3dSurvey::class, FK::get(\App\Tables::ZONE_TABLE));
    }

    /**
     * Returns surveys that belong to zone
     */
    public function mosaics(){
        return $this->hasMany(\App\Models\Mosaic::class, FK::get(\App\Tables::ZONE_TABLE));
    }

    /**
     * Returns interpreted models that belong to zone
     */
    public function interpretedModels(){
        return $this->hasMany(\App\Models\InterpretedModel::class, FK::get(\App\Tables::ZONE_TABLE));
    }


    /**
     * Returns query for recieving zone's mosaics and permissions for them.
     */
    private function allMosaicsWithPermissions($user){
        $userId = $user->id;
        $groupId = $user->group->id;
        $mosaicTable = \App\Tables::MOSAIC_TABLE;
        $mosaicPermTable = \App\Tables::MOSAIC_PERM_TABLE;

        $seeMosaicDefault = is_null($user->see_mosaic) ? $user->group->see_mosaic : $user->see_mosaic;
        $editMosaicDefault = is_null($user->edit_mosaic) ? $user->group->edit_mosaic : $user->edit_mosaic;

        $seeMosaicColumn = 'coalesce(sum(can_see),0)'.($seeMosaicDefault?">=":">").'0';
        $editMosaicColumn = 'coalesce(sum(can_edit),0)'.($editMosaicDefault?">=":">").'0';

        $res = $this->mosaics()->where('is_deleted', false);
        $res->selectRaw($mosaicTable.'.*');
        $res->selectRaw($seeMosaicColumn.' as is_visible');
        $res = $res->selectRaw($editMosaicColumn.' as is_editable');
        
        $res->leftJoin($mosaicPermTable, $mosaicTable.'.id', '=', $mosaicPermTable.'.'.$mosaicTable.'_id')
        ->where(function($query)use($userId,$groupId,$mosaicPermTable){
            $query->where($mosaicPermTable.'.user_id', $userId)->
            orWhere($mosaicPermTable.'.user_id', $groupId)->
            orWhereNull($mosaicPermTable.'.user_id');
        })->groupBy($mosaicTable.'.id');

        $res->havingRaw($seeMosaicColumn);
        
        return $res;
    }

    /**
     * Return surveys that belong to zone and visible for user.
     */
    public function visibleSssMbesSurveys($user = null){
        if(is_null($user)){
            $user = Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($user));
        }

        if($user->isAdmin()){
            return $this->sssMbesSurveys();
        }

        $query = SssMbesSurvey::visible($user);
        $query->where(FK::get(\App\Tables::ZONE_TABLE), $this->id);
        return $query;
    }


    /**
     * Return mosaics that belong to zone and visible for user.
     */
    public function visibleMosaics($user = null){
        if(is_null($user)){
            $user = Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($user));
        }

        if($user->isAdmin()){
            return $this->mosaics();
        }

        $query = $this->allMosaicsWithPermissions($user);
        
        return $query;
    }

    public function visibleSeismic2dSurveys($user = null){
        if(is_null($user)){
            $user = Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($user));
        }

        if($user->isAdmin()){
            return $this->seismic2dSurveys();
        }

        $query = Seismic2dSurvey::visible($user);
        $query->where(FK::get(\App\Tables::ZONE_TABLE), $this->id);
        return $query;
    }

    public function visibleSeismic3dSurveys($user = null){
        if(is_null($user)){
            $user = Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($user));
        }

        if($user->isAdmin()){
            return $this->seismic3dSurveys();
        }

        $query = Seismic3dSurvey::visible($user);
        $query->where(FK::get(\App\Tables::ZONE_TABLE), $this->id);
        return $query;
    }

    public function visibleInterpretedModels($user = null){
        return $this->interpretedModels();
    }

    /**
     * Returns zones visible for user.
     */
    public static function visibleZones(){
        return static::query()->where('is_deleted', false);
    }

    public static function subZonesId($zone = null, $res = null){
        if(is_null($zone)){
            return DB::table(\App\Tables::ZONE_TABLE)->select("id");
        }

        $query = DB::table('zone');
        $query->select('id')
        ->where('id', $zone)
        ->unionAll(
            DB::table('zone')
            ->select('zone.id')
            ->join('all_sub_zones', 'all_sub_zones.id', '=', 'parent_zone_id')
        );

        if(is_null($res)) $res = DB::table('all_sub_zones');
        else $res->from('all_sub_zones');
        $res->withRecursiveExpression('all_sub_zones', $query);

        return $res;
    }

    /**
     * Get all subzones.
     */
    public static function subZones($zone = null){
        return static::query()->whereIn('id', static::subZonesId($zone));
    }

    /**
     * Return zone object that associated with given complex zone code.
     * For example for code "N/W/BK/SVS" zone object that represents velikaya salma
     * will be return.
     * Function returns last succesfuly decoded zone. For example for code 
     * "N/W/BK/SVS/L1" function also returns velikaya salma zone because in it
     * there is no subzone with code L1.
     */
    public static function deduceZone($zoneCode, string $sepReg = "/"){
        if(is_array($zoneCode)){
            $zoneCodes = $zoneCode;
        } else {
            $zoneCodes = \preg_split("~".$sepReg."~", $zoneCode);
        }
        

        $prevZone = null;
        $zone = Zone::query()->root()->byCode($zoneCodes[0])->first();
        $i=1;
        while(!is_null($zone) && $i < count($zoneCodes)){
            $prevZone = $zone;
            $zone = $zone->children()->byCode($zoneCodes[$i])->first();
            $i += 1;
        }

        // if(is_null($zone)) return $prevZone;
        
        return $zone;
    }

    public static function getStoreRules($shapeType = ZoneShapeType::Simple){
        $rules = [
            'name'       => 'required|max:255',
            'code'       => 'required|max:10',
            'parentZone' => ['nullable', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'type' => ['required', Rule::in(array_keys(Zone::ZONE_CLASSES))]
        ];

        if($shapeType == ZoneShapeType::Simple){
            $rules['area'] = ['required', new \App\Rules\Polygon()];
        }else if($shapeType == ZoneShapeType::WKT){
            $rules['area'] = ['required', new \App\Rules\WktShape()];
        }
        return $rules;
    }

    public static function getPatchRules($shapeType = ZoneShapeType::Simple){
        $rules = [
            'name' => ['nullable', 'max:255'],
            'code' => ['nullable', 'max:10'],
            'parentZone' => ['nullable', 'exists:'.\App\Tables::ZONE_TABLE.',id']
        ];

        if($shapeType == ZoneShapeType::Simple){
            $rules['area'] = ['nullable', new \App\Rules\Polygon()];
        }else if($shapeType == ZoneShapeType::WKT){
            $rules['area'] = ['nullable', new \App\Rules\WktShape()];
        }
        return $rules;
    }

    public function scopeOfType($query, $zoneType = null)
    {
        $zoneType = Zone::zoneTypeToZonableType($zoneType);
        if(is_null($zoneType)) return $query;

        return $query->where('zonable_type',$zoneType);
    }

    public function scopeContainsString($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(name) like (?)',["%$substring%"]);
    }

    public function scopeByCode($query, $code = null){
        if(is_null($code)) return $query;
        return $query->where('code', $code);
    }

    public function scopeRoot($query){
        return $query->where('parent_zone_id', null);
    }
}
