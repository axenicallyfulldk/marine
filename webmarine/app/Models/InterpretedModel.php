<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\NameSearchTrait;

use Illuminate\Validation\Rule;

use App\Helpers\FK;
use App\Helpers\DataSystem;

class InterpretedModel extends Model
{
    use HasFactory;
    use NameSearchTrait;

    // protected $with = ['hazards.modelable'];

    protected $table = \App\Tables::INTERPRETED_MODEL_TABLE;

    public function hazards(){
        return $this->hasMany(Modelable::class);
    }

    public function parent(){
        return $this->belongsTo(\App\Models\Zone::class, FK::get(\App\Tables::ZONE_TABLE));
    }


    public static function getStoreRules(){
        return [
            'name' => ['required', 'max:255'],
            'area' => ['required', new \App\Rules\WktShape()],
            'parentZone' => ['required', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'creationDate' => ['required', 'date'],
            'filePath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(\App\Helpers\DataSystem::getDataPath())],
            'hazards' => ['nullable', 'array'],
            'hazards.*.name' => ['required'],
            'hazards.*.type' => ['required', Rule::in(array_keys(HazardType::KeyToModelMapping))],
            'hazards.*.shape' => ['required', new \App\Rules\WktShape()],
            'hazards.*.mark' => ['nullable', 'numeric']
        ];
    }

    public static function getPatchRules(){
        return [
            'name' => ['nullable', 'max:255'],
            'area' => ['nullable', new \App\Rules\WktShape()],
            'parentZone' => ['nullable', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'creationDate' => ['nullable', 'date'],
            'filePath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'hazards' => ['nullable', 'array'],
            'hazards.*.name' => ['required'],
            'hazards.*.type' => ['required', Rule::in(array_keys(HazardType::KeyToModelMapping))],
            'hazards.*.shape' => ['required', new \App\Rules\WktShape()],
            'hazards.*.mark' => ['nullable', 'numeric']
        ];
    }
}
