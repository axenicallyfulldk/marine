<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Horizon extends Model
{
    use HasFactory;

    protected $table = \App\Tables::HORIZON_TABLE;

    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    public function seismic2dHorizons(){
        return $this->hasMany(\App\Models\Seismic2dHorizon::class, FK::get(\App\Tables::HORIZON_TABLE));
    }

    public function seismic3dHorizons(){
        return $this->hasMany(\App\Models\Seismic3dHorizon::class, FK::get(\App\Tables::HORIZON_TABLE));
    }
}
