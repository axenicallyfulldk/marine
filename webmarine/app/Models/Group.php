<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{

    use SoftDeletes;

    protected $table = \App\Tables::GROUP_TABLE;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static $groups = [
        'admin' => ['email' => 'admin_group@marine.marine'],
        'moderator' => ['email' => 'moderator_group@marine.marine'],
        'gnostic' => ['email' => 'gnostic_group@marine.marine'],
        'agnostic' => ['email' => 'agnostic_group@marine.marine'],
    ];

    public static function all($columns = ['*']){
        return Group::whereNull('group_id')->get(is_array($columns) ? $columns : func_get_args());
    }

    public static function query()
    {
        return (new static)->newQuery()->whereNull('group_id');
    }

    public static function get(string $group){
        $gg = static::query()->where('email', static::$groups[$group]['email'])->first();
        return $gg;
    }

    public function users()
    {
        return $this->hasMany(\App\Models\User::class,'group_id');
    }

    public function isAdminGroup(){
        return $this->id == \Config::get('auth.default_groups_id.admin');
    }

    public function isModeratorGroup(){
        return $this->id == \Config::get('auth.default_groups_id.moderator');
    }


    /**
     * Returns true if user can edit tools by default.
     */
    public function editToolsByDefault(){
        return $this->edit_tool;
    }

    /**
     * Returns true if user can edit vessels by default.
     */
    public function editVesselsByDefault(){
        return $this->edit_vessel;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editZonesByDefault(){
        return $this->edit_zone;
    }


    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithSssMbesSurveysPermissions(){
        return $this->sssmbes_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeSssMbesSurveysByDefault(){
        return $this->see_sssmbes_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadSssMbesSurveysByDefault(){
        return $this->download_sssmbes_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editSssMbesSurveysByDefault(){
        return $this->edit_sssmbes_survey;
    }


    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithSeismic2dSurveysPermissions(){
        return $this->seismic2d_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeSeismic2dSurveysByDefault(){
        return $this->see_seismic2d_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadSeismic2dSurveysByDefault(){
        return $this->download_seismic2d_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editSeismic2dSurveysByDefault(){
        return $this->edit_seismic2d_survey;
    }

    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithSeismic3dSurveysPermissions(){
        return $this->seismic3d_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeSeismic3dSurveysByDefault(){
        return $this->see_seismic3d_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadSeismic3dSurveysByDefault(){
        return $this->download_seismic3d_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editSeismic3dSurveysByDefault(){
        return $this->edit_seismic3d_survey;
    }
    
    /**
     * Return true if user can grant and revoke special permissions on mosaics
     */
    public function workWithMosaicPermissions(){
        return $this->mosaic_permission;
    }

    /**
     * Returns true if user can see mosaics by default.
     */
    public function seeMosaicsByDefault(){
        return $this->see_mosaic;
    }

    /**
     * Returns true if user can download mosaics by default.
     */
    public function downloadMosaicsByDefault(){
        return $this->download_mosaic;
    }

    /**
     * Returns true if user can edit mosaics by default.
     */
    public function editMosaicsByDefault(){
        return $this->edit_mosaic;
    }
}
