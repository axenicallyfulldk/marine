<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;
use App\Helpers\DataSystem;

use App\Traits\NameSearchTrait;

class Seismic3dStack extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::SEISMIC3D_STACK_TABLE;

    public function survey(){
        return $this->belongsTo(\App\Models\Seismic3dSurvey::class, FK::get(\App\Tables::SEISMIC3D_SURVEY_TABLE));
    }

    public function faults(){
        return $this->hasMany(\App\Models\Seismic3dFault::class, FK::get(\App\Tables::SEISMIC3D_STACK_TABLE));
    }

    public function horizons(){
        return $this->hasMany(\App\Models\Seismic3dHorizon::class, FK::get(\App\Tables::SEISMIC3D_STACK_TABLE));
    }

    // Returns absolute path to file.
    public function getAbsoluteFilePath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataFilePath();
        }else{
            $path = $this->getAbsoluteTrashFilePath();
        }
        return $path;
    }

    public function getAbsoluteDataFilePath(){
        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->file_path," /\/"));
        return $path;
    }

    public function getAbsoluteTrashFilePath(){
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $fileRelFolder = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $fileRelFolder, $this->id, $archName);
        return $path;
    }

    public static function visible($user = null){
        return static::query()->whereIn(FK::get(\App\Tables::SEISMIC3D_SURVEY_TABLE), Seismic2dSurvey::visible()->select('id'));
    }

    public function isVisible($user = null){
        return $this->survey->isVisible($user);
    }

    public function isDownloadable($user = null){
        return $this->survey->isDownloadable($user);
    }

    public function isEditable($user = null){
        return $this->survey->isEditable();
    }

    public static function getStoreRules($data){
        return [
            'name' => ['required', 'max:255'],
            'alias' => ['nullable', 'max:255'],
            'survey' => ['required', 'exists:'.\App\Tables::SEISMIC3D_SURVEY_TABLE.',id'],
            'isMigrated' => ['nullable', 'boolean'],
            'isAmplCorrected' => ['nullable', 'boolean'],
            'isDeconvolved' => ['nullable', 'boolean'],
            'inlineOffset' => ['required', 'integer', 'min:0'],
            'inlineSize' => ['required', 'integer', 'min:1'],
            'xlineOffset' => ['required', 'integer', 'min:0'],
            'xlineSize' => ['required', 'integer', 'min:1'],
            'filePath' => ['required', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

    public static function getPatchRules($data){
        return [
            'name' => ['nullable', 'max:255'],
            'alias' => ['nullable', 'max:255'],
            'survey' => ['nullable', 'exists:'.\App\Tables::SEISMIC3D_SURVEY_TABLE.',id'],
            'isMigrated' => ['nullable', 'boolean'],
            'isAmplCorrected' => ['nullable', 'boolean'],
            'isDeconvolved' => ['nullable', 'boolean'],
            'inlineOffset' => ['nullable', 'integer', 'min:0'],
            'inlineSize' => ['nullable', 'integer', 'min:1'],
            'xlineOffset' => ['nullable', 'integer', 'min:0'],
            'xlineSize' => ['nullable', 'integer', 'min:1'],
            'filePath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

    public static function findByAlias($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(alias) like (?)',["$name"]);
    }

    public static function findByAliasSubstr($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(alias) like (?)',["%$name%"]);
    }

    public function scopeByAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(alias) like (?)',["%$substring%"]);
    }

    public function scopeByNameOrAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        $query->whereRaw('lower(name) like (?)',["%$substring%"])->orWhereRaw('lower(alias) like (?)',["%$substring%"]);
        return $query;
    }
}
