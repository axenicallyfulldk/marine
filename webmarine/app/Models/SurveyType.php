<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class SurveyType extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SURVEY_TYPE_TABLE;

    public function surveys(){
        return $this->hasMany(\App\Models\SssMbesSurvey::class, FK::get(\App\Tables::SURVEY_TYPE_TABLE));
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }

    public static function findByKey($key){
        $key = \mb_strtolower($key);
        return static::query()->whereRaw('lower(key) like (?)',["$key"]);
    }
}
