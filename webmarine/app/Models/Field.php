<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\NameSearchTrait;

class Field extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::FIELD_TABLE;

    /**
     * Returns seismic 2D surveys that belongs to field
     */
    public function seismic2dSurveys(){
        return $this->hasMany(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::FIELD_TABLE));
    }

    public static function getStoreRules(){
        return [
            'name' => ['required', 'max:128'],
            'area' => ['nullable', new \App\Rules\Polygon()],
        ];
    }

    public static function getPatchRules(){
        return [
            'name' => ['nullable', 'max:128'],
            'area' => ['nullable', new \App\Rules\Polygon()],
        ];
    }
}
