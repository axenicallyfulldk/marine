<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Strait extends Model
{
    use HasFactory;

    protected $table = \App\Tables::STRAIT_TABLE;

    public const name = "strait";

    public function zone(){
        return $this->morphOne('Zone', 'zonable');
    }
}
