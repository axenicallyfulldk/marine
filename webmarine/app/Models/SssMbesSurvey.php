<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;
use App\Helpers\DataSystem;

use Illuminate\Validation\Rule;

use App\Traits\CreateUpdateUserRecorder;

use Illuminate\Support\Facades\Storage;

class SssMbesSurvey extends Model
{
    use HasFactory;
    use CreateUpdateUserRecorder;

    protected $table = \App\Tables::SSSMBES_SURVEY_TABLE;

    protected $guarded = ['id']; 

    const DIRECTION = ['L', 'X', 'D'];

    private static function userParamToUserId($user){
        if(is_int($user)) return $user;
        if(is_array($user) && array_key_exists ('id', $user)){
            return intval($user['id']);
        }
        if(\is_object($user) && property_exists($user, 'id')){
            return intval($user->id);
        }
        if(property_exists($user, 'attributes') && array_key_exists('id', $user->attributes)){
            return intval($user->id);
        }
        
        return NULL;
    }

    private static function userParamToUser($userParam){
        if($userParam instanceof User) return $userParam;
        if(is_null($userParam)){
            $user = \Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($userParam));
        }
        return $user;
    }

    public function parent(){
        return $this->belongsTo(\App\Models\Zone::class, FK::get(\App\Tables::ZONE_TABLE));
    }

    public function trip(){
        return $this->belongsTo(\App\Models\Trip::class, FK::get(\App\Tables::TRIP_TABLE));
    }

    public function tool(){
        return $this->belongsTo(\App\Models\Tool::class, FK::get(\App\Tables::TOOL_TABLE));
    }

    public function permissions(){
        return $this->hasMany(\App\Models\SssMbesSurveyPermission::class, FK::get($this->table));
    }

    public function type(){
        return $this->belongsTo(\App\Models\SurveyType::class, FK::get(\App\Tables::SURVEY_TYPE_TABLE));
    }

    public function archFiles(){
        return $this->hasMany(\App\Models\SssMbesSurveyArchFile::class, FK::get($this->table));
    }

    /**
     * Returns whether the survey is visible for the user.
     */
    public function isVisible($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_visible', $this->attributes)){
            // We suppose that field 'is_visible' was calculated for given user.
            return $this->is_visible;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $visibleDefault = $user->seeSssMbesSurveysByDefault();
            $visibleIndex = 0;
            foreach($perms as $perm){
                $visibleIndex += $perm->can_see;
            }
            return $visibleDefault ? $visibleIndex>=0 : $visibleIndex>0;
        }
    }

    /**
     * Returns whether the user can edit the survey.
     */
    public function isEditable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_editable', $this->attributes)){
            return $this->is_editable;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $editableDefault = $user->editSssMbesSurveysByDefault();
            $editableIndex = 0;
            foreach($perms as $perm){
                $editableIndex += $perm->can_edit;
            }
            return $editableDefault ? $editableIndex>=0 : $editableIndex>0;
        }
    }

    /**
     * Returns whether the user can download the survey.
     */
    public function isDownloadable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_downloadable', $this->attributes)){
            return $this->is_downloadable;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $downloadableDefault = $user->downloadSssMbesSurveysByDefault();
            $downloadableIndex = 0;
            foreach($perms as $perm){
                $downloadableIndex += $perm->can_download;
            }
            return $downloadableDefault ? $downloadableIndex>=0 : $downloadableIndex>0;
        }
    }

    /**
     * Returns absolute path to survey's archive.
     */
    public function getAbsoluteArchPath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataArchPath();
        }else{
            $path = $this->getAbsoluteTrashArchPath();
        }
        return $path;
    }

    // Returns absolute path to archive.
    // If survey is deleted this path would not be valid.
    public function getAbsoluteDataArchPath(){
        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->arch_path," /\/"));
        return $path;
    }

    // Returns absolute path to archive in trash folder.
    // If survey is not deleted this path would not be valid.
    public function getAbsoluteTrashArchPath(){
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $archRelFolder = pathinfo(ltrim($this->arch_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->arch_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $archRelFolder, $this->id, $archName);
        return $path;
    }

    // Returns absolute path to file.
    public function getAbsoluteFilePath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataFilePath();
        }else{
            $path = $this->getAbsoluteTrashFilePath();
        }
        return $path;
    }

    public function getAbsoluteDataFilePath(){
        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->file_path," /\/"));
        return $path;
    }

    public function getAbsoluteTrashFilePath(){
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $fileRelFolder = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $fileRelFolder, $this->id, $archName);
        return $path;
    }

    /**
     * Deletes survey. Marks surveys as deleted and moves file and archive
     * to the trash folder.
     */
    public function delete(){
        //If it is already deleted do nothing
        if($this->is_deleted)
            return;

        // Get path to file and archive
        $srcArchPath = $this->getAbsoluteDataArchPath();
        $srcFilePath = $this->getAbsoluteDataFilePath();
    
        // Generate resulting path in trash folder
        $dstArchPath = $this->getAbsoluteTrashArchPath();
        $dstArchFolder = pathinfo($dstArchPath, PATHINFO_DIRNAME);

        $dstFilePath = $this->getAbsoluteTrashFilePath();
        $dstFileFolder = pathinfo($dstFilePath, PATHINFO_DIRNAME);

        $disk = Storage::disk('data');

        try{
            // Make necessary folders in trash folder
            if(!$disk->exists($dstArchFolder) && !$disk->makeDirectory($dstArchFolder))
                throw new \Exception("Failed to delete survey");

            // Make necessary folders in trash folder
            if(!$disk->exists($dstFileFolder) && !$disk->makeDirectory($dstFileFolder))
                throw new \Exception("Failed to delete survey");

            // Move archive
            if($disk->exists($srcArchPath))
                $disk->move($srcArchPath, $dstArchPath);
            
            // Move file
            if($disk->exists($srcFilePath))
                $disk->move($srcFilePath, $dstFilePath);
        }catch(\Exception $e){
            throw new \Exception("Failed to delete survey");
        }
        
        // Set flag
        $this->is_deleted = true;
        $this->save();
    }

    // Returns all surveys visible for user
    private static function allVisibleSurveys($user){
        $userId = $user->id;
        $groupId = $user->group->id;
        $survTable = \App\Tables::SSSMBES_SURVEY_TABLE;
        $survPermTable = \App\Tables::SSSMBES_SURVEY_PERM_TABLE;

        // select * from survey left join survey_perm on survey.id = survey_perm.survey_id
        // where
        //  is_deleted = false
        //  and
        //  (surv_perm.user_id = $uid or surv_perm.user_id = $guid or surv_perm.user_id is null)
        // group by survey.id having {coalesce(sum(can_see),0)>=0 or coalesce(sum(can_see),0)>0}

        $seeSurveyDefault = $user->seeSssMbesSurveysByDefault();

        $seeSurvColumn = 'coalesce(sum(can_see),0)'.($seeSurveyDefault?">=":">").'0';

        $res = static::query()->where('is_deleted', false);

        $ufk = FK::get(\App\Tables::USER_TABLE);
        
        $res->leftJoin($survPermTable, $survTable.'.id', '=', $survPermTable.'.'.FK::get(\App\Tables::SSSMBES_SURVEY_TABLE))
        ->where(function($query)use($userId,$groupId,$survPermTable,$ufk){
            $query->where($survPermTable.'.'.$ufk, $userId)->
            orWhere($survPermTable.'.'.$ufk, $groupId)->
            orWhereNull($survPermTable.'.'.$ufk);
        })->groupBy($survTable.'.id');

        $res->havingRaw($seeSurvColumn);
        
        return $res;
    }

    // Returns all visible for the user surveys and permisssion.
    private static function allWithPermissions($user){
        $survTable = \App\Tables::SSSMBES_SURVEY_TABLE;
        $survPermTable = \App\Tables::SSSMBES_SURVEY_PERM_TABLE;

        $seeSurveyDefault = $user->seeSssMbesSurveysByDefault();
        $editSurveyDefault = $user->editSssMbesSurveysByDefault();
        $downloadSurveyDefault = $user->downloadSssMbesSurveysByDefault();

        $seeSurvColumn = 'coalesce(sum(can_see),0)'.($seeSurveyDefault?">=":">").'0';
        $editSurvColumn = 'coalesce(sum(can_edit),0)'.($editSurveyDefault?">=":">").'0';
        $downloadSurvColumn = 'coalesce(sum(can_download),0)'.($downloadSurveyDefault?">=":">").'0';

        $res = static::allVisibleSurveys($user);

        $res->selectRaw($survTable.'.*');
        $res->selectRaw($seeSurvColumn.' as is_visible');
        $res->selectRaw($editSurvColumn.' as is_editable');
        $res->selectRaw($downloadSurvColumn.' as is_downloadable');

        return $res;
    }

    
    public static function visible($user = null){
        $user = static::userParamToUser($user);

        if($user->isAdmin()) return static::query();

        return static::allWithPermissions($user);
    }

    public static function getStatisticsByYear($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $query = \DB::query()->fromSub($res, 'srv')->selectRaw('extract(year from start_date) as year, count(id) as count')->
            groupBy('year');

        return $query;
    }

    public static function getStatisticsByType($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $survTypeColumn = FK::get(\App\Tables::SURVEY_TYPE_TABLE);
        $query = \DB::query()->fromSub($res, 'srv')->selectRaw($survTypeColumn.', count(id) as count')->
            groupBy($survTypeColumn);

        return $query;
    }

    public static function getStatisticsByTool($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $toolColumn = FK::get(\App\Tables::TOOL_TABLE);
        $query = \DB::query()->fromSub($res, 'srv')->selectRaw($toolColumn.', count(id) as count')->
            groupBy($toolColumn);

        return $query;
    }

    public static function getStatisticsByDirection($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $directionColumn = 'direction';
        $query = \DB::query()->fromSub($res, 'srv')->selectRaw($directionColumn.', count(id) as count')->
            groupBy($directionColumn);

        return $query;
    }

    public static function getUniqueYears($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'srv')->selectRaw('extract(year from start_date) as year')->distinct();

        return $query;
    }

    public static function getUniqueDirections($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'srv')->select('direction')->distinct();

        return $query;
    }

    public static function getUniqueToolIds($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'srv')->select(FK::get(\App\Tables::TOOL_TABLE))->distinct();

        return $query;
    }

    public static function getUniqueTypeIds($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'srv')->select(FK::get(\App\Tables::SURVEY_TYPE_TABLE))->distinct();

        return $query;
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }
    
    public function scopeOfType($query, $type = null)
    {
        if(is_null($type)) return $query;
        return $query->where(FK::get(\App\Tables::SURVEY_TYPE_TABLE), $type);
    }

    public function scopeInYear($query, $year = null)
    {
        if(is_null($year)) return $query;
        return $query->whereYear('start_date', $year);
    }

    public function scopeWithDirection($query, $direction = null)
    {
        if(is_null($direction)) return $query;
        return $query->where('direction', $direction);
    }

    public function scopeInZone($query, $zoneId = null)
    {
        if(is_null($zoneId)) return $query;
        return $query->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($zoneId));
    }

    public function scopeByTool($query, $tool = null)
    {
        if(is_null($tool)) return $query;
        return $query->where(FK::get(\App\Tables::TOOL_TABLE), $tool);
    }

    public function scopeContainsString($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(name) like (?)',["%$substring%"]);
    }

    public static function getStoreRules($data){
        return [
            'name' => ['required', 'max:255', Rule::unique(\App\Tables::SSSMBES_SURVEY_TABLE, 'name')->where(function ($query) use($data) {
                return $query->where(FK::get(\App\Tables::ZONE_TABLE), @$data["parent"]);
            })],
            'area' => ['required', new \App\Rules\Polygon()],
            'parent' => ['required', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['required', Rule::in(array_values(SssMbesSurvey::DIRECTION))],
            'type' => ['required', 'exists:'.\App\Tables::SURVEY_TYPE_TABLE.',key'],
            'startDate' => ['required', 'date'],
            'filePath' => ['required', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archFiles' => ['nullable', new \App\Rules\ArchiveFiles()],
            'vessel' => ['required', 'exists:'.\App\Tables::VESSEL_TABLE.',id'],
            'tool' => ['required', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
        ];
    }

    public static function getPatchRules($data){
        return [
            // 'name' => ['nullable', 'max:255', Rule::unique(\App\Tables::SSSMBES_SURVEY_TABLE, 'name')->where(function ($query) use($data) {
            //     return $query->where(FK::get(\App\Tables::ZONE_TABLE), @$data["parent"]);
            // })],
            'name' => ['nullable', 'max:255'],
            'area' => ['nullable', new \App\Rules\Polygon()],
            'parent' => ['nullable', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'direction' => ['nullable', Rule::in(array_values(SssMbesSurvey::DIRECTION))],
            'type' => ['nullable', 'exists:'.\App\Tables::SURVEY_TYPE_TABLE.',key'],
            'startDate' => ['nullable', 'date'],
            'filePath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archFiles' => ['nullable', new \App\Rules\ArchiveFiles()],
            'vessel' => ['nullable', 'exists:'.\App\Tables::VESSEL_TABLE.',id'],
            'tool' => ['nullable', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
        ];
    }

}
