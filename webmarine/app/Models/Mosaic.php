<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreateUpdateUserRecorder;

use App\Helpers\FK;
use App\Helpers\DataSystem;

use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Storage;

class Mosaic extends Model
{
    use HasFactory;
    use CreateUpdateUserRecorder;

    protected $table = \App\Tables::MOSAIC_TABLE;

    const INPUT_DIRECTION = ['L', 'X', 'D', 'I'];

    private static function userParamToUserId($user){
        if(is_int($user)) return $user;
        if(is_array($user) && array_key_exists ('id', $user)){
            return intval($user['id']);
        }
        if(\is_object($user) && property_exists($user, 'id')){
            return intval($user->id);
        }
        if(property_exists($user, 'attributes') && array_key_exists('id', $user->attributes)){
            return intval($user->id);
        }
        
        return NULL;
    }

    private static function userParamToUser($userParam){
        if($userParam instanceof User) return $userParam;
        if(is_null($userParam)){
            $user = \Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($userParam));
        }
        return $user;
    }

    public function parent(){
        return $this->belongsTo(\App\Models\Zone::class, FK::get(\App\Tables::ZONE_TABLE));
    }

    public function mainTool(){
        return $this->belongsTo(\App\Models\Tool::class, 'main_tool_id');
    }

    public function permissions(){
        return $this->hasMany(\App\Models\MosaicPermission::class, $this->table.'_id');
    }

    public function type(){
        return $this->belongsTo(\App\Models\MosaicType::class, FK::get(\App\Tables::MOSAIC_TYPE_TABLE));
    }

    public function archFiles(){
        return $this->hasMany(\App\Models\MosaicArchFile::class, $this->table.'_id');
    }

    public function isVisible($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return True;

        if(array_key_exists('is_visible', $this->attributes)){
            return $this->is_visible;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $visibleDefault = $user->seeMosaicsByDefault();
            $visibleIndex = 0;
            foreach($perms as $perm){
                $visibleIndex += $perm->can_see;
            }
            
            return $visibleDefault ? $visibleIndex>=0 : $visibleIndex>0;
        }
    }

    public function isEditable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_editable', $this->attributes)){
            return $this->is_editable;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $editableDefault = $user->editMosaicsByDefault();
            $editableIndex = 0;
            foreach($perms as $perm){
                $editableIndex += $perm->can_edit;
            }
            return $editableDefault ? $editableIndex>=0 : $editableIndex>0;
        }
    }

    public function isDownloadable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_downloadable', $this->attributes)){
            return $this->is_downloadable;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $downloadableDefault = $user->downloadMosaicsByDefault();
            $downloadableIndex = 0;
            foreach($perms as $perm){
                $downloadableIndex += $perm->can_download;
            }
            return $downloadableDefault ? $downloadableIndex>=0 : $downloadableIndex>0;
        }
    }

    public function getAbsoluteArchPath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataArchPath();
        }else{
            $path = $this->getAbsoluteTrashArchPath();
        }
        return $path;
    }

    public function getAbsoluteDataArchPath(){
        if(empty($this->arch_path)) return null;

        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->arch_path," /\/"));
        return $path;
    }

    public function getAbsoluteTrashArchPath(){
        if(empty($this->arch_path)) return null;

        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $archRelFolder = pathinfo(ltrim($this->arch_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->arch_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $archRelFolder, $this->id, $archName);
        return $path;
    }

    public function getAbsoluteFilePath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataFilePath();
        }else{
            $path = $this->getAbsoluteTrashFilePath();
        }
        return $path;
    }

    public function getAbsoluteDataFilePath(){
        if(empty($this->file_path)) return null;

        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->file_path," /\/"));
        return $path;
    }

    public function getAbsoluteTrashFilePath(){
        if(empty($this->file_path)) return null;
        
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $fileRelFolder = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $fileRelFolder, $this->id, $archName);
        return $path;
    }

    public function delete(){
        //If it is already deleted do nothing
        if($this->is_deleted)
            return;

        // Get path to file and archive
        $srcArchPath = $this->getAbsoluteDataArchPath();
        $srcFilePath = $this->getAbsoluteDataFilePath();
    
        // Generate resulting path in trash folder
        $dstArchPath = $this->getAbsoluteTrashArchPath();
        $dstArchFolder = pathinfo($dstArchPath, PATHINFO_DIRNAME);

        $dstFilePath = $this->getAbsoluteTrashFilePath();
        $dstFileFolder = pathinfo($dstFilePath, PATHINFO_DIRNAME);

        try{
            // FIXME where did you initialized $disk variable?
            $disk = Storage::disk('data');

            // Make necessary folders in trash folder
            if(!$disk->exists($dstArchFolder) && !$disk->makeDirectory($dstArchFolder))
                throw new \Exception("Failed to delete survey");
            
            // Make necessary folders in trash folder
            if(!$disk->exists($dstFileFolder) && !$disk->makeDirectory($dstFileFolder))
                throw new \Exception("Failed to delete survey");

            // Move archive
            if($disk->exists($srcArchPath))
                $disk->move($srcArchPath, $dstArchPath);
            
            // Move file
            if($disk->exists($srcFilePath))
                $disk->move($srcFilePath, $dstFilePath);
        }catch(\Exception $e){
            throw new \Exception("Failed to delete mosaic");
        }
        
        // Set flag
        $this->is_deleted = true;
        $this->save();
    }

    private static function allWithPermissions($user){
        $userId = $user->id;
        $groupId = $user->group->id;
        $mosaicTable = \App\Tables::MOSAIC_TABLE;
        $mosaicPermTable = \App\Tables::MOSAIC_PERM_TABLE;

        $seeMosaicDefault = $user->seeMosaicsByDefault();
        $editMosaicDefault = $user->editMosaicsByDefault();
        $downloadMosaicDefault = $user->downloadMosaicsByDefault();

        $seeMosaicColumn = 'coalesce(sum(can_see),0)'.($seeMosaicDefault?">=":">").'0';
        $editMosaicColumn = 'coalesce(sum(can_edit),0)'.($editMosaicDefault?">=":">").'0';
        $downloadSurvColumn = 'coalesce(sum(can_download),0)'.($downloadMosaicDefault?">=":">").'0';

        $res = static::query()->where('is_deleted', false);
        $res->selectRaw($mosaicTable.'.*');
        $res->selectRaw($seeMosaicColumn.' as is_visible');
        $res->selectRaw($editMosaicColumn.' as is_editable');
        $res->selectRaw($downloadSurvColumn.' as is_downloadable');
        
        $res->leftJoin($mosaicPermTable, $mosaicTable.'.id', '=', $mosaicPermTable.'.'.$mosaicTable.'_id')
        ->where(function($query)use($userId,$groupId,$mosaicPermTable){
            $query->where($mosaicPermTable.'.user_id', $userId)->
            orWhere($mosaicPermTable.'.user_id', $groupId)->
            orWhereNull($mosaicPermTable.'.user_id');
        })->groupBy($mosaicTable.'.id');

        $res->havingRaw($seeMosaicColumn);
        
        return $res;
    }

    /**
     * Returns mosaics visible for user
     */
    public static function visibleMosaics($user = null){
        $user = static::userParamToUser($user);

        if($user->isAdmin()) return static::query();

        return static::allWithPermissions($user);
    }

    /**
     * Returns statistics by surveys by years
     */
    public static function getStatisticsByYear($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $query = \DB::query()->fromSub($res, 'msc')->selectRaw('extract(year from creation_date) as year, count(id) as count')->
            groupBy('year');

        return $query;
    }

    public static function getStatisticsByType($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $mosaicTypeColumn = FK::get(\App\Tables::MOSAIC_TYPE_TABLE);
        $query = \DB::query()->fromSub($res, 'msc')->selectRaw($mosaicTypeColumn.', count(id) as count')->
            groupBy($mosaicTypeColumn);

        return $query;
    }

    public static function getStatisticsByTool($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $toolColumn = 'main_'.FK::get(\App\Tables::TOOL_TABLE);
        $query = \DB::query()->fromSub($res, 'msc')->selectRaw($toolColumn.', count(id) as count')->
            groupBy($toolColumn);

        return $query;
    }

    public static function getStatisticsByDirection($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);

        $directionColumn = 'input_direction';
        $query = \DB::query()->fromSub($res, 'srv')->selectRaw($directionColumn.', count(id) as count')->
            groupBy($directionColumn);

        return $query;
    }

    public static function getUniqueYears($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'msc')->selectRaw('extract(year from creation_date) as year')->distinct();

        return $query;
    }

    public static function getUniqueInputDirections($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'msc')->select('input_direction')->distinct();

        return $query;
    }

    public static function getUniqueMainToolIds($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'msc')->select('main_tool_id')->distinct();

        return $query;
    }

    public static function getUniqueTypeIds($user = null){
        $user = static::userParamToUser($user);

        $res = static::allWithPermissions($user);
        $query = \DB::query()->fromSub($res, 'msc')->select(FK::get(\App\Tables::MOSAIC_TYPE_TABLE))->distinct();

        return $query;
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }

    public function scopeOfType($query, $type = null)
    {
        if(is_null($type)) return $query;
        return $query->where(FK::get(\App\Tables::MOSAIC_TYPE_TABLE), $type);
    }

    public function scopeInYear($query, $year = null)
    {
        if(is_null($year)) return $query;
        return $query->whereYear('creation_date', $year);
    }

    public function scopeWithInputDirection($query, $direction = null)
    {
        if(is_null($direction)) return $query;
        return $query->where('input_direction', $direction);
    }

    public function scopeWithMainTool($query, $mainTool = null){
        if(is_null($mainTool)) return $query;
        return $query->where('main_tool_id', $mainTool);
    }

    public function scopeInZone($query, $zoneId = null)
    {
        if(is_null($zoneId)) return $query;
        return $query->where(FK::get(\App\Tables::ZONE_TABLE), $zoneId);
    }

    public function scopeContainsString($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(name) like (?)',["%$substring%"]);
    }


    public static function getStoreRules($data){
        return [
            'name' => ['required', 'max:255'],
            'area' => ['required', new \App\Rules\Polygon()],
            'parent' => ['required', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'inputDirection' => ['required', Rule::in(array_values(Mosaic::INPUT_DIRECTION))],
            'type' => ['required', 'exists:'.\App\Tables::MOSAIC_TYPE_TABLE.',key'],
            'mainTool' => ['required', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'creationDate' => ['required', 'date'],
            'filePath' => ['required', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

    public static function getPatchRules($data){
        return [
            'name' => ['nullable', 'max:255'],
            'area' => ['nullable', new \App\Rules\Polygon()],
            'parent' => ['nullable', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'inputDirection' => ['nullable', Rule::in(array_values(Mosaic::INPUT_DIRECTION))],
            'type' => ['nullable', 'exists:'.\App\Tables::MOSAIC_TYPE_TABLE.',key'],
            'mainTool' => ['nullable', 'exists:'.\App\Tables::TOOL_TABLE.',id'],
            'creationDate' => ['nullable', 'date'],
            'filePath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
            'archFiles' => ['nullable', new \App\Rules\ArchiveFiles()]
        ];
    }
}
