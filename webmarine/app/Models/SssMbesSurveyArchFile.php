<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class SssMbesSurveyArchFile extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SSSMBES_SURVEY_ARCHIVE_FILES_TABLE;

    public function survey(){
        return $this->belongsTo(\App\Models\SssMbesSurvey::class, FK::get(\App\Tables::SSSMBES_SURVEY_TABLE));
    }
}
