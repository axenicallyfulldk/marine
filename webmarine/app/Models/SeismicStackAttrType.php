<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class SeismicStackAttrType extends Model
{
    use HasFactory;

    // const GROUPS = ['L', 'X', 'D', 'I'];

    protected $table = \App\Tables::SEISMIC_STACK_ATTR_TYPE_TABLE;

    public function stacks2d(){
        return $this->hasMany(\App\Models\Seismic2dStack::class, FK::get(\App\Tables::SEISMIC_STACK_ATTR_TYPE_TABLE));
    }

    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }

    public static function findByNameSubstr($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }
}
