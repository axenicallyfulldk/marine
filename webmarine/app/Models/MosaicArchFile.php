<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class MosaicArchFile extends Model
{
    use HasFactory;

    protected $table = \App\Tables::MOSAIC_ARCHIVE_FILES_TABLE;

    public function mosaic(){
        return $this->belongsTo(\App\Models\Mosaic::class, FK::get(\App\Tables::MOSAIC_TABLE));
    }

    public function scopeWithName($query, $name = null)
    {
        if(is_null($name)) return $query;
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }
}
