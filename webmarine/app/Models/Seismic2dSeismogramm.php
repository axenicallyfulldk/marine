<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Seismic2dSeismogramm extends Model
{
    use HasFactory;


    protected $table = \App\Tables::SEISMIC2D_SEISMOGRAM_TABLE;

    public function survey(){
        return $this->belongsTo(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE));
    }

    // Returns absolute path to file.
    public function getAbsoluteFilePath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataFilePath();
        }else{
            $path = $this->getAbsoluteTrashFilePath();
        }
        return $path;
    }

    public function getAbsoluteDataFilePath(){
        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->file_path," /\/"));
        return $path;
    }

    public function getAbsoluteTrashFilePath(){
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $fileRelFolder = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $fileRelFolder, $this->id, $archName);
        return $path;
    }

    public static function visible($user = null){
        return static::query()->whereIn(FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE), Seismic2dSurvey::visible()->select('id'));
    }

    public function isVisible($user = null){
        return $this->survey->isVisible($user);
    }

    public function isDownloadable($user = null){
        return $this->survey->isDownloadable($user);
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }

    public static function findByNameSubstr($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    public static function findByAlias($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(alias) like (?)',["$name"]);
    }

    public static function findByAliasSubstr($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(alias) like (?)',["%$name%"]);
    }

    public function scopeByNameSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(name) like (?)',["%$substring%"]);
    }

    public function scopeByAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(alias) like (?)',["%$substring%"]);
    }

    public function scopeByNameOrAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        $query->whereRaw('lower(name) like (?)',["%$substring%"])->orWhereRaw('lower(alias) like (?)',["%$substring%"]);
        return $query;
    }
}
