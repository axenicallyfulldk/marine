<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OceanBasin extends Model
{
    use HasFactory;

    protected $table = \App\Tables::OCEAN_BASIN_TABLE;

    public const name = "ocean_basin";

    public function zone(){
        return $this->morphOne('Zone', 'zonable');
    }
}
