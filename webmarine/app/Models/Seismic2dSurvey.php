<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Seismic2dStack;
use App\Helpers\FK;
use App\Helpers\DataSystem;

use App\Traits\NameSearchTrait;

use Illuminate\Validation\Rule;

class Seismic2dSurvey extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::SEISMIC2D_SURVEY_TABLE;

    const DIRECTION = ['L', 'X', 'D'];

    private static function userParamToUserId($user){
        if(is_int($user)) return $user;
        if(is_array($user) && array_key_exists ('id', $user)){
            return intval($user['id']);
        }
        if(\is_object($user) && property_exists($user, 'id')){
            return intval($user->id);
        }
        if(property_exists($user, 'attributes') && array_key_exists('id', $user->attributes)){
            return intval($user->id);
        }
        
        return NULL;
    }

    private static function userParamToUser($userParam){
        if($userParam instanceof User) return $userParam;
        if(is_null($userParam)){
            $user = \Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($userParam));
        }
        return $user;
    }

    public function seismogramms(){
        return $this->hasMany(\App\Models\Seismic2dSeismogramm::class, FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE));
    }

    public function stacks(){
        return $this->hasMany(\App\Models\Seismic2dStack::class, FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE));
    }

    public function trip(){
        return $this->belongsTo(\App\Models\Trip::class, FK::get(\App\Tables::TRIP_TABLE));
    }

    public function source(){
        return $this->belongsTo(\App\Models\SeismicSource::class, FK::get(\App\Tables::SEISMIC_SOURCE_TABLE));
    }

    public function streamer(){
        return $this->belongsTo(\App\Models\SeismicStreamer::class, FK::get(\App\Tables::STREAMER_TABLE));
    }

    public function resolution(){
        return $this->belongsTo(\App\Models\SeismicResolutionClass::class, FK::get(\App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE));
    }

    public function zone(){
        return $this->belongsTo(\App\Models\Zone::class, FK::get(\App\Tables::ZONE_TABLE));
    }

    public function field(){
        return $this->belongsTo(\App\Models\Field::class, FK::get(\App\Tables::FIELD_TABLE));
    }

    public function permissions(){
        return $this->hasMany(\App\Models\Seismic2dSurveyPermission::class, FK::get($this->table));
    }

    public function archFiles(){
        return $this->hasMany(\App\Models\Seismic2dSurveyArchFile::class, FK::get($this->table));
    }

    /**
     * Returns whether the survey is visible for the user.
     */
    public function isVisible($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_visible', $this->attributes)){
            // We suppose that field 'is_visible' was calculated for given user.
            return $this->is_visible;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $visibleDefault = $user->seeSeismic2dSurveysByDefault();
            $visibleIndex = 0;
            foreach($perms as $perm){
                $visibleIndex += $perm->can_see;
            }
            return $visibleDefault ? $visibleIndex>=0 : $visibleIndex>0;
        }
    }

    /**
     * Returns whether the user can edit the survey.
     */
    public function isEditable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_editable', $this->attributes)){
            return $this->is_editable;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $editableDefault = $user->seeSeismic2dSurveysByDefault();
            $editableIndex = 0;
            foreach($perms as $perm){
                $editableIndex += $perm->can_edit;
            }
            return $editableDefault ? $editableIndex>=0 : $editableIndex>0;
        }
    }

    /**
     * Returns whether the user can download the survey.
     */
    public function isDownloadable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_downloadable', $this->attributes)){
            return $this->is_downloadable;
        }else{
            $perms = $this->permissions()->where(FK::get(\App\Tables::USER_TABLE), $user->id)->orWhere(FK::get(\App\Tables::USER_TABLE), $group->id)->get();
            $downloadableDefault = $user->downloadSeismic2dSurveysByDefault();
            $downloadableIndex = 0;
            foreach($perms as $perm){
                $downloadableIndex += $perm->can_download;
            }
            return $downloadableDefault ? $downloadableIndex>=0 : $downloadableIndex>0;
        }
    }

    /**
     * Returns absolute path to survey's archive.
     */
    public function getAbsoluteArchPath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataArchPath();
        }else{
            $path = $this->getAbsoluteTrashArchPath();
        }
        return $path;
    }

    // Returns absolute path to archive.
    // If survey is deleted this path would not be valid.
    public function getAbsoluteDataArchPath(){
        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->arch_path," /\/"));
        return $path;
    }

    // Returns absolute path to archive in trash folder.
    // If survey is not deleted this path would not be valid.
    public function getAbsoluteTrashArchPath(){
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $archRelFolder = pathinfo(ltrim($this->arch_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->arch_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $archRelFolder, $this->id, $archName);
        return $path;
    }


    // Returns all surveys visible for user
    private static function allVisibleSurveys($user){
        $userId = $user->id;
        $groupId = $user->group->id;
        $survTable = \App\Tables::SEISMIC2D_SURVEY_TABLE;
        $survPermTable = \App\Tables::SEISMIC2D_SURVEY_PERM_TABLE;

        // select * from survey left join survey_perm on survey.id = survey_perm.survey_id
        // where
        //  is_deleted = false
        //  and
        //  (surv_perm.user_id = $uid or surv_perm.user_id = $guid or surv_perm.user_id is null)
        // group by survey.id having {coalesce(sum(can_see),0)>=0 or coalesce(sum(can_see),0)>0}

        $seeSurveyDefault = $user->seeSeismic2dSurveysByDefault();

        $seeSurvColumn = 'coalesce(sum(can_see),0)'.($seeSurveyDefault?">=":">").'0';

        $res = static::query()->where('is_deleted', false);

        $ufk = FK::get(\App\Tables::USER_TABLE);
        
        $res->leftJoin($survPermTable, $survTable.'.id', '=', $survPermTable.'.'.FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE))
        ->where(function($query)use($userId,$groupId,$survPermTable,$ufk){
            $query->where($survPermTable.'.'.$ufk, $userId)->
            orWhere($survPermTable.'.'.$ufk, $groupId)->
            orWhereNull($survPermTable.'.'.$ufk);
        })->groupBy($survTable.'.id');

        $res->havingRaw($seeSurvColumn);
        
        return $res;
    }

    // Returns all visible for the user surveys and permisssion.
    private static function allWithPermissions($user){
        $survTable = \App\Tables::SEISMIC2D_SURVEY_TABLE;
        $survPermTable = \App\Tables::SEISMIC2D_SURVEY_PERM_TABLE;

        $seeSurveyDefault = $user->seeSeismic2dSurveysByDefault();
        $editSurveyDefault = $user->editSeismic2dSurveysByDefault();
        $downloadSurveyDefault = $user->downloadSeismic2dSurveysByDefault();

        $seeSurvColumn = 'coalesce(sum(can_see),0)'.($seeSurveyDefault?">=":">").'0';
        $editSurvColumn = 'coalesce(sum(can_edit),0)'.($editSurveyDefault?">=":">").'0';
        $downloadSurvColumn = 'coalesce(sum(can_download),0)'.($downloadSurveyDefault?">=":">").'0';

        $res = static::allVisibleSurveys($user);

        $res->selectRaw($survTable.'.*');
        $res->selectRaw($seeSurvColumn.' as is_visible');
        $res->selectRaw($editSurvColumn.' as is_editable');
        $res->selectRaw($downloadSurvColumn.' as is_downloadable');

        return $res;
    }

    /**
     * Returns visible for user surveys
     */
    public static function visible($user = null){
        $user = static::userParamToUser($user);

        if($user->isAdmin()){
            return static::query()->select('*',\DB::raw('true as is_visible'), \DB::raw('true as is_editable'), \DB::raw('true as is_downloadable'));
        }

        return static::allWithPermissions($user);
    }


    public static function getStoreRules($data){
        return [
            'name' => ['required', 'max:255'],
            'alias' => ['nullable', 'max:255'],
            'area' => ['required', new \App\Rules\Line()],
            'direction' => ['required', Rule::in(array_values(Seismic2dSurvey::DIRECTION))],
            'startDate' => ['required', 'date'], 
            'parentZone' => ['required', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'field' => ['nullable', 'exists:'.\App\Tables::FIELD_TABLE.',id'],
            'vessel' => ['required', 'exists:'.\App\Tables::VESSEL_TABLE.',id'],
            'source' => ['required', 'exists:'.\App\Tables::SEISMIC_SOURCE_TABLE.',id'],
            'srcGroupCount' => ['required', 'integer', 'min:0'],
            'srcPerGroup' => ['required', 'integer', 'min:0'],
            'streamer' => ['required', 'exists:'.\App\Tables::STREAMER_TABLE.',id'],
            'isTowed' => ['required', 'boolean'],
            'resolution' => ['required', 'exists:'.\App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE.',id'],
            'customer' => ['nullable'],
            'operator' => ['nullable'],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

    public static function getPatchRules($data){
        return [
            'name' => ['nullable', 'max:255'],
            'alias' => ['nullable', 'max:255'],
            'area' => ['nullable', new \App\Rules\Line()],
            'direction' => ['nullable', Rule::in(array_values(Seismic2dSurvey::DIRECTION))],
            'startDate' => ['nullable', 'date'], 
            'parentZone' => ['nullable', 'exists:'.\App\Tables::ZONE_TABLE.',id'],
            'field' => ['nullable', 'exists:'.\App\Tables::FIELD_TABLE.',id'],
            'vessel' => ['nullable', 'exists:'.\App\Tables::VESSEL_TABLE.',id'],
            'source' => ['nullable', 'exists:'.\App\Tables::SEISMIC_SOURCE_TABLE.',id'],
            'srcGroupCount' => ['nullable', 'integer', 'min:0'],
            'srcPerGroup' => ['nullable', 'integer', 'min:0'],
            'streamer' => ['nullable', 'exists:'.\App\Tables::STREAMER_TABLE.',id'],
            'isTowed' => ['nullable', 'boolean'],
            'resolution' => ['nullable', 'exists:'.\App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE.',id'],
            'customer' => ['nullable'],
            'operator' => ['nullable'],
            'archPath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

        public function scopeInYear($query, $year = null)
        {
            if(is_null($year)) return $query;
            return $query->whereYear('start_date', $year);
        }

    public function scopeWithDirection($query, $direction = null)
    {
        if(is_null($direction)) return $query;
        return $query->where('direction', $direction);
    }

    public function scopeInZone($query, $zoneId = null)
    {
        if(is_null($zoneId)) return $query;
        return $query->whereIn(FK::get(\App\Tables::ZONE_TABLE), Zone::subZonesId($zoneId));
    }

    public function scopeBySource($query, $source = null)
    {
        if(is_null($source)) return $query;
        return $query->where(FK::get(\App\Tables::SEISMIC_SOURCE_TABLE), $source);
    }

    public function scopeContainsString($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(name) like (?)',["%$substring%"]);
    }

    public function scopeByNameOrAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        $query->whereRaw('lower(name) like (?)',["%$substring%"])->orWhereRaw('lower(alias) like (?)',["%$substring%"]);
        return $query;
    }
}
