<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

use App\Traits\NameSearchTrait;

class HazardType extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::HAZARD_TYPE_TABLE;

    const KeyToModelMapping = [
        'extraction' => \App\Models\GlacialHazard::class,
        'thermal_abrasion' => \App\Models\GlacialHazard::class,
        'ice_rafting' => \App\Models\GlacialHazard::class,
        'curve_seabottom' => \App\Models\GlacialHazard::class,
        'lythodynamic' => \App\Models\GeomorphologicalHazard::class,
        'gravity' => \App\Models\GeomorphologicalHazard::class,
        'morphological' => \App\Models\GeomorphologicalHazard::class,
        'wave' => \App\Models\HydrologicalHazard::class,
        'tidal' => \App\Models\HydrologicalHazard::class,
        'hydrological_temperature' => \App\Models\HydrologicalHazard::class,
        'tsunami' => \App\Models\HydrologicalHazard::class,
        'wind' => \App\Models\AtmosphericHazard::class,
        'rainfall_and_snowfall' => \App\Models\AtmosphericHazard::class,
        'atmospheric_temperature' => \App\Models\AtmosphericHazard::class,
        'electromagnetics' => \App\Models\AtmosphericHazard::class,
        'permafrost' => \App\Models\GeocryogenicHazard::class,
        'post-perfmafrost' => \App\Models\GeocryogenicHazard::class,
        'cryogenic_soil' => \App\Models\GeocryogenicHazard::class,
        'boulder_beds' => \App\Models\GeologicalHazard::class,
        'soft_sediments' => \App\Models\GeologicalHazard::class,
        'inhomogeneous' => \App\Models\GeologicalHazard::class,
        'rock_soils_specific_in_composition' => \App\Models\GeologicalHazard::class,
        'structure-specific_rock_soils' => \App\Models\GeologicalHazard::class,
        'buried_paleovalleys_beds' => \App\Models\GeologicalHazard::class,
        'seabottom_litochange' => \App\Models\GeologicalHazard::class,
        'shallow_gas' => \App\Models\FluidogenicHazard::class,
        'deep-lying_gas' => \App\Models\FluidogenicHazard::class,
        'AHRP' => \App\Models\FluidogenicHazard::class,
        'clathrates' => \App\Models\FluidogenicHazard::class,
        'gas_shows' => \App\Models\FluidogenicHazard::class,
        'fluid_flowout' => \App\Models\FluidogenicHazard::class,
        'earthquakes' => \App\Models\SeismotectonicHazard::class,
        'warping_movements' => \App\Models\SeismotectonicHazard::class,
        'buried_dangerous' => \App\Models\TechnogenicHazard::class,
        'thermal_effect' => \App\Models\TechnogenicHazard::class,
        'mechanical_action' => \App\Models\TechnogenicHazard::class,
        'chemical_action' => \App\Models\TechnogenicHazard::class,
    ];

    public function class(){
        return $this->belongsTo(\App\Models\HazardClass::class, FK::get(\App\Tables::HAZARD_CLASS_TABLE));
    }

    public static function findByKey($key){
        return HazardType::query()->where("key", $key);
    }
    
    public function getModel(){
        return static::KeyToModelMapping[$this->key];
    }

    public function atmosphericHazards(){
        return $this->hasMany(\App\Models\AtmosphericHazard::class, FK::get(static::$table));
    }

    public function fluidogenicHazards(){
        return $this->hasMany(\App\Models\FluidogenicHazard::class, FK::get(static::$table));
    }

    public function geocryogenicHazards(){
        return $this->hasMany(\App\Models\GeocryogenicHazard::class, FK::get(static::$table));
    }

    public function geologicalHazards(){
        return $this->hasMany(\App\Models\GeologicalHazards::class, FK::get(static::$table));
    }

    public function geomorphologicalHazards(){
        return $this->hasMany(\App\Models\GeomorphologicalHazard::class, FK::get(static::$table));
    }

    public function glacialHazards(){
        return $this->hasMany(\App\Models\GlacialHazard::class, FK::get(static::$table));
    }   
    
    public function hydrologicalHazards(){
        return $this->hasMany(\App\Models\HydrologicalHazard::class, FK::get(static::$table));
    } 

    public function seismotectonicHazards(){
        return $this->hasMany(\App\Models\SeismotectonicHazard::class, FK::get(static::$table));
    }

    public function technogenicHazards(){
        return $this->hasMany(\App\Models\TechnogenicHazard::class, FK::get(static::$table));
    }
}
