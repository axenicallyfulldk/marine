<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seismic3dHorizon extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SEISMIC3D_HORIZON_TABLE;

    public function horizon(){
        return $this->belongsTo(\App\Models\Horizon::class, FK::get(\App\Tables::HORIZON_TABLE));
    }

    public function stack(){
        return $this->belongsTo(\App\Models\Seismic3dStack::class, FK::get(\App\Tables::SEISMIC3D_STACK_TABLE));
    }
}
