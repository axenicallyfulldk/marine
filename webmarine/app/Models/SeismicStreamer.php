<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\NameSearchTrait;

class SeismicStreamer extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::STREAMER_TABLE;
    
    /**
     * Returns surveys that uses this streamer
     */
    public function surveys2d(){
        return $this->hasMany(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::STREAMER_TABLE));
    }

    /**
     * Returns surveys that uses this streamer
     */
    public function surveys3d(){
        return $this->hasMany(\App\Models\Seismic3dSurvey::class, FK::get(\App\Tables::STREAMER_TABLE));
    }

    public static function getStoreRules(){
        return [
            'name' => ['required', 'max:255'],
            'receiverDist' => ['required', 'numeric'],
            'length' => ['required', 'numeric'],
        ];
    }

    public static function getPatchRules(){
        return [
            'name' => ['nullable', 'max:255'],
            'receiverDist' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
        ];
    }
}
