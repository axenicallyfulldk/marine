<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class MosaicType extends Model
{
    use HasFactory;

    protected $table = \App\Tables::MOSAIC_TYPE_TABLE;

    public function mosaics(){
        return $this->hasMany(\App\Models\Mosaic::class, FK::get(\App\Tables::MOSAIC_TYPE_TABLE));
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["$name"]);
    }

    public static function findByKey($key){
        return static::query()->where('key',$key);
    }
}
