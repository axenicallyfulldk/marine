<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreateUpdateUserRecorder;

use App\Helpers\FK;

class Trip extends Model
{
    use HasFactory;
    use CreateUpdateUserRecorder;

    protected $table = \App\Tables::TRIP_TABLE;

    public function sssmbesSurveys(){
        return $this->hasMany(\App\Models\SssMbesSurvey::class, FK::get(\App\Tables::TRIP_TABLE));
    }

    public function seismic2dSurveys(){
        return $this->hasMany(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::TRIP_TABLE));
    }

    public function vessel(){
        return $this->belongsTo(\App\Models\Vessel::class, FK::get(\App\Tables::VESSEL_TABLE));
    }
}
