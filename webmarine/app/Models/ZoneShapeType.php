<?php

namespace App\Models;

abstract class ZoneShapeType{
    const Simple = 0;
    const WKT = 1;
};