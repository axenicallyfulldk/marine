<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

use App\Traits\NameSearchTrait;

class SeismicSource extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::SEISMIC_SOURCE_TABLE;

    public function type(){
        return $this->belongsTo(\App\Models\SeismicSourceType::class, FK::get(\App\Tables::SEISMIC_SOURCE_TYPE_TABLE));
    }

    /**
     * Returns surveys that uses this streamer
     */
    public function surveys2d(){
        return $this->hasMany(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::SEISMIC_SOURCE_TABLE));
    }

    /**
     * Returns surveys that uses this streamer
     */
    public function surveys3d(){
        return $this->hasMany(\App\Models\Seismic3dSurvey::class, FK::get(\App\Tables::SEISMIC_SOURCE_TABLE));
    }

    public static function getStoreRules(){
        return [
            'name' => ['required', 'max:255'],
            'type' => ['required', 'exists:'.\App\Tables::SEISMIC_SOURCE_TYPE_TABLE.',id'],
        ];
    }

    public static function getPatchRules(){
        return [
            'name' => ['nullable', 'max:255'],
            'type' => ['nullable', 'exists:'.\App\Tables::SEISMIC_SOURCE_TYPE_TABLE.',id'],
        ];
    }
}
