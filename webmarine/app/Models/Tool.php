<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreateUpdateUserRecorder;

use App\Helpers\FK;

class Tool extends Model
{
    use HasFactory;
    use CreateUpdateUserRecorder;

    protected $hidden = ['created_at', 'updated_at'];

    protected $guarded = ['id']; 

    protected $table = \App\Tables::TOOL_TABLE;

    public function surveys(){
        return $this->hasMany(\App\Models\SssMbesSurvey::class, FK::get(\App\Tables::TOOL_TABLE));
    }

    public function mosaics(){
        return $this->hasMany(\App\Models\Mosaic::class, 'main_tool_id');
    }

    public static function visibleTools(){
        return static::query()->where('is_deleted', false);
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(brand || \' \' || model) like (?)',["%$name%"]);
    }

    public static function getStoreRules(){
        return [
            'brand' => 'required|max:64',
            'model' => 'required|max:128',
            'mapColor' => ['nullable', 'regex:/^#([0-9a-fA-F]{6}|[0-9a-fA-F]{8})$/i']
        ];
    }

    public static function getPatchRules(){
        return [
            'brand' => 'nullable|max:64',
            'model' => 'nullable|max:128',
            'mapColor' => ['nullable', 'regex:/^#([0-9a-fA-F]{6}|[0-9a-fA-F]{8})$/i']
        ];
    }
}
