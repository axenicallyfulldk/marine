<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HazardClass extends Model
{
    use HasFactory;

    protected $table = \App\Tables::HAZARD_CLASS_TABLE;

    public function types(){
        return $this->hasMany(\App\Models\HazardType::class, FK::get(\App\Tables::HAZARD_CLASS_TABLE));
    }

    public static function findByName($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }    
}
