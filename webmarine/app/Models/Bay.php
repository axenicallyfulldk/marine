<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bay extends Model
{
    use HasFactory;

    protected $table = \App\Tables::BAY_TABLE;

    public const name = "bay";

    public function zone(){
        return $this->morphOne('Zone', 'zonable');
    }
}
