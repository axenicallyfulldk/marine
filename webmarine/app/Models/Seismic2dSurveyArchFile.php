<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Seismic2dSurveyArchFile extends Model
{
    use HasFactory;

    protected $table = \App\Tables::SEISMIC2D_SURVEY_ARCHIVE_FILES_TABLE;

    public function survey(){
        return $this->belongsTo(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE));
    }
}
