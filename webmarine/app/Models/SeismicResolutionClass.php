<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\NameSearchTrait;

use App\Helpers\FK;

class SeismicResolutionClass extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE;    

    /**
     * Returns surveys that have this seismic resolution class
     */
    public function surveys2d(){
        return $this->hasMany(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE));
    }

    public function surveys3d(){
        return $this->hasMany(\App\Models\Seismic3dSurvey::class, FK::get(\App\Tables::SEISMIC_RESOLUTION_CLASS_TABLE));
    }

    public static function findByKey($key){
        return static::query()->byKey($key);
    }

    public function scopeByKey($query, $key = null)
    {
        if(is_null($key)) return $query;
        $key = \mb_strtolower($key);
        return $query->whereRaw('lower(key) like (?)',["$key"]);
    }

    public function scopeByKeyOrName($query, $str){
        if(is_null($str)) return $query;
        $str = mb_strtolower($str);
        return $query->byName($str)->orWhereRaw('lower(key) like (?)',["$str"]);
    }
}
