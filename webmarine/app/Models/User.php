<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Validation\Rule;

use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    protected $table = \App\Tables::USER_TABLE;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function query()
    {
        $query = (new static)->newQuery();
        return $query->whereNotNull('group_id');
    }

    public static function all($columns = ['*']){
        return User::whereNotNull('group_id')->get(is_array($columns) ? $columns : func_get_args());
    }

    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class, 'group_id', 'id');
    }

    public function isAdmin(){
        return $this->group->isAdminGroup();
    }

    public function isModerator(){
        return $this->group->isModeratorGroup();
    }

    /**
     * Returns true if user can edit tools by default.
     */
    public function editToolsByDefault(){
        return is_null($this->edit_tool) ? $this->group->edit_tool : $this->edit_tool;
    }

    /**
     * Returns true if user can edit vessels by default.
     */
    public function editVesselsByDefault(){
        return is_null($this->edit_vessel) ? $this->group->edit_vessel : $this->edit_vessel;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editZonesByDefault(){
        return is_null($this->edit_zone) ? $this->group->edit_zone : $this->edit_zone;
    }


    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithSssMbesSurveysPermissions(){
        return is_null($this->sssmbes_survey_permission) ? $this->group->sssmbes_survey_permission : $this->sssmbes_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeSssMbesSurveysByDefault(){
        return is_null($this->see_sssmbes_survey) ? $this->group->see_sssmbes_survey : $this->see_sssmbes_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadSssMbesSurveysByDefault(){
        return is_null($this->download_sssmbes_survey) ? $this->group->download_sssmbes_survey : $this->download_sssmbes_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editSssMbesSurveysByDefault(){
        return is_null($this->edit_sssmbes_survey) ? $this->group->edit_sssmbes_survey : $this->edit_sssmbes_survey;
    }


    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithSeismic2dSurveysPermissions(){
        return is_null($this->seismic2d_survey_permission) ? $this->group->seismic2d_survey_permission : $this->seismic2d_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeSeismic2dSurveysByDefault(){
        return is_null($this->see_seismic2d_survey) ? $this->group->see_seismic2d_survey : $this->see_seismic2d_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadSeismic2dSurveysByDefault(){
        return is_null($this->download_seismic2d_survey) ? $this->group->download_seismic2d_survey : $this->download_seismic2d_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editSeismic2dSurveysByDefault(){
        return is_null($this->edit_seismic2d_survey) ? $this->group->edit_seismic2d_survey : $this->edit_seismic2d_survey;
    }

    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithSeismic3dSurveysPermissions(){
        return is_null($this->seismic3d_survey_permission) ? $this->group->seismic3d_survey_permission : $this->seismic3d_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeSeismic3dSurveysByDefault(){
        return is_null($this->see_seismic3d_survey) ? $this->group->see_seismic3d_survey : $this->see_seismic3d_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadSeismic3dSurveysByDefault(){
        return is_null($this->download_seismic3d_survey) ? $this->group->download_seismic3d_survey : $this->download_seismic3d_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editSeismic3dSurveysByDefault(){
        return is_null($this->edit_seismic3d_survey) ? $this->group->edit_seismic3d_survey : $this->edit_seismic3d_survey;
    }
    
    /**
     * Return true if user can grant and revoke special permissions on mosaics
     */
    public function workWithMosaicPermissions(){
        return is_null($this->mosaic_permission) ? $this->group->mosaic_permission : $this->mosaic_permission;
    }

    /**
     * Returns true if user can see mosaics by default.
     */
    public function seeMosaicsByDefault(){
        return is_null($this->see_mosaic) ? $this->group->see_mosaic : $this->see_mosaic;
    }

    /**
     * Returns true if user can download mosaics by default.
     */
    public function downloadMosaicsByDefault(){
        return is_null($this->download_mosaic) ? $this->group->download_mosaic : $this->download_mosaic;
    }

    /**
     * Returns true if user can edit mosaics by default.
     */
    public function editMosaicsByDefault(){
        return is_null($this->edit_mosaic) ? $this->group->edit_mosaic : $this->edit_mosaic;
    }

    /**
     * Return true if user can grant and revoke special permissions on surveys
     */
    public function workWithModelsPermissions(){
        return is_null($this->seismic2d_survey_permission) ? $this->group->seismic2d_survey_permission : $this->seismic2d_survey_permission;
    }

    /**
     * Returns true if user can see surveys by default.
     */
    public function seeModelsByDefault(){
        return is_null($this->see_model) ? $this->group->see_seismic2d_survey : $this->see_seismic2d_survey;
    }

    /**
     * Returns true if user can download surveys by default.
     */
    public function downloadModelsByDefault(){
        return is_null($this->download_seismic2d_survey) ? $this->group->download_seismic2d_survey : $this->download_seismic2d_survey;
    }

    /**
     * Returns true if user can edit surveys by default.
     */
    public function editModelsByDefault(){
        return is_null($this->edit_seismic2d_survey) ? $this->group->edit_seismic2d_survey : $this->edit_seismic2d_survey;
    }


    public function zonePermissions()
    {
        return $this->belongsToMany(\App\Models\Zone::class, \App\Tables::ZONE_PERM_TABLE);
    }

    public function surveyPermissions()
    {
        return $this->belongsToMany(\App\Models\SssMbesSurvey::class, \App\Tables::SSSMBES_SURVEY_PERM_TABLE);
    }

    public static function getStoreRules(){
        return [
            'name' => ['required', 'max:255', Rule::unique(\App\Tables::USER_TABLE, 'name')],
            'role' => ['required', Rule::in(array_keys(Group::$groups))],
            'email' => ['required', 'email'],
            'password' => ['required', 'min:4'],
        ];
    }
}
