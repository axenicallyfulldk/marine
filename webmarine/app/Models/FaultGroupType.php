<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaultGroupType extends Model
{
    use HasFactory;

    protected $table = \App\Tables::FAULT_GROUP_TYPE_TABLE;

    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    public function groups(){
        return $this->hasMany(\App\Models\FaultGroup::class, FK::get(\App\Tables::FAULT_GROUP_TYPE_TABLE));
    }
}
