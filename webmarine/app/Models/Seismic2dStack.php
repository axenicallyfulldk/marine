<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

use App\Traits\NameSearchTrait;

use App\Helpers\DataSystem;

class Seismic2dStack extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::SEISMIC2D_STACK_TABLE;

    private static function userParamToUserId($user){
        if(is_int($user)) return $user;
        if(is_array($user) && array_key_exists ('id', $user)){
            return intval($user['id']);
        }
        if(\is_object($user) && property_exists($user, 'id')){
            return intval($user->id);
        }
        if(property_exists($user, 'attributes') && array_key_exists('id', $user->attributes)){
            return intval($user->id);
        }
        
        return NULL;
    }

    private static function userParamToUser($userParam){
        if($userParam instanceof User) return $userParam;
        if(is_null($userParam)){
            $user = \Auth::user();
            if(is_null($user)) return null;
        }else{
            $user = User::find(static::userParamToUserId($userParam));
        }
        return $user;
    }

    public function survey(){
        return $this->belongsTo(\App\Models\Seismic2dSurvey::class, FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE));
    }

    public function faults(){
        return $this->hasMany(\App\Models\Seismic2dFault::class, FK::get(\App\Tables::SEISMIC2D_STACK_TABLE));
    }

    public function horizons(){
        return $this->hasMany(\App\Models\Seismic2dHorizon::class, FK::get(\App\Tables::SEISMIC2D_STACK_TABLE));
    }

    public function attributes(){
        return $this->hasMany(\App\Models\Seismic2dStackAttr::class, FK::get(\App\Tables::SEISMIC2D_STACK_TABLE));
    }

    // Returns absolute path to file.
    public function getAbsoluteFilePath(){
        if(!$this->is_deleted){
            $path = $this->getAbsoluteDataFilePath();
        }else{
            $path = $this->getAbsoluteTrashFilePath();
        }
        return $path;
    }

    public function getAbsoluteDataFilePath(){
        $dataFolder = rtrim(\Config::get('app.data_folder')," /\/");
        $path = sprintf('%s/%s', $dataFolder, ltrim($this->file_path," /\/"));
        return $path;
    }

    public function getAbsoluteTrashFilePath(){
        $trashFolder = rtrim(\Config::get('app.trash_data_folder')," /\/");
        $fileRelFolder = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_DIRNAME);
        $archName = pathinfo(ltrim($this->file_path," /\/"),PATHINFO_BASENAME);
        $path = sprintf('%s/%s/%d_%s', $trashFolder, $fileRelFolder, $this->id, $archName);
        return $path;
    }

    private static function builderToString($builder){
        return vsprintf(str_replace('?', '%s', $builder->toSql()), collect($builder->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    public static function visible($user = null){
        $query = static::query()->whereIn(FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE), Seismic2dSurvey::visible($user)->select(\App\Tables::SEISMIC2D_SURVEY_TABLE.'.id'));
        $qq = "(".static::builderToString(Seismic2dSurvey::visible($user)).") as qq";  
        $qq = str_replace("''", "false", $qq);
        $query->join(\DB::raw($qq), FK::get(\App\Tables::SEISMIC2D_SURVEY_TABLE), "=", "qq.id");
        $query->select(\App\Tables::SEISMIC2D_STACK_TABLE.".*", "is_visible", "is_downloadable", "is_editable");
        return $query;
    }

    public function isVisible($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return true;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_visible', $this->attributes)){
            // We suppose that field 'is_visible' was calculated for given user.
            return $this->is_visible;
        }

        return $this->survey->isVisible($user);
    }

    public function isDownloadable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return false;
        $group = $user->group;

        if($user->isAdmin()) return true;

        if(array_key_exists('is_downloadable', $this->attributes)){
            return $this->is_downloadable;
        }

        return $this->survey->isDownloadable($user);
    }

    public function isEditable($user = null){
        $user = static::userParamToUser($user);
        if(is_null($user)) return false;

        if($user->isAdmin()) return true;

        return $this->survey->isEditable();
    }

    public static function findByAlias($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(alias) like (?)',["$name"]);
    }

    public static function findByAliasSubstr($name){
        $name = \mb_strtolower($name);
        return static::query()->whereRaw('lower(alias) like (?)',["%$name%"]);
    }

    public static function getStoreRules($data){
        return [
            'name' => ['required', 'max:255'],
            'alias' => ['nullable', 'max:255'],
            'survey' => ['required', 'exists:'.\App\Tables::SEISMIC2D_SURVEY_TABLE.',id'],
            'isMigrated' => ['nullable', 'boolean'],
            'isAmplCorrected' => ['nullable', 'boolean'],
            'isDeconvolved' => ['nullable', 'boolean'],
            'cdpOffset' => ['required', 'integer', 'min:0'],
            'cdpSize' => ['required', 'integer', 'min:1'],
            'filePath' => ['required', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

    public static function getPatchRules($data){
        return [
            'name' => ['nullable', 'max:255'],
            'alias' => ['nullable', 'max:255'],
            'survey' => ['nullable', 'exists:'.\App\Tables::SEISMIC2D_SURVEY_TABLE.',id'],
            'isMigrated' => ['nullable', 'boolean'],
            'isAmplCorrected' => ['nullable', 'boolean'],
            'isDeconvolved' => ['nullable', 'boolean'],
            'cdpOffset' => ['nullable', 'integer', 'min:0'],
            'cdpSize' => ['nullable', 'integer', 'min:1'],
            'filePath' => ['nullable', 'max:200', 'regex:/^[\w\d]+(\/\.*[\w\d][-\(\)\w\d\.]*)*$/i',
                new \App\Rules\FileExists(DataSystem::getDataPath())],
        ];
    }

    public function scopeByAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        return $query->whereRaw('lower(alias) like (?)',["%$substring%"]);
    }

    public function scopeByNameOrAliasSubstr($query, $substring = null)
    {
        if(is_null($substring)) return $query;
        $substring = mb_strtolower($substring);
        $query->whereRaw('lower(name) like (?)',["%$substring%"])->orWhereRaw('lower(alias) like (?)',["%$substring%"]);
        return $query;
    }
}
