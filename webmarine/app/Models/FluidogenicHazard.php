<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;
use App\Traits\NameSearchTrait;

class FluidogenicHazard extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::FLUIDOGENIC_HAZARD_TABLE;

    const FLOOR_DIVISION = ['шельф', 'бровка шельфа', 'материковый склон', 'подножие материкового склона', 'абисальная равнина'];

    public function type(){
        return $this->belongsTo(\App\Models\HazardType::class, FK::get(\App\Tables::HAZARD_TYPE_TABLE));
    }

    public static function findByType($type){
        return static::query()->byType($type);
    }

    public function scopeByType($query, $type = null)
    {
        if(is_null($type)) return $query;
        return $query->where(FK::get(\App\Tables::HAZARD_TYPE_TABLE), $type);
    }
}
