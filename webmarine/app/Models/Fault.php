<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Helpers\FK;

class Fault extends Model
{
    use HasFactory;

    protected $table = \App\Tables::FAULT_TABLE;

    public static function findByName($name){
        return static::query()->whereRaw('lower(name) like (?)',["%$name%"]);
    }

    public function type(){
        return $this->belongsTo(\App\Models\FaultType::class, FK::get(\App\Tables::FAULT_TYPE_TABLE));
    }

    public function group(){
        return $this->belongsTo(\App\Models\FaultGroup::class, FK::get(\App\Tables::FAULT_GROUP_TABLE));
    }

    public function activity(){
        return $this->belongsTo(\App\Models\FaultAticivityDegree::class, FK::get(\App\Tables::FAULT_ACITIVITY_DEGREE_TABLE));
    }

    public function risk(){
        return $this->belongsTo(\App\Models\FaultRiskDegree::class, FK::get(\App\Tables::FAULT_RISK_DEGREE_TABLE));
    }

    public function stacks2d(){
        return $this->hasMany(\App\Models\Seismic2dFault::class, FK::get(\App\Tables::FAULT_TABLE));
    }
}
