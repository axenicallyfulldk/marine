<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\NameSearchTrait;

class SeismicSourceType extends Model
{
    use HasFactory;
    use NameSearchTrait;

    protected $table = \App\Tables::SEISMIC_SOURCE_TYPE_TABLE;

    public function sources(){
        return $this->hasMany(\App\Models\SeismicSource::class, FK::get(\App\Tables::SEISMIC_SOURCE_TYPE_TABLE));
    }
}
