<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\MorphPivot;

use App\Helpers\FK;

class Modelable extends MorphPivot
{
    use HasFactory;

    protected $table = \App\Tables::INTERPRETED_MODEL_OBJECT_TABLE;

    public function model(){
        return $this->belongsTo(InterpretedModel::class, FK::get(\App\Tables::INTERPRETED_MODEL_TABLE));
    }

    public function modelable(){
        return $this->morphTo(__FUNCTION__, 'object_type', 'object_id');
    }
}
