<?php

namespace App\Helpers;

/**
 * Parses polygon presented as string and outputs it in different formats
 */
class PolygonParser{
    /**
     * Parses polygon presented in format:
     * lat1 long1
     * lat2 long2
     * ...
     */
    public static function parse(string $str, bool $inverse = false, string $pointSepReg = "\n+", string $coordSepReg = "\s+"){
        $parser = new PolygonParser();
        $parser->data = [];
        $points = preg_split('/'.$pointSepReg.'/',trim($str));
        foreach($points as $point){
            $latLong = preg_split('/'.$coordSepReg.'/', trim($point));
            if(count($latLong) != 2) return false;
            if($inverse) $latlong = [$latLong[1],$latLong[0]];
            if(!is_numeric($latLong[0])) return false;
            if(!is_numeric($latLong[1])) return false;
            if(floatval($latLong[0]) < -90.0000000000001 || floatval($latLong[0]) > 90.0000000000001) return false;
            if(floatval($latLong[1]) < -180.0000000000001 || floatval($latLong[1]) > 180.0000000000001) return false;
            
            array_push($parser->data, [$latLong[0], $latLong[1]]);
        }
        if($parser->data[0] != end($parser->data)){
            array_push($parser->data, $parser->data[0]);
        }
        return $parser;
    }

    /**
     * Parses polygon written in csv format
     * lat1,long1
     * lat1,long2
     * ...
     */
    public static function parseCsv(string $str, bool $inverse = false, string $sepReg = ","){
        return static::parse($str, $inverse, "\n+", $sepReg);
    }

    /**
     * Returns polygon in EKWT format:
     * POLYGON((long1 lat1, long2 lat2, ...))
     */
    public function toEKWT(){
        $res='POLYGON((';
        foreach($this->data as $point){
            $res .= "$point[1] $point[0],";
        }
        $res = rtrim($res, ', ').'))';
        return $res;
    }

    /**
     * Returns polygon in simple format:
     * lat1 long1
     * lat2 long2
     * lat3 long3
     * ...
     */
    public function toSimple(){
        return implode("\n", array_map(function($point){return implode(" ", $point);}, $this->data));
    }

    // Array of points $data = [[lat1,long1],[lat2,long2],...]
    private $data;
};