<?php

namespace App\Helpers;


/**
 * Converts color presented as integer to hex string and backward.
 */
class ColorIntConverter{
    public static function intToStr(int $intColor){
        return "#".substr("00000000".dechex($intColor),-8);
    }

    public static function strToInt(string $strColor){
        return $strColor[0] == "#" ? hexdec(substr($strColor, 1)) : hexdec($strColor);
    }
};