<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class TransactionManager{
    // public __autoload

    public function __construct(){
        $this->isStarted = false;
    }

    public function __destruct(){
        if($this->isStarted){
            DB::rollBack();
        }
    }

    public function begin(){
        DB::beginTransaction();
        $this->isStarted = true;
    }

    public function rollBack(){
        DB::rollBack();
        $this->isStarted = false;
    }

    public function end(){
        DB::commit();
        $this->isStarted = false;
    }

    private bool $isStarted;
}