<?php

namespace App\Helpers;

/**
 * Formats wkt string.
 */
class WktStringFormatter{
    public static function formatWktString($wktString){
        $wktString = strtoupper(trim($wktString));
        if(\str_starts_with($wktString, 'POLYGON')) return static::formatPolygonWktString($wktString);
        if(\str_starts_with($wktString, 'MULTIPOLYGON')) return static::formatMultipolygonWktString($wktString);
        return $wktString;
    }

    public static function formatPolygonWktString($wktString){
        $wktString = trim($wktString);
        if(preg_match('/^POLYGON\s*\((.*)\)$/', $wktString, $matches)){
            return "POLYGON ".static::formatPolygonPointsString('('.$matches[1].')');
        }
        return $wktString;
    }

    public static function formatMultipolygonWktString($wktString){
        $wktString = trim($wktString);

        if(preg_match('/^MULTIPOLYGON\s*\((.*)\)$/', $wktString, $matches)){
            $polygonRegex = '/(\(\s*\(([^\)]+)\)\s*\)|\(\s*\(([^\)]+)\)\s*,\s*\(([^\)]+)\)\s*\))/';
            $formattedPolygons = [];
            if(\preg_match_all($polygonRegex, $matches[1], $pmatches, PREG_SET_ORDER)){
                foreach($pmatches as $match){
                    array_push($formattedPolygons, static::formatPolygonPointsString($match[1]));
                }
                return 'MULTIPOLYGON ('.implode(',', $formattedPolygons).')';
            }
            // preg_match_all
            return static::formatPolygonPointsString('('.$matches[1].')');
        }
        return $wktString;
    }

    public static function formatPolygonPointsString($wktString){
        $simplePolygonRegex = '/^\(\s*\(([^\)]+)\)\s*\)$/';
        $excludePolygonRegex = '/^\(\s*\(([^\)]+)\),\s+\(([^\)]+)\)\s*\)$/';
        if(preg_match($simplePolygonRegex, $wktString, $matches)){
            $pointsStr = $matches[1];
            $pointStrArr = static::enclosePolygon($pointsStr);
            if(count($pointStrArr)<3) return $wktString;
            return "((".\implode(",", $pointStrArr)."))";
        } else if(preg_match($excludePolygonRegex, $wktString, $matches)){
            $pointStrArr = static::enclosePolygon($matches[1][0]);
            if(count($pointStrArr)<3) return $wktString;

            $pointExclStrArr = static::enclosePolygon($matches[2][0]);
            if(count($pointExclStrArr)<3) return $wktString;

            return sprintf("((%s),(%s))", implode(",", $pointStrArr), implode(",", $pointExclStrArr));
        }
        return $wktString;
    }

    // private static function

    private static function enclosePolygon($pointsStr){
        $pointStrArr = array_map(fn($v) => trim($v), preg_split('/,/', $pointsStr));
        if($pointStrArr[0] != end($pointStrArr)){
            array_push($pointStrArr,$pointStrArr[0]);
        }
        return $pointStrArr;
    }
}