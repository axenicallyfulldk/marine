<?php

namespace App\Helpers;

/**
 * Formats wkt string.
 */
class WktValidator{

    public function isValidString($wktString){
        $wktString = strtoupper(trim($wktString));
        $this->message = "";
        if(\str_starts_with($wktString, 'POLYGON')) return static::isValidPolygonWktString($wktString, $this->message);
        if(\str_starts_with($wktString, 'MULTIPOLYGON')) return static::isValidMultipolygonWktString($wktString, $this->message);
        return true;
    }

    public function getMessage(){
        return $this->message;
    }

    public static function isValidPolygonWktString($wktString, &$message = ""){
        $wktString = trim($wktString);
        if(preg_match('/^POLYGON\s*\((.*)\)$/', $wktString, $matches)){
            return static::isValidPolygonPointsString('('.$matches[1].')', $message);
        } else {
            $message .= "multipolygon format is incorrect!";
            return false;
        }
    }

    public static function isValidMultipolygonWktString($wktString, &$message = ""){
        $wktString = trim($wktString);

        if(preg_match('/^MULTIPOLYGON\s*\((.*)\)$/', $wktString, $matches)){
            $polygonRegex = '/(\(\s*\(([^\)]+)\)\s*\)|\(\s*\(([^\)]+)\)\s*,\s*\(([^\)]+)\)\s*\))/';
            $formattedPolygons = [];
            if(\preg_match_all($polygonRegex, $matches[1], $pmatches, PREG_SET_ORDER)){
                foreach($pmatches as $match){
                    array_push($formattedPolygons, static::isValidPolygonPointsString($match[1], $message));
                }
                return 'MULTIPOLYGON ('.implode(',', $formattedPolygons).')';
            }
            // preg_match_all
            return static::isValidPolygonPointsString('('.$matches[1].')', $message);
        }
        $message .= "multipolygon format is incorrect!";
        return false;
    }

    public static function isValidPolygonPointsString($wktString, &$message = ""){
        $simplePolygonRegex = '/^\(\s*\(([^\)]+)\)\s*\)$/';
        $excludePolygonRegex = '/^\(\s*\(([^\)]+)\),\s+\(([^\)]+)\)\s*\)$/';
        if(preg_match($simplePolygonRegex, $wktString, $matches)){
            $pointsStr = $matches[1];
            return static::isPointListCorrect($pointsStr, $message);
        } else if(preg_match($excludePolygonRegex, $wktString, $matches)){
            if(static::isPointListCorrect($matches[1][0], $message) == false)
                return false;

            return static::isPointListCorrect($matches[2][0], $message);
        }
        $message .= "multipolygon format is incorrect!";
        return false;
    }

    private static function isPointListCorrect($pointsStr, &$message = ""){
        $pointStrArr = array_map(fn($v) => trim($v), preg_split('/,/', $pointsStr));
        if($pointStrArr[0] != end($pointStrArr)){
            $message .= "polygon has to be enclosed!";
            return false;
        }
        foreach($pointStrArr as $pointStr){
            if(count(explode(" ", trim($pointStr))) != 2){
                $message .= "Point: ($pointStr) has incorrect format!";
                return false;
            }   
        }
        return true;
    }
}