<?php

namespace App\Helpers;

/**
 * Class for working with segy files. Allows reading segy header and trace headers.
 */
class SegyFile {
    function __construct(string $filePath, bool $isLittleEndian = false) {
        $this->isLittleEndian = $isLittleEndian;

        $this->$filePath = $filePath;
        $this->segyFile = fopen($filePath, "rb");

        // Get segy size
        {
            fseek($this->segyFile, 0, SEEK_END);
            $this->fileLength = ftell($this->segyFile);
        }

        // Get number of samples per trace and sample size
        {
            fseek($this->segyFile, static::ebcidicHeaderSize + static::samplesFieldOffset, SEEK_SET);
            $format = static::uintSizeToFormat["2".$this->isLittleEndian];
            $binData = fread($this->segyFile, 2);
            $this->sampleCount = array_values(unpack($format, $binData))[0];

            fseek($this->segyFile, static::ebcidicHeaderSize + static::sampleSizeFileOffset, SEEK_SET);
            $format = static::uintSizeToFormat["2".$this->isLittleEndian];
            $binData = fread($this->segyFile, 2);
            $this->sampleSize = static::uintSampleFormatToSize[array_values(unpack($format, $binData))[0]];
        }

        // Get trace count
        $this->traceCount = ($this->fileLength - static::ebcidicHeaderSize - static::segyHeaderSize)/
            ($this->sampleSize*$this->sampleCount + static::traceHeaderSize);
    }

    function __destruct(){
        fclose($this->segyFile);
    }

    function getTraceCount(){
        return $this->traceCount;
    }

    function getUintHeaderFieldValue(int $offset, int $size){
        fseek($this->segyFile, static::ebcidicHeaderSize + $offset, SEEK_SET);
        $format = static::uintSizeToFormat["2".$this->isLittleEndian];
        $binData = fread($this->segyFile, $size);
        return array_values(unpack($format, $binData))[0];
    }

    function getUintTraceHeaderFieldValues(int $offset, int $size, int $traceOffset = 0, int $traceLimit = PHP_INT_MAX){
        $vals = array();
        $shift = static::traceHeaderSize + $this->sampleSize*$this->sampleCount;
        $fieldOffset = static::ebcidicHeaderSize + static::segyHeaderSize + ($offset - 1) + $traceOffset*$shift;        
        $i = 0;
        while($fieldOffset < $this->fileLength && $i < $traceLimit){
            fseek($this->segyFile, $fieldOffset, SEEK_SET);
            $format = static::uintSizeToFormat[$size.$this->isLittleEndian];
            $binData = fread($this->segyFile, $size);
            $fieldVal = array_values(unpack($format, $binData))[0];
            $vals[$i]=$fieldVal;
            $fieldOffset += $shift;
            $i += 1;
        }
        return $vals;
    }

    private $segyFile;
    private $isLittleEndian;
    private $traceCount;
    private $sampleCount;
    private $sampleSize;

    private $filePath;
    private $fileLength;

    const ebcidicHeaderSize = 3200;
    const segyHeaderSize = 400;
    const traceHeaderSize = 240;
    const samplesFieldOffset = 20;
    const sampleSizeFileOffset = 24;

    const uintSizeToFormat = [1 => 'c', 2 => 'n', 21 => 'v', 4 => 'N', 41 => 'V', 8 => 'J', 81 => 'P'];
    const uintSampleFormatToSize = [1 => 4, 2 => 4, 3 => 2, 4 => 4, 5 => 4, 6 => 8, 7 => 3, 8 => 1, 9 => 8, 10 => 4, 11 => 2, 12 => 8, 15 => 3, 16 => 1];

};