<?php

namespace App\Helpers;

class FolderSynchronizer {
    public static function copy(string $from, string $to){
        $output=null;
        $retval=null;
        $cmd = sprintf("rsync -auv \"%s/\" \"%s\"", $from, $to);
        exec($cmd, $output, $retval);
        if($retval != 0){
            $outputStr = is_array($output) ? implode("\n", $output) : $output;
            throw new \Exception(sprintf("Failed to copy files: %s", $outputStr));
        }
    }
};