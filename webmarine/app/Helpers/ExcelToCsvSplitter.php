<?php

namespace App\Helpers;

/**
 * Splits given excel file to multiple csv files
 */
class ExcelToCsvSplitter{
    public function __construct(string $excelFileName, string $destFolder){
        $this->excelFileName = $excelFileName;
        $this->destFolder = $destFolder;

        $splitter = static::joinPaths(dirname(__FILE__), "excel_splitter.py");
        $output=null;
        $retval=null;
        $cmd = sprintf("python3 \"%s\" -e \"%s\" -o \"%s\"", $splitter, $excelFileName, $destFolder);
        exec($cmd, $output, $retval);
        if($retval != 0){
            throw new \Exception("Failed to split excel file");
        }

        $this->csvFiles = $output;
    }

    public function getCsvFileNames(){
        return $this->csvFiles;
    }

    public function getCsvBySheetName($sheetName){
        return static::joinPaths($this->destFolder, $sheetName.".csv");
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }

    private $excelFileName;
    private $destFolder;
    private $csvFiles;
}