<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

/**
 * Provides path for different directories that contains different files.
 */
class DataSystem{
    public static function getDataPath(...$subPath){
        $dd = static::getRelDataPath(...$subPath);
        return Storage::disk('data')->path($dd);
    }

    public static function fileExistsInData(...$subPath){
        return Storage::disk('data')->exists(static::getRelDataPath(...$subPath));
    }

    public static function getRelDataPath(...$subPath){
        if(is_null($subPath)){
            return \Config::get('app.data_folder');
        }
        
        return static::joinPaths(\Config::get('app.data_folder'), ...$subPath);
    }

    public static function downloadFromData($filePath, $name, $headers){
        $dataPath = static::getRelDataPath($filePath);
        return Storage::disk('data')->download($dataPath, $name, $headers);
    }

    public static function downloadFromDataByteRange($filename, $name, $headers){
        $relFilepath = static::getRelDataPath($filename);
        $filepath = static::getDataPath($filename);

        $size = Storage::disk('data')->size($relFilepath);
        $stream = fopen($filepath, "r");
        
        $start = 0;
        $length = $size;
        $status = 200;
        
        // $headers['Content-Length'] = $size;
        $headers['Accept-Ranges'] = 'bytes';

        if(!is_null($name)){
            $headers['Content-Disposition'] = sprintf('attachment; filename="%s"', $name);
        }
            
        if (false !== $range = \Request::server('HTTP_RANGE', false)) {
            list($param, $range) = explode('=', $range);
            if (strtolower(trim($param)) !== 'bytes') {
                header('HTTP/1.1 400 Invalid Request');
                exit;
            }
            list($from, $to) = explode('-', $range);
            if ($from === '') {
                $end = $size - 1;
                $start = $end - intval($from);
            } elseif ($to === '') {
                $start = intval($from);
                $end = $size - 1;
            } else {
                $start = intval($from);
                $end = intval($to);
            }
            $length = $end - $start + 1;
            $status = 206;
            $headers['Content-Range'] = sprintf('bytes %d-%d/%d', $start, $end, $size);
        }
        
        return \Response::stream(
            function() use ($stream, $start, $length) {
                fseek($stream, $start, SEEK_SET);
                echo fread($stream, $length);
                fclose($stream);
            },
            $status,
            $headers);
    }

    public static function getTrashPath(...$subPath){
        $dd = static::getRelTrashPath(...$subPath);
        return Storage::disk('data')->path($dd);
    }

    public static function getRelTrashPath(...$subPath){
        if(is_null($subPath)){
            return Storage::disk('data')->path(\Config::get('app.trash_data_folder'));
        }
        $dd = static::joinPaths(\Config::get('app.trash_data_folder'), ...$subPath);
        return $dd;
    }

    public static function fileExistsInTrash(...$subPath){
        return Storage::disk('data')->exists(DataSyste::getRelTrashPath($subPath));
    }

    public static function getPath(...$subPath){
        $dd = static::joinPaths(...$subPath);
        return Storage::disk('data')->path($dd);
    }

    private static function joinPaths(...$paths) {
        $fpaths = array_filter($paths, function($el){return $el != null;});
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $fpaths));
    }
}