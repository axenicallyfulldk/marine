<?php

namespace App\Helpers;

/**
 * Class that helps to work with code system.
 * Code usually contains next parts:
 * Z[/SZ1/SZ2]/LEVEL/YEAR/TYPE
 * Z - root zone (usually ocean, for example N - arctic ocean)
 * SZ1, SZ2 - subzones
 * LEVEL - level of entity (for exapmple L1 - surveys, L2 - mosaics and stacks)
 * YEAR - year when entity was created
 * TYPE - type of entity (SSS - side scan sonar, mbes - multibeam echo sounder)
 * 
 * Example of code:
 * N-W-BK-SVS-L1-2016-SSS
 * N - Arctic ocean
 * W - White sea
 * BK - Kandalaksha bay
 * SVS - Velikay salma strait
 * L1 - level
 * 2016 - year
 * SSS - type
 */
class CodeHelper{
    function __construct(string $code, string $sep = "-"){
        $this->sep = $sep;
        $this->regEx = sprintf("/^(?P<zone>([a-zA-Z]+%s)+)((?P<level>L\d)(%s(?P<year>\d+)(%s(?P<type>\w+))?)?)?/", $sep, $sep, $sep);
        $this->matches = [];
        preg_match($this->regEx, $code, $this->matches);
    }

    /**
     * Returns array of zones
     */
    public function getZones(){
        return preg_split("/".$this->sep."/", trim(@$this->matches["zone"], $this->sep));
    }

    /**
     * Returns level of entity
     */
    public function getLevel(){
        return @$this->matches["level"];
    }

    /**
     * Returns year
     */
    public function getYear(){
        return @$this->matches["year"];
    }    

    /**
     * Return entity type
     */
    public function getType(){
        return @$this->matches["type"];
    }
    

    private $sep;
    private $regEx;
    private $matches;
}