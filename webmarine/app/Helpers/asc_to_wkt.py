import enum, re, os, argparse, codecs
from pyproj import Proj

# python3 script.py -z 54N -e cp1251

parser = argparse.ArgumentParser(description='Converts ASC shapes to WKT')
parser.add_argument('-z', required=True, type=str, help='UTM zone')
parser.add_argument('-e', required=False, type=str, default='utf-8', help='ASC file encoding')
parser.add_argument('-f', required=True, type=str, help='Input ASC file')
parser.add_argument('-o', required=False, type=str, default='', help='Outpu WKT file')

args = parser.parse_args()

class ParsingState(enum.Enum):
    WaitingHeader = 0
    WaitingFirstPoint = 1
    WaitingSecondPoint = 2
    WaitingPointOrHeader = 3

class ShapeType(enum.Enum):
	Polygon = 0 # POLYGON
	Line = 1 # LINE
	MultiPolygon = 2
	MultiLine = 3
	Unknown = 4

class LineType(enum.Enum):
	ShapeHeader = 0
	Point = 1
	Unknown = 2

def getLineType(lineStr):
	if re.match(r"(POLYGON|LINE).*", lineStr):
		return LineType.ShapeHeader
	elif re.match(r"XY,[^\,]*,[^\,]*", lineStr):
		return LineType.Point

	return LineType.Unknown

def getShapeTypeFromLine(shapeLine):
	if shapeLine.startswith("LINE"):
		return ShapeType.Line
	elif shapeLine.startswith("POLYGON"):
		return ShapeType.Polygon

	return ShapeType.Unknown

def extracePointFromLine(pointLine):
	res = re.match(r"XY,([^\,]*),([^\,]*)", pointLine)
	xStr = res.groups()[0]
	yStr = res.groups(2)[1]
	return [float(xStr), float(yStr)]

def convertToGeoCoords(point, utmZone):
	myProj = Proj("+proj=utm +zone={}, +ellps=WGS84 +datum=WGS84 +units=m +no_defs".format(utmZone))
	lon, lat = myProj(point[0], point[1], inverse = True)
	return [lon, lat]

class Shape:
	def __init__(self, shapeType, points):
		self.shapeType = shapeType
		self.points = points

def shapeToWkt(shape):
	if shape.shapeType == ShapeType.Polygon:
		# Enclose polygon if it is not enclosed
		if shape.points[0] != shape.points[-1]:
			pointsStr = ",".join([" ".join([str(c) for c in point]) for point in shape.points])
		return "POLYGON (({}))".format(pointsStr)
	elif shape.shapeType == ShapeType.Line:
		pointsStr = ",".join([" ".join([str(c) for c in point]) for point in shape.points])
		return "LINESTRING ({})".format(pointsStr)
	elif shape.shapeType == ShapeType.MultiPolygon:
		pointsStr = ""
		for polygon in shape.points:
			# Enclose polygon if it is not enclosed
			if polygon[0] != polygon[-1]:
				polygon.append(polygon[0])
			pointsStr += ",(("+",".join([" ".join([str(c) for c in p]) for p in polygon])+"))"
		return "MULTIPOLYGON (" + pointsStr[1:] + ")"
	elif shape.shapeType == ShapeType.MultiLine:
		pointsStr = ""
		for line in shape.points:
			pointsStr += ",("+",".join([" ".join([str(c) for c in p]) for p in line])+")"
		return "MULTILINESTRING (" + pointsStr[1:] + ")"
	return ""

def writeShapesToWkt(shapes):
	shape = shapes[0]
	if len(shapes) > 1:
		if shape.shapeType == ShapeType.Polygon:
			shape = Shape(ShapeType.MultiPolygon, [s.points for s in shapes])
		elif shape.shapeType == ShapeType.Line:
			shape = Shape(ShapeType.MultiLine, [s.points for s in shapes])

	return shapeToWkt(shape)


state = ParsingState.WaitingHeader
curShapeType = ShapeType.Unknown
curPoints = []

def changeAscExtToWkt(filename):
	if filename.endswith(".ASC") or filename.endswith(".asc"):
		return filename[0:-4] + ".wkt"
	return filename + ".wkt"

inputFilename = args.f
outputFilename = args.o if args.o != '' else changeAscExtToWkt(args.f)

shapes = []

state = ParsingState.WaitingHeader
curShapeType = ShapeType.Unknown
curPoints = []

lineIndex = -1

for line in codecs.open(inputFilename, 'r', args.e).readlines():
	lineIndex += 1

	line = line.upper()

	lineType = getLineType(line)

	lineState = state

	if lineState == ParsingState.WaitingHeader:
		if lineType != LineType.ShapeHeader:
			continue

		curPoints = []
		curShapeType = getShapeTypeFromLine(line)
		if curShapeType == ShapeType.Unknown:
			state = ParsingState.WaitingHeader
			print("WARNING: {} is unknown type of shape (line {})".format(line[:line.find(",")], lineIndex))
			continue

		state = ParsingState.WaitingFirstPoint

	elif lineState in (ParsingState.WaitingFirstPoint, ParsingState.WaitingSecondPoint):
		if lineType == LineType.Unknown:
			continue

		if lineType == LineType.ShapeHeader:
			state = ParsingState.WaitingFirstPoint
			curPoints = []
			curShapeType = getShapeTypeFromLine(line)

			print("WARNING: Shape before line {} doesn't contain enough points so it will be skipped".format(lineIndex))

			if curShapeType == ShapeType.Unknown:
				state = ParsingState.WaitingHeader
				print("WARNING: {} is unknown type of shape (line {})".format(line[:line.find(",")], lineIndex))
				continue
			continue

		# new point case
		state = ParsingState.WaitingSecondPoint if state == ParsingState.WaitingFirstPoint else ParsingState.WaitingPointOrHeader
		point = extracePointFromLine(line)
		if args.z:
			point = convertToGeoCoords(point, args.z)
		curPoints.append(point)

	elif lineState == ParsingState.WaitingPointOrHeader:
		if lineType == LineType.Unknown:
			continue
		
		if lineType == LineType.ShapeHeader:
			shapes.append(Shape(curShapeType, curPoints))

			state = ParsingState.WaitingFirstPoint
			curPoints = []
			curShapeType = getShapeTypeFromLine(line)
			
			if curShapeType == ShapeType.Unknown:
				state = ParsingState.WaitingHeader
				print("WARNING: {} is unknown type of shape (line {})".format(line[:line.find(",")], lineIndex))
				continue

			continue

		# new point case
		point = extracePointFromLine(line)
		if args.z:
			point = convertToGeoCoords(point, args.z)
		curPoints.append(point)

if state in (ParsingState.WaitingFirstPoint, ParsingState.WaitingSecondPoint):
	print("WARNING: Shape before line {} doesn't contain enough points so it will be skipped".format(lineIndex))
elif state == ParsingState.WaitingPointOrHeader:
	shapes.append(Shape(curShapeType, curPoints))

outputWktString = writeShapesToWkt(shapes)

open(outputFilename, "w").write(outputWktString)
