<?php

namespace App\Helpers;

/**
 * Returns typical foreign key for given table
 */
class FK{
    public static function get(string $table){
        return $table.'_id';
    }
}