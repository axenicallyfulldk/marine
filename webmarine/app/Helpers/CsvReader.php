<?php

namespace App\Helpers;

/**
 * Reads csv file and returns arrays of rows.
 * First row is used as header.
 * 
 * Example:
 * 
 * col1Name,col2Name,col3Name
 * a,b,c
 * 1,2,3
 *         ↓
 * [
 *  ['col1Name' => 'a','col2Name' => 'b', 'col3Name' => 'c'],
 *  ['col1Name' => 1,'col2Name' => 2, 'col3Name' => 3],
 * ]
 */
class CsvReader{
    public static function read(string $csvFile, string $sep = ";"){
        $row = 0;
        $res = [];
        if (($handle = fopen($csvFile, "r")) !== FALSE) {
            $header = fgetcsv($handle, 100000, $sep);
            // Lower header and trim spaces and *.
            for($i = 0; $i < count($header) ; ++$i){
                $header[$i] = trim(mb_strtolower($header[$i]), " \n\r\t\v\0*");
            }
            while (($data = fgetcsv($handle, 100000, $sep)) !== FALSE) {
                $num = count($data);
                $res[$row] = [];
                for ($c=0; $c < $num; $c++) {
                    $res[$row][mb_strtolower($header[$c])] = $data[$c];
                }
                $row++;
            }
            fclose($handle);
        }
        return $res;
    }
}