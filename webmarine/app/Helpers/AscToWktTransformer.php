<?php

namespace App\Helpers;


class AscToWktTransformer{
    public function __construct($fileEncoding = "utf-8", $utmZone = "55N")
    {
        $this->fileEncoding = $fileEncoding;
        $this->utmZone = $utmZone;
        $this->script = static::joinPaths(dirname(__FILE__), "asc_to_wkt.py");
    }

    public function convertFile($filename, $utmZone = null, $fileEncoding = null){
        if(is_null($utmZone)) $utmZone = $this->utmZone;
        if(is_null($fileEncoding)) $fileEncoding = $this->fileEncoding;

        $output=null;
        $retval=null;
        $cmd = sprintf("python3 \"%s\" -f \"%s\" -z \"%s\" -e \"%s\"", $this->script, $filename, $utmZone, $fileEncoding);
        exec($cmd, $output, $retval);
        if($retval != 0){
            throw new \Exception("Failed to split excel file");
        }

        return $output[0];
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }
}