import openpyxl
import csv
import os
import argparse
import pathlib


def writeSheetToCsv(sheet, outCsvFileName):
    with open(outCsvFileName, 'w') as f:
        c = csv.writer(f)
        for r in sheet.rows:
            c.writerow([cell.value for cell in r])

def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return pathlib.Path(arg)

parser = argparse.ArgumentParser(description='Split excel files on csv files')
parser.add_argument('-e', required=True, metavar="FILE", type=lambda x: is_valid_file(parser, x), help="path to excel file")
parser.add_argument('-o', required=True, metavar="FOLDER", type = pathlib.Path, help='path to folder where csv files will be saved')

args = parser.parse_args()

outFolder = args.o
workbook = openpyxl.load_workbook(filename=args.e)

sheets = set(workbook.sheetnames)

for sheet in sheets:
    print("{}.csv".format(sheet))
    outCsv = outFolder / "{}.csv".format(sheet)
    writeSheetToCsv(workbook[sheet], outCsv)
