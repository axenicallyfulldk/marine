<?php

namespace App\Services;

use App\Models\User;
use App\Models\Group;

use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpKernel\Exception\HttpException;

class UserService {
    public function add($req){
        // Check rights
        if(!\Auth::user()->isAdmin()){
            throw new HttpException(403, "You have no right to create users.");
        }
        
        // Validate
        Validator::make($req, User::getStoreRules())->validate();
        
        // ['name' => 'nav', 'email' => 'nav@marine.marine', 'password' => Hash::make('nav'), 'group_id' => $adminGroupId]
        $user = User::create([
            'name' => @$req['name'],
            'email' => @$req['email'],
            'password' => \Hash::make(@$req['password']),
            // 'group_id' => Group::get(@$req['role'])->id
        ]);
        
        $user->group()->associate(Group::get(@$req['role']));
        $user->save();
        return $user;
    }

    public function patch($req, Vessel $user){
    }

    public function destroy(User $user){
    }
}