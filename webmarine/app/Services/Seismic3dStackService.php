<?php

namespace App\Services;

use App\Models\{Seismic3dSurvey, Seismic3dStack, Seismic3dSurveyArchFile, Trip};

use App\Helpers\PolygonParser;
use App\Helpers\DataSystem;
use App\Helpers\SegyFile;
use App\Helpers\LatLongDistance;

use Illuminate\Support\Facades\Validator;

class Seismic3dStackService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editSeismic3dSurveysByDefault()){
            throw new HttpException(403, "You have no right to create stacks.");
        }

        // Validate
        Validator::make($req, Seismic3dStack::getStoreRules($req))->validate();

        $setIfNotNull = function($srcField, $dstField, &$obj) use($req) {
            if(array_key_exists($srcField, $req)) $obj->{$dstField} = $req[$srcField];
        };

        $stack = new Seismic3dStack();
        $stack->name = $req['name'];
        $setIfNotNull('alias', 'alias', $stack);

        $stack->survey()->associate($req['survey']);

        $stack->file_path = $req['filePath'];
        $stack->file_size = filesize(DataSystem::getDataPath($req['filePath']));

        $setIfNotNull('isMigrated', 'is_migrated', $stack);
        $setIfNotNull('isAmplCorrected', 'is_ampl_corrected', $stack);
        $setIfNotNull('isDeconvolved', 'is_deconvolved', $stack);

        $setIfNotNull('inlineOffset', 'inline_offset', $stack);
        $setIfNotNull('inlineSize', 'inline_size', $stack);

        $setIfNotNull('xlineOffset', 'xline_offset', $stack);
        $setIfNotNull('xlineSize', 'xline_size', $stack);

        $segyFilePath = DataSystem::getDataPath($stack->file_path);
        $segyFile = new SegyFile($segyFilePath, false);
        $lineInfo = static::getInlineXlineInfo($segyFile, $stack->inline_offset, $stack->inline_size, $stack->xline_offset, $stack->xline_size);
        $stack->inline_distance = $lineInfo['inlineDist'];
        $stack->xline_distance = $lineInfo['xlineDist'];
        
        $stack->save();
        
        return $stack;
    }

    public function destroy(Seismic3dStack $stack){
        
    }

    public function patch($req, Seismic3dStack $stack){
        // Check rights
        if(!$stack->isEditable()){
            throw new HttpException(403, "You have no right to update this stack.");
        }

        // Validate
        Validator::make($req, Seismic3dStack::getPatchRules($req))->validate();

        $setField = function(string $key, string $field) use($req, $stack){
            if(array_key_exists($key, $req))
                $stack->{$field} = $req[$key];
        };
        $assocField = function(string $key, string $field) use($req, $stack){
            if(array_key_exists($key, $req)) $stack->{$field}()->associate($req[$key]);
        };

        // Check rights
        if(!\Auth::user()->editSeismic3dSurveysByDefault()){
            throw new HttpException(403, "You have no right to create stacks.");
        }

        $setField('name', 'name');
        $setField('alias', 'alias');

        $setField('filePath', 'file_path');
        $stack->file_size = filesize(DataSystem::getDataPath($stack->file_path));

        $setField('isMigrated', 'is_migrated');
        $setField('isAmplCorrected', 'is_ampl_corrected');
        $setField('isDeconvolved', 'is_deconvolved');

        $setField('inlineOffset', 'inline_offset');
        $setField('inlineSize', 'inline_size');

        $setField('xlineOffset', 'xline_offset');
        $setField('xlineSize', 'xline_size');

        if(isset($req['filePath']) || isset($req['inlineOffset']) || isset($req['inlineSize']) ||
                isset($req['xlineOffset']) || isset($req['xlineSize'])){

            $segyFilePath = DataSystem::getDataPath($stack->file_path);
            $segyFile = new SegyFile($segyFilePath, false);
            $lineInfo = static::getInlineXlineInfo($segyFile, $stack->inline_offset, $stack->inline_size, $stack->xline_offset, $stack->xline_size);
            $stack->inline_distance = $lineInfo['inlineDist'];
            $stack->xline_distance = $lineInfo['xlineDist'];
        }
        
        $stack->save();
        
        return $stack;
    }

    private static function pointDist($p1, $p2){
        return sqrt(($p1[0]-$p2[0])*($p1[0]-$p2[0]) + ($p1[1]-$p2[1])*($p1[1]-$p2[1]));
    }

    private static function getLineDist($inlines, $xlines, $xy){
        $inlineStat = [];
        $xlineStat = [];
        foreach(array_map(null, $inlines, $xlines, $xy) as $ixxy){
            if(!isset($inlineStat[$ixxy[0]])){
                $inlineStat[$ixxy[0]] = [[$ixxy[1], $ixxy[2]]];
            }else{
                array_push($inlineStat[$ixxy[0]], [$ixxy[1], $ixxy[2]]);
            }

            if(!isset($xlineStat[$ixxy[1]])){
                $xlineStat[$ixxy[1]] = [[$ixxy[0], $ixxy[2]]];
            }else{
                array_push($xlineStat[$ixxy[1]], [$ixxy[0], $ixxy[2]]);
            }
        }

        $cmp = function($v1, $v2){
            if($v1 > $v2) return 1;
            if($v2 > $v1) return -1;
            return 0;
        };

        foreach($inlineStat as $v){
            usort($v, $cmp);
        }

        foreach($xlineStat as $v){
            usort($v, $cmp);
        }

        $diff = function($v1, $v2){
            return static::pointDist($v1[1], $v2[1]);
        };

        foreach($inlineStat as $k => $v){
            $inlineStat[$k] = static::arr_diff($v, $diff);
        }

        foreach($xlineStat as $k => $v){
            $xlineStat[$k] = static::arr_diff($v, $diff);
        }

        $minInlineDist = PHP_FLOAT_MAX;
        $minXlineDist = PHP_FLOAT_MAX;
        
        foreach($inlineStat as $k => $v){
            $minInlineDist = min($minInlineDist, min($v));
        }

        foreach($xlineStat as $k => $v){
            if(count($v)) $minXlineDist = min($minXlineDist, min($v));
        }

        return [$minXlineDist, $minInlineDist];
    }

    private static function getInlineXlineInfo($segyFile, int $inlineOffset, int $inlineSize, int $xlineOffset, int $xlineSize){
        $pointsNum = 2000;
        $traceOffset = max(0, $segyFile->getTraceCount() - $pointsNum);

        $inlines1 = $segyFile->getUintTraceHeaderFieldValues($inlineOffset, $inlineSize, 0, $pointsNum);
        $inlines2 = $segyFile->getUintTraceHeaderFieldValues($inlineOffset, $inlineSize, $traceOffset, $pointsNum);
        $inlines = array_merge($inlines1,$inlines2);

        $xlines1 = $segyFile->getUintTraceHeaderFieldValues($xlineOffset, $xlineSize, 0, $pointsNum);
        $xlines2 = $segyFile->getUintTraceHeaderFieldValues($xlineOffset, $xlineSize, $traceOffset, $pointsNum);
        $xlines = array_merge($xlines1,$xlines2);

        $x1 = $segyFile->getUintTraceHeaderFieldValues(73, 4, 0, $pointsNum);
        $x2 = $segyFile->getUintTraceHeaderFieldValues(73, 4, $traceOffset, $pointsNum);

        $y1 = $segyFile->getUintTraceHeaderFieldValues(77, 4, 0, $pointsNum);
        $y2 = $segyFile->getUintTraceHeaderFieldValues(73, 4, $traceOffset, $pointsNum);

        $xy1 = array_map(null, $x1, $y1);
        $xy2 = array_map(null, $x2, $y2);
        $xy = array_merge($xy1, $xy2);

        $inlineMin = min(min($inlines1), min($inlines2));
        $inlineMax = max(max($inlines1), max($inlines2));
        $xlineMin = min(min($xlines1), min($xlines2));
        $xlineMax = max(max($xlines1), max($xlines2));

        $uinlines = array_unique($inlines);
        asort($uinlines);
        $inlineStep = min(static::arr_diff($uinlines));

        $uxlines = array_unique($xlines);
        asort($uxlines);
        $xlineStep = min(static::arr_diff($uxlines));

        $lineDist = static::getLineDist($inlines, $xlines, $xy);

        return [
            'inlineMin' => $inlineMin,
            'inlineMax' => $inlineMax,
            'inlineStep' => $inlineStep,
            'inlineDist' => $lineDist[0],
            'xlineMin' => $xlineMin,
            'xlineMax' => $xlineMax,
            'xlineStep' => $xlineStep,
            'xlineDist' => $lineDist[1],
        ];
    }

    private static function arr_diff($arr, $func = null){
        $res = [];
        $prevEl = reset($arr);
        $el = next($arr);
        while($el){
            if(is_null($func)){
                array_push($res, $el-$prevEl);
            }else{
                array_push($res, $func($prevEl, $el));
            }
            
            $prevEl = $el;
            $el = next($arr);
        }
        return $res;
    }

    
}