<?php

namespace App\Services;

use App\Models\SeismicStreamer;

use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpKernel\Exception\HttpException;

class SeismicStreamerService {
    public function add($req){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to create seismic streamers.");
        }
        
        // Validate
        Validator::make($req, SeismicStreamer::getStoreRules())->validate();
        
        // $streamer = SeismicStreamer::create([
        //     'name' => @$req['name'],
        //     'receiver_dist' => @$req['receiverDist'],
        //     'length' => @$req['length'],
        // ]);

        $streamer  = new SeismicStreamer();
        $streamer->name = $req['name'];
        $streamer->receiver_dist = $req['receiverDist'];
        $streamer->length = $req['length'];
        $streamer->save();
        return $streamer;
    }

    public function patch($req, SeismicStreamer $streamer){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to update seismic streamers.");
        }

        // Validate
        Validator::make($req, SeismicStreamer::getPatchRules())->validate();

        $setField = function(string $key, string $fieldName)use($req, $streamer){
            if(array_key_exists($key, $req)) $streamer->{$fieldName} = $req[$key];
        };

        $setField("name", "name");
        $setField("receiverDist", "receiver_dist");
        $setField("length", "length");
    
        $streamer->save();
        return $streamer;
    }

    public function destroy(SeismicStreamer $streamer){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to delete seismic streamer.");
        }
        
        $streamer->delete();
    }
}