<?php

namespace App\Services;

use App\Models\Vessel;

use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpKernel\Exception\HttpException;

class VesselService {
    public function add($req){
        // Check rights
        if(!\Auth::user()->editVesselsByDefault()){
            throw new HttpException(403, "You have no right to create vessels.");
        }
        
        // Validate
        Validator::make($req, Vessel::getStoreRules())->validate();
        
        $vessel = Vessel::create([
            'name' => @$req['name'],
            'home_port' => @$req['homePort'],
            'shipowner' => @$req['shipowner'],
            'imo' => @$req['imo'],
            'mmsi' => @$req['mmsi'],
            'callsign' => @$req['callsign'],
            'length' => @$req['length'],
            'width' => @$req['width'],
            'drought' => @$req['drought'],
            'displacement' => @$req['displacement'],
            'speed_kn' => @$req['speedKn'],
            'build_date' => @$req['buildDate']
        ]);
        $vessel->save();
        return $vessel;
    }

    public function patch($req, Vessel $vessel){
        // Check rights
        if(!\Auth::user()->editVesselsByDefault()){
            throw new HttpException(403, "You have no right to edit vessels.");
        }

        // Validate
        Validator::make($req, Vessel::getPatchRules())->validate();

        $setField = function(string $key, string $field)use($req, $vessel){
            if(array_key_exists($key, $req)) $vessel->{$field} = $req[$key];
        };

        $setField("name", "name");
        $setField("homePort","home_port");
        $setField("shipowner","shipowner");
        $setField("imo","imo");
        $setField("mmsi","mmsi");
        $setField("callsign","callsign");
        $setField("length","length");
        $setField("width","width");
        $setField("drought","drought");
        $setField("displacement","displacement");
        $setField("speedKn","speed_kn");
        $setField("buildDate","build_date");
    
        $vessel->save();
        return $vessel;
    }

    public function destroy(Vessel $vessel){
        // Check rights
        if(!\Auth::user()->editVesselsByDefault()){
            throw new HttpException(403, "You have no right to delete vessels.");
        }

        $vessel->is_deleted = true;
        $vessel->save();
    }
}