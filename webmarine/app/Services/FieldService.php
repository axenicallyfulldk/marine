<?php

namespace App\Services;

use App\Models\Field;

use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Helpers\PolygonParser;

class FieldService {
    public function add($req){
        // Check rights
        if(!\Auth::user()->editZonesByDefault()){
            throw new HttpException(403, "You have no right to create fields.");
        }
        
        // Validate
        Validator::make($req, Field::getStoreRules())->validate();
        
        $field = new Field();
        $field->name = $req['name'];
        if(!empty($req['area'])){
            $field->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
        }
        $field->save();
        return $field;
    }

    public function patch($req, Field $field){
        // Check rights
        if(!\Auth::user()->editZonesByDefault()){
            throw new HttpException(403, "You have no right to update fields.");
        }

        // Validate
        Validator::make($req, Field::getPatchRules())->validate();

        $setField = function(string $key, string $fieldName)use($req, $field){
            if(array_key_exists($key, $req)) $field->{$fieldName} = $req[$key];
        };

        $setField("name", "name");
        $area = PolygonParser::parse($req['area']);
        if($area) $setField("area", \DB::raw("'".$area->toEKWT()."'"));
    
        $field->save();
        return $field;
    }

    public function destroy(Field $field){
        // Check rights
        if(!\Auth::user()->editZonesByDefault()){
            throw new HttpException(403, "You have no right to delete fields.");
        }
        
        $field->delete();
    }
}