<?php

namespace App\Services;

use App\Models\{Seismic2dSurvey, Seismic2dSurveyArchFile, Trip};

use App\Helpers\LineParser;
use App\Helpers\DataSystem;

use Illuminate\Support\Facades\Validator;

class Seismic2dSurveyService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editSeismic2dSurveysByDefault()){
            throw new HttpException(403, "You have no right to create surveys.");
        }

        // Validate
        Validator::make($req, Seismic2dSurvey::getStoreRules($req))->validate();

        $setIfNotNull = function($srcField, $dstField, &$obj) use($req) {
            if(array_key_exists($srcField, $req)) $obj->{$dstField} = $req[$srcField];
        };
        

        $trip = new Trip();
        $trip->vessel()->associate($req['vessel']);
        $trip->save();

        $survey = new Seismic2dSurvey();
        $survey->name = $req['name'];
        $setIfNotNull('alias', 'alias', $survey);
        $survey->area = \DB::raw("'".LineParser::parse($req['area'])->toEKWT()."'");
        $survey->direction = $req['direction'];
        $survey->start_date = $req['startDate'];

        if(isset($req['archPath'])){
            $survey->arch_path = $req['archPath'];
            $survey->arch_size = filesize(DataSystem::getDataPath($req['archPath']));
        }

        $survey->zone()->associate($req['parentZone']);
        $survey->field()->associate($req['field']);
        $survey->trip()->associate($trip);
        $survey->source()->associate($req['source']);
        $survey->src_group_count = $req['srcGroupCount'];
        $survey->src_per_group = $req['srcPerGroup'];
        $survey->streamer()->associate($req['streamer']);
        $survey->is_towed = $req['isTowed'];
        $survey->resolution()->associate($req['resolution']);
        $setIfNotNull('customer', 'customer', $survey);
        $setIfNotNull('operator', 'operator', $survey);
        
        $survey->is_deleted = false;
        
        $survey->save();

        if(isset($req['archFiles'])){
            foreach($req['archFiles'] as $file){
                $archFile = new Seismic2dSurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }
        
        return $survey;
    }

    public function patch($req, Seismic2dSurvey $survey){
        // Check rights
        if(!$survey->isEditable()){
            throw new HttpException(403, "You have no right to update this survey.");
        }

        // Validate
        Validator::make($req, Seismic2dSurvey::getPatchRules($req))->validate();

        $setField = function(string $key, string $field) use(&$req, &$survey){
            if(array_key_exists($key, $req)) $survey->{$field} = $req[$key];
        };
        $assocField = function(string $key, string $field) use(&$req, &$survey){
            if(array_key_exists($key, $req)) $survey->{$field}()->associate($req[$key]);
        };

        $setField('name', 'name');
        $setField('alias', 'alias');
        if(isset($req['area'])){
            $survey->area = \DB::raw("'".LineParser::parse($req['area'])->toEKWT()."'");
        }
        $setField('direction', 'direction');
        $setField('startDate', 'start_date');

        if(isset($req['archPath'])){
            $survey->arch_path = $req['archPath'];
            $survey->arch_size = filesize(DataSystem::getDataPath($req['archPath']));
        }

        $assocField('parentZone', 'zone');
        $assocField('field', 'field');
        if(isset($req['vessel'])){
            $survey->trip->vessel()->associate($req['vessel']);
            $survey->trip->save();
        }

        $assocField('source', 'source');
        $setField('srcGroupCount', 'src_group_count');
        $setField('srcPerGroup', 'src_per_group');

        $assocField('streamer', 'streamer');
        $setField('isTowed', 'is_towed');

        $assocField('resolution', 'resolution');

        $setField('customer', 'customer');
        $setField('operator', 'operator');
        
        if(isset($req['archFiles'])){
            $survey->archFiles()->delete();
            foreach($req['archFiles'] as $file){
                $archFile = new Seismic2dSurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }

        $survey->save();
        return $survey;
    }

    public function destroy(Seismic2dSurvey $survey){
        
    }
}