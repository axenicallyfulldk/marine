<?php

namespace App\Services;

use App\Models\Mosaic;
use App\Models\MosaicType;
use App\Models\MosaicArchFile;

use App\Helpers\PolygonParser;
use App\Helpers\DataSystem;

use Illuminate\Support\Facades\Validator;

class MosaicService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editMosaicsByDefault()){
            throw new HttpException(403, "You have no right to create mosaics.");
        }

        // Validate
        Validator::make($req, Mosaic::getStoreRules($req))->validate();

        $mosaic = new Mosaic();
        $mosaic->name = $req['name'];
        $mosaic->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
        $mosaic->parent()->associate($req['parent']);
        $mosaic->input_direction = $req['inputDirection'];
        $mosaic->type()->associate(MosaicType::findByKey($req['type'])->first());
        $mosaic->mainTool()->associate($req['mainTool']);
        $mosaic->creation_date = $req['creationDate'];
        $mosaic->is_deleted = false;
        $mosaic->file_path = $req['filePath'];
        $mosaic->file_size = filesize(DataSystem::getDataPath($req["filePath"]));
        if(!empty($req['archPath'])){
            $mosaic->arch_path = $req['archPath'];
            $mosaic->arch_size = filesize(DataSystem::getDataPath($req["archPath"]));
        }
        $mosaic->save();

        if(isset($req['archFiles'])){
            foreach($req['archFiles'] as $file){
                $archFile = new MosaicArchFile();
                $archFile->mosaic()->associate($mosaic);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }
        
        return $mosaic->fresh();
    }

    public function patch($req, Mosaic $mosaic){
        // Check rights
        if(!$mosaic->isEditable()){
            throw new HttpException(403, "You have no right to update this mosaic.");
        }

        // Validate
        Validator::make($req, Mosaic::getPatchRules($req))->validate();
        
        if(isset($req['name'])) $mosaic->name = $req['name'];
        if(isset($req['area'])) $mosaic->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
        if(isset($req['parent'])) $mosaic->parent()->associate($req['parent']);
        if(isset($req['inputDirection'])) $mosaic->input_direction = $req['inputDirection'];
        if(isset($req['type'])) $mosaic->type()->associate(MosaicType::findByKey($req['type'])->first());
        if(isset($req['mainTool'])) $mosaic->mainTool()->associate($req['mainTool']);
        if(isset($req['creationDate'])) $mosaic->creation_date = $req['creationDate'];
        if(isset($req['filePath'])) $mosaic->file_path = $req['filePath'];
        if(isset($req['archPath'])) $mosaic->arch_path = $req['archPath'];
        if(isset($req['archFiles'])){
            $mosaic->archFiles()->delete();
            foreach($req['archFiles'] as $file){
                $archFile = new MosaicArchFile();
                $archFile->mosaic()->associate($mosaic);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }
        $mosaic->save();
        
        return $mosaic->fresh();
    }

    public function destroy(Mosaic $mosaic){
        // Check rights
        if(!$mosaic->isEditable()){
            throw new HttpException(403, "You have no right to update this mosaic.");
        }

        $mosaic->delete();
    }
}