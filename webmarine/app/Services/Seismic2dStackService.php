<?php

namespace App\Services;

use App\Models\{Seismic2dSurvey, Seismic2dStack, Seismic2dSurveyArchFile, Trip};

use App\Helpers\DataSystem;
use App\Helpers\SegyFile;
use App\Helpers\LatLongDistance;

use Illuminate\Support\Facades\Validator;

use App\Jobs\CalculateStackCdpInfo;
use App\Jobs\CalculateStackNormFactor;

class Seismic2dStackService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editSeismic2dSurveysByDefault()){
            throw new HttpException(403, "You have no right to create stacks.");
        }

        // Validate
        Validator::make($req, Seismic2dStack::getStoreRules($req))->validate();

        $setIfNotNull = function($srcField, $dstField, &$obj) use($req) {
            if(array_key_exists($srcField, $req)) $obj->{$dstField} = $req[$srcField];
        };

        $stack = new Seismic2dStack();
        $stack->name = $req['name'];
        $setIfNotNull('alias', 'alias', $stack);

        $stack->survey()->associate($req['survey']);

        $stack->file_path = $req['filePath'];
        $stack->file_size = filesize(DataSystem::getDataPath($req['filePath']));

        $setIfNotNull('isMigrated', 'is_migrated', $stack);
        $setIfNotNull('isAmplCorrected', 'is_ampl_corrected', $stack);
        $setIfNotNull('isDeconvolved', 'is_deconvolved', $stack);

        $setIfNotNull('cdpOffset', 'cdp_offset', $stack);
        $setIfNotNull('cdpSize', 'cdp_size', $stack);

        // $segyFilePath = DataSystem::getDataPath($stack->file_path);

        // $rangeStep = static::findCdpRangeAndStep($segyFilePath, $stack->cdp_offset, $stack->cdp_size);
        // $stack->cdp_start = $rangeStep[0];
        // $stack->cdp_end = $rangeStep[1];
        // $stack->cdp_step = $rangeStep[2];

        // $polylinePoints = \geoPHP::load($stack->survey->area)->asArray();
        // $fp = reset($polylinePoints);
        // $lp = end($polylinePoints);
        // $segyFile = new SegyFile($segyFilePath, false);
        // $cdpDist = LatLongDistance::vincentyGreatCircleDistance($fp[1], $fp[0], $lp[1], $lp[0])/($segyFile->getTraceCount()-1);

        // $stack->cdp_distance = $cdpDist;
        
        $stack->save();

        CalculateStackNormFactor::dispatch($stack);
        CalculateStackCdpInfo::dispatch($stack);

        return $stack;
    }

    private static function findCdpRangeAndStep($segyPath, $cdpOffset, $cdpSize){
        $segyFile = new SegyFile($segyPath, false);
        $vals = $segyFile->getUintTraceHeaderFieldValues($cdpOffset, $cdpSize);
        $diffs = [];
        for($i = 0; $i < count($vals)-1; ++$i){
            array_push($diffs, $vals[$i+1]-$vals[$i]);
        }

        return [min($vals), max($vals), array_sum($diffs)/count($diffs)];
    }

    public function patch($req, Seismic2dStack $stack){
        // Check rights
        if(!$stack->isEditable()){
            throw new HttpException(403, "You have no right to update this stack.");
        }

        // Validate
        Validator::make($req, Seismic2dStack::getPatchRules($req))->validate();

        $setField = function(string $key, string $field) use($req, $stack){
            if(array_key_exists($key, $req))
                $stack->{$field} = $req[$key];
        };
        $assocField = function(string $key, string $field) use($req, $stack){
            if(array_key_exists($key, $req)) $stack->{$field}()->associate($req[$key]);
        };

        // Check rights
        if(!\Auth::user()->editSeismic2dSurveysByDefault()){
            throw new HttpException(403, "You have no right to create stacks.");
        }

        $setField('name', 'name');
        $setField('alias', 'alias');

        $setField('filePath', 'file_path');
        $stack->file_size = filesize(DataSystem::getDataPath($stack->file_path));

        $setField('isMigrated', 'is_migrated');
        $setField('isAmplCorrected', 'is_ampl_corrected');
        $setField('isDeconvolved', 'is_deconvolved');

        $setField('cdpOffset', 'cdp_offset');
        $setField('cdpSize', 'cdp_size');

        if(isset($req['filePath']) || isset($req['cdpOffset']) || isset($req['cdpSize'])){
            CalculateStackCdpInfo::dispatch($stack);
            // $segyFilePath = DataSystem::getDataPath($stack->file_path);

            // $rangeStep = static::findCdpRangeAndStep($segyFilePath, $stack->cdp_offset, $stack->cdp_size);
            // $stack->cdp_start = $rangeStep[0];
            // $stack->cdp_end = $rangeStep[1];
            // $stack->cdp_step = $rangeStep[2];

            // $polylinePoints = \geoPHP::load($stack->survey->area)->asArray();
            // $fp = reset($polylinePoints);
            // $lp = end($polylinePoints);
            // $segyFile = new SegyFile($segyFilePath, false);
            // $cdpDist = LatLongDistance::vincentyGreatCircleDistance($fp[1], $fp[0], $lp[1], $lp[0])/($segyFile->getTraceCount()-1);

            // $stack->cdp_distance = $cdpDist;
        }
        
        $stack->save();
        
        return $stack;
    }

    public function destroy(Seismic2dSurvey $stack){
        
    }
}