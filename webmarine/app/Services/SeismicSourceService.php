<?php

namespace App\Services;

use App\Models\SeismicSource;

use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Helpers\FK;

class SeismicSourceService {
    public function add($req){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to create seismic sources.");
        }
        
        // Validate
        Validator::make($req, SeismicSource::getStoreRules())->validate();
        
        $source = new SeismicSource();
        $source->name = $req['name'];
        $source->type()->associate($req['type']);
        $source->save();
        return $source;
    }

    public function patch($req, SeismicSource $source){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to update seismic source.");
        }

        // Validate
        Validator::make($req, SeismicSource::getPatchRules())->validate();

        $setField = function(string $key, string $fieldName)use($req, $source){
            if(array_key_exists($key, $req)) $source->{$fieldName} = $req[$key];
        };

        $setField("name", "name");
        $setField(FK::get(\App\Tables::SEISMIC_SOURCE_TYPE_TABLE), "type");
    
        $source->save();
        return $source;
    }

    public function destroy(SeismicSource $source){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to delete seismic source.");
        }
        
        $source->delete();
    }
}