<?php

namespace App\Services;

use App\Models\Tool;

use Illuminate\Support\Facades\Validator;

use App\Helpers\ColorIntConverter;

class SssMbesToolService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to create tools.");
        }

        // Validate
        Validator::make($req, Tool::getStoreRules())->validate();
        
        $tool = Tool::create([
            'brand' => @$req['brand'],
            'model' => @$req['model'],
            'map_color' => ColorIntConverter::strToInt(@$req['mapColor'])
        ]);

        $tool->save();
        return $tool;
    }

    public function patch($req, Tool $tool){
        // Check rights
        if(!\Auth::user()->editToolsByDefault()){
            throw new HttpException(403, "You have no right to edit tools.");
        }

        // Validate
        Validator::make($req, Tool::getPatchRules())->validate();
        

        $setField = function(string $key, string $field)use($req, $tool){
            if(array_key_exists($key, $req)) $tool->{$field} = $req[$key];
        };

        $setField("brand", "brand");
        $setField("model","model");
        if(isset($req['mapColor'])){
            $tool->map_color = ColorIntConverter::strToInt($req['mapColor']);
        }
    
        $tool->save();
        return $tool;
    }

    public function destroy(Tool $tool){
        
    }
}