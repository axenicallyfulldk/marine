<?php

namespace App\Services;

use App\Models\Zone;
use App\Models\Vessel;
use App\Models\Tool;
use App\Models\SurveyType;
use App\Models\SssMbesSurvey;
use App\Models\MosaicType;
use App\Models\Mosaic;

use App\Models\Field;

use App\Models\{SeismicSource, SeismicSourceType};
use App\Models\SeismicStreamer;

use App\Models\{Seismic2dSurvey, Seismic2dStack, Seismic3dSurvey, Seismic3dStack, SeismicResolutionClass};

use App\Helpers\{ExcelToCsvSplitter, CsvReader, PolygonParser, LineParser, DataSystem, CodeHelper, FolderSynchronizer, TransactionManager};
use Symfony\Component\HttpKernel\Exception\HttpException;

use \DB;

/**
 * Service that imports surveys and mosaics from excel file.
 */
class BatchImportService{
    function __construct(){
        $this->bfs = env('BATCH_FILE_SYNCHRONIZER');

        $this->srcf = env('IMPORT_DATA_FOLDER');
        $this->dstf = DataSystem::getDataPath();

        $this->vesselService = new VesselService();
        $this->toolService = new SssMbesToolService();
        $this->sssMbesSurveyService = new SssMbesSurveyService();
        $this->mosaicService = new MosaicService();

        $this->fieldService = new FieldService();
        $this->sourceService = new SeismicSourceService();
        $this->streamerService = new SeismicStreamerService();

        $this->seismic2dSurveyService = new Seismic2dSurveyService();
        $this->seismic2dStackService = new Seismic2dStackService();

        $this->seismic3dSurveyService = new Seismic3dSurveyService();
        $this->seismic3dStackService = new Seismic3dStackService();

        $this->message = "";
    }

    public function getMessage(){
        return $this->message;
    }

    private function synchronizeFiles(){
        $this->message .= "==== Files synchronizing ====\n";
        // Run copying from pub-1
        FolderSynchronizer::copy($this->srcf, $this->dstf);

        $this->message .= "==== Files successfully synchronized ====\n";
    }

    public function importSssMbesData(){
        if(!is_null($this->bfs) && file_exists($this->bfs)){
            $this->synchronizeFiles();
        }

        // Import sss and mbes surveys and mosaics
        $sssmbesFile = DataSystem::getDataPath("GS2021/sssmbes_surveys.xlsx");

        try{
            // Generate folder for csv files
            $csvFolder = $this->genTempFolder();
            // Split excel file to csv files
            $etcs = new ExcelToCsvSplitter($sssmbesFile, $csvFolder);
        }
        catch(\Exception $e){
            $m = sprintf("%s%s\nFailed to get csv files from %s", $this->message, $e->getMessage(), $sssmbesFile);
            throw new \Exception($m);
        }

        try{
            $transManager = new TransactionManager();
            $transManager->begin();
                // Import data from csv files
                $this->message.="===Importing vessels===\n";
                $this->importVessels($etcs->getCsvBySheetName("vessels"));
                $this->message.="===Vessels successfully imported===\n";
                $this->message.="===Importing tools===\n";
                $this->importSssMbesTools($etcs->getCsvBySheetName("tools"));
                $this->message.="===Tools successfully imported===\n";
                $this->message.="===Importing surveys===\n";
                $this->importSssMbesSurveys($etcs->getCsvBySheetName("surveys"), $etcs->getCsvBySheetName("survey_files"));
                $this->message.="===Surveys successfully imported===\n";
                $this->message.="===Importing mosaics===\n";
                $this->importMosaics($etcs->getCsvBySheetName("mosaics"), $etcs->getCsvBySheetName("mosaic_files"));
                $this->message.="===Mosaics successfully imported===\n";
            $transManager->end();
        }
        catch(\Exception $e){
            $m = sprintf("%s%s\n", $this->message, $e->getMessage());
            throw new \Exception($m, previous: $e->getPrevious());
        }
    }

    public function importSeismicData(){
        $this->synchronizeFiles();

        // Import sss and mbes surveys and mosaics
        $seismicFile = DataSystem::getDataPath("GS2021/seismic_surveys.xlsx");

        try{
            // Generate folder for csv files
            $csvFolder = $this->genTempFolder();
            // Split excel file to csv files
            $etcs = new ExcelToCsvSplitter($seismicFile, $csvFolder);
        }
        catch(\Exception $e){
            $m = sprintf("%s%s\nFailed to get csv files from %s", $this->message, $e->getMessage(), $seismicFile);
            throw new \Exception($m, previous: $e->getPrevious());
        }

        try{
            $transManager = new TransactionManager();
            $transManager->begin();
                // Import data from csv files
                $this->message.="===Importing fields===\n";
                $this->importFields($etcs->getCsvBySheetName("fields"));
                $this->message.="===Fields successfully imported===\n";

                $this->message.="===Importing vessels===\n";
                $this->importVessels($etcs->getCsvBySheetName("vessels"));
                $this->message.="===Vessels successfully imported===\n";

                $this->message.="===Importing seismic sources===\n";
                $this->importSeismicSources($etcs->getCsvBySheetName("sources"));
                $this->message.="===Seismic sources successfully imported===\n";

                $this->message.="===Importing streamers===\n";
                $this->importSeismicStreamers($etcs->getCsvBySheetName("streamers"));
                $this->message.="===Streamers successfully imported===\n";

                $this->message.="===Importing 2D surveys ===\n";
                $this->importSeismic2dSurveys($etcs->getCsvBySheetName("surveys2d"), $etcs->getCsvBySheetName("survey2d_files"));
                $this->message.="===Surveys successfully imported===\n";

                $this->message.="===Importing 2D stacks ===\n";
                $this->importSeismic2dStacks($etcs->getCsvBySheetName("surveys2d"));
                $this->message.="===Stacks successfully imported===\n";

                $this->message.="===Importing 3D surveys ===\n";
                $this->importSeismic3dSurveys($etcs->getCsvBySheetName("surveys3d"), $etcs->getCsvBySheetName("survey3d_files"));
                $this->message.="===Surveys successfully imported===\n";

                $this->message.="===Importing 3D stacks ===\n";
                $this->importSeismic3dStacks($etcs->getCsvBySheetName("surveys3d"));
                $this->message.="===Stacks successfully imported===\n";
            $transManager->end();
        }
        catch(\Exception $e){
            $m = sprintf("%s%s\n", $this->message, $e->getMessage());
            throw new \Exception($m, previous: $e->getPrevious());
        }
    }

    static function initRequest(&$req, $data){
        foreach($data as $x => $v) {
            $req->replace([$x => $v]);
        }
    }

    private function importVessels(string $vesselCsv){
        $data = CsvReader::read($vesselCsv, ",");

        foreach($data as $v){
            try{
                $vessel = static::findVessel($v);
                if(is_null($vessel)){
                    $this->vesselService->add($v);
                }else{
                    $this->vesselService->patch($v, $vessel);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import vessel \"%s\"", static::getValidationErrorMessage($e), $v['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import vessel \"%s\"", $e->getMessage(), $v['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }            
        }
    }

    private static function findVessel($vessel){
        return Vessel::findByName($vessel['name'])->first();
    }

    private function importSssMbesTools(string $toolCsv){
        $data = CsvReader::read($toolCsv, ",");

        foreach($data as $toolData){
            $toolData['mapColor'] = $toolData['map color'];
            try{
                $tool = static::findTool($toolData);
                if(is_null($tool)){
                    $this->toolService->add($toolData);
                }else{
                    $this->toolService->patch($toolData, $tool);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import tool \"%s\"", static::getValidationErrorMessage($e), $toolData['name']);
                throw new \Exception($m);
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import tool \"%s\"", $e->getMessage(), $toolData['name']);
                throw new \Exception($m);
            }
        }
    }

    private static function findTool($tool){
        return Tool::findByName($tool['name'])->first();
    }

    private function importFields(string $fieldCsv){
        $data = CsvReader::read($fieldCsv, ",");

        foreach($data as $fieldData){
            // split points separated by comma and join them with \n
            $fieldData['area'] = implode("\n", preg_split("/\s*,\s*/",trim($fieldData['area'])));
            try{
                $field = static::findField($fieldData);
                if(is_null($field)){
                    $this->fieldService->add($fieldData);
                }else{
                    $this->fieldService->patch($fieldData, $field);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import field \"%s\"", static::getValidationErrorMessage($e), $fieldData['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import field \"%s\"", $e->getMessage(), $fieldData['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findField($field){
        return Field::findByName($field['name'])->first();
    }

    private function importSeismicSources(string $sourceCsv){
        $data = CsvReader::read($sourceCsv, ",");

        foreach($data as $sourceData){
            $type = SeismicSourceType::findByName($sourceData['type'])->first();
            if(is_null($type)){
                throw new HttpException(404, sprintf("Type \"%s\" for seismic source \"%s\" doesn't exist", $sourceData['type'], $sourceData['name']));
            }
            $sourceData['type'] = $type->id;
            
            try{
                $source = static::findSeismicSource($sourceData);
                if(is_null($source)){
                    $this->sourceService->add($sourceData);
                }else{
                    $this->sourceService->patch($sourceData, $source);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import seismic source \"%s\"", static::getValidationErrorMessage($e), $sourceData['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import seismic source \"%s\"", $e->getMessage(), $sourceData['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findSeismicSource($source){
        return SeismicSource::findByName($source['name'])->first();
    }

    private function importSeismicStreamers(string $streamerCsv){
        $data = CsvReader::read($streamerCsv, ",");

        foreach($data as $streamerData){ 
            $streamerData['receiverDist'] = $streamerData['receiver_dist'];
            try{
                $streamer = static::findSeismicStreamer($streamerData);
                if(is_null($streamer)){
                    $this->streamerService->add($streamerData);
                }else{
                    $this->streamerService->patch($streamerData, $streamer);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import seismic streamer \"%s\"", static::getValidationErrorMessage($e), $streamerData['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import seismic streamer \"%s\"", $e->getMessage(), $streamerData['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findSeismicStreamer($streamer){
        return SeismicStreamer::findByName($streamer['name'])->first();
    }

    private function importSssMbesSurveys(string $surveyCsv, string $archFilesCsv){
        $survData = $this->mergeSurveyArchFiles($surveyCsv, $archFilesCsv);

        foreach($survData as $surv){
            try{
                $s = static::findSssMbesSurvey($surv);
                if(is_null($s)){
                    $this->sssMbesSurveyService->add($surv);
                }else{
                    $this->sssMbesSurveyService->patch($surv, $s);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import survey \"%s\"", static::getValidationErrorMessage($e), $surv['name']);
                throw new \Exception($m);
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import survey \"%s\"", $e->getMessage(), $surv['name']);
                throw new \Exception($m);
            }
        }
    }

    private static function findSssMbesSurvey($surv){
        return SssMbesSurvey::findByName($surv['name'])->first();
    }

    /**
     * Merges surveys with corresponding archive files
     */
    static function mergeSurveyArchFiles(string $surveyCsv, string $archCsv){
        $survData = CsvReader::read($surveyCsv, ",");
        $archData = CsvReader::read($archCsv, ",");
        
        return SssMbesDataPreparator::fromCsv($survData, $archData);
    }

    private function importMosaics(string $mosaicCsv, string $archFilesCsv){
        $mosaicData = $this->mergeMosaicArchFiles($mosaicCsv, $archFilesCsv);

        foreach($mosaicData as $mosaic){
            try{
                $s = static::findMosaic($mosaic);
                if(is_null($s)){
                    $this->mosaicService->add($mosaic);
                }else{
                    $this->mosaicService->patch($mosaic, $s);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import mosaic \"%s\"", static::getValidationErrorMessage($e), $mosaic['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import mosaic \"%s\"", $e->getMessage(), $mosaic['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findMosaic($mosaic){
        return Mosaic::findByName($mosaic['name'])->first();
    }

    static function mergeMosaicArchFiles(string $mosaicCsv, string $archCsv){
        $mosaicData = CsvReader::read($mosaicCsv, ",");
        $archData = CsvReader::read($archCsv, ",");
        
        return MosaicDataPreparator::fromCsv($mosaicData, $archData);
    }

    private function importSeismic2dSurveys(string $surveyCsv, string $archFilesCsv){
        $surveyData = $this->mergeSeismic2dSurveyArchFiles($surveyCsv, $archFilesCsv);

        foreach($surveyData as $survey){
            try{
                $s = static::findSeismic2dSurvey($survey);
                if(is_null($s)){
                    $this->seismic2dSurveyService->add($survey);
                }else{
                    $this->seismic2dSurveyService->patch($survey, $s);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import survey \"%s\"", static::getValidationErrorMessage($e), $survey['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import survey \"%s\"", $e->getMessage(), $survey['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findSeismic2dSurvey($survey){
        return Seismic2dSurvey::findByName($survey['name'])->first();
    }

    static function mergeSeismic2dSurveyArchFiles(string $surveyCsv, string $archCsv){
        $survData = CsvReader::read($surveyCsv, ",");
        $archData = CsvReader::read($archCsv, ",");
        
        return Seismic2dSurveyDataPreparator::fromCsv($survData, $archData);
    }

    private function importSeismic2dStacks(string $csvPath){
        $stacksData = Seismic2dStackDataPreparator::fromCsv(CsvReader::read($csvPath, ","));

        foreach($stacksData as $stack){
            try{
                $s = static::findSeismic2dStack($stack);
                // echo($stack['name']."\n");
                if(is_null($s)){
                    $this->seismic2dStackService->add($stack);
                }else{
                    $this->seismic2dStackService->patch($stack, $s);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import stack \"%s\"", static::getValidationErrorMessage($e), $stack['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import stack \"%s\"", $e->getMessage(), $stack['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findSeismic2dStack($stack){
        return Seismic2dStack::findByName($stack['name'])->first();
    }


    private function importSeismic3dSurveys(string $surveyCsv, string $archFilesCsv){
        $surveyData = $this->mergeSeismic3dSurveyArchFiles($surveyCsv, $archFilesCsv);

        foreach($surveyData as $survey){
            try{
                $s = static::findSeismic3dSurvey($survey);
                if(is_null($s)){
                    $this->seismic3dSurveyService->add($survey);
                }else{
                    $this->seismic3dSurveyService->patch($survey, $s);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import survey \"%s\"", static::getValidationErrorMessage($e), $survey['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import survey \"%s\"", $e->getMessage(), $survey['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findSeismic3dSurvey($survey){
        return Seismic3dSurvey::findByName($survey['name'])->first();
    }

    static function mergeSeismic3dSurveyArchFiles(string $surveyCsv, string $archCsv){
        $survData = CsvReader::read($surveyCsv, ",");
        $archData = CsvReader::read($archCsv, ",");
        
        return Seismic3dSurveyDataPreparator::fromCsv($survData, $archData);
    }

    private function importSeismic3dStacks(string $csvPath){
        $stacksData = Seismic3dStackDataPreparator::fromCsv(CsvReader::read($csvPath, ","));

        foreach($stacksData as $stack){
            try{
                $s = static::findSeismic3dStack($stack);
                if(is_null($s)){
                    $this->seismic3dStackService->add($stack);
                }else{
                    $this->seismic3dStackService->patch($stack, $s);
                }
            }
            catch(\Illuminate\Validation\ValidationException $e){
                $m = sprintf("%s\nFailed to import stack \"%s\"", static::getValidationErrorMessage($e), $stack['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
            catch(\Exception $e){
                $m = sprintf("%s\nFailed to import stack \"%s\"", $e->getMessage(), $stack['name']);
                throw new \Exception($m, previous: $e->getPrevious());
            }
        }
    }

    private static function findSeismic3dStack($stack){
        return Seismic3dStack::findByName($stack['name'])->first();
    }


    private static function getValidationErrorMessage($e){
        $m = "";
        $errors = array();
        foreach($e->errors() as $field=>$message){
            $m .= $message[0]."\n";
        }
        return mb_substr($m, 0, strlen($m)-1);
    }


    private function genTempFolder(){
        $tempfile=tempnam(sys_get_temp_dir(),'');
        if (file_exists($tempfile)) { unlink($tempfile); }
        mkdir($tempfile);
        return $tempfile;
    }

    private $bfs;
}

/**
 * Prepares data from different formats to format suitable for SssMbesSurveyService
 */
class SssMbesDataPreparator{
    public static function fromCsv($survData, $archFileData){
        // Gather information about files in archive
        $archFiles = [];
        foreach($archFileData as $arch){
            $survName = $arch['survey name'];
            if(!array_key_exists($survName, $archFiles)){
                $archFiles[$survName] = [];
            }

            array_push($archFiles[$survName], ['name' => $arch['file'], 'description' => $arch['description']]);
        }
        
        // Build survey data
        $surveys = [];
        foreach($survData as $surv){
            $s = [
                'name' => $surv['name'],
                'direction' => $surv['direction'],
                'startDate' => $surv['date'],
            ];

            // Check existence of area file and parsing area
            $pathToArea = DataSystem::getDataPath($surv['path'], $surv['polygon file']);
            if(!\file_exists($pathToArea)){
                throw new HttpException(404, sprintf("Area file \"%s\" for survey \"%s\" doesn't exist", $surv['polygon file'], $surv['name']));
            }
            $s['area'] = PolygonParser::parseCsv(\file_get_contents($pathToArea))->toSimple();

            // Find zone
            $zoneCodes = (new CodeHelper($surv['polygon file']))->getZones();
            $zone = Zone::deduceZone($zoneCodes);
            if(is_null($zone)){
                throw new HttpException(404, sprintf("Zone \"%s\" for survey \"%s\" doesn't exist", implode("-", $zoneCodes), $surv['name']));
            }
            $s['parent'] = $zone->id;

            // Find type
            $type = SurveyType::findByName($surv['type'])->first();
            if(is_null($type)){
                throw new HttpException(404, sprintf("Type \"%s\" for survey \"%s\" doesn't exist", $surv['type'], $surv['name']));
            }
            $s['type'] = $type->key;

            // Find tool
            $tool = Tool::findByName($surv['tool'])->first();
            if(is_null($tool)){
                throw new HttpException(404, sprintf("Tool \"%s\" for survey \"%s\" doesn't exist", $surv['tool'], $surv['name']));
            }
            $s['tool'] = $tool->id;

            // Find vessel
            $vessel = Vessel::findByName($surv['vessel'])->first();
            if(is_null($vessel)){
                throw new HttpException(404, sprintf("Vessel \"%s\" for survey \"%s\" doesn't exist", $surv['vessel'], $surv['name']));
            }
            $s['vessel'] = $vessel->id;

            // File path
            if(!empty($surv['survey file'])){
                $s['filePath'] = static::joinPaths($surv['path'], $surv['survey file']);
            }

            // Arch path
            if(!empty($surv['arch file'])){
                $s['archPath'] = static::joinPaths($surv['path'], $surv['arch file']);
            }

            if(isset($archFiles[$surv['name']])){
                $s['archFiles'] = $archFiles[$surv['name']];
            }
            array_push($surveys, $s);
        }

        return $surveys;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }
}

/**
 * Prepares data from different formats to format suitable for seismic surveys
 */
class Seismic2dSurveyDataPreparator{
    public static function fromCsv($survData, $archFileData){
        // Gather information about files in archive
        $archFiles = [];
        foreach($archFileData as $arch){
            $survName = $arch['survey name'];
            if(!array_key_exists($survName, $archFiles)){
                $archFiles[$survName] = [];
            }

            array_push($archFiles[$survName], ['name' => $arch['file'], 'description' => $arch['description']]);
        }
        
        // Build survey data
        $surveys = [];
        foreach($survData as $surv){
            if(empty($surv['name'])) continue;

            $s = [
                'name' => $surv['name'],
                'alias' => $surv['alias'],
                'direction' => $surv['direction'],
                'startDate' => $surv['date'],
            ];

            // Check existence of area file and parsing area
            $pathToArea = DataSystem::getDataPath($surv['path'], $surv['area_file']);
            if(!\file_exists($pathToArea)){
                throw new HttpException(404, sprintf("Area file \"%s\" for survey \"%s\" doesn't exist", $surv['area_file'], $surv['name']));
            }
            // echo($surv['name']."\n");
            $s['area'] = LineParser::parseCsv(\file_get_contents($pathToArea), sepReg: ";")->toSimple();

            // Find zone
            $zoneCodes = (new CodeHelper(substr($surv['path'], strpos($surv['path'],"/")+1), "\/"))->getZones();
            $zone = Zone::deduceZone($zoneCodes);
            if(is_null($zone)){
                throw new HttpException(404, sprintf("Zone \"%s\" for survey \"%s\" doesn't exist", implode("-", $zoneCodes), $surv['name']));
            }
            $s['parentZone'] = $zone->id;

            // Find field
            $field = Field::findByName($surv['field'])->first();
            if(!is_null($field)){
                $s['field'] = $field->id;
            }
            

            // Find seismic source
            $source = SeismicSource::findByName($surv['source'])->first();
            if(!is_null($source)){
                $s['source'] = $source->id;
            }

            $s['srcGroupCount'] = $surv['src_group_count'];
            $s['srcPerGroup'] = $surv['src_group_count'];

            // Find streamer
            $streamer = SeismicStreamer::findByName($surv['streamer'])->first();
            if(!is_null($streamer)){
                $s['streamer'] = $streamer->id;
            }            

            $s['isTowed'] = $surv['is_towed'];
            $s['streamerCount'] = $surv['streamer_count'];

            $resol = SeismicResolutionClass::byKeyOrName($surv['resolution_class'])->first();
            if(!is_null($resol)){
                $s['resolution'] = $resol->id;
            }
            

            // Find vessel
            $vessel = Vessel::findByName($surv['vessel'])->first();
            if(is_null($vessel)){
                throw new HttpException(404, sprintf("Vessel \"%s\" for survey \"%s\" doesn't exist", $surv['vessel'], $surv['name']));
            }
            $s['vessel'] = $vessel->id;

            $s['customer'] = $surv['customer'];

            $s['operator'] = $surv['operator'];

            // Arch path
            if(!empty($surv['arch_file'])){
                $s['archPath'] = static::joinPaths($surv['path'], $surv['arch_file']);
            }

            if(isset($archFiles[$surv['name']])){
                $s['archFiles'] = $archFiles[$surv['name']];
            }
            array_push($surveys, $s);
        }

        return $surveys;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }
}

class Seismic2dStackDataPreparator {
    public static function fromCsv($stacksData){
        $stacks = [];
        foreach($stacksData as $stackData){
            if(empty($stackData['name'])) continue;
            // File path
            $s = array();
            $s['name'] = $stackData['name'];

            $addIfExist = function($srcField, $targetField) use(&$s, $stackData){
                if(array_key_exists($srcField,$stackData) && $stackData[$srcField] != ""){
                    $s[$targetField] = $stackData[$srcField];
                }
            };

            $survey = Seismic2dSurvey::findByName($stackData['name'])->first();
            if(is_null($survey)){
                throw new HttpException(404, sprintf("Survey \"%s\" for stack \"%s\" doesn't exist", $stackData['name'], $stackData['name']));
            }
            $s['survey'] = $survey->id;

            $addIfExist('alias', 'alias');
            $addIfExist('migrated', 'isMigrated');
            $addIfExist('deconvolution', 'isDeconvolved');
            $addIfExist('ampl_correction', 'isAmplCorrected');
            $addIfExist('cdp_offset', 'cdpOffset');
            $addIfExist('cdp_size', 'cdpSize');
            $addIfExist('sp_offset', 'spOffset');
            $addIfExist('sp_size', 'spSize');            
            
            if(!empty($stackData['survey_file'])){
                $s['filePath'] = static::joinPaths($stackData['path'], $stackData['survey_file']);
            }
            array_push($stacks, $s);
        }
        return $stacks;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }
}

class Seismic3dSurveyDataPreparator extends Seismic2dSurveyDataPreparator{
    public static function fromCsv($survData, $archFileData){
        $data = parent::fromCsv($survData, $archFileData);
        foreach($survData as $surv){
            foreach($data as &$d){
                if($d['name'] == $surv['name']){
                    $d['distBetweenStreamers'] = $surv['distance_between_streamers'];
                }
            }
        }
        return $data;
    }
}


class Seismic3dStackDataPreparator {
    public static function fromCsv($stacksData){
        $stacks = [];
        foreach($stacksData as $stackData){
            // File path
            $s = [];
            $addIfExist = function($srcField, $targetField) use(&$s, &$stackData){
                if(array_key_exists($srcField,$stackData)){
                    $s[$targetField] = $stackData[$srcField];
                }
            };
            

            $survey = Seismic3dSurvey::findByName($stackData['name'])->first();
            if(is_null($survey)){
                throw new HttpException(404, sprintf("Survey \"%s\" for stack \"%s\" doesn't exist", $stackData['name'], $stackData['name']));
            }
            $s['survey'] = $survey->id;

            $s['name'] = $stackData['name'];
            $addIfExist('alias', 'alias');
            $addIfExist('is_migrated', 'isMigrated');
            $addIfExist('is_deconvolved', 'isDeconvolved');
            $addIfExist('is_ampl_corrected', 'isAmplCorrected');
            $addIfExist('inline_offset', 'inlineOffset');
            $addIfExist('inline_size', 'inlineSize');
            $addIfExist('xline_offset', 'xlineOffset');
            $addIfExist('xline_size', 'xlineSize');            
            
            if(!empty($stackData['survey_file'])){
                $s['filePath'] = static::joinPaths($stackData['path'], $stackData['survey_file']);
            }
            array_push($stacks, $s);
        }
        return $stacks;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }
}


/**
 * Prepares data from different formats to format suitable for MosaicService
 */
class MosaicDataPreparator{
    public static function fromCsv($mosaicData, $archFileData){
        // Gather information about files in archive
        $archFiles = [];
        foreach($archFileData as $arch){
            $survName = $arch['mosaic name'];
            if(!array_key_exists($survName, $archFiles)){
                $archFiles[$survName] = [];
            }

            array_push($archFiles[$survName], ['name' => $arch['file'], 'description' => $arch['description']]);
        }
        
        // Build survey data
        $mosaics = [];
        foreach($mosaicData as $mosaic){
            $s = [
                'name' => $mosaic['name'],
                'inputDirection' => $mosaic['direction'],
                'creationDate' => $mosaic['date'],
            ];

            // Check existence of area file and parsing area
            $pathToArea = DataSystem::getDataPath($mosaic['path'], $mosaic['polygon file']);
            if(!\file_exists($pathToArea)){
                throw new HttpException(404, sprintf("Area file \"%s\" for mosaic \"%s\" doesn't exist", $mosaic['polygon file'], $mosaic['name']));
            }
            $s['area'] = PolygonParser::parseCsv(\file_get_contents($pathToArea))->toSimple();

            // Find zone
            $zoneCodes = (new CodeHelper($mosaic['polygon file']))->getZones();
            $zone = Zone::deduceZone($zoneCodes);
            if(is_null($zone)){
                throw new HttpException(404, sprintf("Zone \"%s\" for mosaic \"%s\" doesn't exist", implode("-", $zoneCodes), $mosaic['name']));
            }
            $s['parent'] = $zone->id;

            // Find type
            $type = MosaicType::findByName($mosaic['type'])->first();
            if(is_null($type)){
                throw new HttpException(404, sprintf("Type \"%s\" for mosaic \"%s\" doesn't exist", $mosaic['type'], $mosaic['name']));
            }
            $s['type'] = $type->key;

            // Find tool
            $tool = Tool::findByName($mosaic['tool'])->first();
            if(is_null($tool)){
                throw new HttpException(404, sprintf("Tool \"%s\" for mosaic \"%s\" doesn't exist", $mosaic['tool'], $mosaic['name']));
            }
            $s['mainTool'] = $tool->id;

            // File path
            if(!empty($mosaic['mosaic file'])){
                $s['filePath'] = static::joinPaths($mosaic['path'], $mosaic['mosaic file']);
            }

            // Arch path
            if(!empty($mosaic['arch file'])){
                $s['archPath'] = static::joinPaths($mosaic['path'], $mosaic['arch file']);
            }

            if(isset($archFiles[$mosaic['name']])){
                $s['archFiles'] = $archFiles[$mosaic['name']];
            }
            array_push($mosaics, $s);
        }

        return $mosaics;
    }

    private static function joinPaths(...$paths) {
        return preg_replace('~[/\\\\]+~', DIRECTORY_SEPARATOR, implode(DIRECTORY_SEPARATOR, $paths));
    }
}