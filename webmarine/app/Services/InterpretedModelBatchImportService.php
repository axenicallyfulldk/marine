<?php

namespace App\Services;

use App\Models\Zone;
use App\Models\ZoneShapeType;

use Illuminate\Support\Facades\Validator;

use App\Helpers\TransactionManager;
use App\Helpers\{ExcelToCsvSplitter, CsvReader, DataSystem};
use App\Models\InterpretedModel;

class InterpretedModelBatchImportService {
    public function __construct(){
        $this->modelService = new InterpretedModelService();
        $this->transManager = new TransactionManager();
    }

    public function importFromExcel(string $excelFilename){
        $csvFolder = $this->genTempFolder();
        $etcs = new ExcelToCsvSplitter($excelFilename, $csvFolder);

        $modelsCsv = $etcs->getCsvBySheetName("models");
        $hazardsCsv = $etcs->getCsvBySheetName("hazards");

        $modelsRaw = CsvReader::read($modelsCsv, ",");
        $hazardsRaw = CsvReader::read($hazardsCsv, ",");

        $models = static::prepareModelsData($modelsRaw, $hazardsRaw);

        $this->transManager->begin();

        $this->importModelsImpl($models);

        $this->transManager->end();
    }

    public function importModels($models){
        $this->transManager->begin();
        $this->importModelsImpl($models);
        $this->transManager->end();
    }

    private function importModelsImpl($models){
        foreach($models as $model){
            $modelObj = InterpretedModel::findByName($model['name'])->first();
            if(is_null($modelObj)){
                $this->modelService->add($model);
            } else {
                $this->modelService->patch($model, $modelObj);
            }
        }
    }

    private static function prepareModelsData($modelsSheet, $hazardsSheet){
        $modelHazards = array();
        foreach($hazardsSheet as $hazard){
            if(!array_key_exists($hazard['model_name'], $modelHazards)){
                $modelHazards[$hazard['model_name']] = [];
            }
            $fhazard = $hazard;
            $fhazard['shape'] = file_get_contents(DataSystem::getDataPath($hazard['shape_file']));
            array_push($modelHazards[$hazard['model_name']], $fhazard);
        }

        foreach($modelsSheet as &$model){
            if(!array_key_exists($model['name'], $modelHazards)) continue;
            $model['parentZone'] = Zone::deduceZone($model['zone_code'])->id;
            $model['creationDate'] = $model['creation_date'];
            $model['filePath'] = $model['file_path'];
            $model['hazards'] = $modelHazards[$model['name']];
            $model['area'] = file_get_contents(DataSystem::getDataPath($model['shape_file']));
        }

        return $modelsSheet;
    }

    private function genTempFolder(){
        $tempfile=tempnam(sys_get_temp_dir(),'');
        if (file_exists($tempfile)) { unlink($tempfile); }
        mkdir($tempfile);
        return $tempfile;
    }

}