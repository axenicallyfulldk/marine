<?php

namespace App\Services;

use App\Models\{Seismic3dSurvey, Seismic3dSurveyArchFile, Trip};

use App\Helpers\PolygonParser;
use App\Helpers\DataSystem;

use Illuminate\Support\Facades\Validator;

class Seismic3dSurveyService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editSeismic2dSurveysByDefault()){
            throw new HttpException(403, "You have no right to create surveys.");
        }

        // Validate
        Validator::make($req, Seismic3dSurvey::getStoreRules($req))->validate();

        $setIfNotNull = function($srcField, $dstField, &$obj) use($req) {
            if(array_key_exists($srcField, $req)) $obj->{$dstField} = $req[$srcField];
        };

        $assocIfNotNull = function($srcField, $dstField, &$obj) use(&$req) {
            if(array_key_exists($srcField, $req)) $obj->{$dstField}()->associate($req[$srcField]);
        };
        

        $trip = new Trip();
        $trip->vessel()->associate($req['vessel']);
        $trip->save();

        $survey = new Seismic3dSurvey();
        $survey->name = $req['name'];
        $setIfNotNull('alias', 'alias', $survey);
        $survey->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
        $survey->direction = $req['direction'];
        $survey->start_date = $req['startDate'];

        if(isset($req['archPath'])){
            $survey->arch_path = $req['archPath'];
            $survey->arch_size = filesize(DataSystem::getDataPath($req['archPath']));
        }

        $survey->zone()->associate($req['parentZone']);
        $survey->field()->associate(@$req['field']);
        $survey->trip()->associate($trip);
        $assocIfNotNull('source', 'source', $survey);
        $survey->src_group_count = $req['srcGroupCount'];
        $survey->src_per_group = $req['srcPerGroup'];
        $assocIfNotNull('streamer', 'streamer', $survey);
        $survey->is_towed = $req['isTowed'];
        $setIfNotNull('streamerCount', 'streamer_count', $survey);
        $setIfNotNull('distBetweenStreamers', 'dist_between_streamers', $survey);
        $assocIfNotNull('resolution', 'resolution', $survey);
        $setIfNotNull('customer', 'customer', $survey);
        $setIfNotNull('operator', 'operator', $survey);
        
        $survey->is_deleted = false;
        
        $survey->save();

        if(isset($req['archFiles'])){
            foreach($req['archFiles'] as $file){
                $archFile = new Seismic3dSurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }
        
        return $survey;
    }

    public function patch($req, Seismic3dSurvey $survey){
        // Check rights
        if(!$survey->isEditable()){
            throw new HttpException(403, "You have no right to update this survey.");
        }

        // Validate
        Validator::make($req, Seismic3dSurvey::getPatchRules($req))->validate();

        $setField = function(string $key, string $field) use(&$req, &$survey){
            if(array_key_exists($key, $req)) $survey->{$field} = $req[$key];
        };
        $assocField = function(string $key, string $field) use(&$req, &$survey){
            if(array_key_exists($key, $req)) $survey->{$field}()->associate($req[$key]);
        };

        $setField('name', 'name');
        $setField('alias', 'alias');
        if(isset($req['area'])){
            $survey->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
        }
        $setField('direction', 'direction');
        $setField('startDate', 'start_date');

        if(isset($req['archPath'])){
            $survey->arch_path = $req['archPath'];
            $survey->arch_size = filesize(DataSystem::getDataPath($req['archPath']));
        }

        $assocField('parentZone', 'zone');
        $assocField('field', 'field');
        if(isset($req['vessel'])){
            $survey->trip->vessel()->associate($req['vessel']);
            $survey->trip->save();
        }

        $assocField('source', 'source');
        $setField('srcGroupCount', 'src_group_count');
        $setField('srcPerGroup', 'src_per_group');

        $assocField('streamer', 'streamer');
        $setField('isTowed', 'is_towed');

        $assocField('resolution', 'resolution');

        $setField('customer', 'customer');
        $setField('operator', 'operator');
        
        if(isset($req['archFiles'])){
            $survey->archFiles()->delete();
            foreach($req['archFiles'] as $file){
                $archFile = new Seismic3dSurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }

        $survey->save();
        return $survey;
    }

    public function destroy(Seismic2dSurvey $survey){
        
    }
}