<?php

namespace App\Services;

use App\Models\Zone;
use App\Models\ZoneShapeType;

use Illuminate\Support\Facades\Validator;

use App\Helpers\PolygonParser;
use App\Helpers\TransactionManager;
use App\Helpers\{ExcelToCsvSplitter, CsvReader};

use Symfony\Component\HttpKernel\Exception\HttpException;

class ZoneBatchImportService {
    public function __construct(){
        $this->zoneService = new ZoneService(ZoneShapeType::WKT);
        $this->transManager = new TransactionManager();
    }

    public function importFromExcel(string $excelFilename){
        $this->transManager->begin();

        $csvFolder = $this->genTempFolder();
        $etcs = new ExcelToCsvSplitter($excelFilename, $csvFolder);
        foreach($etcs->getCsvFileNames() as $csvFilename){
            $this->importCsvImpl($csvFilename);
        }

        $this->transManager->end();
    }

    public function importCsv(string $csvFilename){
        $this->transManager->begin();
        $this->importCsvImpl($csvFilename);
        $this->transManager->end();
    }

    private function importCsvImpl(string $csvFilename){
        $data = CsvReader::read($csvFilename, ",");
        $this->importZones($data);
    }

    public function importZones($zones){
        $this->transManager->begin();
        $this->importZonesImpl($zones);
        $this->transManager->end();
    }

    private function importZonesImpl($zones){
        foreach($zones as $zoned){
            $zone = Zone::findByName($zoned['name'])->first();
            $prepZone = static::prepareZoneData($zoned);
            if(is_null($zone)){
                $this->zoneService->add($prepZone);
            } else {
                $this->zoneService->patch($prepZone, $zone);
            }
        }
    }

    private static function prepareZoneData($zoneIn){
        $zoneOut = $zoneIn;
        if(array_key_exists('parent_name', $zoneIn)){
            if($zoneIn['parent_name'] == ""){
                $zoneOut['parentZone'] = null;
            } else {
                $pZone = Zone::findByName($zoneIn['parent_name'])->first();
                if(is_null($pZone)){
                    throw new HttpException(404, sprintf('Parent zone "%s" doesn\'t exist', $zoneIn['parent_name']));
                }

                $zoneOut['parentZone'] = $pZone->id;
            }
        }
        return $zoneOut;
    }

    private function genTempFolder(){
        $tempfile=tempnam(sys_get_temp_dir(),'');
        if (file_exists($tempfile)) { unlink($tempfile); }
        mkdir($tempfile);
        return $tempfile;
    }

}