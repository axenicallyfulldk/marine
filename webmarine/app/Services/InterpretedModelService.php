<?php

namespace App\Services;

use App\Models\InterpretedModel;
use App\Models\HazardType;
use App\Models\Modelable;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Symfony\Component\HttpKernel\Exception\HttpException;


class InterpretedModelService {
    public function add($req){
        // Check rights
        // TODO

        // Validate
        Validator::make($req, InterpretedModel::getStoreRules())->validate();
        
        $model = new InterpretedModel();
        $model->name = $req['name'];
        $model->shape = DB::raw("'".$req['area']."'");
        $model->parent()->associate($req['parentZone']);
        $model->creation_date = $req['creationDate'];
        $model->file_path = $req['filePath'];
        $model->save();

        if(array_key_exists("hazards", $req)){
            $this->addHazards($model, $req['hazards']);
        }

        return $model;
    }

    public function patch($req, InterpretedModel $model){
        // Check rights
        if(!Auth::user()->editModelsByDefault()){
            throw new HttpException(403, "You have no right to update fields.");
        }

        // Validate
        Validator::make($req, InterpretedModel::getPatchRules())->validate();

        $setField = function(string $key, string $field) use(&$req, &$model){
            if(array_key_exists($key, $req)) $model->{$field} = $req[$key];
        };
        $assocField = function(string $key, string $field) use(&$req, &$model){
            if(array_key_exists($key, $req)) $model->{$field}()->associate($req[$key]);
        };

        $setField("name", "name");
        if(array_key_exists("shape", $req)) $model->shape = DB::raw("'".$req['shape']."'");
        $assocField("parentZone", "parent");
        $setField("creationDate", "creation_date");
        $setField("filePath", "file_path");
        $model->save();

        if(array_key_exists("hazards", $req)){
            $this->deleteHazards($model);
            $this->addHazards($model, $req['hazards']);
        }
        
        return $model;
    }

    private function deleteHazards(InterpretedModel $model){
        foreach($model->hazards as $hazard){
            $origHazard = $hazard->modelable();
            $hazard->delete();
            $origHazard->delete();
        }
    }

    private function addHazards(InterpretedModel $model, $hazards){
        foreach($hazards as $hazardD){
            $hazardType = HazardType::findByKey($hazardD['type'])->first();

            $typeClass = $hazardType->getModel();
            $hazard = new $typeClass();
            $hazard->name = $hazardD['name'];
            $hazard->type()->associate($hazardType);
            $hazard->shape = DB::raw("'".$hazardD['shape']."'");
            $hazard->save();

            $obj = new Modelable();
            $obj->model()->associate($model);
            $obj->modelable()->associate($hazard);
            $obj->save();
        }
    }

    public function destroy(InterpretedModel $model){
        // Check rights
        // if(!\Auth::user()->editZonesByDefault()){
        //     throw new HttpException(403, "You have no right to delete fields.");
        // }
        
        $model->delete();
    }
}