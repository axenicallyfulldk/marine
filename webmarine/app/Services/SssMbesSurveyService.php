<?php

namespace App\Services;

use App\Models\SssMbesSurvey;
use App\Models\SurveyType;
use App\Models\Trip;

use App\Helpers\PolygonParser;
use App\Helpers\DataSystem;

use Illuminate\Support\Facades\Validator;

class SssMbesSurveyService{
    public function add($req){
        // Check rights
        if(!\Auth::user()->editSssMbesSurveysByDefault()){
            throw new HttpException(403, "You have no right to create surveys.");
        }

        // Validate
        Validator::make($req, SssMbesSurvey::getStoreRules($req))->validate();
        

        $trip = new Trip();
        $trip->vessel()->associate($req['vessel']);
        $trip->save();

        $survey = new SssMbesSurvey();
        $survey->name = $req['name'];
        $survey->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
        $survey->parent()->associate($req['parent']);
        $survey->direction = $req['direction'];
        // $survey->type()->associate(SurveyType::query()->where('key', $req['type'])->get()[0]);
        $survey->type()->associate(SurveyType::findByKey($req['type'])->first());
        $survey->tool()->associate($req['tool']);
        $survey->trip()->associate($trip);
        $survey->start_date = $req['startDate'];
        $survey->is_deleted = false;
        $survey->file_path = $req['filePath'];
        $survey->file_size = filesize(DataSystem::getDataPath($req["filePath"]));
        if(!empty($req['archPath'])){
            $survey->arch_path = $req['archPath'];
            $survey->arch_size = filesize(DataSystem::getDataPath($req["archPath"]));
        }
        $survey->save();

        if(isset($req['archFiles'])){
            foreach($req['archFiles'] as $file){
                $archFile = new SurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }
        
        return $survey->fresh();
    }

    public function patch($req, SssMbesSurvey $survey){
        if($survey->is_deleted)
            throw new NotFoundHttpException("Survey doesn't exist.");

        // Check rights
        if(!$survey->isEditable()){
            throw new HttpException(403, "You have no right to update this survey.");
        }

        // Validate
        Validator::make($req, SssMbesSurvey::getPatchRules($req))->validate();
        

        $setField = function(string $key, string $field)use($req, $survey){
            if(array_key_exists($key, $req)) $survey->{$field} = $req[$key];
        };
        $assocField = function(string $key, string $field) use($req, $survey){
            if(array_key_exists($key, $req)) $survey->{$field}()->associate($req[$key]);
        };

        $setField("name", "name");
        if(isset($req["area"])){
            $survey->area = \DB::raw("'".PolygonParser::parse($req["area"])->toEKWT()."'");
        }
        $assocField("parent", "parent");
        $setField("direction", "direction");
        if(isset($req["type"])){
            //$survey->type()->associate(SurveyType::query()->where('key', $req["type"])->get()[0]);
            $survey->type()->associate(SurveyType::findByKey($req["type"])->first());
        }
        $assocField("tool", "tool");
        if(isset($req["vessel"])){
            $survey->trip->vessel()->associate($req["vessel"]);
        }
        $setField("startDate", "start_date");
        if(isset($req["filePath"])){
            $survey->file_path = $req["filePath"];
            $survey->file_size = filesize(DataSystem::getDataPath($req["filePath"]));
        }
        if(isset($req["archPath"])){
            $survey->arch_path = $req["archPath"];
            $survey->arch_size = filesize(DataSystem::getDataPath($req["archPath"]));
        }
        if(isset($req["archFiles"])){
            $survey->archFiles()->delete();
            foreach($req["archFiles"] as $file){
                $archFile = new SurveyArchFile();
                $archFile->survey()->associate($survey);
                $archFile->name = $file['name'];
                $archFile->description = $file['description'];
                $archFile->save();
            }
        }

        $survey->trip->save();
        $survey->save();
        return $survey->fresh();
    }

    public function destroy(SssMbesSurvey $survey){
        if(!$survey->isEditable())
            throw AccessDeniedHttpException("You have no permission to delete this survey.");
        $survey->delete();

        return \Response::make("", 204);
    }
}