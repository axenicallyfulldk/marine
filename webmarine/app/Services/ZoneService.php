<?php

namespace App\Services;

use App\Models\Zone;
use App\Models\ZoneShapeType;

use Illuminate\Support\Facades\Validator;

use App\Helpers\PolygonParser;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ZoneService {
    
    public function __construct($shapeType = ZoneShapeType::Simple){
        $this->shapeType = $shapeType;
    }

    public function add($data){
        // Check rights
        $user = \Auth::user();
        if(!$user->editZonesByDefault())
            throw new AccessDeniedHttpException('You have no right to create zones.');

        Validator::make($data, Zone::getStoreRules($this->shapeType))->validate();

        $ZoneClass = Zone::ZONE_CLASSES[$data['type']];
        $zoneObj = new $ZoneClass();
        $zoneObj->save();

        $zone = new Zone();
        $zone->name = $data['name'];
        $zone->code = $data['code'];
        if(isset($data['parentZone'])) $zone->parent()->associate($data['parentZone']);
        if($this->shapeType == ZoneShapeType::Simple){
            $zone->area = \DB::raw("'".PolygonParser::parse($data['area'])->toEKWT()."'");
        } else if($this->shapeType == ZoneShapeType::WKT){
            $zone->area = \DB::raw("'".$data['area']."'");
        }
        $zone->zonable()->associate($zoneObj);
        $zone->save();
        
        return $zone->fresh();
    }

    public function patch($req, Zone $zone){
        // Check rights
        if(!\Auth::user()->editZonesByDefault()){
            throw new HttpException(403, "You have no right to edit zones.");
        }

        // Validate
        Validator::make($req, Zone::getPatchRules($this->shapeType))->validate();

        $setField = function(string $key, string $field)use($req, $zone){
            if(array_key_exists($key, $req)) $zone->{$field} = $req[$key];
        };

        $setField("name", "name");
        $setField("code","code");
        if(\array_key_exists('area', $req)){
            if($this->shapeType == ZoneShapeType::Simple){
                $zone->area = \DB::raw("'".PolygonParser::parse($req['area'])->toEKWT()."'");
            } else if($this->shapeType == ZoneShapeType::WKT){
                $zone->area = \DB::raw("'".$req['area']."'");
            }
        }
        if(\array_key_exists('parentZone', $req)) $zone->parent()->associate($req['parentZone']);

        $zone->save();
        return $zone->fresh();
    }

    public function destroy(Zone $zone){
        $user = \Auth::user();
        if(!$user->editZonesByDefault()){
            throw new HttpException(403, "You have no right to delete zones.");
        }
        $zone->is_deleted = true;
        $zone->save();

        return true;
    }
}