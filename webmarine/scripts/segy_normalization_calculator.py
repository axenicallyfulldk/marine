import os, math, re, argparse, pathlib, segyio

parser = argparse.ArgumentParser(description='Calculates normalization coefficient for traces')
parser.add_argument('-s', type = pathlib.Path, required=True, help='path to segy file')
parser.add_argument('-t', type = str, choices = ("avg", "minmax"), default="minmax", required=True, help = 'normalization type')

args = parser.parse_args()

f = segyio.open(args.s, ignore_geometry=True)

minV = f.trace[0][0]
maxV = minV
avg = 0

for trace in f.trace:
    traceAvg = 0
    for sample in trace:
        traceAvg += sample
        minV = min(minV, sample)
        maxV = max(maxV, sample)
    traceAvg = traceAvg / len(trace)

if args.t == "avg":
    print(1.0/(abs(traceAvg/f.tracecount)))
elif args.t == "minmax":
    print(1.0/max(abs(minV), abs(maxV)))